<?php

namespace App\Console\Commands\Finance;

use Evibe\Handlers\Finance\UpdateScheduledBookingSettlementsCommandHandler;
use Illuminate\Console\Command;

class UpdateScheduledBookingSettlementsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:update-booking-settlements';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updating scheduled booking settlements';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(UpdateScheduledBookingSettlementsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->updateBookingSettlements($this);
	}
}
