<?php

namespace App\Console\Commands\Finance;

use Evibe\Handlers\Finance\CreatePartnerSettlementsCommandHandler;
use Illuminate\Console\Command;

class CreatePartnerSettlementsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:create-partner-settlements';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creating partner settlements on every thursday 12 PM';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(CreatePartnerSettlementsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->createPartnerSettlements();
	}
}
