<?php

namespace App\Console\Commands\Finance;

use Evibe\Handlers\Finance\CreateScheduledBookingSettlementsCommandHandler;
use Illuminate\Console\Command;

class CreateScheduledBookingSettlementsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:create-booking-settlements';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creating scheduled booking settlements';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(CreateScheduledBookingSettlementsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->createBookingSettlements($this);
	}
}
