<?php

namespace App\Console\Commands\PartnerDigest;

use Illuminate\Console\Command;
use Evibe\Handlers\PartnerDigestReportHandler;

class PartnerDigestReport extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:partner-digest';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Report to partner of their previous week bookings & orders along with images & ratings.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(PartnerDigestReportHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReport();

		return true;
	}
}