<?php

namespace App\Console\Commands;

use Evibe\Handlers\BookingSettlementReminder;
use Illuminate\Console\Command;

class SendBookingSettlementReminderToCustomer extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:booking-settlement-reminder-customer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sending booking reminder to customer before 1 hrs of party';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(BookingSettlementReminder $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->SendReminderToCustomer();
	}
}
