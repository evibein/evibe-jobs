<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Evibe\Facades\EvibeUtilFacade as AppUtil;

class DeleteGalleryTypeInfoCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:delete-gallery-info';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Give the info about type of command';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$types = AppUtil::getGalleryTables();

		foreach ($types as $id => $data)
		{

			$this->info(str_pad($data['name'], 30, '-') . '->  ' . $id);
		}
	}
}
