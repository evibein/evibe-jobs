<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\UpdateProductPricesCommandHandler;
use Illuminate\Console\Command;

class UpdateProductPricesCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:update-product-prices
							{--category=* : to which category this should be applied}
							{--startId= : start from this id}';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update prices of the products.';

	/**
	 * Create a new command instance.
	 *
	 * @param UpdateProductPricesCommandHandler $handler
	 */
	public function __construct(UpdateProductPricesCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		//$this->handler->updatePrices($this, $this->option());

		return true;
	}
}
