<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\GenerateCouponCodeHandler;
use Illuminate\Console\Command;

class GenerateCouponCodes extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:generate-coupon-codes
							{--cid= : Enter FC or VC}
							{--count= : Enter total coupons needed}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate Coupon Codes.';

	/**
	 * Create a new command instance.
	 *
	 * @param AutoFillNullPincodes $handler
	 */
	public function __construct(GenerateCouponCodeHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->GenerateCouponCodes($this, $this->option());

		return true;
	}
}