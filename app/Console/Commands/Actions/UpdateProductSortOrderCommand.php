<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\UpdateProductSortOrderCommandHandler;
use Illuminate\Console\Command;

class UpdateProductSortOrderCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:set-product-score
							{--cityId= : city id of the category to be sorted}
							{--eventId= : event id of the category to be sorted}
							{--productTypeId= : product type id of the category}
							{--startTimeString= : time from which data should be considered for sort ordering (in epoch timestamp)}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set scores of products.';

	/**
	 * Create a new command instance.
	 *
	 * @param UpdateProductSortOrderCommand $handler
	 */
	public function __construct(UpdateProductSortOrderCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->updateSortScores($this, $this->option());

		return true;
	}
}
