<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\UpdateProductProviderRatingsCommandHandler;
use Illuminate\Console\Command;

class UpdateProductProviderRatings extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:update-provider-ratings {--first : if the command needs to run for the first time}';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update provider ratings for the products.';

	/**
	 * Create a new command instance.
	 *
	 * @param UpdateProductProviderRatingsCommandHandler $handler
	 */
	public function __construct(UpdateProductProviderRatingsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->updateRatings($this, $this->option());

		return true;
	}
}
