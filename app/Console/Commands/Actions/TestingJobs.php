<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\TestingJobHandler;
use Illuminate\Console\Command;

class TestingJobs extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:test-jobs';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Testing jobs after composer update / project installation.';

	/**
	 * Create a new command instance.
	 *
	 * @param AutoFillNullPincodes $handler
	 */
	public function __construct(TestingJobHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->requestToMainUsingGuzzleHTTP($this);

		return true;
	}
}
