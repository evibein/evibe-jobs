<?php

namespace App\Console\Commands;
use Evibe\Handlers\UpdateReviewsCommandHandler;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class UpdateReviewsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:reviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update reviews of venues around the web.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $options = $this->option();
        $handler = new UpdateReviewsCommandHandler($options);
        $handler->fetchLatestReviews();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [// array('example', InputArgument::REQUIRED, 'An example argument.'),
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('ids', null, InputOption::VALUE_REQUIRED, 'Comma separated list of ids', null),
            array('type', null, InputOption::VALUE_REQUIRED, 'Comma separated list of website type ids', null)
        );
    }

}
