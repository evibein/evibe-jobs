<?php

namespace App\Console\Commands;

use Evibe\Handlers\SendCancellationMailCommandHandler;
use Illuminate\Console\Command;

class SendCancellationEmailCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $name = 'evibe:send-cancellation-mail';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send cancellation emails to customers (unpaid)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(SendCancellationMailCommandHandler $handler)
	{
		parent::__construct();
		$this->handler =$handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->sendCancellationMail();
	}

}
