<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\UpdateCategoryTagsCommandHandler;
use Illuminate\Console\Command;

class UpdateCategoryTagsCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:update-category-tags
							{--firstCatTags : if the command needs to create all type_category_tags from type_tags}
							{--allTagsInfo : if the command needs to update details for all type_category_tags}';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update type category tags.';

	/**
	 * Create a new command instance.
	 *
	 * @param UpdateCategoryTagsCommandHandler $handler
	 */
	public function __construct(UpdateCategoryTagsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->updateCategoryTags($this, $this->option());

		return true;
	}
}
