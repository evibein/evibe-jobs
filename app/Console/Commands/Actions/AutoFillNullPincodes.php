<?php

namespace App\Console\Commands\Actions;

use App\Evibe\Handlers\Actions\AutoFillPincodesHandler;
use Illuminate\Console\Command;

class AutoFillNullPincodes extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:update-pin-codes';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update all pincodes which are NULL.';

	/**
	 * Create a new command instance.
	 *
	 * @param AutoFillNullPincodes $handler
	 */
	public function __construct(AutoFillPincodesHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->AutoFillNullPincodes($this);

		return true;
	}
}
