<?php

namespace App\Console\Commands;

use Evibe\Handlers\DeleteGalleryCommandHandler;
use Illuminate\Console\Command;
use Evibe\Facades\EvibeUtilFacade as AppUtil;

class DeleteGalleryImageCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:delete-gallery {type}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for deleting gallery images based on options';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $type_table = [];
	private $handler;

	public function __construct(DeleteGalleryCommandHandler $handler)
	{
		parent::__construct();
		$this->type_table = AppUtil::getGalleryTables();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$type = $this->argument('type');
		$typeTables = $this->type_table;

		if (in_array($type, array_keys($typeTables)))
		{
			if ($this->confirm('Do you wish to continue, It will permanently delete your gallery?'))
			{
				$this->handler->clearGallery($typeTables, $type);
				$this->info('Images deleted successfully');
			}
		}
		else
		{
			$this->error('Please enter a valid type, if you are not sure then run "evibe:delete-gallery-info" command to see the list of gallery types');
		}
	}
}
