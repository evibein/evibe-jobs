<?php

namespace App\Console\Commands\Search;

use Evibe\Handlers\Search\ImportProductsToTableCommandHandler;
use Illuminate\Console\Command;

class ImportProductsToTableCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:import-products-table';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import all live products to single table';

	/**
	 * The handler
	 *
	 * @var ImportProductsToTableCommandHandler
	 */
	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @param  \Evibe\Handlers\Search\ImportProductsToTableCommandHandler $handler
	 *
	 * @return void
	 */
	public function __construct(ImportProductsToTableCommandHandler $handler)
	{
		parent::__construct();

		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->importProductsToSingleTable();

		return true;
	}
}
