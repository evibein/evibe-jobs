<?php

namespace App\Console\Commands\Search;

use Evibe\Handlers\Search\ImportProductsToESCommandHandler;
use Illuminate\Console\Command;

class ImportProductsToESCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:import-products-es';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import products from single table to Elasticsearch';

	/**
	 * The job handler.
	 *
	 * @var string
	 */
	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ImportProductsToESCommandHandler $handler)
	{
		parent::__construct();

		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->importProductsToES();

		return true;
	}
}
