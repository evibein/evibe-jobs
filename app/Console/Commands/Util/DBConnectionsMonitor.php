<?php

namespace App\Console\Commands\Util;

use App\Evibe\Handlers\Util\DBConnectionsMonitorHandler;
use Illuminate\Console\Command;

class DBConnectionsMonitor extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:db-connections-monitor';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Log all the connections data & reset the connections cross 100 limit';

	/**
	 * Create a new command instance.
	 */
	public function __construct(DBConnectionsMonitorHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->testDBConnections();

		return true;
	}
}