<?php

namespace App\Console\Commands\Util;

use App\Evibe\Handlers\Util\AutoWaterMarkImageCommandHandler;
use App\Evibe\Handlers\Util\SettlementInvoiceCreationCommandHandler;
use Illuminate\Console\Command;

class SettlementInvoiceCreationCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:create-settlement-invoice 
							{--partnerId= : enter partnerId}
							{--partnerTypeId= : enter partnerTypeId}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Add watermark on the products';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(SettlementInvoiceCreationCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->createPartnerSettlementInvoices($this, $this->option());
	}
}