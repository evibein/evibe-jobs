<?php

namespace App\Console\Commands\Util;

use App\Evibe\Handlers\Util\AutoWaterMarkImageCommandHandler;
use Illuminate\Console\Command;

class AutoWaterMarkImageCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:watermark-products 
							{categoryId : enter the category ID}
							{type : Either add or remove watermark}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Add watermark on the products';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(AutoWaterMarkImageCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->argument('type') == "remove")
		{
			$this->handler->removeWaterMarkOnTheProducts($this->argument('categoryId'));
		}
		elseif ($this->argument('type') == "add")
		{
			$this->handler->addWaterMarkOnTheProducts($this->argument('categoryId'));
		}

		return true;
	}
}