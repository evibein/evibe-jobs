<?php

namespace App\Console\Commands\Util;

use App\Evibe\Handlers\Util\UpdateTicketAnalyticsDataCommandHandler;
use Illuminate\Console\Command;

class UpdateTicketAnalyticsData extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:update-ticket-analytics-data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update initiated & return tickets Ids to show the line graph in analytics';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(UpdateTicketAnalyticsDataCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->updateTicketAnalyticsData();

		return true;
	}
}