<?php

namespace App\Console\Commands\Payments;

use App\Evibe\Handlers\Payment\PaymentReportToTeamCommandHandler;
use Illuminate\Console\Command;

class PaymentReportToTeamCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:payment-report-team-alert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Report to team of all the payment transactions happened today';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(PaymentReportToTeamCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReport();

		$this->handler->sendPIABReport();
	}
}