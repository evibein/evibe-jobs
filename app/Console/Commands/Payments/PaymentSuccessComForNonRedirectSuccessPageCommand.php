<?php

namespace App\Console\Commands\Payments;

use App\Evibe\Handlers\Payment\PaymentSuccessComForNonRedirectSuccessPageCommandHandler;
use Illuminate\Console\Command;

class PaymentSuccessComForNonRedirectSuccessPageCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:payment-success-com-non-redirect';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Automatically accepting payments if not redirected back to the success page';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(PaymentSuccessComForNonRedirectSuccessPageCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}