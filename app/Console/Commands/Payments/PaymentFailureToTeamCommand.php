<?php

namespace App\Console\Commands\Payments;

use App\Evibe\Handlers\Payment\PaymentFailureToTeamCommandHandler;
use Illuminate\Console\Command;

class PaymentFailureToTeamCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:payment-failure-team-alert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Alerts to team if customer faces issue with payments';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(PaymentFailureToTeamCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}