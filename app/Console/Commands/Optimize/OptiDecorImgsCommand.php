<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptiDecorImgsCommandHandler;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class OptiDecorImgsCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'evibe:opti-decor-imgs';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize decor style images (thumbs, results & profile)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$idMin = null;
		$idMax = null;
		$ids = $this->option('ids');

		if ($ids)
		{
			$idsObj = explode(",", $ids);
			$idMin = $idsObj[0];
			$idMax = $idsObj[1];
		}
		$profileWidth = $this->option('profileWidth');
		$resultWidth = $this->option('resultWidth');
		$thumbsWidth = $this->option('thumbsWidth');

		$handler = new OptiDecorImgsCommandHandler();
		$handler->optiImgs($this, $profileWidth, $resultWidth, $thumbsWidth, $idMin, $idMax);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['ids', null, InputOption::VALUE_REQUIRED, 'Give IDs of decors for which images needs to be optimised', null],
			['thumbsWidth', null, InputOption::VALUE_OPTIONAL, 'Width for thumbnails', 80],
			['profileWidth', null, InputOption::VALUE_OPTIONAL, 'Width for profile images', 640],
			['resultWidth', null, InputOption::VALUE_OPTIONAL, 'Width for images in results', 394],
		];
	}
}
