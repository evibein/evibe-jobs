<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptPkgImgCommandHandler;
use Illuminate\Console\Command;

class OptPkgImgCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:opt-pkg-img
							{--sid= : Start ID of the package}
							{--eid= : End ID of the package}
							{--id=* : Array of package IDs}
							{--pid=* : Array of provider IDs}
							{--tw|thumbsWidth=80 : Width for thumbnails}
							{--pw|profileWidth=640 : Width for profile images}
							{--rw|resultWidth=394 : Width for images in results}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize package images (thumbs, results & profile)';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(OptPkgImgCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->optimize($this, $this->option());
	}
}
