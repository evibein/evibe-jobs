<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptCakeImgsCommandHandler;
use Illuminate\Console\Command;

class OptiCakeImgsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:opt-cake-img
							{--sid= : Start ID of the cake}
							{--eid= : End ID of the cake}
							{--id=* : Array of cake IDs}
							{--tw|thumbsWidth=80 : Width for thumbnails}
							{--pw|profileWidth=640 : Width for profile images}
							{--rw|resultWidth=320 : Width for images in results}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize cake images (thumbs, results & profile)';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(OptCakeImgsCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->optimize($this, $this->option());
	}
}
