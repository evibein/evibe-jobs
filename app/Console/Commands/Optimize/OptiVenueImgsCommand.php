<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptiVenueImgsCommandHandler;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class OptiVenueImgsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:opti-venue-imgs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize venue (halls) images (thumbs, results & profile)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $idMin = NULL;
        $idMax = NULL;
        $ids = $this->option('ids');

        if ($ids)
        {
            $idsObj = explode(",", $ids);
            $idMin = $idsObj[0];
            $idMax = $idsObj[1];
        }

        $profileWidth = $this->option('profileWidth');
        $resultWidth = $this->option('resultWidth');
        $thumbsWidth = $this->option('thumbsWidth');

        $handler = new OptiVenueImgsCommandHandler();
        $handler->optiImgs($this, $profileWidth, $resultWidth, $thumbsWidth, $idMin, $idMax);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            // array('ids', InputArgument::OPTIONAL, 'Give IDs of venues for which images needs to be optimised'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('ids', null, InputOption::VALUE_REQUIRED, 'Give IDs of venues for which images needs to be optimised', null),
            array('thumbsWidth', null, InputOption::VALUE_OPTIONAL, 'Width for thumbnails', 80),
            array('profileWidth', null, InputOption::VALUE_OPTIONAL, 'Width for profile images', 640),
            array('resultWidth', null, InputOption::VALUE_OPTIONAL, 'Width for images in results', 394),
        );
    }

}
