<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptiCollectionImgsCommandHandler;
use Illuminate\Console\Command;

class OptiCollectionImgsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:opti-collection-imgs
                            {--phw|profileHomeWidth=470 : Width for profile images in home page}
                            {--phh|profileHomeHeight=190 : Height for profile images in home page}
                            {--plw|profileListWidth=80 : Width for profile images in list page}
                            {--plh|profileListHeight=80 : Height for profile images in list page}
                            {--clw|coverListWidth=460 : Width for cover images in list page}
                            {--clh|coverListHeight=190 : Height for cover images in list page}
                            {--ppw|profileProfileWidth=210 : Width for profile images in profile page}
                            {--pph|profileProfileHeight=210 : Height for profile images in profile page}
                            {--cpw|coverProfileWidth=1024 : Width for cover images in profile page}
                            {--cph|coverProfileHeight=310 : Height for cover images in profile page}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize collection images';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(OptiCollectionImgsCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->optimize($this, $this->option());
	}
}
