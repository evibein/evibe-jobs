<?php

namespace App\Console\Commands;

use Evibe\Handlers\OptTrendImgCommandHandler;
use Illuminate\Console\Command;

class OptiTrendImgsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:opti-trend-imgs
							{--sid= : Start ID of the trend}
							{--eid= : End ID of the trend}
							{--id=* : Array of trend IDs}
							{--tw|thumbsWidth=80 : Width for thumbnails}
							{--pw|profileWidth=640 : Width for profile images}
							{--rw|resultWidth=394 : Width for images in results}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize trend images';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(OptTrendImgCommandHandler $handler)
	{
		$this->handler = $handler;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->optimize($this, $this->option());
	}
}
