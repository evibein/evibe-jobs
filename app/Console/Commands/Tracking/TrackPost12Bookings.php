<?php

namespace App\Console\Commands;

use Evibe\Handlers\TrackPost12BookingsHandler;
use Illuminate\Console\Command;

class TrackPost12Bookings extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:track-post-12';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Track bookings happening post 12 noon today';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(TrackPost12BookingsHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'options' => $this->option(),
			'args'    => $this->argument()
		];

		$this->handler->notifyVendors($data);
	}
}
