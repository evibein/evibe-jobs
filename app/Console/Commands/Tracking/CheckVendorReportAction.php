<?php

namespace App\Console\Commands;

use Evibe\Handlers\CheckVendorReportActionHandler;
use Illuminate\Console\Command;

class CheckVendorReportAction extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:report-check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check if vendors responded to reporting notifications, else act';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CheckVendorReportActionHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'options' => $this->option(),
			'args'    => $this->argument()
		];

		$this->handler->check($data);
	}
}
