<?php

namespace App\Console\Commands;

use Evibe\Handlers\AskVendorReportingHandler;
use Evibe\Handlers\SendVendorReportingOTPHandler;
use Illuminate\Console\Command;

class SendVendorReportingOTP extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:send-report-otp';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send notifications to vendors to check reporting';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(SendVendorReportingOTPHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'options' => $this->option(),
			'args'    => $this->argument()
		];

		$this->handler->sendOTP($data);
	}
}
