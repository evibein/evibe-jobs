<?php

namespace App\Console\Commands;

use Evibe\Handlers\TrackBookingCheckHandler;
use Illuminate\Console\Command;

class TrackBookingCheck extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:track-check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check if vendors responded to track else act';

	protected $handler;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(TrackBookingCheckHandler $handler)
	{
		$this->handler = $handler;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'options' => $this->option(),
			'args'    => $this->argument()
		];

		$this->handler->check($data);
	}
}
