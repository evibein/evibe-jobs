<?php

namespace App\Console\Commands;

use Evibe\Handlers\AlertFeedbackResponseStatusTeamHandler;
use Illuminate\Console\Command;

class AlertFeedbackRespondStatusTeamCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:feedback-status';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'It will give the status which customer has responded to the feedback which is send today';
	protected $feedbackStatus;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(AlertFeedbackResponseStatusTeamHandler $feedbackStatus)
	{
		parent::__construct();
		$this->feedbackStatus = $feedbackStatus;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->feedbackStatus->nonRespondedFeedback();
	}
}
