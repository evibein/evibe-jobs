<?php

namespace App\Console\Commands;

use Evibe\Handlers\DropOffFeedbackCommandHandler;
use Illuminate\Console\Command;

class DropOffFeedbackCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'evibe:ask-drop-off-feedback';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send sms to drop off customers (last week party date) for their feedback.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$handler = new DropOffFeedbackCommandHandler();
		$handler->askFeedback();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}