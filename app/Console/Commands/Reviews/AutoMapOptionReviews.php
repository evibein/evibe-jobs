<?php

namespace App\Console\Commands\Reviews;

use App\Evibe\Handlers\Reviews\AutoMapOptionReviewsCommandHandler;
use Illuminate\Console\Command;

class AutoMapOptionReviews extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $handler;

	protected $signature = 'evibe:map-option-reviews';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Map possible partner reviews to options';

	/**
	 * Create a new command instance.
	 *
	 * @param AutoMapOptionReviews $handler
	 */
	public function __construct(AutoMapOptionReviewsCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle()
	{
		$this->handler->mapPartnerReviewsToOptions($this);

		return true;
	}
}