<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\RecoFollowupRemindersToCustomerHandler;
use Illuminate\Console\Command;

class SendRecoFollowupRemindersToCustomer extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:reco-followup-reminders-customer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send Recommendation email Followup Reminders To Customer at time given by User.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(RecoFollowupRemindersToCustomerHandler $handler)
	{
		parent::__construct();

		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}
