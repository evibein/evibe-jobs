<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\NonRespondedEnquiryReminderToTeamHandler;
use Illuminate\Console\Command;

class NonRespondedEnquiryReminderToTeamCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:non-responded-reminder-team';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'send reminder to team for the non responded enquiry';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(NonRespondedEnquiryReminderToTeamHandler $handler)
	{
		$handler->sendReminder();
	}
}
