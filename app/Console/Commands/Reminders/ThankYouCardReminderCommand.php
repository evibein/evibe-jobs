<?php

namespace App\Console\Commands\Reminders;


use Evibe\Handlers\Reminders\ThankYouCardReminderCommandHandler;
use Illuminate\Console\Command;

class ThankYouCardReminderCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:thank-you-card';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send thank you card reminder to customer';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(ThankYouCardReminderCommandHandler $handler)
	{
		$handler->sendReminder($this);
	}
}
