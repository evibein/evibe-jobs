<?php

namespace App\Console\Commands;

use Evibe\Handlers\EventRemindersTeamHandler;
use Illuminate\Console\Command;

class SendEventRemindersToTeamPost12 extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:team-reminders-post-12';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sending event reminder to team for today after 12 events.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(EventRemindersTeamHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$name = $this->getName();
		$this->handler->sendRemindersPost12($name);
	}
}
