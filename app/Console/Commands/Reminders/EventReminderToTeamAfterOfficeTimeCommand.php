<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\EventRemindersTeamHandler;
use Illuminate\Console\Command;

class EventReminderToTeamAfterOfficeTimeCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:event-reminders-after-office';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sending event reminder to team for the parties which timing fall in between evibe office closed hours';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(EventRemindersTeamHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendAfterOfficeHoursPartyReminders();
	}
}
