<?php

namespace App\Console\Commands\Reminders;

use App\Evibe\Handlers\Reminders\RSVPAlertToCustomerCommandHandler;
use Illuminate\Console\Command;

class RSVPAlertToCustomerCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:rsvp-alert-customer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Regular RSVP status alert to customer';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(RSVPAlertToCustomerCommandHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}