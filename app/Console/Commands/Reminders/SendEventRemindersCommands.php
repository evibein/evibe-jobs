<?php

namespace App\Console\Commands;

use Evibe\Handlers\SendEventRemindersCommandHandler;
use Illuminate\Console\Command;

class SendEventRemindersCommands extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'evibe:event-reminders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send event reminders (1 day prior) via SMS + Email to partners and customer.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(SendEventRemindersCommandHandler $handler)
	{
		$handler->sendReminders();

		return true;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		];
	}
}
