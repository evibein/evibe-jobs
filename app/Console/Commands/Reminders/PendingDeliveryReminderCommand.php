<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\PendingDeliveryRemindersHandler;
use Illuminate\Console\Command;

class PendingDeliveryReminderCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:pending-delivery';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send Pending delivery notification to partner';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(PendingDeliveryRemindersHandler $handler)
	{
		$handler->sendReminders();
	}
}
