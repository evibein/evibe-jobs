<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\PartnerAppEnquiryPendingReminderHandler;
use Illuminate\Console\Command;

class PartnerAppEnquiryPendingReminder extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:pending-enquiry';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Partner App notification email and SMS for pending enquiry before 10 minutes';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(PartnerAppEnquiryPendingReminderHandler $handler)
	{
		$handler->sendReminder();

		return true;
	}
}
