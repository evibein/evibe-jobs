<?php

namespace App\Console\Commands\Reminders;

use App\Evibe\Handlers\Reminders\CreateRetentionRemindersCommandHandler;
use Illuminate\Console\Command;

class CreateRetentionRemindersCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:create-retention-reminders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Trigger API to create retention reminders';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(CreateRetentionRemindersCommandHandler $handler)
	{
		$handler->createReminders($this);
	}
}
