<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\PartnerFollowupsReminderHandler;
use Illuminate\Console\Command;

class PartnerFollowupsReminder extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:partner-delivery-followup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Followup partners(video/photo) 3 days after the party.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(PartnerFollowupsReminderHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminder();
	}
}
