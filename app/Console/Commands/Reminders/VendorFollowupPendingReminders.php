<?php

namespace App\Console\Commands;

use Evibe\Handlers\VendorFollowupPendingRemindersHandler;
use Illuminate\Console\Command;

class VendorFollowupPendingReminders extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:vendor-followup-pending-reminders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send all pending followup reminders of vendor to bd head';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(VendorFollowupPendingRemindersHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendPendingReminders();
	}
}
