<?php

namespace App\Console\Commands\Reminders;

use App\Evibe\Handlers\Reminders\ServiceAutoBookingRemindersToTeamHandler;
use Illuminate\Console\Command;

class ServiceAutoBookingRemindersToTeam extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:service-auto-booking-team-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reminder sent to team for which service auto booking no action taken within 1 day';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(ServiceAutoBookingRemindersToTeamHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminder();
	}
}