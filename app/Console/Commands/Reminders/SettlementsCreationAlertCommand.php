<?php

namespace App\Console\Commands\Reminders;


use Evibe\Handlers\Reminders\SettlementsCreationAlertCommandHandler;
use Illuminate\Console\Command;

class SettlementsCreationAlertCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:settlements-creation-alert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send settlements creation reminder alert to team';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(SettlementsCreationAlertCommandHandler $handler)
	{
		$handler->sendReminder();
	}
}
