<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\AlertBookingConceptTypeCommandHandler;
use Illuminate\Console\Command;

class AlertBookingConceptTypeCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:concept-alert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Notification to operations team regarding booking concepts with complete details';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(AlertBookingConceptTypeCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendNotification();

		return true;
	}
}