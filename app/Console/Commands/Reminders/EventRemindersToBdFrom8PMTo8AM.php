<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\EventRemindersToBdHandler;
use Illuminate\Console\Command;

class EventRemindersToBdFrom8PMTo8AM extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:event-reminders-bd-after-8';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Event reminders to Bd between 8 PM to 8 AM';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(EventRemindersToBdHandler $handler)
	{
		parent::__construct();

		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders(false, true);
	}
}
