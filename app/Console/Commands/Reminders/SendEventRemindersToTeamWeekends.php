<?php

namespace App\Console\Commands;

use Evibe\Handlers\EventRemindersTeamHandler;
use Illuminate\Console\Command;

class SendEventRemindersToTeamWeekends extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:team-reminders-weekends';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send event reminders to team for all weekend parties including friday.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(EventRemindersTeamHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$name = $this->getName();
		$this->handler->sendRemindersAtWeekends($name);
	}
}
