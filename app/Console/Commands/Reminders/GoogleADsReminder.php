<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\GoogleADsReminderCommandHandler;
use Illuminate\Console\Command;

class GoogleADsReminder extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:google-ad-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Notification to customer one day before the entered special date';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(GoogleADsReminderCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendNotification();

		return true;
	}
}