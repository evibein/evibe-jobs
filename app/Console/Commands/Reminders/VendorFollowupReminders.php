<?php

namespace App\Console\Commands;

use Evibe\Handlers\VendorFollowupReminderHandler;
use Illuminate\Console\Command;

class VendorFollowupReminders extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:vendor-followup-reminders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'send the reminders of vendor followups';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(VendorFollowupReminderHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}
