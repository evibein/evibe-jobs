<?php

namespace App\Console\Commands\Reminders;

use Carbon\Carbon;
use Evibe\Handlers\AutoBookingRemindersToTeamHandler;
use Illuminate\Console\Command;

class AutoBookingRemindersToTeam extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:auto-booking-team-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reminder sent to team for which auto booking no action taken within 1 day';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(AutoBookingRemindersToTeamHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminder();
	}
}
