<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\Reminders\TicketRemindersCommandHandler;
use Illuminate\Console\Command;

class TicketReminders extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:ticket-reminders
							{--reminderIds=* : reminders to which this needs to be executed}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sending reminders for tickets from reminders stack';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(TicketRemindersCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendTicketReminders($this->option());
	}
}
