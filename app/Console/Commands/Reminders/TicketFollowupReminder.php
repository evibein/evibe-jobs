<?php

namespace App\Console\Commands;

use Evibe\Handlers\TicketFollowupReminderHandler;
use Illuminate\Console\Command;

class TicketFollowupReminder extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:ticket-followup-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send ticket followup reminder to CRM 10 minutes before & 30 minutes after the followup, only if its not completed';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $followup;

	public function __construct(TicketFollowupReminderHandler $followup)
	{
		parent::__construct();
		$this->followup = $followup;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->followup->sendReminders();
	}
}
