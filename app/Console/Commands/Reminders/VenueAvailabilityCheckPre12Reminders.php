<?php

namespace App\Console\Commands;

use Evibe\Handlers\VenueAvailabilityCheckRemindersHandler;
use Illuminate\Console\Command;

class VenueAvailabilityCheckPre12Reminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evibe:venue-avl-pre12';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Venue availability check reminders to BD, (pre 12)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	protected $handler;
    public function __construct(VenueAvailabilityCheckRemindersHandler $handler)
    {
	    $this->handler = $handler;
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->handler->pre12AvailabilityCheck();
    }
}
