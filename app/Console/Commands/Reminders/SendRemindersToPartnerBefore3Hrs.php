<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\SendRemindersToPartnerBefore3HrsHandler;
use Illuminate\Console\Command;

class SendRemindersToPartnerBefore3Hrs extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:event-reminder-partner';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Event reminders to partner via SMS 3 hrs before the party';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(SendRemindersToPartnerBefore3HrsHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}
