<?php

namespace App\Console\Commands;

use Evibe\Handlers\TicketFollowupPendingReminderHandler;
use Illuminate\Console\Command;

class TicketFollowupPendingReminder extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:ticket-followup-pending-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sending pending followup reminders for ticket';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(TicketFollowupPendingReminderHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendPendingReminders();
	}
}
