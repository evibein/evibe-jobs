<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\PendingActivationReminderHandler;
use Illuminate\Console\Command;

class PendingActivationReminderCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:pending-activation';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for sending email for all pending activation which approval has already asked.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(PendingActivationReminderHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendNotification();
	}
}
