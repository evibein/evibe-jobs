<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\EventRemindersToBdHandler;
use Illuminate\Console\Command;

class EventRemindersToBd extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:event-reminders-bd';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Event reminders to bd 2 hrs before party';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(EventRemindersToBdHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders(true, false);
	}
}
