<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EmailErrorLogCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'evibe:email-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch website errors and send an email report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // get all errors
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            // array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            // array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
