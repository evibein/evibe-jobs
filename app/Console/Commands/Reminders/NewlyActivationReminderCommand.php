<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\NewlyActivationReminderHandler;
use Illuminate\Console\Command;

class NewlyActivationReminderCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:newly-activation';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for sending email with all the yesterday\'s activated items';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $handler;

	public function __construct(NewlyActivationReminderHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendNotification();
	}
}
