<?php

namespace App\Console\Commands\Reminders;


use Evibe\Handlers\Reminders\PendingRefundsReminderHandler;
use Illuminate\Console\Command;

class PendingCustomerRefundsCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:pending-refunds';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send pending refunds reminder to team';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(PendingRefundsReminderHandler $handler)
	{
		$handler->sendReminder();
	}
}
