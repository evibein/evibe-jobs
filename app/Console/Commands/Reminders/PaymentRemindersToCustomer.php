<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\PaymentReminderHandler;
use Illuminate\Console\Command;

class PaymentRemindersToCustomer extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:payment-reminder-customer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Payment reminder will be sent to customer before two hours of due payment time';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(PaymentReminderHandler $handler)
	{
		$this->handler = $handler;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendReminders();
	}
}
