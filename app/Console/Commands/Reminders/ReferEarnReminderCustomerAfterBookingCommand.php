<?php

namespace App\Console\Commands\Reminders;

use Evibe\Handlers\ReferEarnReminderCustomerAfterBookingCommandHandler;
use Illuminate\Console\Command;

class ReferEarnReminderCustomerAfterBookingCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'evibe:send-refer-earn-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Notification to customer to  participate in refer & earn after 30min of payment success';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	protected $handler;

	public function __construct(ReferEarnReminderCustomerAfterBookingCommandHandler $handler)
	{
		parent::__construct();
		$this->handler = $handler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->handler->sendNotification();

		return true;
	}
}