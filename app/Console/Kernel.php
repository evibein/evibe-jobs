<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [

		// System util
		Commands\EmailErrorLogCommand::class,
		Commands\DeleteGalleryImageCommand::class,
		Commands\DeleteGalleryTypeInfoCommand::class,

		// Update Product Sort Order score
		Commands\Actions\UpdateProductSortOrderCommand::class,

		// Update Product Price
		//Commands\Actions\UpdateProductPricesCommand::class,

		// Update Partner Ratings for Products
		Commands\Actions\UpdateProductProviderRatings::class,
		Commands\Actions\UpdateCategoryTagsCommand::class,

		//Auto filling of null pin codes
		Commands\Actions\AutoFillNullPincodes::class,

		//Testing Jobs
		Commands\Actions\TestingJobs::class,

		//Generate coupon codes
		Commands\Actions\GenerateCouponCodes::class,

		// Image optimizations
		Commands\OptiDecorImgsCommand::class,
		Commands\OptiVenueImgsCommand::class,
		Commands\OptPkgImgCommand::class,
		Commands\OptiServiceImgsCommand::class,
		Commands\OptiTrendImgsCommand::class,
		Commands\OptiCakeImgsCommand::class,
		Commands\OptiCollectionImgsCommand::class,

		// Partner tracking & reporting (not in use)
		// Commands\TrackBookingCheck::class,
		// Commands\SendVendorReportingOTP::class,
		// Commands\CheckVendorReportAction::class,

		// CRM team reminders / followups
		Commands\TicketFollowupReminder::class,
		Commands\TicketFollowupPendingReminder::class,
		Commands\AlertFeedbackRespondStatusTeamCommand::class,

		// BD team event reminders for tracking
		Commands\TrackPre12Bookings::class,
		Commands\TrackPost12Bookings::class,
		Commands\SendEventRemindersToTeamPre12::class,
		Commands\SendEventRemindersToTeamPost12::class,
		Commands\SendEventRemindersToTeamWeekends::class,
		Commands\Reminders\EventRemindersToBdFrom8PMTo8AM::class,
		Commands\Reminders\AutoBookingRemindersToTeam::class,
		Commands\Reminders\ServiceAutoBookingRemindersToTeam::class,
		Commands\Reminders\EventRemindersToBd::class,
		Commands\Reminders\EventReminderToTeamAfterOfficeTimeCommand::class,

		// BD team partner sign up followups
		Commands\VendorFollowupReminders::class,
		Commands\VendorFollowupPendingReminders::class,

		// Avail checks (BD)
		Commands\VenueAvailabilityCheckPre12Reminders::class,
		Commands\VenueAvailabilityCheckPost12Reminders::class,

		// Generic reminders
		Commands\Reminders\TicketReminders::class,
		Commands\Reminders\CreateRetentionRemindersCommand::class,

		// Reminders / followup to Customers
		Commands\AskFeedbackCommand::class,
		Commands\Reminders\SendRecoFollowupRemindersToCustomer::class,
		Commands\Reminders\PaymentRemindersToCustomer::class,
		Commands\SendCancellationEmailCommand::class,
		Commands\SendEventRemindersCommands::class,
		Commands\SendBookingSettlementReminderToCustomer::class,
		Commands\DropOffFeedbackCommand::class,

		// Reminders / followups to Partners
		Commands\Reminders\PartnerFollowupsReminder::class,
		Commands\Reminders\SendRemindersToPartnerBefore3Hrs::class,

		// Reminder for Evibe.in Partner App
		Commands\Reminders\PartnerAppEnquiryPendingReminder::class,
		Commands\Reminders\PendingDeliveryReminderCommand::class,
		Commands\Reminders\NonRespondedEnquiryReminderToTeamCommand::class,

		// Reminder about activation to team
		Commands\Reminders\PendingActivationReminderCommand::class,
		Commands\Reminders\NewlyActivationReminderCommand::class,

		// Alert Booking Concept types to team
		Commands\Reminders\AlertBookingConceptTypeCommand::class,

		// Alert Thank You card to customer
		Commands\Reminders\ThankYouCardReminderCommand::class,

		// Search
		Commands\Search\ImportProductsToTableCommand::class,
		Commands\Search\ImportProductsToESCommand::class,

		// Partner Digest
		Commands\PartnerDigest\PartnerDigestReport::class,

		// Finance
		Commands\Finance\CreatePartnerSettlementsCommand::class,
		Commands\Finance\CreateScheduledBookingSettlementsCommand::class,
		Commands\Finance\UpdateScheduledBookingSettlementsCommand::class,

		// Reminders followup to accounts team to add adjustments/refunds
		Commands\Reminders\PendingSettlementsReminderCommand::class,
		Commands\Reminders\PendingCustomerRefundsCommand::class,
		Commands\Reminders\SettlementsCreationAlertCommand::class,

		/* Auto water mark the images yearly*/
		Commands\Util\AutoWaterMarkImageCommand::class,

		/* Generate settled partner settlement invoices */
		Commands\Util\SettlementInvoiceCreationCommand::class,

		/* Refer & Earn after booking reminder */
		Commands\Reminders\ReferEarnReminderCustomerAfterBookingCommand::class,

		/* Google Ad customer party reminders */
		Commands\Reminders\GoogleADsReminder::class,

		/* RSVP response alert to Customer(host) */
		Commands\Reminders\RSVPAlertToCustomerCommand::class,

		/* Payment Monitoring alerts */
		Commands\Payments\PaymentFailureToTeamCommand::class,

		/* Every day payments report */
		Commands\Payments\PaymentReportToTeamCommand::class,

		/* Payment check every minute */
		Commands\Payments\PaymentSuccessComForNonRedirectSuccessPageCommand::class,

		/* Auto map partner reviews with options */
		Commands\Reviews\AutoMapOptionReviews::class,

		/* DB connections monitor & log all the data */
		Commands\Util\DBConnectionsMonitor::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		// alert team to tally all accounts / resolve all issues
		// Will run at 10:00AM monday & tuesday
		$schedule->command('evibe:settlements-creation-alert')
			->cron('0 10 * * 1,2');

		// remind accounts team to process / reject customer refunds every morning 10:00 AM
		$schedule->command('evibe:pending-refunds')
			->dailyAt('10:00');

		// remind accounts team to settle pending partner settlements -> tuesday, friday 10:00 AM
		$schedule->command('evibe:pending-settlements')
			->cron('0 10 * * 2,5');

		// update product sort order table with computed scores daily at 11:00 PM
		$schedule->command('evibe:set-product-score')
			->dailyAt('23:00');

		// update product tables with provider ratings (2 times a day - at '8:00' and '20:00')
		$schedule->command('evibe:update-provider-ratings')
			->twiceDaily(8, 20);

		// update type category tags along with their ratings (2 times a day - at '16:00' and '22:00')
		$schedule->command('evibe:update-category-tags')
			->twiceDaily(16, 22);

		// ask customer for feedback -> monday to friday at 4:00 PM
		$schedule->command('evibe:ask-feedback')
			->twiceDaily(12, 15);

		// ask customer for drop off feedback -> Wednesday and Thursday at 8:00 PM
		// $schedule->command('evibe:ask-drop-off-feedback')
		//          ->cron('0 20 * * 3,4');

		//take the backup of database daily at 7 PM
		$schedule->command('backup:run', ['--only-db'])
			->twiceDaily(11, 22)
			->description('Evibe Database Backup')
			->sendOutputTo('storage/logs/backup.log');

		// clean the backup daily
		$schedule->command('backup:clean')
			->dailyAt('23:00');

		// send the feedback status to team, whether customer has replied or not
		$schedule->command('evibe:feedback-status')
			->weekdays()
			->at('18:00');

		// send ticket reminders (follow-up, retention, no-response, etc.,)
		// @todo: edit before creating release
		$schedule->command('evibe:ticket-reminders')
			->everyMinute();

		// make API call to create retention reminders every day
		$schedule->command('evibe:create-retention-reminders')
			->dailyAt('0:30');

		// sending advance payment due reminder: 60 mins, 30 mins
		$schedule->command('evibe:payment-reminder-customer')
			->everyMinute();

		// send cancellation emails after payment deadline
		$schedule->command('evibe:send-cancellation-mail')
			->everyMinute();

		// sending auto book reminder to the team
		$schedule->command('evibe:auto-booking-team-reminder')
			->dailyAt('9:00');

		$schedule->command('evibe:auto-booking-team-reminder')
			->dailyAt('17:00');

		/* // @see: removed on 16 Mar 2020
		$schedule->command('evibe:service-auto-booking-team-reminder')
		         ->dailyAt('9:00');

		$schedule->command('evibe:service-auto-booking-team-reminder')
		         ->dailyAt('17:00');
		*/

		// send followup reminder to partners (photo/video)
		$schedule->command('evibe:partner-delivery-followup')
			->dailyAt('10:30');

		// send event reminders to customers / partners -> everyday at 8:00 AM
		$schedule->command('evibe:event-reminders')
			->dailyAt('8:00');

		// send sms to partner before 3 hrs of party
		$schedule->command('evibe:event-reminder-partner')
			->everyMinute();

		// send app notification to partner before 10 minutes of deadline time
		$schedule->command('evibe:pending-enquiry')
			->everyMinute();

		// send app notification to partner for pending deliveries
		$schedule->command('evibe:pending-delivery')
			->dailyAt('9:30');

		// send app notification to operations team regarding booking concepts on thursdays at 4:00 pm
		$schedule->command('evibe:concept-alert')
			->thursdays()
			->at('16:00');

		// Update searchable product table at 2pm & 8pm
		$schedule->command('evibe:import-products-table')
			->twiceDaily(14, 20);

		// Push searchable product table to elasticsearch index at 3pm & 9pm
		$schedule->command('evibe:import-products-es')
			->twiceDaily(15, 21);

		// Send G-Ad customer - SMS one day before the party date
		$schedule->command('evibe:google-ad-reminder')
			->dailyAt('11:00');

		// Payment report to team to check for any deviations
		$schedule->command('evibe:payment-report-team-alert')
			->dailyAt('20:10');

		// Trigger notifications if payment is done & not redirected back to success page
		$schedule->command('evibe:payment-success-com-non-redirect')
			->everyMinute();

		/*$schedule->command('evibe:non-responded-reminder-team')
		         ->everyMinute();*/

		// send event reminders to team at 9:30 AM for parties which will happen after 12 PM
		/*$schedule->command('evibe:team-reminders-post-12')
		         ->dailyAt('9:30');*/

		// send event reminders to team at 2:00 PM for parties which will happen tomorrow after 12 PM
		/*$schedule->command('evibe:team-reminders-pre-12')
		         ->dailyAt('14:00');*/

		// send event reminders to team at 9:30 on friday for all parties which will happen on friday, saturday & sunday
		/*$schedule->command('evibe:team-reminders-weekends')
		         ->weekdays()
		         ->fridays()
		         ->at('9:30');*/

		// sending event reminder to bd team {3} hrs before party (events between 8AM to 8PM).
		/*$schedule->command('evibe:event-reminders-bd')
		         ->everyMinute();*/

		// sending event reminders to BD team for all event between 8PM to 8AM
		/*$schedule->command('evibe:event-reminders-bd-after-8')
		         ->dailyAt('18:00');*/

		// pending activation reminder
		/*$schedule->command('evibe:pending-activation')
		         ->dailyAt('10:00');*/

		// yesterday activated reminder
		/*$schedule->command('evibe:newly-activation')
		         ->dailyAt('10:00');*/

		// Send RSVP status to host daily at 8 PM
		/*$schedule->command('evibe:rsvp-alert-customer')
		         ->dailyAt('20:00');*/

		// send list of pending followup daily at 9:30 AM to BD
		/*$schedule->command('evibe:vendor-followup-pending-reminders')
		         ->dailyAt('9:30');*/

		// send followup reminder to BD handler before 10 minutes
		/*$schedule->command('evibe:vendor-followup-reminders')
		         ->everyMinute();*/

		// send thank-you card reminders to customers daily at 9AM, 12PM, 3PM, 6PM
		/*$schedule->command('evibe:thank-you-card')
		         ->cron('0 9,12,15,18 * * * *');*/

		// @todo: re-enable it once the mysql service utilization is normal
		// Send referral Email after 30min of payment
		/*$schedule->command('evibe:send-refer-earn-reminder')
		         ->everyMinute();*/

		// Alert team if failure happens with respect to payments
		$schedule->command('evibe:payment-failure-team-alert')
			->everyMinute();

		// Log & alert team for max user connections
		$schedule->command('evibe:db-connections-monitor')
			->everyMinute();

		// create partner settlement at every thursday 12 PM
		// @see: disabled on 20 Apr 2018, settlements to be created manually from Dash.
		/*$schedule->command('evibe:create-partner-settlements')
		         ->wednesdays()
		         ->at('12:00');*/

		// send reminder to business team of evibe for the availability
		// which is asked by CRM team before 12 AM yesterday
		/*
		$schedule->command('evibe:venue-avl-pre12')
		         ->dailyAt('11:00');
		*/

		// send reminder to business team of evibe for the availability
		// which is asked by CRM team after 12 PM yesterday
		/*
		$schedule->command('evibe:venue-avl-post12')
		         ->dailyAt('17:00');
		*/

		// @disabled: 8 Dec 2018,
		// @author: Anji <anji@evibe.in>
		// @comments: Ignored by team and not useful
		/*

		// send list of pending followup daily at 9:30 AM to CRM head
		$schedule->command('evibe:ticket-followup-pending-reminder')
		         ->dailyAt('9:30');

		// send followup reminder to CRM handler before 10 minutes
		$schedule->command('evibe:ticket-followup-reminder')
		         ->everyMinute();
		*/

		// send settlement reminders to customer after 1 hrs 30 minute the party
		// $schedule->command('evibe:booking-settlement-reminder-customer')
		//       ->everyMinute();

		/*
		// recommendation email followups at the followup time
		$schedule->command('evibe:reco-followup-reminders-customer')
		         ->everyMinute();
		*/

		// disabled since 18 Jul 2018.
		/*
		$schedule->command('evibe:event-reminders-after-office')
		         ->dailyAt('18:30');
		*/

		// send digest mails on every thursday morning 10AM
		//$schedule->command('evibe:partner-digest')
		//         ->wednesdays()
		//         ->at('07:30');
	}
}
