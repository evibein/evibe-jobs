<?php

namespace App\Jobs\Email;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class NewlyActivationReminderNotificationJob extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$yesterday = Carbon::yesterday()->startOfDay()->timestamp;
		$yesterday = date('d M Y', $yesterday);
		$sub = "[Evibe.in]  New activations on [$yesterday]";

		$emailData = [
			'to'    => config('evibe.contact.customer.group'),
			'sub'   => $sub,
			'cc'    => config('evibe.contact.marketing.group'),
			'name'  => 'CRM Team',
			'items' => $this->data
		];

		Mail::send('emails.reminder.newly_activation', $emailData, function ($mail) use ($emailData) {
			$mail->from(config('evibe.email'), 'Team Evibe');
			$mail->to($emailData['to']);
			$mail->cc($emailData['cc']);
			$mail->subject($emailData['sub']);
		});
	}
}
