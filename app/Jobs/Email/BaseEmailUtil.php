<?php

namespace App\Jobs\Email;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Aws\Laravel\AwsFacade as AWS;

class BaseEmailUtil extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		//
	}

	public function sendEmail($data)
	{
		$ses = AWS::createClient('Ses');

		if ($data['to'])
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => $data['replyTo'],
				                'Destination'      => [
					                'ToAddresses' => $data['to'],
					                'CcAddresses' => $data['cc']
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
		else
		{
			$ses->sendEmail([
				                'Source'           => 'Null Email Alert <ping@evibe.in>',
				                'ReplyToAddresses' => config('evibe.contact.tech.group'),
				                'Destination'      => [
					                'ToAddresses' => config('evibe.contact.customer.group'),
					                'CcAddresses' => config('evibe.contact.tech.group')
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => '[Null Email address]. Sub: ' . $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
	}
}
