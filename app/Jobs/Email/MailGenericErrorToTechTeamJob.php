<?php

namespace App\Jobs\Email;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class MailGenericErrorToTechTeamJob extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = '[JOB Error] ' . $data['code'] . '-' . date('M d, Y, h:i A', time());

		Mail::send('emails.report.team.generic-error', ['data' => $data], function ($mail) use ($data)
		{
			$mail->from(config('evibe.email'), 'Evibe.in');
			$mail->to(config('evibe.contact.tech.group'));
			$mail->subject($data['sub']);
			$mail->replyTo(config('evibe.contact.tech.group'));
		});

	}
}
