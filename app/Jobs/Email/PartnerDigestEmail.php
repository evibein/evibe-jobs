<?php

namespace App\Jobs\Email;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class PartnerDigestEmail extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$lastBeforeMonday = Carbon::today()->startOfWeek()->subWeek(1)->getTimestamp(); // last week monday
		$lastSunday = Carbon::today()->startOfWeek()->subWeek(1)->endOfWeek()->getTimestamp(); // last week sunday
		$lastBeforeMonday = date('d-M-Y', $lastBeforeMonday);
		$lastSunday = date('d-M-Y', $lastSunday);
		$sub = 'Your week summary from ' . $lastBeforeMonday . ' - ' . $lastSunday;
		if (($data['response']['data']['completed_orders_count'] > 0) && ($data['response']['data']['delivery_percent'] != 100))
		{
			$sub = '[Action Needed] Your week summary from ' . $lastBeforeMonday . ' - ' . $lastSunday . ' & pending deliveries';
		}

		$emailData = [
			'sub'  => $sub,
			'data' => $data
		];

		if (!is_null($emailData['data']['provider']['email']))
		{
			Mail::send('emails.report.partner.week_digest_report', $emailData, function ($mail) use ($emailData) {
				$mail->from(config('evibe.contact.business.group'), 'Team Evibe.in');
				$mail->to($emailData['data']['provider']['email']);
				$mail->cc([config("evibe.contact.business.group")]);
				$mail->subject($emailData['sub']);
			});
		}
		else
		{
			Mail::send('emails.report.partner.week_digest_report', $emailData, function ($mail) use ($emailData) {
				$mail->from(config('evibe.contact.business.group'), 'Team Evibe.in');
				$mail->to(config('evibe.contact.business.group'));
				$mail->cc(config('evibe.contact.tech.group'));
				$mail->subject('[Null Email address]. Sub: ' . $emailData['sub']);
			});
		}
	}
}
