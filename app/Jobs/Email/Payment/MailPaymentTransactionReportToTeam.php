<?php

namespace App\Jobs\Email\Payment;

use App\Jobs\Email\BaseEmailUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class MailPaymentTransactionReportToTeam extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$timeSlot = Carbon::today()->startOfDay()->subHours(4) . " - " . Carbon::today()->endOfDay()->subHours(4);
		$data["timeSlot"] = $timeSlot;
		$sub = ($data["is_piab"] == "1") ? "[PIAB] Payment Report [" . $timeSlot . "]" : "Payment Report [" . $timeSlot . "]";

		Mail::send("emails.payment.report", ['data' => $data], function ($mail) use ($data, $sub) {
			$mail->from(config("evibe.email"), ($data["is_piab"] == "1") ? "PIAB - Evibe.in" : "Team Evibe.in")
			     ->to(config("evibe.contact.tech.group"))
			     ->subject($sub);
		});
	}
}