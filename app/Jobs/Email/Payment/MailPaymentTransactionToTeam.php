<?php

namespace App\Jobs\Email\Payment;

use App\Jobs\Email\BaseEmailUtil;
use Illuminate\Support\Facades\Mail;

class MailPaymentTransactionToTeam extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "[#P1 - Payment Error] Customer Payment Issue - #" . $data["ticket"]->id;

		Mail::send("emails.payment.transaction_error.mail-payment-transaction-error-team", ['data' => $data], function ($mail) use ($data, $sub) {
			$mail->from(config("evibe.email"), "Team Evibe.in")
			     ->to(config("evibe.contact.tech.group"))
			     ->subject($sub);
		});
	}
}