<?php

namespace App\Jobs\Email\Payment;

use App\Jobs\Email\BaseEmailUtil;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\Ticket;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class TriggerPaymentSuccessNotifications extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		foreach ($data as $transactionId => $refId)
		{
			$paymentTransaction = PaymentTransactionAttempts::where("payment_transaction_id", $transactionId)
			                                                ->first();

			$ticketId = $paymentTransaction ? $paymentTransaction->ticket_id : null;

			if (!is_null($ticketId))
			{
				$ticket = Ticket::find($ticketId);

				if ($ticket)
				{
					$count = 0;
					foreach ($ticket->bookings as $booking)
					{
						$count++;
						Log::info("Auto accepting payment initiated, ticketId: " . $ticketId);
						if (!$booking->is_advance_paid)
						{
							$booking->update([
								                 'payment_reference'       => $refId,
								                 'is_advance_paid'         => 1,
								                 'type_payment_gateway_id' => 2,
								                 'coupon_id'               => $booking->prepay_coupon_id,
								                 'discount_amount'         => $booking->prepay_discount_amount
							                 ]);
						}
					}

					if($count > 0)
					{
						$isAutoBooked = $ticket->is_auto_booked;

						if ($isAutoBooked)
						{
							$token = Hash::make($ticket->phone);
							$url = config("evibe.main_url") . "auto-book/now/process/" . $ticketId . "?token=" . $token;
						}
						else
						{
							$url = config("evibe.main_url") . "pay/process/" . $ticketId;
						}

						try
						{
							$ch = curl_init($url);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

							$cUrlResponse = json_decode(curl_exec($ch), true);
							if ($cUrlResponse['success'])
							{
								Log::info("Auto payment transaction success");
							}
							else
							{
								$error = isset($cUrlResponse['error']) ? $cUrlResponse['error'] : '';
								Log::info("Auto payment transaction fail: " . $error);
							}

							curl_close($ch);

						} catch (\Exception $exception)
						{
							$this->sendErrorReport($exception);
						}
					}
				}
			}
		}
	}
}