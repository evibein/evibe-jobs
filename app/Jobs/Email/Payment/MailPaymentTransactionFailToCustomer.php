<?php

namespace App\Jobs\Email\Payment;

use App\Jobs\Email\BaseEmailUtil;
use Illuminate\Support\Facades\Mail;

class MailPaymentTransactionFailToCustomer extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "Payment Failed for your Order - #" . $data["enquiryId"];

		Mail::send("emails.payment.transaction_fail", ['data' => $data], function ($mail) use ($data, $sub) {
			$mail->from(config("evibe.email"), "Evibe Payments")
			     ->to($data["to"])
			     ->subject($sub);
		});
	}
}