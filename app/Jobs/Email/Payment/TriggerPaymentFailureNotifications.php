<?php

namespace App\Jobs\Email\Payment;

use App\Jobs\Email\BaseEmailUtil;
use App\Jobs\SMS\Payment\SMSPaymentFailedToCustomer;
use App\Jobs\SMS\Payment\SMSPaymentRetryToCustomer;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\Ticket;
use App\Models\TicketUpdate;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;
use Illuminate\Support\Facades\Hash;

class TriggerPaymentFailureNotifications extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		foreach ($data as $transactionId => $refId)
		{
			$paymentTransaction = PaymentTransactionAttempts::where("payment_transaction_id", $transactionId)
			                                                ->first();

			if ($paymentTransaction->is_failed_notification_sent != 1)
			{
				$ticketId = $paymentTransaction ? $paymentTransaction->ticket_id : null;

				if (!is_null($ticketId))
				{
					$ticket = Ticket::find($ticketId);

					if ($ticket)
					{
						$paymentTransaction->update([
							                            "is_failed_notification_sent" => 1
						                            ]);

						$updateData = [
							'ticket_id'   => $ticket->id,
							'status_id'   => $ticket->status_id,
							'comments'    => "Payment transaction failed, Money will be refunded back to the customer with in 3-5 business days. Ref Id: " . $refId,
							'status_at'   => time(),
							'type_update' => "Auto"
						];

						TicketUpdate::create($updateData);

						if ($ticket->is_auto_booked == 1)
						{
							$paymentLink = config('evibe.live.ab-checkout');
							$paymentLink .= "?id=" . $ticket->id;
							$paymentLink .= "&token=" . Hash::make($ticket->id);
							$paymentLink .= "&uKm8=" . Hash::make($ticket->phone);
						}
						else
						{
							$paymentLink = config('evibe.live.checkout');
							$paymentLink .= "?id=" . $ticket->id;
							$paymentLink .= "&token=" . Hash::make($ticket->id);
							$paymentLink .= "&uKm8=" . Hash::make($ticket->email);
						}

						$paymentLink = EvibeUtil::getShortLink($paymentLink);

						dispatch(new MailPaymentTransactionFailToCustomer([
							                                                  "paymentReferenceId" => $refId,
							                                                  "paymentLink"        => $paymentLink,
							                                                  "customerName"       => $ticket->name,
							                                                  "enquiryId"          => $ticket->enquiry_id,
							                                                  "to"                 => $ticket->email
						                                                  ]));

						dispatch(new SMSPaymentFailedToCustomer([
							                                        "enquiryId"     => "#" . $ticket->enquiry_id,
							                                        "customerPhone" => $ticket->phone
						                                        ]));

						dispatch(new SMSPaymentRetryToCustomer([
							                                       'shortUrl'      => $paymentLink,
							                                       "customerName"  => $ticket->name,
							                                       "customerPhone" => $ticket->phone
						                                       ]));
					}
				}
			}
		}
	}
}