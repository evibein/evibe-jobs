<?php

namespace App\Jobs\Emails;

use App\Jobs\Email\BaseEmailUtil;

class MailSMSErrorToAdminJob extends BaseEmailUtil
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$mailData = [
			'to'      => [$data['to']],
			'cc'      => [$data['to']],
			'from'    => "SMS Error Alert <ping@evibe.in>",
			'replyTo' => ['ping@evibe.in'],
			'sub'     => $data['subject'],
			'body'    => isset($data['text']) ? $data['text'] : 'SMS Sending Failed, check and fix at the earliest. Data unavailable. Number will be in the subject'
		];

		$this->sendEmail($mailData);
	}
}