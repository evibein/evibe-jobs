<?php

namespace App\Jobs\Email;

use Illuminate\Support\Facades\Mail;

class MailRSVPResponseRemindersToCustomer extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if ($data['email'])
		{
			Mail::send($data['mailView'], ['data' => $data], function ($mail) use ($data) {
				$mail->from($data['from'], (isset($data['fromText']) && $data['fromText']) ? $data['fromText'] : "Team Evibe.in")
				     ->to($data['email'])
				     ->subject($data['emailSub']);
			});
		}
		else
		{
			Mail::send($data['mailView'], ['data' => $data], function ($mail) use ($data) {
				$mail->from($data['from'], (isset($data['fromText']) && $data['fromText']) ? $data['fromText'] : "Team Evibe.in")
				     ->to(config('evibe.contact.customer.group'))
				     ->cc(config('evibe.contact.tech.group'))
				     ->subject('[Null Email address]. Sub: ' . $data['emailSub']);
			});
		}

	}
}
