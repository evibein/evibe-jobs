<?php

namespace App\Jobs\Email;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendReferralLinkToCustomer extends BaseEmailUtil
{

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$user = $data["user"];
		$sub = 'Exclusive: Get 20% off for every friend you refer';

		$emailData = [
			"email"       => $user->username,
			"subject"     => 'Exclusive: Get Rs 200* OFF for every friend you refer',
			"referralUrl" => config("evibe.main_url") . "re/" . $user->referral_code
		];

		$hashContent = str_replace(' ', '', $user->id . $user->name);
		$userToken = Hash::make((string)$hashContent);
		$dashBoardUrl = config("evibe.main_url") . "my/re/share?id=" . $user->id . "&token=" . $userToken . "&ref=email";

		$emailData["dashboardUrl"] = $dashBoardUrl;
		$emailData["whatsappShareLink"] = $dashBoardUrl . "#EmailWhatsapp";
		$emailData["messengerShareLink"] = $dashBoardUrl . "#EmailMessenger";
		$emailData["smsShareLink"] = $dashBoardUrl . "#EmailSMS";

		Mail::send('emails.reminder.referral-link-to-customer', ["data" => $emailData], function ($mail) use ($sub, $emailData) {
			$mail->from(config('evibe.email'), 'Evibe.in');
			$mail->to($emailData["email"]);
			$mail->subject($sub);
		});

	}
}