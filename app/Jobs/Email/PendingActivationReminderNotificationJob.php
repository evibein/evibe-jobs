<?php

namespace App\Jobs\Email;

use Illuminate\Support\Facades\Mail;

class PendingActivationReminderNotificationJob extends BaseEmailUtil
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$today = date('d M Y', time());
		$sub = "[Evibe.in] Pending Activation reminder at $today";

		$emailData = [
			'to'    => config('evibe.contact.business.email'),
			'sub'   => $sub,
			'name'  => 'Admin',
			'items' => $this->data
		];

		Mail::send('emails.reminder.pending_activation', $emailData, function ($mail) use ($emailData) {
			$mail->from(config('evibe.email'), 'Team Evibe');
			$mail->to($emailData['to']);
			$mail->subject($emailData['sub']);
		});
	}
}
