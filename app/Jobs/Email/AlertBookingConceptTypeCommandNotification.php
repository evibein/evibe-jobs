<?php

namespace App\Jobs\Email;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class AlertBookingConceptTypeCommandNotification extends BaseEmailUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$lastBeforeMonday = Carbon::today()->startOfWeek()->subWeek(1)->getTimestamp(); // last week monday
		$lastSunday = Carbon::today()->startOfWeek()->subWeek(1)->endOfWeek()->getTimestamp(); // last week sunday
		$lastBeforeMonday = date('d-M-Y', $lastBeforeMonday);
		$lastSunday = date('d-M-Y', $lastSunday);
		$sub = 'Digest of concept bookings from ' . $lastBeforeMonday . ' to ' . $lastSunday;

		$emailData = [
			'sub'   => $sub,
			'items' => $this->data
		];

		Mail::send('emails.reminder.booking_concept_reminder', $emailData, function ($mail) use ($emailData)
		{
			$mail->from(config('evibe.email'), 'Concept Digest');
			$mail->to(config('evibe.contact.customer.group'));
			$mail->subject($emailData['sub']);
		});

	}
}
