<?php

namespace App\Jobs\Email;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendPriceRevisionUpdatesToPartnersJob extends BaseEmailUtil
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "Revision of your package prices to include GST";

		$data['sub'] = $sub;

		if ($data['to'])
		{
			Mail::send('emails.partner.update-prices', $data, function ($mail) use ($data) {
				$mail->from(config('evibe.accounts_email'), 'Team Evibe.in');
				$mail->to($data['to']);
				$mail->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.partner.update-prices', $data, function ($mail) use ($data) {
				$mail->from(config('evibe.accounts_email'), 'Team Evibe.in');
				$mail->to(config('evibe.contact.business.group'));
				$mail->cc(config('evibe.contact.tech.group'));
				$mail->subject('[Null Email address]. Sub: ' . $data['sub']);
			});
		}
	}
}
