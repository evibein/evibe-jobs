<?php

namespace App\Jobs\Search;

use App\Jobs\Job;
use App\Models\Search\SearchableProduct;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddProductsToSingleTable extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * @array
	 */
	protected $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$products = collect($this->data);

		foreach ($products as $product)
		{
			$this->insertIntoSingleTable($product);
		}
	}

	private function insertIntoSingleTable($values)
	{
		$searchProduct = SearchableProduct::updateOrCreate([
			                                                   "option_type_id" => $values["option_type_id"],
			                                                   "option_id"      => $values["option_id"],
			                                                   "city_id"        => $values["city_id"],
		                                                   ], [
			                                                   "updated_at" => Carbon::now()
		                                                   ]);

		$searchProduct->update($values);
	}
}
