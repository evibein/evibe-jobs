<?php

namespace App\Jobs\Search;

use App\Jobs\Job;
use App\Models\Search\SearchableProduct;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemoveDefunctProductsFromSingleTable extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * @array
	 */
	protected $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$defunctProducts = collect($this->data);

		foreach ($defunctProducts as $defunctProduct)
		{
			$defunctSearchProducts = SearchableProduct::where("option_id", $defunctProduct["option_id"])
			                                          ->where("option_type_id", $defunctProduct["option_type_id"])
			                                          ->get();

			if ($defunctSearchProducts->count())
			{
				foreach ($defunctSearchProducts as $defunctSearchProduct)
				{
					try
					{
						$defunctSearchProduct->removeFromIndex();
						$defunctSearchProduct->forceDelete();
					} catch (\Exception $e)
					{
						continue;
					}
				}
			}
		}
	}
}