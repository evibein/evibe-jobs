<?php

namespace App\Jobs\Search;

use App\Jobs\Job;
use App\Models\Search\SearchableProduct;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddProductsToESIndex extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$products = SearchableProduct::select("id", "code", "name", "inclusions", "tags", "occasion_name", "city_name", "price_min", "price_max", "city_id", "option_type_id", "option_id", "option_sub_type_id", "created_at")
		                             ->get();

		foreach ($products->chunk(20) as $chunk)
		{
			$chunk->addToIndex();
		}
	}
}
