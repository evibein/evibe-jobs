<?php

namespace App\Jobs\SMS;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SMSTicketReminderToCustomer extends BaseSMSUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$smsType = (isset($data['smsType']) && $data['smsType']) ? $data['smsType'] : null;
		$this->sms($data, $smsType);
	}
}
