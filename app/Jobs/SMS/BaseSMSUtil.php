<?php

namespace App\Jobs\SMS;

use App\Jobs\Emails\MailSMSErrorToAdminJob;
use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BaseSMSUtil extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		//
	}

	public function sms($data, $smsType = null)
	{
		$username = config('smsc.username');
		$password = config('smsc.password');
		$senderId = config('smsc.sender_id');
		if ($smsType == "PROMOTIONAL")
		{
			$smsType = config('smsc.route.promotional');
		}
		else
		{
			$smsType = config('smsc.route.transactional');
		}
		$text = rawurlencode($data['text']);
		$to = $data['to'];

		if ($to && strlen($to) == 10)
		{
			$smsGatewayApi = "http://smsc.biz/httpapi/send?username=" . $username .
				"&password=" . $password .
				"&sender_id=" . $senderId .
				"&route=" . $smsType .
				"&phonenumber=" . $to .
				"&message=" . $text;

			$ch = curl_init($smsGatewayApi);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);

			// sms sending error occurred
			if ($result < 0)
			{
				$this->triggerErrorEmail(['to'         => $to,
				                          'text'       => $data['text'],
				                          'error_code' => $result
				                         ]);
			}
		}
		else
		{
			$this->triggerErrorEmail([
				                         'to'         => $to,
				                         'text'       => $data['text'],
				                         'error_code' => 'Invalid'
			                         ]);
		}
	}

	private function triggerErrorEmail($data, $fromAppGateway = false)
	{
		$errorType = '[SMS Error]';
		if ($fromAppGateway)
		{
			$errorType = '[SMS App Error]';
		}

		$errorData = [
			'to'      => config('evibe.contact.tech.email'),
			'subject' => $errorType . ' SMS sending failed - ' . $data['to'] . " - " . $data['error_code'],
			'text'    => $data["text"]
		];

		dispatch(new MailSMSErrorToAdminJob($errorData));
	}
}