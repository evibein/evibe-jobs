<?php

namespace App\Jobs\SMS\Payment;

use App\Jobs\SMS\BaseSMSUtil;

class SMSPaymentFailedToCustomer extends BaseSMSUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.payment.failed');
		$replaces = [
			'#field1#' => $data['enquiryId'],
			'#field2#' => "3-5"
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms([
			           "to"   => $data['customerPhone'],
			           "text" => $smsText
		           ]);
	}
}