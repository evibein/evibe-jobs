<?php

namespace App\Jobs\SMS\Payment;

use App\Jobs\SMS\BaseSMSUtil;

class SMSPaymentRetryToCustomer extends BaseSMSUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.payment.retry_customer1');
		$replaces = [
			'#field1#' => $data['customerName'],
			'#field2#' => $data['shortUrl'],
			'#field3#' => config("evibe.email_hello"),
			'#field4#' => config("evibe.contact.company")
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms([
			           "to"   => $data['customerPhone'],
			           "text" => $smsText
		           ]);
	}
}