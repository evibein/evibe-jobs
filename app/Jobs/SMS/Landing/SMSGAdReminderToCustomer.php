<?php

namespace App\Jobs\SMS\Landing;

use App\Jobs\SMS\BaseSMSUtil;
use App\Models\Coupon\Coupon;
use Illuminate\Support\Facades\Log;

class SMSGAdReminderToCustomer extends BaseSMSUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$coupon = Coupon::find($data["couponId"]);

		if ($coupon)
		{
			// send SMS to customer
			$tplCustomer = config('evibe.sms_tpl.landing.surprises');
			$replaces = [
				'#field1#' => "Kushi",
				'#field2#' => $coupon->coupon_code,
				'#field3#' => "200",
				'#field4#' => "www.evibe.in",
				'#field5#' => config("evibe.phone_plain")
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

			$this->sms([
				           "to"   => $data['phone'],
				           "text" => $smsText
			           ], "PROMOTIONAL");
		}
		else
		{
			Log::info("Coupon code not found.");
		}
	}
}