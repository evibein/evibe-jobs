<?php

namespace App\Jobs\SMS\ReferEarn;

use App\Jobs\SMS\BaseSMSUtil;

class SMSREReminderToCustomer extends BaseSMSUtil
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.refer_earn.referral.signup_success');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['referralLink'],
			'#field3#' => "Rs 200",
			'#field4#' => "Rs 200"
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms([
			           "to"   => $data['referralPhone'],
			           "text" => $smsText
		           ]);
	}
}