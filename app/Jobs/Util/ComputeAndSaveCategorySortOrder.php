<?php

namespace App\Jobs\Util;

use App\Jobs\Email\BaseEmailUtil;
use App\Models\Cake\Cake;
use App\Models\Decor;
use App\Models\Package;
use App\Models\PartyBag\ShortlistOptions;
use App\Models\Ticket;
use App\Models\Ticket\TicketMapping;
use App\Models\TicketBooking;
use App\Models\Trend;
use App\Models\TypeService;
use App\Models\Util\ProductSortOrder;
use App\Models\VenueHall;
use App\Models\ViewCount\ProfileViewCounter;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ComputeAndSaveCategorySortOrder extends BaseEmailUtil
{

	/**
	 * How product scores are calculated?
	 *
	 */

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$timeInterval = $data['timeInterval'];
		$productTypeId = $data['productTypeId'];
		$eventId = $data['eventId'];
		$cityId = $data['cityId'];
		$productsDataArray = [];

		if (!count($timeInterval))
		{
			Log::error("ERROR:: Time interval is not valid");

			return;
		}

		switch ($productTypeId)
		{
			case(config('evibe.ticket.type.packages')):
			case(config('evibe.ticket.type.resort')):
			case(config('evibe.ticket.type.villa')):
			case(config('evibe.ticket.type.lounge')):
			case(config('evibe.ticket.type.food')):
			case(config('evibe.ticket.type.couple-experiences')):
			case(config('evibe.ticket.type.venue-deals')):
			case(config('evibe.ticket.type.priests')):
			case(config('evibe.ticket.type.tents')):
			case(config('evibe.ticket.type.generic-package')):

				$products = Package::isLive()
				                   ->forEvent($eventId)
				                   ->forCity($cityId)
				                   ->where('type_ticket_id', $productTypeId)
				                   ->whereNull('deleted_at')
				                   ->select('id', 'name', 'code', 'price', 'created_at AS createdAt')
				                   ->get();

				if (count($products) == 0)
				{
					Log::info("There are no packages with product type: $productTypeId, event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;

			case(config('evibe.ticket.type.cakes')):

				$products = Cake::isLive()
				                ->forEvent($eventId)
				                ->forCity($cityId)
				                ->whereNull('cake.deleted_at')
				                ->select('cake.id', 'cake.title AS name', 'cake.code AS code', 'cake.price_per_kg AS price', 'cake.created_at AS createdAt')
				                ->get();

				if (count($products) == 0)
				{
					Log::info("There are no cakes product type: $productTypeId, event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;

			case(config('evibe.ticket.type.decors')):

				$products = Decor::join('planner', 'planner.id', '=', 'decor.provider_id')
				                 ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
				                 ->isLive()
				                 ->whereNull('decor.deleted_at')
				                 ->whereNull('planner.deleted_at')
				                 ->whereNull('decor_event.deleted_at')
				                 ->where('decor_event.event_id', $eventId)
				                 ->where('planner.city_id', $cityId)
				                 ->select('decor.id', 'decor.name', 'decor.code', 'decor.min_price AS price', 'decor.created_at AS createdAt')
				                 ->get();

				if (count($products) == 0)
				{
					Log::info("There are no decors with product type id: $productTypeId, event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;

			case(config('evibe.ticket.type.entertainments')):

				$products = TypeService::isLive()
				                       ->forEvent($eventId)
				                       ->forCity($cityId)
				                       ->whereNull('type_service.deleted_at')
				                       ->select('type_service.id', 'type_service.name', 'type_service.code', 'type_service.min_price AS price', 'type_service.created_at AS createdAt')
				                       ->get();

				if (count($products) == 0)
				{
					Log::error("There are no services for event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;

			case(config('evibe.ticket.type.trends')):

				$products = Trend::isLive()
				                 ->forEvent($eventId)
				                 ->forCity($cityId)
				                 ->whereNull('trending.deleted_at')
				                 ->select('trending.id', 'trending.name', 'trending.code', 'trending.price', 'trending.created_at AS createdAt')
				                 ->get();

				if (count($products) == 0)
				{
					Log::error("There are no trends for event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;

			case(config('evibe.ticket.type.venues')):
			case(config('evibe.ticket.type.venue_halls')):

				$products = VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
				                     ->where('venue.city_id', $cityId)
				                     ->where('venue.is_live', 1)
				                     ->forEvent($eventId)
				                     ->whereNull('venue.deleted_at')
				                     ->where('venue_hall.deleted_at')
				                     ->select('venue_hall.id', 'venue_hall.name', 'venue_hall.code', 'venue_hall.price_min_veg AS price', 'venue_hall.created_at AS createdAt')
				                     ->distinct('venue_hall.id')
				                     ->get();

				if (count($products) == 0)
				{
					Log::error("There are no venue halls for event: $eventId, city: $cityId");
					break;
				}

				$productsDataArray = $this->formSpecificSortOrderArray($products->toArray());
				break;
		}
		
		if (!(isset($productsDataArray['options']) && count($productsDataArray['options'])))
		{
			Log::info("No products found");

			return;
		}

		$productIdArray = array_keys($productsDataArray['options']);
		foreach ($timeInterval as $key => $interval)
		{
			/* === A.1 Product Views computation === */
			$intervalProductViews = ProfileViewCounter::select('product_id', DB::raw("SUM(total_views) AS views"))
			                                          ->where('start_time', '>=', strtotime($interval['startTime']))
			                                          ->where('end_time', '<', strtotime($interval['endTime']))
			                                          ->where('product_type_id', $productTypeId)
			                                          ->whereIn('product_id', $productIdArray)
			                                          ->whereNull('deleted_at')
			                                          ->groupBy('product_id')
			                                          ->get();

			foreach ($intervalProductViews as $item)
			{
				if (isset($productsDataArray['options'][$item->product_id]['visits']['productViews']['i' . $key]))
				{
					$productsDataArray['options'][$item->product_id]['visits']['productViews']['i' . $key] = $item->views;
				}
				else
				{
					Log::error("Default views interval ($key) data not found for product type: " . $item->product_type_id . ", product id: " . $item->product_id);
				}
			}

			/* === A.2 Shortlists computation === */
			$intervalShortlists = TicketMapping::join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
			                                   ->select('ticket_mapping.map_id', DB::raw('count(*) AS value'))
			                                   ->where('ticket_mapping.selected_at', '>=', $interval['startTime'])
			                                   ->where('ticket_mapping.selected_at', '<', $interval['endTime'])
			                                   ->where('ticket_mapping.map_type_id', $productTypeId)
			                                   ->whereIn('ticket_mapping.map_id', $productIdArray)
			                                   ->whereNull('ticket.deleted_at')
			                                   ->whereNull('ticket_mapping.deleted_at')
			                                   ->whereNotNull('ticket_mapping.is_selected')
			                                   ->whereNotNull('ticket_mapping.selected_at')
			                                   ->groupBy('ticket_mapping.map_id')
			                                   ->get();

			foreach ($intervalShortlists as $item)
			{
				if (isset($productsDataArray['options'][$item->map_id]['visits']['shortlists']['i' . $key]))
				{
					$productsDataArray['options'][$item->map_id]['visits']['shortlists']['i' . $key] = $item->value;
				}
				else
				{
					Log::error("Default shortlist interval ($key) data not found for product type: " . $item->map_type_id . ", product id: " . $item->map_id);
				}
			}

			/* === A.3 PartyBag computation === */
			$intervalPBCounts = ShortlistOptions::select('map_id', DB::raw('COUNT(*) AS value'))
			                                    ->where('created_at', '>=', $interval['startTime'])
			                                    ->where('created_at', '<', $interval['endTime'])
			                                    ->where('map_type_id', $productTypeId)
			                                    ->whereIn('map_id', $productIdArray)
			                                    ->whereNull('deleted_at')
			                                    ->groupBy('map_id')
			                                    ->get();

			foreach ($intervalPBCounts as $item)
			{
				if (isset($productsDataArray['options'][$item->map_id]['visits']['partyBag']['i' . $key]))
				{
					$productsDataArray['options'][$item->map_id]['visits']['partyBag']['i' . $key] = $item->value;
				}
				else
				{
					// todo: inform team (or) command
					Log::error("Default party bag shortlist interval ($key) data not found for product type: " . $item->map_type_id . ", product id: " . $item->map_id);
				}
			}

			/* === B.1 Orders computation === */
			// @see: added cancellation condition
			$intervalOrders = TicketBooking::join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
			                               ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                               ->select('ticket_mapping.map_id', DB::raw('count(*) AS value'))
			                               ->where('ticket.paid_at', '>=', strtotime($interval['startTime']))
			                               ->where('ticket.paid_at', '<', strtotime($interval['endTime']))
			                               ->where('ticket_mapping.map_type_id', $productTypeId)
			                               ->whereIn('ticket_mapping.map_id', $productIdArray)
			                               ->whereNull('ticket.deleted_at')
			                               ->whereNull('ticket_mapping.deleted_at')
			                               ->whereNull('ticket_bookings.deleted_at')
			                               ->where('ticket_bookings.is_advance_paid', 1)
			                               ->where('ticket.status_id', config('evibe.ticket.status.booked'))
			                               ->whereNull('ticket_bookings.cancelled_at')
			                               ->groupBy('ticket_mapping.map_id')
			                               ->get();

			foreach ($intervalOrders as $item)
			{
				if (isset($productsDataArray['options'][$item->map_id]['sales']['orders']['i' . $key]))
				{
					$productsDataArray['options'][$item->map_id]['sales']['orders']['i' . $key] = $item->value;
				}
				else
				{
					// todo: inform team
					// todo: (or) command
				}
			}

			/* === B.2 Enquiries computation === */
			$intervalEnquiries = Ticket::select('type_id', DB::raw('count(*) AS value'))
			                           ->where('created_at', '>=', $interval['startTime'])
			                           ->where('created_at', '<', $interval['endTime'])
			                           ->where('type_ticket_id', $productTypeId)
			                           ->whereIn('type_id', $productIdArray)
			                           ->whereNull('deleted_at')
			                           ->groupBy('type_id')
			                           ->get();

			foreach ($intervalEnquiries as $item)
			{
				if (isset($productsDataArray['options'][$item->type_id]['sales']['enquiries']['i' . $key]))
				{
					$productsDataArray['options'][$item->type_id]['sales']['enquiries']['i' . $key] = $item->value;
				}
				else
				{
					// todo: inform team
					// todo: (or) command
				}
			}

			/* === B.3 Avail. Checks computation === */
			$intervalAvailChecks = TicketMapping::select('map_id', DB::raw('count(*) AS value'))
			                                    ->where('enquiry_sent_at', '>=', $interval['startTime'])
			                                    ->where('enquiry_sent_at', '<', $interval['endTime'])
			                                    ->where('map_type_id', $productTypeId)
			                                    ->whereIn('map_id', $productIdArray)
			                                    ->whereNotNull('enquiry_sent_at')
			                                    ->whereNull('deleted_at')
			                                    ->groupBy('map_id')
			                                    ->get();

			foreach ($intervalAvailChecks as $item)
			{
				if (isset($productsDataArray['options'][$item->map_id]['sales']['availabilityChecks']['i' . $key]))
				{
					$productsDataArray['options'][$item->map_id]['sales']['availabilityChecks']['i' . $key] = $item->value;
				}
				else
				{
					// todo: inform team
					// todo: (or) command
				}
			}
		}

		// visits and sales calculation
		$optionsCount = count($productsDataArray['options']);
		$totalVisits = [
			'i1' => 0,
			'i2' => 0,
			'i3' => 0
		];
		$totalSales = [
			'i1' => 0,
			'i2' => 0,
			'i3' => 0
		];
		$totalRecency = 0;

		foreach ($productsDataArray['options'] as $optionId => $optionData)
		{
			foreach ($timeInterval as $key => $interval)
			{
				$intervalVisits =
					$productsDataArray['options'][$optionId]['visits']['productViews']['i' . $key] * $data['weights']['compute']['visits']['product-views'] +
					$productsDataArray['options'][$optionId]['visits']['shortlists']['i' . $key] * $data['weights']['compute']['visits']['shortlists'] +
					$productsDataArray['options'][$optionId]['visits']['partyBag']['i' . $key] * $data['weights']['compute']['visits']['party-bag'];

				$intervalSales =
					$productsDataArray['options'][$optionId]['sales']['orders']['i' . $key] * $data['weights']['compute']['sales']['orders'] +
					$productsDataArray['options'][$optionId]['sales']['enquiries']['i' . $key] * $data['weights']['compute']['sales']['enquiries'] +
					$productsDataArray['options'][$optionId]['sales']['availabilityChecks']['i' . $key] * $data['weights']['compute']['sales']['availability-checks'];

				$productsDataArray['options'][$optionId]['visits']['i' . $key] = $intervalVisits;
				$productsDataArray['options'][$optionId]['sales']['i' . $key] = $intervalSales;

				$totalVisits['i' . $key] += $intervalVisits;
				$totalSales['i' . $key] += $intervalSales;
			}

			$totalRecency += $productsDataArray['options'][$optionId]['recency']['days'];
		}

		foreach ($timeInterval as $key => $interval)
		{
			$productsDataArray['averageVisits']['i' . $key] = $totalVisits['i' . $key] / $optionsCount;
			$productsDataArray['averageSales']['i' . $key] = $totalSales['i' . $key] / $optionsCount;
		}

		$productsDataArray['averageRecency'] = $totalRecency / $optionsCount;

		foreach ($productsDataArray['options'] as $optionId => $optionData)
		{
			// boost calculation
			$intervalVisitsComputation = 0;
			$intervalSalesComputation = 0;
			if (!$productsDataArray['options'][$optionId]['recency']['days'])
			{
				$productsDataArray['options'][$optionId]['recency']['days'] = 1;
			}
			$recencyComputation = $totalRecency / $productsDataArray['options'][$optionId]['recency']['days'];
			foreach ($timeInterval as $key => $interval)
			{
				if ($productsDataArray['averageVisits']['i' . $key])
				{
					$intervalVisitsComputation += $optionData['visits']['i' . $key] / $productsDataArray['averageVisits']['i' . $key] * $data['weights']['boost']['visits']['i' . $key];
				}
				else
				{
					$intervalVisitsComputation += 0;
				}

				if ($productsDataArray['averageSales']['i' . $key])
				{
					$intervalSalesComputation += $optionData['sales']['i' . $key] / $productsDataArray['averageSales']['i' . $key] * $data['weights']['boost']['sales']['i' . $key];
				}
				else
				{
					$intervalVisitsComputation += 0;
				}
			}

			$productsDataArray['options'][$optionId]['visits']['boost'] = log((1 + $intervalVisitsComputation), 10);
			$productsDataArray['options'][$optionId]['sales']['boost'] = log((1 + $intervalSalesComputation), 10);
			$productsDataArray['options'][$optionId]['recency']['boost'] = log((1 + $recencyComputation), 10);

			// overall score calculation
			// default over weights
			$visitsWeight = 1;
			$salesWeight = 1;
			$recencyWeight = 1;
			// overall weights based on price
			foreach ($data['weights']['overall'] as $item)
			{
				if (($productsDataArray['options'][$optionId]['price'] >= $item['minPrice']) &&
					(
						(!isset($item['maxPrice']) || (isset($item['maxPrice']) && $item['maxPrice'] == 0)) ||
						(isset($item['maxPrice']) && ($productsDataArray['options'][$optionId]['price'] < $item['maxPrice']))
					)
				)
				{
					$visitsWeight = $item['visits'];
					$salesWeight = $item['sales'];
					$recencyWeight = $item['recency'];
				}
			}

			$productsDataArray['options'][$optionId]['computedScore'] = (($visitsWeight * $productsDataArray['options'][$optionId]['visits']['boost']) +
				($salesWeight * $productsDataArray['options'][$optionId]['sales']['boost']) +
				($recencyWeight * $productsDataArray['options'][$optionId]['recency']['boost']));
		}

		// update or save
		foreach ($productsDataArray['options'] as $productId => $details)
		{
			$sortOrder = ProductSortOrder::where([
				                                     'product_id'      => $productId,
				                                     'product_type_id' => $productTypeId,
				                                     'event_id'        => $eventId,
				                                     'city_id'         => $cityId
			                                     ])
			                             ->whereNull('deleted_at')
			                             ->first();
			if ($sortOrder)
			{
				if (isset($details['computedScore']) && $details['computedScore'])
				{
					$sortOrder->score = $details['computedScore'];
					$sortOrder->updated_at = Carbon::now();
					$sortOrder->save();

				}
				else
				{
					// todo: inform team
					Log::error("ERROR:: [Update] Unable to calculate score : $productId - $productTypeId - $eventId - $cityId");
				}
			}
			else
			{
				if (isset($details['computedScore']) && $details['computedScore'])
				{
					ProductSortOrder::create([
						                         'product_id'      => $productId,
						                         'product_type_id' => $productTypeId,
						                         'event_id'        => $eventId,
						                         'city_id'         => $cityId,
						                         'score'           => $details['computedScore'],
						                         'created_at'      => Carbon::now(),
						                         'updated_at'      => Carbon::now()
					                         ]);

				}
				else
				{
					// todo: inform team
					Log::error("ERROR:: [Create] Unable to calculate score : $productId - $productTypeId - $eventId - $cityId");
				}
			}
		}
	}

	private function formSpecificSortOrderArray($options)
	{
		if (!is_array($options))
		{
			$options = $options->toArray();
		}

		$productsDataArray = [
			'options'        => [],
			'averageVisits'  => [
				'i1' => 0,
				'i2' => 0,
				'i3' => 0
			],
			'averageSales'   => [
				'i1' => 0,
				'i2' => 0,
				'i3' => 0
			],
			'averageRecency' => 0,
		];

		foreach ($options as $option)
		{
			$productsDataArray['options'][$option['id']] = [
				'name'            => $option['name'],
				'code'            => $option['code'],
				'price'           => $option['price'],
				'visits'          => [
					'productViews' => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'shortlists'   => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'partyBag'     => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'i1'           => 0,
					'i2'           => 0,
					'i3'           => 0,
					'boost'        => 0
				],
				'sales'           => [
					'orders'             => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'enquiries'          => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'availabilityChecks' => [
						'i1' => 0,
						'i2' => 0,
						'i3' => 0
					],
					'i1'                 => 0,
					'i2'                 => 0,
					'i3'                 => 0,
					'boost'              => 0
				],
				'recency'         => [
					'days'  => strtotime($option['createdAt']) <= time() ? (int)((time() - strtotime($option['createdAt'])) / (24 * 60 * 60)) : 0,
					'boost' => 0
				],
				'computedScore'   => 0,
				'artificialBoost' => 0
			];
		}

		return $productsDataArray;
	}
}