<?php

namespace Evibe\Utilities;

/**
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

use Evibe\Facades\EvibeUtilFacade as AppUtil;
use Illuminate\Support\Facades\Queue;

class SendSMS
{
	// Generic method to send SMS
	public function send($job, $data)
	{
		$this->sendViaSMSGateway($job, $data);
	}

	public function smsEventReminder($job, $data)
	{
		$this->sendViaSMSGateway($job, $data);
	}

	public function smsAdvPayReminder($job, $data)
	{
		$this->sendViaSMSGateway($job, $data);
	}

	public function smsRecoFollowup($job, $data)
	{
		$this->sendViaSMSGateway($job, $data);
	}

	/**
	 * @since 3 Jan 2015
	 */
	public function smsAskFeedback($job, $data)
	{
		$feedbackTpl = config('evibe.sms_tpl.cust_feedback');
		$tplVars = [
			'#field1#' => $data['name'],
			'#field2#' => $data['feedbackUrl']
		];

		$text = str_replace(array_keys($tplVars), array_values($tplVars), $feedbackTpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSGateway($job, $smsData);
	}

	public function smsDropOffFeedback($job, $data)
	{
		$feedbackTpl = config('evibe.sms_tpl.drop-off-feedback');
		$tplVars = [
			'#field1#' => $data['name'],
			'#field2#' => $data['amazonCash'],
			'#field3#' => $data['formLink'],
		];

		$text = str_replace(array_keys($tplVars), array_values($tplVars), $feedbackTpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSGateway($job, $smsData);
	}

	public function smsCancellationToCustomer($job, $data)
	{
		if (!$data)
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.cancel.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['partyDate'],
			'#field3#' => $data['reason'],
			'#field4#' => config('evibe.phone'),
			'#field5#' => $data['pgLink']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSGateway($job, $smsData);
	}

	public function smsCancellationToVendor($job, $data)
	{
		if (!$data)
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.cancel.vendor');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['partyDate'],
			'#field3#' => $data['customer'],
			'#field4#' => $data['reason']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['phone'],
			'text' => $text
		];

		$this->sendViaSMSGateway($job, $smsData);
	}

	private function sendViaSMSGateway($job, $data)
	{
		$username = config('smsc.username');
		$password = config('smsc.password');
		$senderId = config('smsc.sender_id');
		$smsType = config('smsc.route.transactional');
		$text = rawurlencode($data['text']);
		$to = $data['to'];

		if ($to && strlen($to) == 10)
		{
			$smsGatewayApi = "http://smsc.biz/httpapi/send?username=" . $username .
				"&password=" . $password .
				"&sender_id=" . $senderId .
				"&route=" . $smsType .
				"&phonenumber=" . $to .
				"&message=" . $text;

			$ch = curl_init($smsGatewayApi);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);

			// sms sending error occurred
			if ($result < 0)
			{
				$this->triggerErrorEmail(['to'         => $to,
				                          'text'       => $data['text'],
				                          'error_code' => $result
				                         ]);
			}
		}

		$job->delete();
	}

	private function triggerErrorEmail($data, $fromAppGateway = false)
	{
		$errorType = '[SMS Error]';
		if ($fromAppGateway)
		{
			$errorType = '[SMS App Error]';
		}

		$errorData = [
			'to'      => config('evibe.contact.tech.group'),
			'subject' => $errorType . ' SMS sending failed - ' . $data['to'] . " - " . $data['error_code'],
			'data'    => $data
		];

		Queue::push('Evibe\Utilities\SendEmail@mailSMSErrorToAdmin', $errorData);
	}

}