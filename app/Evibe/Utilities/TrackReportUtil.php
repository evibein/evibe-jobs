<?php

/**
 * Utility file for tracking & reporting module
 *
 * @author Anji <anji@evibe.in>
 * @since  30 Jan 2016
 */

namespace Evibe\Utilities;

use App\Models\TicketBooking;
use App\Models\TrackBookingLive;
use App\Models\TypeCloudPhone;
use App\Models\TypeScenario;

use App\Models\Vendor;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;

class TrackReportUtil
{
	public function fetchTrackListAndExecute($data)
	{
		$startTime = $data['startTime'];
		$endTime = $data['endTime'];
		$dayType = $data['dayType'];
		$currentTime = time();

		// get scenario details
		$scenario = TypeScenario::find(config('evibe.scenario.track'));
		$ttl = $scenario->ttl * 60;
		$expiresAt = date('h:i A', ($currentTime + $ttl)); // format: 04:00 PM
		$expiresAtTime = date('Y-m-d H:i:s', strtotime("+$ttl seconds", $currentTime));

		// get bookings happening in between given time frame
		$bookings = TicketBooking::with('ticket', 'ticket.area', 'vendor')
		                         ->where('ticket_bookings.party_date_time', '>=', $startTime)
		                         ->where('ticket_bookings.party_date_time', '<=', $endTime)
		                         ->where('ticket_bookings.is_advance_paid', 1)
		                         ->get();

		foreach ($bookings as $booking)
		{
			$ticket = $booking->ticket;
			$vendor = $booking->vendor;

			// set track info
			$cloudPhone = $this->getAvailableCloudPhone($vendor->id);
			$phoneId = $cloudPhone['phoneId'];
			$phoneNumber = $cloudPhone['phoneNumber'];

			$trackLive = new TrackBookingLive;
			$trackLive->ticket_booking_id = $booking->id;
			$trackLive->type_scenario_id = config('evibe.scenario.track');
			$trackLive->type_cloud_phone_id = $phoneId;
			$trackLive->expires_at = $expiresAtTime;
			$trackLive->created_at = date('Y-m-d H:i:s');
			$trackLive->updated_at = date('Y-m-d H:i:s');
			$trackLive->vendor_phone = $vendor->phone;

			// send SMS to vendor
			// Ex: Hi Kiran, hope everything is set for Divya's party (Indiranagar, 7259509827) at 07:00 PM today and you / team will reach venue by 05:00 PM. Give missed call to 80880660660 before 08:45 AM to inform Divya.
			$smsTpl = config('evibe.sms_tpl.track.vendor');
			$tplVars = [
				'#field1#'  => $vendor->person,
				'#field2#'  => $ticket->name . "'s",
				'#field3#'  => $ticket->area->name,
				'#field4#'  => $ticket->phone,
				'#field5#'  => date('h:i A', $booking->party_date_time),
				'#daytype#' => $dayType,
				'#field6#'  => date('h:i A', strtotime($booking->reports_at)),
				'#field7#'  => $phoneNumber,
				'#field8#'  => $expiresAt,
				'#field9#'  => $ticket->name
			];
			$smsText = str_replace(array_keys($tplVars), array_values($tplVars), $smsTpl);
			$smsData = ['to' => $vendor->phone, 'text' => $smsText];
			Queue::push('Evibe\Utilities\SendSMS@send', $smsData);

			// save info
			$trackLive->vendor_sms_text = $smsText;
			$trackLive->save();
		}
	}

	public function actOnExpiredLiveMapping($data)
	{
		// @todo: SMS should be sent to post booking handler and email to handler, head (operations)
		$liveMapping = $data['liveMapping'];
		$scenario = $data['scenario'];

		// get vendor by phone
		$vendor = $this->getVendor($liveMapping->vendor_phone);
		if ($vendor === false)
		{
			return null;
		}

		// fetch required data
		$booking = $liveMapping->booking;
		$ticket = $liveMapping->booking->ticket;
		$dayType = "today";

		// determine if party day is today / tomorrow
		if ($booking->party_date_time > strtotime("tomorrow midnight"))
		{
			$dayType = "tomorrow";
		}

		$notifyData = [
			'scenarioId'    => $scenario->id,
			'scenarioName'  => $scenario->name,
			'vendorName'    => $vendor->person,
			'vendorPhone'   => $liveMapping->vendor_phone,
			'customerName'  => $ticket->name,
			'customerPhone' => $ticket->phone,
			'scenarioType'  => $scenario->name,
			'partyDate'     => date('d M h:i A', $booking->party_date_time),
			'partyTime'     => date('h:i A', $booking->party_date_time),
			'dayType'       => $dayType,
			'partyLocation' => $ticket->area->name,
			'subject'       => "[" . strtoupper($scenario->name) . " FAIL] " . $vendor->person . " did not respond to " . $ticket->name . " booking "
		];

		// add extra required fields for email & send SMS
		if ($scenario->id == config('evibe.scenario.report'))
		{
			$notifyData['otp'] = $liveMapping->otp;
			$notifyData['pin'] = $liveMapping->pin;
			$notifyData['cloudPhone'] = $liveMapping->cloudPhone->phone;

			$this->notifyFailToVendorSMS($notifyData);
		}

		$smsText = $this->notifyFailToTeamSMS($notifyData);
		$this->notifyFailToTeamEmail($notifyData);

		return ['smsText' => $smsText];
	}

	public function getAvailableCloudPhone($vendorId)
	{
		$availablePhone = ['phoneId' => -1, 'phoneNumber' => '0000000000'];
		$allPhones = TypeCloudPhone::select('id', 'phone')
		                           ->where('is_for_otp', 0)
		                           ->get()
		                           ->toArray();

		// get all existing cloud phone numbers mapped for this vendor
		$currentMappings = TrackBookingLive::join('ticket_bookings', 'track_booking_live.ticket_booking_id', '=', 'ticket_bookings.id')
		                                   ->where('ticket_bookings.map_id', $vendorId)
		                                   ->pluck('track_booking_live.type_cloud_phone_id')
		                                   ->toArray();

		$allPhoneIds = array_column($allPhones, 'id');
		$currentPhoneIds = array_values($currentMappings);

		if (count($currentMappings) >= count($allPhones))
		{
			// report admin about maximum limit reached (10 cloud phones)
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Max cloud phone limit reached for $vendorId on " . date('d-m-y'),
				'body'    => "All the available could phones for $vendorId have been used and there was a request to assign new cloud phone. Please buy new set of cloud phones ASAP."
			];

			$this->notifyAdmin($data);

			return $availablePhone;
		}

		$availablePhoneIds = array_diff($allPhoneIds, $currentPhoneIds);
		$phoneId = current($availablePhoneIds); // get first element
		$key = array_search($phoneId, array_column($allPhones, 'id'));
		$phoneNumber = null;

		if ($key !== false)
		{
			$availablePhone['phoneId'] = $phoneId;
			$availablePhone['phoneNumber'] = $allPhones[$key]['phone'];
		}
		else
		{
			// report admin about this case
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Could get available cloud phone for $vendorId",
				'body'    => "There was an issue with getting available cloud phone number for $vendorId"
			];

			$this->notifyAdmin($data);
		}

		return $availablePhone;
	}

	public function getVendor($vendorPhone)
	{
		$vendor = Vendor::where('phone', 'LIKE', $vendorPhone)->get();

		if ($vendor->count() == 1)
		{
			$vendor = $vendor->first();
		}
		elseif ($vendor->count() > 1)
		{
			$vendor = false;

			// notify admin - multiple vendors with same phone number exists
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Multiple vendors found with phone number: $vendorPhone",
				'body'    => "There was a missed call from $vendorPhone, and there are multiple vendors matching this number. Please check and fix the issue."
			];
			$this->notifyAdmin($data);
		}
		elseif ($vendor->count() == 0)
		{
			$vendor = false;

			// notify admin - someone else tried reaching our cloud phone number
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => 'Could not find vendor with phone: ' . $vendorPhone,
				'body'    => "There was a missed call from $vendorPhone, and there is no vendor matching this number. Please check and fix the issue."
			];
			$this->notifyAdmin($data);

		}
		else
		{
			// do nothing
		}

		return $vendor;
	}

	public function notifyAdmin($data)
	{
		Mail::send('emails.report.admin.error', $data, function ($message) use ($data)
		{
			$sub = array_key_exists('subject', $data) ? $data['subject'] : "A Jobs error occurred at " . date('d-m-Y H:i');

			$message->from('ping@evibe.in', 'Jobs Error');
			$message->to(config('evibe.contact.admin.email'), config('evibe.contact.admin.name'));
			$message->subject($sub);
		});
	}

	public function notifyFailToTeamSMS($data)
	{
		$scenarioId = $data['scenarioId'];
		$tplVars = [];
		$smsTpl = '';

		if ($scenarioId == config('evibe.scenario.track'))
		{
			// Ex: Hi Abdul, there no response from Uday for Divya’s party tracking (Indiranagar, today, 07:00 PM). Pls call 9203828301 & act immediately.
			$smsTpl = config('evibe.sms_tpl.track.team.fail');
			$tplVars = [
				'#field1#'  => config('evibe.contact.operations.name'),
				'#field2#'  => $data['vendorName'],
				'#field3#'  => $data['customerName'] . "'s",
				'#field4#'  => $data['partyLocation'],
				'#dayType#' => $data['dayType'],
				'#field5#'  => $data['partyTime'],
				'#field6#'  => $data['vendorPhone']
			];
		}

		if ($scenarioId == config('evibe.scenario.report'))
		{
			// Ex: We did not get reporting time for Divya's party (Indiranagar, 7259509827). Party start time: 7 PM. Pls call Uday (9535304100).
			$smsTpl = config('evibe.sms_tpl.report.team.fail');
			$tplVars = [
				'#field1#' => $data['customerName'] . "'s",
				'#field2#' => $data['partyLocation'],
				'#field3#' => $data['customerPhone'],
				'#field4#' => $data['partyTime'],
				'#field5#' => $data['vendorName'],
				'#field6#' => $data['vendorPhone']
			];
		}

		return $this->initiateSMS($smsTpl, $tplVars, config('evibe.contact.operations.phone'));
	}

	public function notifyFailToVendorSMS($data)
	{
		// Ex: Hi Uday, we have not received your reporting time for Divya’s party (Indiranagar). Pls. call 9535304100 to report.
		$smsTpl = config('evibe.sms_tpl.report.vendor.fail');
		$tplVars = [
			'#field1#' => $data['vendorName'],
			'#field2#' => $data['customerName'] . "'s",
			'#field3#' => $data['partyLocation'],
			'#field4#' => config('evibe.contact.operations.phone')
		];

		$this->initiateSMS($smsTpl, $tplVars, $data['vendorPhone']);
	}

	public function notifyFailToTeamEmail($data)
	{
		$emailTpl = 'emails.track.team.fail';
		if ($data['scenarioId'] == config('evibe.scenario.report'))
		{
			$emailTpl = 'emails.report.team.fail';
		}

		$data = array_merge($data, ['name' => config('evibe.contact.operations.name')]);

		Mail::send($emailTpl, $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', $data['scenarioName'] . ' Booking');
			$message->to(config('evibe.contact.operations.email'), config('evibe.contact.operations.name'));
			$message->subject($data['subject']);
		});
	}

	private function initiateSMS($smsTpl, $tplVars, $to)
	{
		$smsText = str_replace(array_keys($tplVars), array_values($tplVars), $smsTpl);
		$smsData = [
			'to'   => $to,
			'text' => $smsText
		];

		Queue::push('Evibe\Utilities\SendSMS@send', $smsData);

		return $smsText;
	}

}