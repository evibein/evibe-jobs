<?php namespace Evibe\Utilities;

use App\Models\Auth\AccessToken;
use App\Models\Auth\AuthClient;
use App\Models\Cake\Cake;
use App\Models\Decor;
use App\Models\Trend;
use App\Models\TypeService;
use App\Models\User;
use App\Models\VendorPackage;
use App\Models\VenueHall;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class EvibeUtil
{

	public function formatPrice($price)
	{
		return $this->moneyFormatIndia($price);
	}

	public function getBookingShortUrl()
	{
		/**
		 * @todo  implement this method
		 */
		return "short_url";
	}

	public function getMappingData($mapTypeId, $mapId)
	{
		$item = [];
		$dashBasePath = config('evibe.dash_url');
		$title = '';
		$url = '';

		switch ($mapTypeId)
		{
			Case config('evibe.ticket.type.packages'):
				$itemObj = VendorPackage::find($mapId);
				$packageType = $this->getPackageType($itemObj->map_type_id);
				$title = $itemObj->name;
				$url = $dashBasePath . 'packages/' . $packageType . 'view/' . $itemObj->id;
				break;

			Case config('evibe.ticket.type.services'):
				$itemObj = TypeService::find($mapId);
				$url = $dashBasePath . 'services/details/' . $itemObj->id;
				$title = $itemObj->name;
				break;

			Case config('evibe.ticket.type.trends'):
				$itemObj = Trend::find($mapId);
				$url = $dashBasePath . 'trends/view/' . $itemObj->id;
				$title = $itemObj->name;
				break;

			Case config('evibe.ticket.type.cakes'):
				$itemObj = Cake::find($mapId);
				$url = $dashBasePath . 'cakes/' . $itemObj->id . '/info';
				$title = $itemObj->title;
				break;

			Case config('evibe.ticket.type.decors'):
				$itemObj = Decor::find($mapId);
				$url = $dashBasePath . 'decors/' . $itemObj->id . '/info';
				$title = $itemObj->name;
				break;

			Case config('evibe.ticket.types.venue_halls');
				$itemObj = VenueHall::with('venue')->find($mapId);
				$url = $dashBasePath . 'venues/hall/' . $itemObj->id;
				$title = $itemObj->name;
				break;
		}

		$item['title'] = $title;
		$item['url'] = $url;

		return $item;
	}

	public function getShortLink($longUrl)
	{
		$shortUrl = $longUrl;

		if ($longUrl && strpos($longUrl, "goo.gl") === false && strpos($longUrl, "bit.ly") === false)
		{
			/* @see: disabling evib.es temporarily and implementing bit.ly
			$url = "http://evib.es/api/v2/action/shorten";
			$method = "POST";
			$accessToken = "";
			$jsonData = [
				'url'           => $longUrl,
				'key'           => config("evibe.evibes.access_token"),
				'custom_ending' => '',
				'is_secret'     => false,
				'response_type' => 'json'
			];

			try
			{
				$client = new Client();
				$res = $client->request($method, $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $jsonData,
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);
				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;

				$this->saveError([
					                 'fullUrl' => request()->fullUrl(),
					                 'message' => 'Error occurred in Base controller while doing guzzle request',
					                 'code'    => 0,
					                 'details' => $res['error']
				                 ]);

				return false;
			}
			 */

			try
			{
				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if(time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);
				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => 'Bitly Url Shortener',
						                                     'method'    => 'GET',
						                                     'message'   => '[BaseCommandHandler.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'exception' => '[BaseCommandHandler.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'trace'     => print_r($response, true),
						                                     'details'   => print_r($response, true)
					                                     ]);
				}

			} catch (\Exception $exception)
			{
				$this->sendAndSaveReport($exception);
			}
		}

		return $shortUrl;
	}

	/**
	 * @param $partnerObj : should be object, cannot be array
	 */
	public function getPartnerCCAddresses($partnerObj, $extraCCEmail = '')
	{
		if (!is_object($partnerObj))
		{
			return [];
		}
		else
		{
			$emails = [];

			if (!empty($extraCCEmail))
			{
				array_push($emails, $extraCCEmail);
			}

			$probableEmails = ['alt_email', 'alt_email_1', 'alt_email_2', 'alt_email_3'];
			foreach ($probableEmails as $probableEmail)
			{
				if ($partnerObj->{$probableEmail})
				{
					array_push($emails, $partnerObj->{$probableEmail});
				}
			}

			return $emails;
		}
	}

	/**
	 * PRIVATE METHODS
	 */
	private function getPackageType($mapTypeId)
	{
		if ($mapTypeId == config('evibe.ticket.type.planners'))
		{
			$url = 'vendor/';
		}
		elseif ($mapTypeId == config('evibe.ticket.type.artists'))
		{
			$url = 'artist/';
		}
		else
		{
			$url = 'venue/';
		}

		return $url;
	}

	private function moneyFormatIndia($num)
	{
		$explrestunits = "";
		$paise = "";

		if (strpos($num, '.'))
		{
			$numArray = explode('.', $num);
			$paise = $numArray[1];
			$num = $numArray[0];
		}

		if (strlen($num) > 3)
		{

			$lastthree = substr($num, strlen($num) - 3, strlen($num));
			$restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestunits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestunits . $lastthree;
		}
		else
		{
			$thecash = $num;
		}

		if ($paise)
		{
			$thecash .= '.' . $paise;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public static function getAccessToken($userId)
	{
		$clientId = config('evibe.api.client_id');
		$user = User::find($userId);

		if (!$userId || !$user)
		{
			Log::info("Enquiry created_for (user_id) is not valid");
			abort('403');
		}
		$token = AccessToken::where('user_id', $userId)->where('auth_client_id', $clientId)->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			$accessToken = self::issueAccessToken($user, $client);
		}
		else
		{
			$accessToken = $token->access_token;
		}

		return $accessToken;
	}

	private static function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken;
		$accessToken->access_token = self::getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private static function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new \Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}

	// gallery table keys for deleting the gallery
	public function getGalleryTables()
	{
		return [
			1 => [
				'table' => 'planner_package',
				'name'  => 'Packages'
			],
			2 => [
				'table' => 'planner_package_gallery',
				'name'  => 'Package Gallery',
			],
			3 => [
				'table' => 'decor',
				'name'  => 'Decors'
			],
			4 => [
				'table' => 'decor_gallery',
				'name'  => 'Decor Gallery'
			],
			5 => [
				'table' => 'type_service',
				'name'  => 'Services'
			],
			6 => [
				'table' => 'service_gallery',
				'name'  => 'Service Gallery'
			],

			7  => [
				'table' => 'trending',
				'name'  => 'Trends'
			],
			8  => [
				'table' => 'trending_gallery',
				'name'  => 'Trend Gallery'
			],
			9  => [
				'table' => 'cake',
				'name'  => 'Cakes'
			],
			10 => [
				'table' => 'cake_gallery',
				'name'  => 'Cake Gallery'
			],
			11 => [
				'table' => 'venue_hall',
				'name'  => 'Venue Halls'
			],
			12 => [
				'table' => 'venue_hall_gallery',
				'name'  => 'Venue Hall Gallery'
			]
		];
	}

}