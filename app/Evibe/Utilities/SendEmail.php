<?php

namespace Evibe\Utilities;

use Aws\Laravel\AwsFacade as AWS;
use Evibe\Facades\EvibeUtilFacade as AppUtil;

class SendEmail
{

	public function mailScrapeSuccess($job, $data)
	{
		$mailData = [
			'to'      => ['anji@evibe.in'],
			'cc'      => [],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Jobs <ping@evibe.in>',
			'sub'     => '[Ratings] Venue ratings report - ' . date('d/m/Y H:i:s'),
			'body'    => $this->getUpdatedRatingsBody($data['logRatings'])
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  31 Dec 2014
	 */
	public function mailBookingReminderToVendor($job, $data)
	{
		$mailData = [
			// 'to' => array($data['to']),
			'to'      => ['swathi@evibe.in'],
			// 'cc' => array(),
			'cc'      => ['anji@evibe.in'],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Evibe <ping@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getBookingsReminderBody($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  2 Jan 2015
	 */
	public function mailSMSErrorToAdmin($job, $data)
	{
		$mailData = [
			'to'      => ['anji@evibe.in'],
			'cc'      => [],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Jobs <ping@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => $this->getSMSErrorBody($data['data'])
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  3 Jan 2015
	 */
	public function mailAskFeedback($job, $data)
	{
		$mailData = [
			'to'      => [$data['email']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => ['enquiry@evibe.in'],
			'from'    => 'Team Evibe <enquiry@evibe.in>',
			'sub'     => $data['emailSubject'],
			'body'    => $this->getAskFeedbackEmailBody($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @since  15 April 2016
	 */

	public function sendFeedbackStatus($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Feedback <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getFeedbackStatusEmailBody($data)
		];

		$this->send($job, $mailData);
	}

	public function sendFollowupReminder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Followup <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getTicketFollowupReminderBody($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send Vendor Followup Reminder
	 *
	 * @author Vikash <vikash@evibe.in>
	 * @Since  2 May 2016.
	 */

	public function sendVendorFollowupReminder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Followup <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getVendorFollowupReminderBody($data)
		];

		$this->send($job, $mailData);
	}

	public function sendVendorFollowupPendingReminder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => ['ping@evibe.in'],
			'from'    => 'Followup <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getVendorPendingFollowupReminderBody($data)
		];

		$this->send($job, $mailData);
	}

	/** event reminder to partner
	 *
	 * @Since 24 August 2016
	 */

	public function mailEventReminderToPartnerWithSingleOrder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getEventReminderToPartnerMarkupWithSingleOrder($data)
		];

		$this->send($job, $mailData);
	}

	public function mailEventReminderToPartnerWithMultipleOrder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getEventReminderToPartnerMarkupWithMultipleOrder($data)
		];

		$this->send($job, $mailData);
	}

	public function mailEventReminderToCustomer($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['cc'],
			'replyTo' => $data['replyTo'],
			'from'    => 'Team Evibe <ops@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->getEventReminderToCustomerMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Harish <harish.annavajjala@evibe.in>
	 * @since  4 Mar 2015
	 */
	public function sendCancellationMailToCustomer($job, $data)
	{
		$mailData = [
			'to'      => [$data['email']],
			'cc'      => $data['ccAddresses'],
			'replyTo' => [$data['replyToAddresses']],
			'from'    => 'Team Evibe <enquiry@evibe.in>',
			'sub'     => '[Evibe.in] Event order cancellation - ' . $data['partyDate'],
			'body'    => self::getCancellationMailToCustomerMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function sendCancellationMailToVendor($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => $data['ccAddresses'],
			'replyTo' => $data['replyToAddresses'],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['subject'],
			'body'    => self::getCancellationMailToVendorMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function sendReminderEmailToOperationHead($job, $data)
	{
		$mailData = [
			'to'      => $data['to']['email'],
			'cc'      => $data['to']['ccAddress'],
			'replyTo' => $data['to']['replyToAddresses'],
			'from'    => 'Event Reminder <ping@evibe.in>',
			'sub'     => $data['to']['sub'],
			'body'    => $this->sendReminderEmailToOperationHeadMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @Since  26 March 2016
	 * Send email if Bd doesn't reply to the venue availability check
	 */
	public function sendVenueAvailabilityCheckReminder($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'replyTo' => $data['replyTo'],
			'cc'      => [],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendVenueAvailabilityCheckReminderMarkup($data)
		];

		$this->send($job, $mailData);
	}

	//@Since  21 June 2016
	public function sendReminderEmailToBusiness($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => [],
			'replyTo' => [],
			'from'    => 'Event Reminder <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendEventReminderToBusiness($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @Since : 12 september 2016
	 * send email to bd team for the party between today 8PM to tomorrow 8 AM
	 */

	public function sendReminderEmailToBusiness8AMTo8PM($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'cc'      => [],
			'replyTo' => [],
			'from'    => 'Event Reminder <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendEventReminderToBusinessAfter8Markup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @Since  17 May 2016
	 * Send email if No action has been taken for auto bookings till 3 hrs before deadline time
	 */
	public function sendAutoBookingReminderEmail($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'replyTo' => $data['replyTo'],
			'cc'      => [],
			'from'    => 'System Alert <system.alert@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendAutoBookingReminderMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function sendServiceAutoBookingReminderEmail($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'replyTo' => $data['replyTo'],
			'cc'      => [],
			'from'    => 'System Alert <system.alert@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendServiceAutoBookingReminderMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Send recommendation followup email
	 *
	 * @Since  13 September 2016
	 * @author vikash <vikash@evibe.in>
	 */

	public function mailRecoFollowupToCustomer($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'replyTo' => $data['replyTo'],
			'cc'      => $data['cc'],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendRecoFollowupMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * @Since 19 September 2016
	 * send followup reminders for videos/photos to customer
	 */

	public function mailFollowupToPartner($job, $data)
	{
		$mailData = [
			'to'      => $data['to'],
			'replyTo' => $data['replyTo'],
			'cc'      => $data['cc'],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => $data['sub'],
			'body'    => $this->sendPartnerFollowupMarkup($data)
		];

		$this->send($job, $mailData);
	}

	public function sendErrorMessageToTeam($job, $data)
	{
		$mailData = [
			'to'      => [config('evibe.contact.tech.group')],
			'replyTo' => [],
			'cc'      => [],
			'from'    => 'Team Evibe <ping@evibe.in>',
			'sub'     => "[JOB ERROR] " . $data['code'] . ' - ' . date('F d Y, h:i A', time()),
			'body'    => $this->sendJobsErrorMarkup($data)
		];

		$this->send($job, $mailData);
	}

	/**
	 * Inner methods
	 */
	private function getUpdatedRatingsBody($logRatings)
	{
		$body = 'Hello Anji, <br><br>';
		$body .= 'Here is a report on fetching venue ratings across the internet. <br><br>';

		foreach ($logRatings as $name => $value)
		{
			$body .= '<b>Ratings for: </b>' . $name;
			$body .= '<br>';

			foreach ($value as $count => $data)
			{
				$body .= '<table style="border-spacing: 0;" border="1">' .
					'<tr>' .
					'<td style="padding: 2px;">' . $data['type'] . '</td>' .
					'<td style="padding: 2px;">' . $data['sname'] . '</td>' .
					'<td style="padding: 2px;">' . $data['old'] . '</td>' .
					'<td style="padding: 2px;">' . $data['new'] . '</td>' .
					'</tr>' .
					'</table>';
			}

			$body .= '<br>';

		}

		return $body;
	}

	private function getSMSErrorBody($data)
	{
		$body = 'Hello Anji, <br><br>';
		$body .= 'An error occured while sending SMS reminder to a vendor. Check details below: <br><br>';

		$body .= '<table style="border-spacing: 0;" border="1">' .
			'<tr>' .
			'<td style="padding: 2px;">Phone number: </td>' .
			'<td style="padding: 2px;">' . $data['to'] . '</td>' .
			'</tr>' .
			'<tr>' .
			'<td style="padding: 2px;">SMS text: </td>' .
			'<td style="padding: 2px;">' . $data['text'] . '</td>' .
			'</tr>' .
			'<tr>' .
			'<td style="padding: 2px;">Error code: </td>' .
			'<td style="padding: 2px;">' . $data['error_code'] . '</td>' .
			'</tr>' .
			'</table>';

		$body .= '<br/>';
		$body .= 'Please resolve the problem at the earliest. <br/><br/>';
		$body .= $this->getEmailFooter() . ' <br/>Admin';

		return $body;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  3 Jan 2015
	 */
	private function getAskFeedbackEmailBody($data)
	{
		$body = '
			<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
				<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Website: </span>
								<span><a target="_blank" href="http://evibe.in">www.evibe.in</a></span>
							</div>
							<div style="padding-top:10px;">Phone: ' . config('evibe.phone') . '</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>
					<div style="padding-top: 10px;">
						<p>Dear ' . $data["name"] . ',</p>
						<p>Thank you for booking your party through Evibe. We hope you had a wonderful party.</p>
						<p>We definitely need your feedback for us to understand your needs better and to improve our services.
							And it would also help many other people who wants to book their party as most of the them depend on these ratings.</p>
							<div style="margin:30px auto;text-align:center;">
								<a target="_blank" href="' . $data["feedbackUrl"] . '"
								style="padding:10px 15px;font-size:20px;color:#fff;background-color:#11a1de;
								border-radius:4px;text-decoration:none;">Click To Submit Feedback</a>
								<div style="margin-top:15px;font-size:13px;color:#11a1de">(takes less than a minute)</div>
							</div> 
						</p>
						<div>
							' . $this->getEmailFooter() . '
							<div style="margin-top: 4px;">Team Evibe</div>
						</div>
					</div>
				</div>
				<div style="padding:0 30px 10px 30px;color:#999;">
					<div style="padding-top:10px;">Like us on Facebook: <a href="http://facebook.com/evibe.in">http://facebook.com/evibe.in</a> and show us your love :-)</div>
				</div>
			</div>
			<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as "not spam" and add senders id to contact list or safe list.</div>
			<div style="padding-top:10px;font-size:10px;color:#333">
				<div>Disclaimer</div>
				<div>The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential or privileged information. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments.</div>
			</div>
		';

		return $body;
	}

	/**
	 * @author Harish <harish.annavajjala@evibe.in>
	 * @since  4 Mar 2015
	 */
	private static function getCancellationMailToCustomerMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: +91 9640204000</div>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['name'] . ",</p>
						<p>We regret to inform you that we had to cancel your party order of  " . $data['partyDate'] . ' because <b>' . $data['reason'] . '</b>';
		$body .= "	<div style=\"padding:30px 0 15px; text-align:center;\">
				<a href=" . $data['pgLink'] . "
            style=\"font-size:20px;text-decoration:none;background-color:#4584EE;color:#FFFFFF;border-color:#4CAE4C;padding:8px 20px;border-radius:4px;\">
				Request New Payment Link
				</a>
				</div>";
		$body .= "<p>In case you have any questions/queries, please feel free to contact us back.</p>";
		$body .= "<p>Phone Number: +91 9640204000<br>E-mail: <u>enquiry@evibe.in</u></p>";
		$body .= "<p>" . self::getEmailFooter() . "<br>Team Evibe</p>";

		return $body;
	}

	public function getCancellationMailToVendorMarkup($data)
	{
		$body = "
			<div style='background-color:#F5F5F5;padding:10px;max-width:700px;'>
				<div style='padding: 20px 30px 10px 30px;background-color: #FFFFFF;'>
					<div>
						<div style='float:left;'>
							<div>
								<a href='http://evibe.in'>
									<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
								</a>
								<div style='padding-top:10px;'>Phone: +91 9640204000</div>
							</div>
						</div>
						<div style='clear:both;'></div>
					</div>
					<hr>
					<div style='padding-top: 10px;'>
						<p>Dear " . $data['vendorName'] . ",</p>
						<p>We regret to inform you that we had to cancelled the order for " . $data['partyDate'] . '. Please check below for more details.
						<br><br><table border="1" style="border-collapse:collapse;margin-top:15px">
							<tr>
								<td style="padding:8px"><b>Customer Name</b></td>
								<td style="padding:8px">' . $data['customerName'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Party Date</b></td>
								<td style="padding:8px">' . $data['partyDate'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Party Location</b></td>
								<td style="padding:8px">' . $data['location'] . '</td>
							</tr>
							<tr>
								<td style="padding:8px"><b>Reason</b></td>
								<td style="padding:8px">' . $data['reason'] . '</td>
							</tr>
						</table>';

		$body .= "<p>In case you have any queries, please feel free to contact us back.</p>";
		$body .= "<p>Phone Number: +91 9640204000<br>E-mail: <u>ping@evibe.in</u></p>";
		$body .= "<p>" . $this->getPartnerSalutationMarkup() . "</p>";

		return $body;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  31 Dec 2014
	 */
	private function getBookingsReminderBody($data)
	{
		$header = '
			<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
				<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">
								<span>Email: </span>
								<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>
		';

		$footer = '
				<div style="padding:0 30px 10px 30px;">
						<p>
							<div style="text-decoration:underline;color:#999;"><b>Note:</b></div>
							<div style="color:#999;">
								<ul>
									<li>In case any of the details mentioned are incorrect, please call us immediately.</li>
									<li>You need to update Evibe, if there are any last minutes changes.</li>
									<li>Please reach the venue on time.</li>
								</ul>
							</div>
						</p>
					</div>
				</div>
				<div style="padding-top:10px;font-size:12px;color:#999">
					<span>If you are receiving the message in Spam or Junk folder, please mark it as "not spam" and add senders id to contact list or safe list.</span>
				</div>';
		$body = '';

		if (count($data['bookings']) > 1)
		{
			// multiple bookings - show limited date for each booking
			// single booking - show majority of the details.
			$body = '
					<div style="padding-top: 10px;">
						<p>Namaste ' . $data["name"] . ',</p>
						<p>This is a reminder for your ' . count($data["bookings"]) . ' events tomorrow. You can check the important booking details below.
						For complete details please login to <a href="http://pro.evibe.in" target="_blank">Evibe Pro</a>.</p>
						<div style="padding-top: 10px;">
							<div style="color:#EF3E75;"><b>Bookings Information</b></div>
							<div style="padding-top:10px;">
								<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
									<thead>
										<tr>
											<th style="border:1px solid #AAA;padding:4px;"><b>Event Date</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Area</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Package</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Booking Amount</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Balance Amount</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Customer Name</b></th>
											<th style="border:1px solid #AAA;padding:4px;"><b>Phone Number</b></th>
										</tr>
									</thead>
									<tbody>';

			foreach ($data['bookings'] as $booking)
			{
				$body .= '<tr>
											<td style="border:1px solid #AAA;padding:4px;">' . $booking["readable_date_time"] . '</td>
											<td style="border:1px solid #AAA;padding:4px;">' . $booking["area"] . '</td>';

				if ($booking["package_name"])
				{
					$body .= '<td style="border:1px solid #AAA;padding:4px;">' . $booking["package_name"];
					if ($booking["has_customization"])
					{
						$body .= " <b>(customized)</b>";
					}
					$body .= '</td>';
				}
				else
				{
					$body .= '<td style="border:1px solid #AAA;padding:4px;"> -- </td>';
				}

				$body .= '
											<td style="border:1px solid #AAA;padding:4px;"> Rs. ' . AppUtil::formatPrice($booking["booking_amount"]) . '</td>
											<td style="border:1px solid #AAA;padding:4px;"> Rs. ' . AppUtil::formatPrice($booking["balance_amount"]) . '</td>
											<td style="border:1px solid #AAA;padding:4px;">' . $booking["name"] . '</td>
											<td style="border:1px solid #AAA;padding:4px;">' . $booking["phone"] . '</td>
										</tr>';
			}

			$body .= '</tbody>
								</table>
							</div>
						</div>';
		}
		else
		{
			$booking = $data['bookings'][0];

			// single booking - show majority of the details.
			$body = '
					<div style="padding-top: 10px;">
						<p>Namaste ' . $data["name"] . ',</p>
						<p>This is a reminder for your event tomorrow. You can check the important booking details below. 
						For complete details please login to <a href="http://pro.evibe.in" target="_blank">Evibe Pro</a>.</p>
						<div style="padding-top: 10px;">
							<div style="color:#EF3E75;"><b>Booking Information</b></div>
							<div style="padding-top:10px;">
								<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Event Date</b></td>
										<td style="border:1px solid #AAA;padding:4px;">' . $booking["readable_date_time"] . '</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Area</b></td>
										<td style="border:1px solid #AAA;padding:4px;">' . $booking["area"] . '</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Package</b></td>
										<td style="border:1px solid #AAA;padding:4px;">';

			$body .= '
										</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Booking Amount</b></td>
										<td style="border:1px solid #AAA;padding:4px;"> Rs. ' . AppUtil::formatPrice($booking["booking_amount"]) . '</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Balance Amount</b></td>
										<td style="border:1px solid #AAA;padding:4px;"> Rs. ' . AppUtil::formatPrice($booking["balance_amount"]) . '</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Customer Name</b></td>
										<td style="border:1px solid #AAA;padding:4px;">' . $booking["name"] . '</td>
									</tr>
									<tr>
										<td style="border:1px solid #AAA;padding:4px;"><b>Phone Number</b></td>
										<td style="border:1px solid #AAA;padding:4px;">' . $booking["phone"] . '</td>
									</tr>
								</table>
							</div>
						</div>';
		}

		$body .= '
							<div style="padding-top:10px;">
								<p>We wish you good luck for the event(s).</p>
							</div>
							<div style="padding-top:10px;">' . $this->getPartnerSalutationMarkup() . '</div>
						</div>
					</div>
		';

		return $header . $body . $footer;
	}

	// Reminder email of today's booking to Operation Head
	public function sendReminderEmailToOperationHeadMarkup($data)
	{
		$wrapper = '<div style="padding: 20px; border:22px solid #f5f5f5">';
		$salutation = 'Namaste ' . $data['to']['name'] . ', <br/><br/>';
		$greetings = $data['to']['greetings'] . '<br/><br/>';
		$body = '';

		foreach ($data['list'] as $key => $booking)
		{
			$body .= '<table border="1"  style="border-collapse:collapse; margin-top: 15px; margin-bottom: 20px;">
						<tr><td colspan="3" style="padding:8px; background-color:#FFFF33; text-align: center;"><b>Concept Type: ' . $booking['conceptType'] . '</b></td></tr>
						<tr>
							<td style="padding:8px">
								<div><b>Vendor</b></div>
								<div>' . $booking['vendorName'] . ' (' . $booking['vendorPhone'] . ')</div>';

			if ($booking['vendorAltPhone'])
			{
				$body .= '<div>Alt Phone: ' . $booking['vendorAltPhone'] . '</div>';
			}

			$body .= '</td>
							<td style="padding:8px">
								<div><b>Date & Location</b></div>
								<div>' . $booking['eventDateTime'] . ' - ' . $booking['areaName'] . '</div>
							</td>
							<td style="text-align: center;padding:8px">
								<div><b> Balance Amount</b></div>
								<div>' . $booking['balanceAmount'] . '</div>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding:8px">
								<div><b>Booking Info</b></div>
								<div>' . $booking['bookingInfo'] . '</div>
							</td>
						</tr>
						<tr>
							<td colspan="1" style="padding:8px">
								<div><b>Customer</b></div>
								<div>' . $booking['customerName'] . ' (' . $booking['customerPhone'] . ')</div>';

			if ($booking['customerAltPhone'])
			{
				$body .= '<div>Alt Phone: ' . $booking['customerAltPhone'] . '</div>';
			}

			$body .= '</td>
						<td colspan="2" style="padding:8px">
							<div><b>Address</b></div>
							<div>' . $booking['venueAddress'] . '</div>
							<div><u>Landmark: </u>' . $booking['venueLandmark'] . '</div>
						</td>';

			$body .= '</tr>
					</table>';

		}
		$footer = '</div>';

		return $wrapper . $salutation . $greetings . $body . $footer;
	}

	private function sendVenueAvailabilityCheckReminderMarkup($data)
	{
		$body = '';
		$markup = 'Hi, <br><br>';
		$markup .= "Availability check enquiry for <b>" . $data['venue'] . " (" . $data['hall'] . ")</b> is waiting for your response.<br>";
		$markup .= 'Here are some details about the availability Check.<br>';

		$body .= '<table border="1"  style="margin-top: 15px; border-collapse: collapse">
						<tr>
							<td><b>Venue Manager</b>
								<div>' . $data['person'] . ' (' . $data['personContact'] . ')</div>';

		if ($data['personAltContact'])
		{
			$body .= '<div>Alt Phone: ' . $data['personAltContact'] . '</div>';
		}

		$body .= '</td>
							<td>
								<div><b>Party date</b></div>
								<div>' . $data['partyDate'] . '</div>
							</td>
							<td>
								<div><b>Party area</b></div>
								<div>' . $data['areaName'] . '</div>
							</td>
						</tr>
						<tr>
							<td style="text-align: center">
								<div><b> Budget</b></div>
								<div>' . AppUtil::formatPrice($data['budget']) . '</div>
							</td>
							<td>
								<div><b>Min guests</b></div>
								<div>' . $data['minGuests'] . '</div>
							</td>
							<td>
								<div><b>Price / Person</b></div>
								<div>' . $data['pricePerPerson'] . '</div>
							</td>
						</tr>';
		if ($data['menu'])
		{
			$body .= '<td colspan="3" >
								<div><b>Menu</b></div>
								<div>' . $data['menu'] . '</div>
							</td>';
		}
		if ($data['remarks'])
		{
			$body .= '<td colspan="3" >
								<div><b>CRM Remarks</b></div>
								<div>' . $data['remarks'] . '</div>
							</td>';
		}
		$body .= '</tr>
					</table><br>';
		$body .= '<div>Please check the availability of venue and let us know as soon as possible.</div><br><br>';

		$signature = '<div style="font-size: 14px;font-weight:400">' . $this->getEmailFooter() . '</div>
						<div style="font-size: 14px;font-weight:300">CRM Team (Evibe)</div>';

		return $markup . $body . $signature;
	}

	public function getFeedbackStatusEmailBody($data)
	{
		$markup = '<div style="border: 21px solid #f5f5f5;padding:20px 10px">';

		$markup .= 'Hi,<br><br> Here are the status of today\'s feedback.';
		$markup .= '<table border="1" style="border-collapse: collapse;border:1px solid #000000;margin-top:15px">
						<tr>
							<th style="padding: 5px">Customer Name</th>
							<th style="padding: 5px">Party Date</th>
							<th style="padding: 5px">Feedback Received</th>
						</tr>';

		foreach ($data['tickets'] as $ticket)
		{
			$markup .= '<tr>
							<td style="padding:5px">' . $ticket['name'] . '</td>
							<td style="padding:5px">' . $ticket['partyDate'] . '</td>';
			if ($ticket['isReply'] == 0)
			{
				$markup .= '<td style="padding: 5px;"><b style="color:#ff0000">No</b> </td>';
			}
			elseif ($ticket['isReply'] == 1)
			{
				$markup .= '<td style="padding: 5px"><b style="color:#2ECC71">YES</b> </td>';
			}
			$markup .= '</tr>';
		}

		$markup .= '</table>';
		$markup .= '<br>' . $this->getEmailFooter() . '<br>Team Evibe</div>';

		return $markup;
	}

	public function getTicketFollowupReminderBody($data)
	{
		$markup = '<div style="border:20px solid #f5f5f5;padding: 15px ">';
		$markup .= '<div>Hi ' . $data['handlerName'] . '<br><br>';
		$markup .= 'You have a followup in next 10 minutes.<br>Please go through below details.</div>';

		$comment = $data['comment'] ? $data['comment'] : '<i>Not Available</i>';
		$markup .= '<table border="1" style="margin-top:15px; border-collapse: collapse">
						<tr>
							<td style="padding: 8px"><b>Customer Name</b></td>
							<td style="padding: 8px"><b>Followup Time</b></td>
							<td style="padding: 8px"><b>Followup Comments</b></td>
						</tr>
						<tr>
							<td style="padding: 8px">' . $data['customerName'] . '</td>
							<td style="padding: 8px">' . $data['followupTime'] . '</td>
							<td style="padding: 8px">' . $comment . '</td>
						</tr>
					</table>';

		$markup .= '<div style="margin-top:10px;"><a style="color:#ff1100" href=' . $data['link'] . '>Click Here</a> to mark this followup as complete.</div>';
		$markup .= '<div style="margin-top:15px">' . $this->getEmailFooter() . '<br>Team Evibe</div></div>';

		return $markup;
	}

	public function getVendorFollowupReminderBody($data)
	{
		$markup = '<div style="border:20px solid #f5f5f5;padding: 15px ">';
		$markup .= '<div>Hi ' . $data['handlerName'] . '<br><br>';
		$markup .= 'You have a partner to followup in next 10 minutes.<br>Please go through below details.</div>';

		$comment = $data['comment'] ? $data['comment'] : '<i>Not Available</i>';
		$markup .= '<table border="1" style="margin-top:15px; border-collapse: collapse">
						<tr>
							<td style="padding: 8px"><b>Partner Name</b></td>
							<td style="padding: 8px"><b>Contact Person</b></td>
							<td style="padding: 8px"><b>Contact</b></td>
							<td style="padding: 8px"><b>Followup Time</b></td>
							<td style="padding: 8px"><b>Followup Comments</b></td>
						</tr>
						<tr>
							<td style="padding: 8px">' . $data['company'] . '</td>
							<td style="padding: 8px">' . $data['person'] . '</td>
							<td style="padding: 8px">' . $data['contact'] . '</td>
							<td style="padding: 8px">' . $data['followupTime'] . '</td>
							<td style="padding: 8px">' . $comment . '</td>
						</tr>
					</table>';

		$markup .= '<div style="margin-top:10px;"><a style="color:#ff1100" href=' . $data['link'] . '>Click Here</a> to mark this followup as complete.</div>';
		$markup .= '<div style="margin-top:15px">' . $this->getEmailFooter() . '<br>Team Evibe</div></div>';

		return $markup;
	}

	public function getVendorPendingFollowupReminderBody($data)
	{
		$markup = '<div style="border:20px solid #f5f5f5;padding: 15px ">';
		$markup .= '<div>Hi,<br> <br>BD Team<br>Here are the list of pending followups .</div>';

		if ($data['pendingFollowups'])
		{
			$markup .= '<div  style="margin-top:15px"><b>Pending Followup</b></div>';
			$markup .= '<table border="1" style="border-collapse: collapse;margin-top:5px">
						<tr>
							<td style="padding:5px;"><b>Company</b></td>
							<td style="padding:5px;"><b>Person</b></td>
							<td style="padding:5px;"><b>Followup Date</b></td>
							<td style="padding:5px;" width="40%"><b>Followup Comments</b></td>
							<td style="padding:5px;"><b>Handler</b></td>
							<td style="padding:5px;"><b>Action</b></td>
						</tr>';
			foreach ($data['pendingFollowups'] as $pendingFollowup)
			{
				$comment = $pendingFollowup['comment'] ? $pendingFollowup['comment'] : '<i>Not Available</i>';
				$markup .= '<tr>
								<td style="padding:5px;"><span style="color:#ff1111">' . $pendingFollowup['name'] . '</span></td>
								<td style="padding:5px;"><span style="color:#ff1111">' . $pendingFollowup['person'] . '</span></td>
								<td style="padding:5px;"><span style="color:#ff1111">' . $pendingFollowup['followupDate'] . '</span></td>
								<td style="padding:5px;"><span style="color:#ff1111">' . $comment . '</span></td>
								<td style="padding:5px;"><span style="color:#ff1111">' . $pendingFollowup['handler'] . '</span></td>
								<td style="padding:5px;"><a href=' . $pendingFollowup['link'] . ' style="color:#1DD11D"><b>Mark done</b></a></td>
							</tr>';
			}
			$markup .= '</table>';
		}

		$markup .= '<div style="margin-top:15px">' . $this->getEmailFooter() . '<br>Team Evibe</div></div>';

		return $markup;
	}

	private function sendServiceAutoBookingReminderMarkup($data)
	{
		$markup = '<div style="margin:15px; padding:15px; border:20px solid #f5f5f5">';

		$markup .= '<div style="font-size: 15px">Hi Business Team,
						<div style="margin-top:2px">Here is the list of service auto bookings which needs to be checked today.</div>
					</div>';

		$markup .= '<div style="background:#ff8f8f;color:#ffffff;padding:10px;margin:15px auto;border-width:6px;font-size:15px;">
                         <b><u>Note</u></b> :	All auto bookings needs to be confirm / reject within its deadline time
				     </div>';

		foreach ($data['autoBookingData'] as $autoBooking)
		{
			$markup .= '<table border="1" style="border-collapse: collapse; margin-bottom:25px;" width="100%">
						<tr>
							<td colspan="4" style="padding:8px">
								<a href=' . $autoBooking['url'] . ' style="font-size:16px">' . $autoBooking['title'] . '</a>
                             </td>
						</tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Customer Name </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['customerName'] . '</td>

							<td style="padding:8px;width:20%">
								<b> Advance amount </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['advanceAmount'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Customer Phone </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['customerPhone'] . '</td>

							<td style="padding:8px;width:20%">
								<b> Booking Amount </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['bookingAmount'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Party Date </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['checkIn'] . '</td>

						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Booking date </b>
							</td>
							<td style="padding:8px;width:10%">' . $autoBooking['bookingDate'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Response deadline date </b>
							</td>
							<td style="padding:8px;width:10%;"><span style="color:#ff0000">' . $autoBooking['deadlineDate'] . '</span></td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Guest Count </b>
							</td>
							<td style="padding:8px;width:10%">' . $autoBooking['guests'] . '</td>
						<tr>
					</table>';
		}

		$markup .= '<div>Please check the availability of the planners and finalize as soon as possible for this service auto booking and revert back to CRM Team with the response.</div><br>';
		$markup .= '<div>' . $this->getEmailFooter() . '<br> Team Evibe</div></div>';

		return $markup;
	}

	private function sendAutoBookingReminderMarkup($data)
	{
		$markup = '<div style="margin:15px; padding:15px; border:20px solid #f5f5f5">';

		$markup .= '<div style="font-size: 15px">Hi Business Team,
						<div style="margin-top:2px">Here is the list of auto bookings which needs to be check today.</div>
					</div>';

		$markup .= '<div style="background:#ff8f8f;color:#ffffff;padding:10px;margin:15px auto;border-width:6px;font-size:15px;">
                         <b><u>Note</u></b> :	All auto bookings needs to be confirm / reject within its deadline time
				     </div>';

		foreach ($data['autoBookingData'] as $autoBooking)
		{
			$providerAltPhone = $autoBooking['provider']['altPhone'] ? $autoBooking['provider']['altPhone'] : '-N/A-';
			$markup .= '<table border="1" style="border-collapse: collapse; margin-bottom:25px;" width="100%">
						<tr>
							<td colspan="4" style="padding:8px">
								<a href=' . $autoBooking['url'] . ' style="font-size:16px">' . $autoBooking['title'] . '</a>
                             </td>
						</tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Customer Name </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['customerName'] . '</td>

							<td style="padding:8px;width:20%">
								<b> Advance amount </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['advanceAmount'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Customer Phone </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['customerPhone'] . '</td>

							<td style="padding:8px;width:20%">
								<b> Booking Amount </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['bookingAmount'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Party Date </b>
							</td>
							<td style="padding:8px;width:22%">' . $autoBooking['checkIn'] . '</td>

							<td style="padding:8px;width:20%">
								<b> Provider Name </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['provider']['name'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Booking date </b>
							</td>
							<td style="padding:8px;width:10%">' . $autoBooking['bookingDate'] . '</td>
							<td style="padding:8px;width:20%">
								<b> Contact Person </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['provider']['person'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Response deadline date </b>
							</td>
							<td style="padding:8px;width:10%;"><span style="color:#ff0000">' . $autoBooking['deadlineDate'] . '</span></td>
							<td style="padding:8px;width:20%">
								<b> Provider phone </b>
							</td>
							<td style="padding:8px;width:30%">' . $autoBooking['provider']['phone'] . '</td>
						<tr>
						<tr>
							<td style="padding:8px;width:20%">
								<b> Guest Count </b>
							</td>
							<td style="padding:8px;width:10%">' . $autoBooking['guests'] . '</td>
							<td style="padding:8px;width:20%">
								<b> Provider Alt phone </b>
							</td>
							<td style="padding:8px;width:30%">' . $providerAltPhone . '</td>
						<tr>
					</table>';
		}

		$markup .= '<div>Please check the availability as soon as possible and revert back to CRM Team with the response.</div><br>';
		$markup .= '<div>' . $this->getEmailFooter() . '<br> Team Evibe</div></div>';

		return $markup;
	}

	private function sendEventReminderToBusiness($data)
	{
		$markup = '<div style="border:20px solid #f5f5f5;padding: 20px ">';
		$markup .= '<div style="font-size:15px">Hi Business Team,<div style="margin-top:8px">' . $data['name'] . '\'s party is going to be start at ' . $data['partyTime'] . ' today. Please check the below details to followup.</div></div>';
		$markup .= '<div style="margin-top:15px; margin-bottom: 5px; font-size:16px"><u>Customer Details</u></div>';

		$markup .= '<table border="1" style="border-collapse: collapse;margin-top:10px"  width="80%">
						<tr>
							<td style="padding: 8px"><b>Customer Name</b></td>
							<td style="padding: 8px"><b>Customer Phone</b></td>
							<td style="padding: 8px"><b>Location</b></td>
						</tr>
						<tr>
							<td style="padding: 8px"><a href=' . $data['ticketLink'] . '>' . $data['name'] . '</a></td>
							<td style="padding: 8px">' . $data['phone'] . '</td>
							<td style="padding: 8px">' . $data['location'] . '</td>
						</tr>
					</table>';

		if (count($data['bookings']) > 0)
		{
			$markup .= '<div style="margin:10px 0"> <div style="margin-top:15px; margin-bottom: 5px; font-size:16px"><u>Vendors Details</u></div>';
			$markup .= '<table border="1" style="border-collapse: collapse;margin-bottom: 10px" width="100%">
							<tr>
								<td style="padding: 8px" width="25%"><b>Contact</b></td>
								<td style="padding: 8px"><b>Booking Info</b></td>
							</tr>';

			foreach ($data['bookings'] as $booking)
			{
				$altPhone = $booking['vendorAltPhone'] ? $booking['vendorAltPhone'] : '';
				$markup .= '<tr><td colspan="3" style="padding:8px; background-color:#FFFF33; text-align: center;"><b>Concept Type: ' . $booking['conceptName'] . '</b></td></tr>';
				$markup .= '<tr>
								<td style="padding: 8px;">' . $booking['vendorPerson'] . ' (' . $booking['vendorPhone'];
				if ($altPhone)
				{
					$markup .= ',' . $booking['vendorAltPhone'];
				}

				$markup .= ')</td>
								<td style="padding: 8px;">' . $booking['info'] . '</td>
							</tr>';

			}

			$markup .= '</table></div>';

		}

		$markup .= $this->getEmailFooter() . '<br>
					Team Evibe.in</div>';

		return $markup;
	}

	public function sendEventReminderToBusinessAfter8Markup($data)
	{
		$markup = '<div style="border:20px solid #f4f4f4;padding: 20px ">';
		$markup .= '<p>Hi Business Team,</p><p>Here is the list of all events going to happen between today 8 PM to tomorrow 8 AM.</p>';

		foreach ($data['allData'] as $allData)
		{
			$markup .= '<table border="1" style="border-collapse: collapse; margin-top:25px; padding:15px;" width="100%">
								<tr>
									<td colspan="4" style="text-align: center; background-color: #404040; color:#ffffff; padding:7px;">
										 <div style="text-align: center">
											Customer Information
										</div>
									</td>
								</tr>
								<tr>
									<td style="padding:8px"><b>Name</b></td>
									<td style="padding:8px"><b>Phone</b></td>
									<td style="padding:8px"><b>Location</b></td>
									<td style="padding:8px"><b>Party Date</b></td>
								</tr>
								<tr>
									<td style="padding:8px"><a href="' . $allData['ticket']['dashUrl'] . '">' . $allData['ticket']['name'] . '</a></td>
									<td style="padding:8px">' . $allData['ticket']['phone'] . '</td>
									<td style="padding:8px">' . $allData['ticket']['location'] . '</td>
									<td style="padding:8px">' . $allData['ticket']['partyDate'] . '</td>
								</tr>
								<td colspan="4" style="text-align: center; background-color: #404040; color:#ffffff; padding:7px">
										 <div style="text-align: center">
											Partner(s) Information
										</div>
									</td>
								</tr>
								<tr>
									<td style="padding:8px"><b>Contact</b></td>
									<td colspan="3" style="padding:8px"><b>Booking Information</b></td>
								</tr>';

			foreach ($allData['bookings'] as $booking)
			{
				$altPhone = $booking['altPhone'] ? ', ' . $booking['altPhone'] : '';
				$markup .= '<tr><td colspan="3" style="padding:8px; background-color:#FFFF33; text-align: center;"><b>Concept Type: ' . $booking['conceptName'] . '</b></td></tr>
							<tr>
							<td style="padding:8px">
								<div>' . $booking['person'] . ', ' . $booking['name'] . '</div>
								<div>(' . $booking['phone'] . $altPhone . ')</div>
							</td>
							<td colspan="3" style="padding:8px">' . $booking['info'] . '</td>
						  </tr>';
			}

			$markup .= '</table>';
		}
		$markup .= '<div style="margin-top:10px">' . $this->getEmailFooter() . '</div>
					Team Evibe.in</div>';

		return $markup;
	}

	public function getEventReminderToPartnerMarkupWithSingleOrder($data)
	{
		$isVenue = $data['isVenue'] == 1 ? true : false;
		$header = '
			<div style="background-color:#F5F5F5;padding:20px;max-width:700px;font-size:14px;line-height: 20px;">
				<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">
								<span>Email: </span>
								<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>';
		$body = "<div style='padding:15px;'> Dear " . $data['partnerName'] . ",<p>Here’s is a gentle reminder for " . $data['customerName'] . "'s party happening tomorrow " . $data['partyDate'] . ', ' . $data['partyTime'];
		if (!$isVenue)
		{
			$body .= " at " . $data['partyLocation'];
		}
		$body .= ". We hope everything is on track. Please click on the link below to review your order details.";

		$body .= "<div style='margin:40px 0; text-align:center'>
					<a href='" . $data['orderLink'] . "' style='text-decoration:none; background-color:#4d90fe;border:1px solid #3079ed;padding:8px 10px;color:#ffffff;border-radius:2px;text-transform:uppercase'>
					Review Order Details </a>
				 </div>";

		$body .= "We wish you all the very best for the event and in case of any difficulties, please feel free to get in touch with us by replying to this email (or) call us " . config('evibe.phone_plain') . " (ext. 4).";

		$footer = '<div style="margin-top:10px;">' . $this->getPartnerSalutationMarkup() . '</div>';
		$footer .= '</div></div>';

		return $header . $body . $footer;
	}

	public function getEventReminderToPartnerMarkupWithMultipleOrder($data)
	{
		$header = '
			<div style="background-color:#F5F5F5;padding:20px;max-width:700px;font-size:14px;line-height: 20px;">
				<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">
								<span>Email: </span>
								<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>';
		$body = "<div style='padding:15px;'> Dear " . $data['partnerName'] . ",<p>This is to inform you that you have " . $data['count'] . " party orders lined up for tomorrow (" . $data['partyDate'] . "). ";

		$body .= "We hope everything is on track for all the events. Please click on the link below to review all the party order details.";

		$body .= "<div style='margin:40px 0; text-align:center'>
					<a href='" . $data['orderLink'] . "' style='text-decoration:none; background-color:#4d90fe;border:1px solid #3079ed;padding:8px 10px;color:#ffffff;border-radius:2px;text-transform:uppercase'>
					Review Orders Details </a>
				 </div>";

		$body .= "We wish you all the very best for the event and in case of any difficulties, please feel free to get in touch with us by replying to this email (or) call us " . config('evibe.phone_plain') . " (ext. 4).";

		$footer = '<div style="margin-top:10px;">' . $this->getPartnerSalutationMarkup() . '</div>';
		$footer .= '</div></div>';

		return $header . $body . $footer;
	}

	public function getEventReminderToCustomerMarkup($data)
	{
		$header = '
			<div style="background-color:#F5F5F5;padding:20px;max-width:700px;font-size:14px;line-height: 20px;">
				<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">
								<span>Email: </span>
								<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>';
		$body = "<div style='padding:20px;'>Namaste " . $data['name'] . ", <p>We are all set for your party on " . $data['partyDateTime'] . " at " . $data['partyLocation'] .
			". Click on the link below for complete order details and all the provider(s) contact information.</p>";

		$body .= "<div style='margin:40px 0; text-align:center'>
					<a href='" . $data['od_link'] . "' style='text-decoration:none; background-color:#4d90fe;border:1px solid #3079ed;padding:8px 10px;color:#ffffff;border-radius:2px;text-transform:uppercase'>
					Click to see order & provider(s) details
				    </a>
				 </div>";

		$body .= "You have already paid <b>Rs. " . $data['totalAdvanceAmount'] . "</b> as advance for your party. Kindly carry <b>Rs. " . $data['totalBalanceAmount'] . "</b>
				  for total balance settlement to all your providers(s). You can check each provider’s balance amount in the link above.";

		$body .= "<div style='margin-top:15px'> <b style='color:#ed3e72;text-transform: uppercase'>Tips to make your party a grand success</b>
					<ul style='list-style-type: bullet; padding:5px; margin-top:3px'>
						<li style='padding:5px 0'>
							Call your provider(s) at least one day in advance for any last minute details that you do not want miss out.
						</li>
						<li style='padding:5px 0'>
						    Though we shared the full venue address with map location, we suggest to have one person from your side to help provide(s) reach the venue and settle the balance in cash.
						 </li>
					</ul></div></div>";

		$footer = "<div> Should you have any queries, please reply to this email (or) call us on +91 9640204000 (10:00 AM - 10:00 PM). We will respond at the earliest.</div>";
		$footer .= '<div style="margin-top:10px">' . $this->getEmailFooter() . '</div><div>Team Evibe.in</div>';
		$footer .= '</div></div>';

		return $header . $body . $footer;
	}

	private function sendRecoFollowupMarkup($data)
	{
		$markup = '<div style="background-color:#F5F5F5;padding:20px;max-width:700px;font-size:14px;line-height: 20px;">
					<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="http://evibe.in">
									<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Phone: </span>
								<span>' . config('evibe.phone') . '</span>
							</div>
							<div style="padding-top:10px;">
								<span>Email: </span>
								<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>';
		$markup .= '<p>Namaste ' . $data['name'] . ',</p>
					<p>Here’s a gentle reminder for you to check the best recommendations for your upcoming party on ' . $data['partyDate'] . '.
					Please click on the link below to view all the options and submit your choices. We really do not want you to miss a memorable party :).
					</p>';
		$markup .= "<div style='text-align:center;margin:35px auto;'>
									<a href = " . $data['recommendationUrl'] . " style=' text-decoration:none; border-radius: 4px;text-align: center; border:0;font-weight:500; background-color:#fa4052;color:#fffff1;
                                         font-size:18px;padding:10px;'>Click To See Recommendations </a>
								</div>";
		$markup .= '<p>
					If you did not like our recommendations (or) have any questions,
					please reply to this email or reach us on ' . config('evibe.phone') . ' (10:00 AM - 10:00 PM). We will respond to you at the earliest.
					</p>';

		$markup .= "<div>" . $this->getEmailFooter() . "</div>
					<div>" . $data['handler'] . "</div>";

		$markup .= file_get_contents(base_path('resources/views/emails/customer/next_steps.html'));
		$markup .= "</div></div>";

		return $markup;
	}

	public function sendPartnerFollowupMarkup($data)
	{
		$markup = '<div style="background-color:#ddd;padding:20px;max-width:700px;line-height: 20px;">
						<div style="padding: 20px 30px 20px 30px;background-color: #FFFFFF;">
						<div>
							<div style="float: left;">
								<div>
									<a href="http://evibe.in">
										<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
									</a>
								</div>
							</div>
							<div style="float:right;">
								<div>
									<span>Phone: </span>
									<span>' . config('evibe.phone') . '</span>
								</div>
								<div style="padding-top:10px;">
									<span>Email: </span>
									<span><a href="mailto:ping@evibe.in" target="_blank">ping@evibe.in</a></span>
								</div>
							</div>
							<div style="clear:both;"></div>
					</div>
					<hr>';
		$markup .= "<p>Hi " . $data['partnerName'] . ", </p>
					<p>
						We hope all the deliverables are on track for " . $data['customerName'] . "’s party that happened on " . $data['partyDate'];

		if ($data['partyLocation'])
		{
			$markup .= " at " . $data['partyLocation'];
		}

		$markup .= ". Please share the courier tracking code along with a picture of the package to this email / WhatsApp to our Evibe Quality check group.
						This will help you in case of any issue that may come :). If you have already submitted the details, please ignore this email.
					</p>
					<p>
					If you have any trouble with the deliverables / need help from our side, please reply to this email / call us on " . config('evibe.phone') . " (ext: 4). We will respond to you at the earliest.
					</p>";

		$markup .= $this->getPartnerSalutationMarkup();

		$markup .= "</div></div>";

		return $markup;
	}

	private function sendJobsErrorMarkup($data)
	{
		$markup = '<div style="border:20px solid #f5f5f5; padding:15px;">
					Hi Team,<br><br>
					Please check the error and fix it immediately.
						<table border="1" style="border-collapse: collapse; padding:10px;margin-top:15px" width="100%">
							<tr>
								<td width=10% style="padding: 8px"><b>Code</b></td>
								<td>' . $data['code'] . '</td>
							</tr>
							<tr>
								<td width=10% style="padding: 8px"><b>Method</b></td>
								<td>' . $data['method'] . '</td>
							</tr>
							<tr>
								<td width=10% style="padding: 8px"><b>Error</b></td>
								<td>' . $data['error'] . '</td>
							</tr>
							<tr>
								<td width=10% style="padding: 8px"><b>Message</b></td>
								<td>' . $data['message'] . '</td>
							</tr>
							<tr>
								<td width=10% style="padding: 8px"><b>Trace</b></td>
								<td>' . $data['trace'] . '</td>
							</tr>

						</table>
						<br><br>
						<div>' . $this->getEmailFooter() . '</div>
						 <div>Team Evibe</div>
				  </div>';

		return $markup;
	}

	private function send($job, $data)
	{
		$ses = AWS::createClient('Ses');

		if ($data['to'])
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => $data['replyTo'],
				                'Destination'      => [
					                'ToAddresses' => $data['to'],
					                'CcAddresses' => $data['cc']
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
		else
		{
			$ses->sendEmail([
				                'Source'           => 'Null Email Alert <ping@evibe.in>',
				                'ReplyToAddresses' => config('evibe.contact.customer.group'),
				                'Destination'      => [
					                'ToAddresses' => config('evibe.contact.customer.group'),
					                'CcAddresses' => config('evibe.contact.tech.group')
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => '[Null Email address]. Sub: ' . $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}

		$job->delete();
	}

	private function getPartnerSalutationMarkup()
	{
		$markup = $this->getEmailFooter() . "<div>Your wellwishers at <a href='" . config('evibe.main_url') . "' style='color:#333'>Evibe.in</div>";

		return $markup;
	}

	public static function getEmailFooter()
	{
		$markup = "<div>" . config('evibe.email_footer') . "</div>";

		return $markup;
	}
}