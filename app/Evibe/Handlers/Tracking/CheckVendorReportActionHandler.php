<?php

/**
 * Check if vendors acted on report SMSes, else notify team
 *
 * @author Anji <anji@evibe.in>
 * @since  04 Feb 2016
 */

namespace Evibe\Handlers;

use App\Models\ReportBookingArchive;
use App\Models\ReportBookingLive;
use App\Models\TypeScenario;
use Evibe\Utilities\TrackReportUtil;

class CheckVendorReportActionHandler extends TrackReportUtil
{
	public function check($options)
	{
		// @logic: If vendor has already responded, the mapping gets removed from live table
		$scenario = TypeScenario::find(config('evibe.scenario.report'));
		$now = date('Y-m-d H:i:s');
		$liveMappings = ReportBookingLive::with('cloudPhone', 'booking', 'booking.ticket', 'booking.ticket.area')
		                                 ->where('type_scenario_id', $scenario->id)
		                                 ->where('expires_at', '<', $now)
		                                 ->get();

		foreach ($liveMappings as $liveMapping)
		{
			$actData = ['liveMapping' => $liveMapping, 'scenario' => $scenario];
			$returnData = $this->actOnExpiredLiveMapping($actData);
			$smsText = $returnData['smsText'];

			// move to archive with fail params
			$reportArchive = new ReportBookingArchive;
			$reportArchive->ticket_booking_id = $liveMapping->ticket_booking_id;
			$reportArchive->type_scenario_id = $liveMapping->type_scenario_id;
			$reportArchive->type_cloud_phone_id = $liveMapping->type_cloud_phone_id;
			$reportArchive->expires_at = $liveMapping->expires_at;
			$reportArchive->vendor_phone = $liveMapping->vendor_phone;
			$reportArchive->vendor_sms_text = $liveMapping->vendor_sms_text;
			$reportArchive->send_otp_at = $liveMapping->send_otp_at; // copying for future reference
			$reportArchive->otp = $liveMapping->otp;
			$reportArchive->pin = $liveMapping->pin;
			$reportArchive->team_sms_text = $smsText;
			$reportArchive->team_sms_sent_at = date('Y-m-d H:i:s');
			$reportArchive->created_at = date('Y-m-d H:i:s');
			$reportArchive->updated_at = date('Y-m-d H:i:s');
			$reportArchive->save();

			// force delete this mapping
			$liveMapping->forceDelete();
		}
	}
}