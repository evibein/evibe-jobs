<?php

/**
 * Send notifications to vendors who have bookings
 * post 12 noon today
 *
 * @author Anji <anji@evibe.in>
 * @since  30 Jan 2016
 */

namespace Evibe\Handlers;

use Evibe\Utilities\TrackReportUtil;

class TrackPost12BookingsHandler extends TrackReportUtil
{
	public function notifyVendors($options)
	{
		$halfDay = 12 * 60 * 60;
		$currentTime = time();
		$todayNoon = strtotime("today noon", $currentTime);
		$todayEnd = $todayNoon + $halfDay - 1;

		$data = [
			'startTime' => $todayNoon,
			'endTime'   => $todayEnd,
			'dayType' => 'today'
		];

		$this->fetchTrackListAndExecute($data);
	}
}