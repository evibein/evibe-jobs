<?php

/**
 * Check if vendors acted on tract SMSes, else notify team
 *
 * @author Anji <anji@evibe.in>
 * @since  03 Feb 2016
 */

namespace Evibe\Handlers;

use App\Models\TrackBookingArchive;
use App\Models\TrackBookingLive;
use App\Models\TypeScenario;
use Evibe\Utilities\TrackReportUtil;

class TrackBookingCheckHandler extends TrackReportUtil
{
	public function check($options)
	{
		// @logic: If vendor has already responded, the mapping gets removed from live table
		$scenario = TypeScenario::find(config('evibe.scenario.track'));
		$now = date('Y-m-d H:i:s');
		$liveMappings = TrackBookingLive::with('booking', 'booking.ticket', 'booking.ticket.area')
		                                ->where('type_scenario_id', $scenario->id)
		                                ->where('expires_at', '<', $now)
		                                ->get();

		// there are few vendors who did not give missed call in stipulated time
		foreach ($liveMappings as $liveMapping)
		{
			$actData = ['liveMapping' => $liveMapping, 'scenario' => $scenario];
			$returnData = $this->actOnExpiredLiveMapping($actData);
			$smsText = $returnData['smsText'];

			// move to archive with fail params
			$trackArchive = new TrackBookingArchive;
			$trackArchive->ticket_booking_id = $liveMapping->ticket_booking_id;
			$trackArchive->type_scenario_id = $liveMapping->type_scenario_id;
			$trackArchive->type_cloud_phone_id = $liveMapping->type_cloud_phone_id;
			$trackArchive->expires_at = $liveMapping->expires_at;
			$trackArchive->vendor_phone = $liveMapping->vendor_phone;
			$trackArchive->vendor_sms_text = $liveMapping->vendor_sms_text;
			$trackArchive->vendor_responded_at = null;
			$trackArchive->call_ref_id = null;
			$trackArchive->cust_sms_text = null;
			$trackArchive->cust_sms_sent_at = null;
			$trackArchive->team_sms_text = $smsText;
			$trackArchive->team_sms_sent_at = date('Y-m-d H:i:s');
			$trackArchive->created_at = date('Y-m-d H:i:s');
			$trackArchive->updated_at = date('Y-m-d H:i:s');
			$trackArchive->save();

			// force delete this mapping
			$liveMapping->forceDelete();
		}
	}
}