<?php

/**
 * Send OTP & Pin to customer and vendor with phone number
 *
 * @author Anji <anji@evibe.in>
 * @since  6 Feb 2016
 */

namespace Evibe\Handlers;

use App\Models\ReportBookingLive;
use App\Models\TypeScenario;
use Evibe\Utilities\TrackReportUtil;

use Illuminate\Support\Facades\Queue;

class SendVendorReportingOTPHandler extends TrackReportUtil
{
	public function sendOTP($options)
	{
		// @see: check will be done for every {10} minutes (from config file)
		$timingGapSecs = config('evibe.scenario.timings.otp_send') * 60 - 1;
		$typeScenario = TypeScenario::find(config('evibe.scenario.report'));
		$ttl = $typeScenario->ttl * 60; // in seconds
		$currentTime = time();
		$validStart = date('Y-m-d H:i:s', ($currentTime - $timingGapSecs));
		$validEnd = date('Y-m-d H:i:s', $currentTime);

		// get list of all live report mappings which have send_otp_at valid
		$liveMappings = ReportBookingLive::with('cloudPhone', 'booking', 'booking.ticket', 'booking.ticket.area')
		                                 ->where('send_otp_at', '>=', $validStart)
		                                 ->where('send_otp_at', '<=', $validEnd)
		                                 ->get();

		foreach ($liveMappings as $liveMapping)
		{
			$timeNow = time();
			$booking = $liveMapping->booking;
			$ticket = $booking->ticket;
			$vendor = $booking->vendor;
			$reportingTime = strtotime($booking->reports_at);
			$otpSentAt = date('Y-m-d H:i:s', $timeNow);
			$expiresAt = date('Y-m-d H:i:s', $reportingTime + $ttl);
			$vendorPhone = $vendor->phone; // can also be got from report_booking_live.vendor_phone

			// send PIN & OTP to customer
			// Ex: Hi Divya, pls pass following PIN & OTP to Kiran / team to record reporting time. PIN: 0093, OTP: 4989. Number to dial: 9640204000. Team Evibe
			$customerOtpSmsTpl = config('evibe.sms_tpl.report.customer.otp');
			$tplVars = [
				'#field1#' => $ticket->name,
				'#field2#' => $vendor->person,
				'#field3#' => $liveMapping->pin,
				'#field4#' => $liveMapping->otp,
				'#field5#' => $liveMapping->cloudPhone->phone,
			];
			$customerOtpSms = str_replace(array_keys($tplVars), array_values($tplVars), $customerOtpSmsTpl);
			$smsData = ['to' => $ticket->phone, 'text' => $customerOtpSms];
			Queue::push('Evibe\Utilities\SendSMS@send', $smsData);

			// send it to customer alt number (if exists)
			if ($ticket->alt_phone)
			{
				$smsData = ['to' => $ticket->alt_phone, 'text' => $customerOtpSms];
				Queue::push('Evibe\Utilities\SendSMS@send', $smsData);
			}

			// send pin to vendor
			// Ex: Hi Uday, PIN number for Divya's party (Indiranagar) is 0093. Pls collect OTP from Divya (7259509827) & call 9035009897 to record your reporting time. Need help? call 9535304100.
			$vendorPinSmsTpl = config('evibe.sms_tpl.report.vendor.pin');
			$tplVars = [
				'#field1#' => $vendor->person,
				'#field2#' => $ticket->name . "'s",
				'#field3#' => $ticket->area->name,
				'#field4#' => $liveMapping->pin,
				'#field5#' => $ticket->name,
				'#field6#' => $ticket->phone,
				'#field7#' => $liveMapping->cloudPhone->phone,
				'#field8#' => config('evibe.contact.operations.phone')
			];

			$vendorPinSms = str_replace(array_keys($tplVars), array_values($tplVars), $vendorPinSmsTpl);
			$smsData = ['to' => $vendorPhone, 'text' => $vendorPinSms];
			Queue::push('Evibe\Utilities\SendSMS@send', $smsData);

			// set expiry time, update values
			$liveMapping->update([
				                     'cust_sms_text'      => $customerOtpSms,
				                     'vendor_sms_text'    => $vendorPinSms,
				                     'vendor_sms_sent_at' => $otpSentAt,
				                     'cust_sms_sent_at'   => $otpSentAt,
				                     'expires_at'         => $expiresAt,
				                     'updated_at'         => $otpSentAt
			                     ]);

		}

	}
}