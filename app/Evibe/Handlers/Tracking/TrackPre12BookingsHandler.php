<?php

/**
 * Send notifications to vendors who have bookings
 * before 12 noon tomorrow
 *
 * @author Anji <anji@evibe.in>
 * @since  30 Jan 2016
 */

namespace Evibe\Handlers;

use Evibe\Utilities\TrackReportUtil;

class TrackPre12BookingsHandler extends TrackReportUtil
{
	public function notifyVendors($options)
	{
		$halfDay = 12 * 60 * 60;
		$currentTime = time();
		$tomorrowMidnight = strtotime("tomorrow midnight", $currentTime);
		$tomorrowNoon = $tomorrowMidnight + $halfDay - 1;

		$data = [
			'startTime' => $tomorrowMidnight,
			'endTime'   => $tomorrowNoon,
			'dayType'   => 'tomorrow'
		];

		$this->fetchTrackListAndExecute($data);
	}
}