<?php

namespace App\Evibe\Handlers\Reviews;

use App\Models\Review\OptionReview;
use App\Models\Vendor\PlannerReview;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\Log;

class AutoMapOptionReviewsCommandHandler extends BaseCommandHandler
{
	public function mapPartnerReviewsToOptions($command)
	{
		$validOptionTypeIds = [
			config('evibe.ticket.type.packages'),
			config('evibe.ticket.type.cakes'),
			config('evibe.ticket.type.decors'),
			config('evibe.ticket.type.services'),
			config('evibe.ticket.type.entertainments'),
			config('evibe.ticket.type.villa'),
			config('evibe.ticket.type.food'),
			config('evibe.ticket.type.priests'),
			config('evibe.ticket.type.tents'),
			config('evibe.ticket.type.couple-experiences'),
			config('evibe.ticket.type.add-ons')
		];

		$partnerReviews = PlannerReview::select('planner_review.*', 'ticket_mapping.map_id AS option_id', 'ticket_mapping.map_type_id AS option_type_id')
		                               ->join('ticket_bookings', 'ticket_bookings.id', '=', 'planner_review.ticket_booking_id')
		                               ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
		                               ->whereIn('ticket_mapping.map_type_id', $validOptionTypeIds)
		                               ->whereNull('planner_review.deleted_at')
		                               ->whereNull('ticket_bookings.deleted_at')
		                               ->whereNull('ticket_mapping.deleted_at')
		                               ->chunk(100, function ($reviews) use (&$command) {
			                               foreach ($reviews as $review)
			                               {
				                               $command->info("Planner Review with id: " . $review->id);

				                               $existingOptionReview = OptionReview::where('partner_review_id', $review->id)->first();
				                               if ($existingOptionReview)
				                               {
					                               // review has already been already mapped to an option
					                               $command->info("Review has already been mapped");
					                               continue;
				                               }
				                               else
				                               {
					                               // map the partner review to an option
					                               OptionReview::create([
						                                                    'partner_review_id' => $review->id,
						                                                    'option_type_id'    => $review->option_type_id,
						                                                    'option_id'         => $review->option_id,
						                                                    'created_at'        => Carbon::now(),
						                                                    'updated_at'        => Carbon::now()
					                                                    ]);
					                               $command->info("Review mapped to option. option Id: " . $review->option_id . " Option Type Id: " . $review->option_type_id);
				                               }
			                               }
		                               });
	}
}