<?php

namespace Evibe\Handlers;

use App\Jobs\Email\PartnerDigestEmail;
use App\Models\SiteErrorLog;
use App\Models\Ticket;
use App\Models\Vendor;
use App\Models\Venue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;

class PartnerDigestReportHandler extends BaseCommandHandler
{
	public function sendReport()
	{
		$startDay = Carbon::today()->startOfWeek()->subWeek(1)->getTimestamp(); // last week monday
		$endDay = Carbon::today()->startOfWeek()->subWeek(1)->endOfWeek()->getTimestamp(); // last week sunday
		$accessToken = Hash::make($startDay . $endDay);

		// get unique partners who got a new order / completed an order last week
		$validPartners = Ticket::select('ticket_bookings.map_id', 'ticket_bookings.map_type_id')
		                       ->join('ticket_bookings', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                       ->whereNull('ticket.deleted_at')
		                       ->whereNull('ticket_bookings.deleted_at')
		                       ->where('ticket_bookings.is_advance_paid', 1)
		                       ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                       ->where(function ($query) use ($startDay, $endDay) {
			                       $query->whereBetween('ticket_bookings.party_date_time', [$startDay, $endDay])
			                             ->orWhereBetween('ticket.paid_at', [$startDay, $endDay]);
		                       })
		                       ->get();

		if (!$validPartners->count())
		{
			return;
		}

		$nonVenuePartnerIds = $validPartners->where('map_type_id', config('evibe.ticket.type.planners'))
		                                    ->pluck('map_id')
		                                    ->unique()->values()->toArray();

		$venuePartnerIds = $validPartners->where('map_type_id', config('evibe.ticket.type.venues'))
		                                 ->pluck('map_id')
		                                 ->unique()->values()->toArray();

		if (count($nonVenuePartnerIds))
		{
			$nonVenuePartners = Vendor::whereIn('id', $nonVenuePartnerIds)->get();
			$providerType = config("evibe.ticket.type.planners");

			foreach ($nonVenuePartners as $partner)
			{
				$this->APIRequest($partner, $providerType, $startDay, $endDay, $accessToken);
			}
		}

		if (count($venuePartnerIds))
		{
			$providerType = config("evibe.ticket.type.venues");
			$venuePartners = Venue::whereIn('id', $venuePartnerIds)->get();

			foreach ($venuePartners as $partner)
			{
				$this->APIRequest($partner, $providerType, $startDay, $endDay, $accessToken);
			}
		}
	}

	private function APIRequest($provider, $providerTypeId, $startDay, $endDay, $accessToken)
	{
		$response = null;
		$data = null;

		try
		{
			$url = config('evibe.api.base_url') . "partner-digest/" . $provider->id . "/" . $providerTypeId . "?token=" . $accessToken . "&from=" . $startDay . "&to=" . $endDay;
			$client = new Client();
			$response = $client->request('POST', $url);
			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);
			$data = [
				'response'       => $response,
				'provider'       => $provider,
				'provider_type'  => $providerTypeId,
				'dashboard_url'  => config("evibe.main_url") . "partner/dashboard?utm_source=weekly_digest&utm_medium=email&utm_campaign=" . date('d-M-Y', $startDay) . "_" . date('d-M-Y', $endDay) . "&utm_term=dashboard&utm_content=" . $provider->user_id,
				'orders_url'     => config("evibe.main_url") . "partner/orders?utm_source=weekly_digest&utm_medium=email&utm_campaign=" . date('d-M-Y', $startDay) . "_" . date('d-M-Y', $endDay) . "&utm_term=dashboard&utm_content=" . $provider->user_id,
				'deliveries_url' => config("evibe.main_url") . "partner/deliveries?utm_source=weekly_digest&utm_medium=email&utm_campaign=" . date('d-M-Y', $startDay) . "_" . date('d-M-Y', $endDay) . "&utm_term=dashboard&utm_content=" . $provider->user_id
			];

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			SiteErrorLog::create($res['error']);
			// @todo: send email to tech team
		}

		// mail after getting all the data
		// API is success, provider has at least one completed party in last week or has a new order
		if (!is_null($response) &&
			$response['success'] &&
			(
				$response['data']['total_bookings_count'] != 0 ||
				$response['data']['completed_orders_count'] != 0
			)
		)
		{
			// @see: no need to send complete data
			$data['provider'] = [
				'person' => $provider->person,
				'email'  => $provider->email
			];

			dispatch(new PartnerDigestEmail($data));
		}
	}
}