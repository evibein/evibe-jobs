<?php

namespace Evibe\Handlers\Finance;

use App\Models\SiteErrorLog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CreatePartnerSettlementsCommandHandler
{
	public function createPartnerSettlements()
	{
		$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken(config('evibe.default_handler'));

		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlement') . 'create';

		try
		{
			$client = new Client();
			$res = $client->request('POST', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => []
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res['success']) && $res['success'] == false)
			{
				$errorData = [
					'url'     => request()->fullUrl(),
					'details' => isset($res['error']) ? $res['error'] : "Some error occurred while creating partner settlements"
				];

				SiteErrorLog::create($errorData);
			}
		} catch (ClientException $e)
		{
			$errorData['exception'] = "Creating Partner Settlements API";
			$errorData['code'] = $e->getCode();
			$errorData['details'] = $e->getTraceAsString();

			SiteErrorLog::create($errorData);
		}
	}
}
