<?php

namespace Evibe\Handlers\Finance;

use App\Models\SiteErrorLog;
use App\Models\TicketBooking;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class UpdateScheduledBookingSettlementsCommandHandler
{
	public function updateBookingSettlements($command)
	{
		$userPartyDateTime = 1524421800; // 23 Apr 2018 (Mon)
		$command->info("Fetching ticket booking ids form ticket booking table ...");

		// assuming that no pending settlements are there
		// updating ticket booking whose ticket status is booked
		$ticketBookings = TicketBooking::join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                               ->select('ticket_bookings.*')
		                               ->where('ticket_bookings.party_date_time', '>=', $userPartyDateTime)
		                               ->where('ticket_bookings.is_advance_paid', 1)
		                               ->where('ticket.status_id', 4)
		                               ->whereNull('ticket.deleted_at')
		                               ->whereNull('ticket_bookings.deleted_at')
		                               ->whereNull('ticket_bookings.settlement_done_at')
		                               ->whereNull('ticket_bookings.cancelled_at')
		                               ->get();

		if ($ticketBookings->count())
		{
			foreach ($ticketBookings as $ticketBooking)
			{
				// make API call to update
				$command->info("API call to update ticket booking with Id: " . $ticketBooking->id);

				$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken(config('evibe.default_handler'));
				$url = config('evibe.api.base_url') . config('evibe.api.finance.booking-settlement') . $ticketBooking->id . '/update';

				try
				{
					$client = new Client();
					$res = $client->request('PUT', $url, [
						'headers' => [
							'access-token' => $accessToken
						],
						'json'    => []
					]);

					$res = $res->getBody();
					$res = \GuzzleHttp\json_decode($res, true);

					if (isset($res['success']) && $res['success'] == false)
					{
						$errorData = [
							'url'     => request()->fullUrl(),
							'details' => isset($res['error']) ? $res['error'] : "Some error occurred while creating partner settlements"
						];

						SiteErrorLog::create($errorData);
						$command->info("ERROROR: Some error occurred while updating scheduled booking settlement");
					}

					$command->info("Updated successfully");
				} catch (ClientException $e)
				{
					$errorData['exception'] = "Creating Partner Settlements API";
					$errorData['code'] = $e->getCode();
					$errorData['details'] = $e->getTraceAsString();

					SiteErrorLog::create($errorData);

					$command->info("ERROR:::::ERRORR: IN CATCH");
				}
			}
		}
	}
}
