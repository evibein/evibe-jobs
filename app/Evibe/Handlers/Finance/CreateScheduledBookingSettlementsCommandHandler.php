<?php

namespace Evibe\Handlers\Finance;

use App\Models\SiteErrorLog;
use App\Models\Ticket;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CreateScheduledBookingSettlementsCommandHandler
{
	public function createBookingSettlements($command)
	{
		// fetch all the tickets with status booked form march 5 00:00
		// iterate through each ticket booking,
		// make API call to create scheduled booking settlement
		$command->info("Fetching all the tickets as per the conditions...");

		// since party date of ticket is the least of all bookings
		$tickets = Ticket::whereNull('deleted_at')
		                 ->where('status_id', config('evibe.ticket.status.booked'))
		                 ->where('created_at', '>=', date('Y-m-d H:i:s', (time() - (180 * (24 * 60 * 60)))))
		                 ->where('event_date', '>=', strtotime('2018-03-14 00:00:00'))
		                 ->get();

		// @see: validation is not there as it will be done on a fresh DB

		$command->info("Count of all the tickets: " . count($tickets));

		if (count($tickets))
		{
			foreach ($tickets as $ticket)
			{
				$command->info("Bookings for ticket: " . $ticket->id);
				$bookings = $ticket->bookings;

				if (count($bookings))
				{
					foreach ($bookings as $booking)
					{
						if ($booking->is_advance_paid)
						{
							$command->info("Calculating service fee and gst charges..");
							$bookingAmount = $booking->booking_amount;
							$advanceAmount = $booking->advance_amount;
							$evibeServiceFee = $bookingAmount * config('evibe.default.evibe_service_fee');

							if ($booking->ticket->city_id == config('evibe.city.bangalore'))
							{
								$cgst = $evibeServiceFee * config('evibe.default.partial_gst');
								$sgst = $evibeServiceFee * config('evibe.default.partial_gst');
								$igst = 0;
							}
							else
							{
								$cgst = 0;
								$sgst = 0;
								$igst = $advanceAmount * config('evibe.default.gst');
							}

							$booking->evibe_service_fee = $evibeServiceFee;
							$booking->cgst_amount = $cgst;
							$booking->sgst_amount = $sgst;
							$booking->igst_amount = $igst;
							$booking->save();

							$command->info("API call to create scheduled booking settlement for booking Id: " . $booking->id);

							$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken(config('evibe.default_handler'));
							$url = config('evibe.api.base_url') . config('evibe.api.finance.booking-settlement') . $booking->id . '/create';

							try
							{
								$client = new Client();
								$res = $client->request('POST', $url, [
									'headers' => [
										'access-token' => $accessToken
									],
									'json'    => []
								]);

								$res = $res->getBody();
								$res = \GuzzleHttp\json_decode($res, true);

								if (isset($res['success']) && $res['success'] == false)
								{
									$errorData = [
										'url'     => request()->fullUrl(),
										'details' => isset($res['error']) ? $res['error'] : "Some error occurred while creating partner settlements"
									];

									SiteErrorLog::create($errorData);
								}
							} catch (ClientException $e)
							{
								$errorData['exception'] = "Creating Partner Settlements API";
								$errorData['code'] = $e->getCode();
								$errorData['details'] = $e->getTraceAsString();

								SiteErrorLog::create($errorData);
							}
						}
					}
				}

				$command->info("-----------------------");
			}
		}
	}
}
