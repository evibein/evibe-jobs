<?php

namespace App\Evibe\Handlers\Util;

use \Evibe\Utilities\EvibeUtil;
use App\Models\SiteErrorLog;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class AutoWaterMarkImageCommandHandler extends BaseCommandHandler
{
	public function addWaterMarkOnTheProducts($categoryId)
	{
		$url = config('evibe.api.base_url') . config('evibe.api.watermark.prefix');
		$accessToken = EvibeUtil::getAccessToken(config('evibe.default_handler'));

		try
		{
			$client = new Client();
			$client->post($url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => [
					"categoryId" => $categoryId
				]
			]);
		} catch (ClientException $e)
		{
			$errorData = [
				'url'        => $url,
				'exception'  => "JOB Notification Api",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);
		}
	}

	public function removeWaterMarkOnTheProducts($categoryId)
	{
		$url = config('evibe.api.base_url') . config('evibe.api.watermark.remove.prefix');
		$accessToken = EvibeUtil::getAccessToken(config('evibe.default_handler'));

		try
		{
			$client = new Client();
			$client->post($url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => [
					"categoryId" => $categoryId
				]
			]);
		} catch (ClientException $e)
		{
			$errorData = [
				'url'        => $url,
				'exception'  => "JOB Notification Api",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);
		}
	}
}