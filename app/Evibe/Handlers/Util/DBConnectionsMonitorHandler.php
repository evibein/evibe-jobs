<?php

namespace App\Evibe\Handlers\Util;

use App\Jobs\Email\MailDBMonitorUpdateToTechTeam;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DBConnectionsMonitorHandler extends BaseCommandHandler
{
	public function testDBConnections()
	{
		try
		{
			$count = $maxUsedConnections = 0;
			$userConnectionsTemplatePHP = "";
			$userConnectionsTemplateHTML = "";

			$results = DB::select(DB::raw("show processlist"));
			$usedConnections = DB::select(DB::raw("show global status like \"Max_used_connections\";"));

			foreach ($usedConnections as $connection)
			{
				$maxUsedConnections = $connection->Value;
			}

			foreach ($results as $result)
			{
				$count++;
				$userConnectionsTemplatePHP .= "\t\nID: " . $result->Id . " | User: " . $result->User . "@" . $result->Host . " | Command: " . $result->Command . " | Time: " . $result->Time;
				$userConnectionsTemplateHTML .= "ID: " . $result->Id . " | User: " . $result->User . "@" . $result->Host . " | Command: " . $result->Command . " | Time: " . $result->Time . "<br>";
			}
			$text = "[" . date("d-m-Y h:i:s", time()) . "] " . "Total Connections: " . $count . " | Max User Connections : " . $maxUsedConnections;
			Storage::append('db-monitor.txt', $text . $userConnectionsTemplatePHP);

			if ($count > 50)
			{
				$emailData = [
					'userConnections' => $maxUsedConnections,
					'trace'           => "<p><b>" . $text . "</b></p>" . "<br>" . $userConnectionsTemplateHTML
				];

				// Send and email to tech team with all the user details
				dispatch(new MailDBMonitorUpdateToTechTeam($emailData));
			}
			else
			{
				if ($count > 120)
				{
					$emailData = [
						'userConnections' => $maxUsedConnections,
						'trace'           => "<p><b>" . $text . "</b></p>" . "<br>" . $userConnectionsTemplateHTML
					];

					// Kill all the user connections
					// Should implement Kill command
					// DB::select('KILL 1000;');
					// 1000 is the connection ID
					dispatch(new MailDBMonitorUpdateToTechTeam($emailData));
				}
			}
		} catch (\Exception $e)
		{
			$data = [
				'fullUrl' => 'Jobs DB monitoring',
				'code'    => $e->getCode() ? $e->getCode() : 'Error',
				'message' => $e->getMessage(),
				'details' => $e->getTraceAsString()
			];

			$this->saveError($data);
			// @todo: send email to tech team
		}
	}
}