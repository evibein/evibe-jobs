<?php

namespace App\Evibe\Handlers\Util;

use App\Models\Settlements;
use \Evibe\Utilities\EvibeUtil;
use App\Models\SiteErrorLog;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SettlementInvoiceCreationCommandHandler extends BaseCommandHandler
{
	public function createPartnerSettlementInvoices($command, $options)
	{
		$partnerId = $options['partnerId'];
		$partnerTypeId = $options['partnerTypeId'];

		$command->info("Settled settlement invoice generation for partnerId: $partnerId - partnerTypeId: $partnerTypeId");

		$settlements = Settlements::where('partner_id', $partnerId)
		                          ->where('partner_type_id', $partnerTypeId)
		                          ->whereNotNull('settlement_done_at')
		                          ->chunk(5, function ($settlements) use ($command) {
			                          foreach ($settlements as $settlement)
			                          {
				                          $command->info("Partner settlement id: " . $settlement->id);
				                          $url = config('evibe.api.base_url') . config('evibe.api.finance.settlement') . 'create-invoice/' . $settlement->id;
				                          $command->info("url: $url");
				                          $accessToken = EvibeUtil::getAccessToken(config('evibe.default_handler'));

				                          try
				                          {
					                          $client = new Client();
					                          $res = $client->post($url, [
						                          'headers' => [
							                          'access-token' => $accessToken
						                          ],
						                          'json'    => []
					                          ]);

					                          $res = $res->getBody();
					                          $res = \GuzzleHttp\json_decode($res, true);

					                          if (isset($res['success']) && $res['success'] == false)
					                          {
						                          $error = "SOme error occurred while generating invoice";
						                          if(isset($res['error']) && $res['error'])
						                          {
						                          	$error = $res['error'];
						                          }
						                          $command->info("--- Failure in the partner settlement ---");
						                          $command->info($error);
					                          }
				                          } catch (ClientException $e)
				                          {
					                          $errorData = [
						                          'url'        => $url,
						                          'exception'  => "JOB Notification Api",
						                          'code'       => $e->getCode(),
						                          'details'    => $e->getTraceAsString(),
						                          'created_at' => date('Y-m-d H:i:s'),
						                          'updated_at' => date('Y-m-d H:i:s')
					                          ];

					                          SiteErrorLog::create($errorData);
				                          }
			                          }
		                          });

		$command->info("Successfully created");
	}
}