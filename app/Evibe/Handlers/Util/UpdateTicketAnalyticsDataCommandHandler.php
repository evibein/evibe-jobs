<?php

namespace App\Evibe\Handlers\Util;

use App\Models\SiteErrorLog;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Exception\ClientException;

class UpdateTicketAnalyticsDataCommandHandler extends BaseCommandHandler
{
	public function updateTicketAnalyticsData()
	{
		try
		{
			// Need to write
		} catch (ClientException $e)
		{
			$errorData = [
				'url'        => "",
				'exception'  => "JOB Notification Api",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);
		}
	}
}