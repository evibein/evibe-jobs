<?php

namespace Evibe\Handlers\Search;

use App\Jobs\Search\AddProductsToSingleTable;
use App\Jobs\Search\RemoveDefunctProductsFromSingleTable;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeEvent;
use App\Models\Cake\CakeTags;
use App\Models\Decor;
use App\Models\Decor\DecorEvent;
use App\Models\Decor\DecorTags;
use App\Models\Package;
use App\Models\Package\PackageTag;
use App\Models\Service\ServiceTags;
use App\Models\SiteErrorLog;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use App\Models\Util\Tags;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;

class ImportProductsToTableCommandHandler extends BaseCommandHandler
{
	public function importProductsToSingleTable()
	{
		$tables = [
			[
				"class" => \App\Models\Decor::class,
				"cols"  => [
					"optionType" => config("evibe.ticket.type.decors"),
					"price_min"  => "min_price",
					"price_max"  => "max_price"
				]
			],
			[
				"class" => \App\Models\Package::class,
				"cols"  => [
					"optionType"    => config("evibe.ticket.type.packages"),
					"price_max"     => "price_max",
					"optionSubType" => "type_ticket_id",
					"hasCityId"     => true,
					"hasEventId"    => true
				]
			],
			[
				"class" => \App\Models\TypeService::class,
				"cols"  => [
					"optionType" => config("evibe.ticket.type.services"),
					"price_min"  => "min_price",
					"price_max"  => "max_price",
					"hasCityId"  => true
				]
			],
			[
				"class" => \App\Models\Cake\Cake::class,
				"cols"  => [
					"optionType" => config("evibe.ticket.type.cakes"),
					"name"       => "title",
					"price_min"  => "price",
					"price_max"  => "price"
				]
			],
			/* VenueHalls not handled in EvibeMain Globalsearch
			[
				"class" => \App\Models\VenueHall::class,
				"cols"  => [
					"optionType" => config("evibe.ticket.type.venue_halls"),
					"price_min"  => "price_min_veg",
					"price_max"  => "price_max_veg"
				]
			],
			*/
			[
				"class" => \App\Models\Trend::class,
				"cols"  => [
					"optionType" => config("evibe.ticket.type.trends"),
					"hasCityId"  => true
				]

			]
		];

		$productBank = [];
		$defunctProductBank = [];

		$allCities = City::where("is_active", 1)
			->whereNull("deleted_at")
			->get()
			->pluck("name", "id")
			->toArray();

		// Restricting only to 3 occasions as we are dealing with only with those occasions
		$allEvents = TypeEvent::whereNull("deleted_at")
			->whereIn("id", [config("evibe.event.kids_birthday"), config("evibe.event.house_warming"), config("evibe.event.special_experience")])
			->get()
			->pluck("name", "id")
			->toArray();

		$allTags = Tags::whereNull("deleted_at")
			->get()
			->pluck("identifier", "id")
			->toArray();

		foreach ($tables as $key => $tableValues) {
			$product = app()->make($tableValues["class"]);
			$productCols = $tableValues["cols"];

			$product->where("is_live", 1)
				->whereNull("deleted_at")
				->chunk(100, function ($products) use (&$productBank, $productCols, $allCities, $allEvents, $allTags) {
					foreach ($products as $product) {
						$values = $this->getValues($productCols, $product);
						$tags = $this->getTags($productCols, $product->id, $allTags);

						// check for provider unavailable
						if (isset($values["city_id"]) || !$values["city_id"]) {
							$values["city_name"] = isset($allCities[$values["city_id"]]) ? $allCities[$values["city_id"]] : "";
							$values["occasion_name"] = isset($allEvents[$values["occasion_name"]]) ? $allEvents[$values["occasion_name"]] : "";
							$values["tags"] = $tags ?: "";
							array_push($productBank, $values);
						}
					}
				});

			// @remove defunct products from `searchable_products`
			// collect all is_live = 0 || deleted_at IS NOT NULL
			$product->withTrashed()
				->where("is_live", 0)
				->orWhereNotNull("deleted_at")
				->chunk(100, function ($products) use (&$defunctProductBank, $productCols) {
					foreach ($products as $product) {
						array_push($defunctProductBank, [
							"option_type_id" => $productCols["optionType"],
							"option_id"      => $product->id
						]);
					}
				});
		}

		// Removing all the products related to the removed occasion - wedding, bachelor parties
		Package::whereIn("event_id", [config("evibe.event.pre_post"), config("evibe.event.bachelor_party")])
			->chunk(100, function ($products) use (&$defunctProductBank, $productCols) {
				foreach ($products as $product) {
					array_push($defunctProductBank, [
						"option_type_id" => config("evibe.ticket.type.packages"),
						"option_id"      => $product->id
					]);
				}
			});

		$defunctDecorIds = DecorEvent::whereIn("event_id", [config("evibe.event.pre_post"), config("evibe.event.bachelor_party")])
			->pluck("decor_id")
			->toArray();

		Decor::whereIn("id", $defunctDecorIds)
			->chunk(100, function ($products) use (&$defunctProductBank, $productCols) {
				foreach ($products as $product) {
					array_push($defunctProductBank, [
						"option_type_id" => config("evibe.ticket.type.decors"),
						"option_id"      => $product->id
					]);
				}
			});

		$defunctCakeIds = CakeEvent::whereIn("event_id", [config("evibe.event.pre_post"), config("evibe.event.bachelor_party")])
			->pluck("cake_id")
			->toArray();

		Cake::whereIn("id", $defunctCakeIds)
			->chunk(100, function ($products) use (&$defunctProductBank, $productCols) {
				foreach ($products as $product) {
					array_push($defunctProductBank, [
						"option_type_id" => config("evibe.ticket.type.cakes"),
						"option_id"      => $product->id
					]);
				}
			});

		if (count($productBank)) {
			$productBank = collect($productBank);

			foreach ($productBank->chunk(20) as $chunk) {
				dispatch(new AddProductsToSingleTable($chunk));
			}
		}

		if (count($defunctProductBank)) {
			$defunctProductBank = collect($defunctProductBank);

			foreach ($defunctProductBank->chunk(20) as $chunk) {
				dispatch(new RemoveDefunctProductsFromSingleTable($chunk));
			}
		}

		try {
			$url = config("evibe.main_url") . "/search/update-table";
			$accessToken = Hash::make('updateSearchableTable' . Carbon::today()->startOfDay()->timestamp);

			$client = new Client([
				'curl' => [
					CURLOPT_TIMEOUT => 60,
				],
			]);
			$res = $client->request('POST', $url, ['verify' => false, 'json' => ["accessToken" => $accessToken]]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res['success']) && $res['success'] == false) {
				$errorData = [
					'exception' => "URL request to main for updating the profile image & url",
					'details'   => (isset($res["message"]) && $res["message"]) ? $res["message"] : ''
				];

				SiteErrorLog::create($errorData);
			}
		} catch (ClientException $e) {
			$errorData['exception'] = "URL request to main for updating the profile image & url";
			$errorData['code'] = $e->getCode();
			$errorData['details'] = $e->getTraceAsString();

			SiteErrorLog::create($errorData);
		}
	}

	private function getValues($productCols, $product)
	{
		$columns = [
			"name"      => "name",
			"price_min" => "price",
			"price_max" => "price_max",
			"code"      => "code"
		];

		$values = [];
		$values["option_type_id"] = $productCols["optionType"];
		$values["option_sub_type_id"] = $productCols["optionType"]; // default same, update if applicable
		$values["option_id"] = $product->id;

		// optionSubType (for packages)
		if (isset($productCols["optionSubType"])) {
			$values["option_sub_type_id"] = $product->{$productCols["optionSubType"]};
		}

		// cityId (provider/direct)
		if (isset($productCols["hasCityId"])) {
			$values["city_id"] = $product->city_id;
		}

		// eventId
		if (isset($productCols["hasEventId"]) && $productCols["hasEventId"]) {
			$values["occasion_name"] = $product->event_id;
		} else {
			$values["occasion_name"] = "";
			if (in_array($values["option_type_id"], [config("evibe.ticket.type.decors"), config("evibe.ticket.type.cakes")])) {
				$values["occasion_name"] = $product->firstEvent();
			}
		}

		foreach ($columns as $key => $col) {
			$columnName = isset($productCols[$key]) ? $productCols[$key] : $col;
			$values[$key] = $product->{$columnName};

			// city_id
			if (!isset($values["city_id"]) && $product->provider) {
				$values["city_id"] = $product->provider->city_id;
			}
		}

		return $values;
	}

	private function getTags($productCols, $productId, $allTags)
	{
		$tagIds = [];
		$optionTypeId = $productCols["optionType"];

		switch ($optionTypeId) {
			case config("evibe.ticket.type.packages"):

				$tagIds = PackageTag::where("planner_package_id", $productId)
					->whereNull("deleted_at")
					->get()
					->pluck("tile_tag_id")
					->toArray();
				break;
			case config("evibe.ticket.type.services"):

				$tagIds = ServiceTags::where("service_id", $productId)
					->whereNull("deleted_at")
					->get()
					->pluck("tag_id")
					->toArray();
				break;
			case config("evibe.ticket.type.cakes"):

				$tagIds = CakeTags::where("cake_id", $productId)
					->whereNull("deleted_at")
					->get()
					->pluck("tag_id")
					->toArray();
				break;
			case config("evibe.ticket.type.decors"):

				$tagIds = DecorTags::where("decor_id", $productId)
					->whereNull("deleted_at")
					->get()
					->pluck("tag_id")
					->toArray();
				break;
		}

		$tags = "";
		if (count($tagIds) > 0) {
			foreach ($tagIds as $tagId) {
				if (isset($allTags[$tagId]) && $allTags[$tagId]) {
					$tags = $tags . $allTags[$tagId] . " ";
				}
			}
		}

		return $tags;
	}
}
