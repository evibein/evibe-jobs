<?php

namespace Evibe\Handlers\Search;

use App\Jobs\Search\AddProductsToESIndex;
use Evibe\Handlers\BaseCommandHandler;

class ImportProductsToESCommandHandler extends BaseCommandHandler
{
	public function importProductsToES()
	{
		dispatch(new AddProductsToESIndex());
	}
}