<?php

namespace Evibe\Handlers;

use App\Models\TypeService;
use Exception;
use Intervention\Image\Facades\Image;

class OptServiceImgCommandHandler extends BaseOptiImgCommandHandler
{
	public function optimize($command, $options)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '512M');

		$ids = $options['id'];
		$sid = $options['sid'];
		$eid = $options['eid'];
		$tw = $options['thumbsWidth'];
		$pw = $options['profileWidth'];
		$rw = $options['resultWidth'];

		$command->info("starting optimizing service images...");
		$services = TypeService::with('gallery');

		if ($sid)
		{
			$services->where('id', '>=', $sid);
		}
		if ($eid)
		{
			$services->where('id', '<=', $eid);
		}
		if (count($ids))
		{
			$services->whereIn('id', $ids);
		}

		$services = $services->get();
		foreach ($services as $service)
		{
			$command->info("------------------------");
			$command->info("Optimizing for service: " . $service->code);

			// optimize serivce images
			foreach ($service->gallery as $serviceGallery)
			{
				if ($serviceGallery->type != 0)
				{
					continue;
				}

				$command->info(" - service image: " . $serviceGallery->url);

				$base = $serviceGallery->getBasePath($service->id);
				$imgPath = $base . $serviceGallery->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';
				$image = null;

				try
				{
					$image = Image::make($imgPath);
				} catch (Exception $e)
				{
					$command->error(" ** Image not found ***");
					continue;
				}

				$resizeOptions = [
					'command'  => $command,
					'image'    => $image,
					'imageUrl' => $serviceGallery->url,
					'height'   => null
				];

				// profile
				$resizeOptions['width'] = $pw;
				$resizeOptions['newImgPath'] = $profileImgPath;
				$this->resizeImg($resizeOptions);

				// results
				$resizeOptions['width'] = $rw;
				$resizeOptions['newImgPath'] = $resultsImgPath;
				$this->resizeImg($resizeOptions);

				// thumbs
				$resizeOptions['width'] = $tw;
				$resizeOptions['newImgPath'] = $thumbsImgPath;
				$this->resizeImg($resizeOptions);
			}
		}
	}
}