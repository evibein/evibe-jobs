<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  28 Sep 2015
 */

namespace Evibe\Handlers;

use App\Models\Venue;
use Intervention\Image\Facades\Image;

class OptiVenueImgsCommandHandler
{
	public function optiImgs($command, $profileWidth, $resultWidth, $thumbsWidth, $idMin = null, $idMax = null)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '128M');

		$command->info("starting optimizing venue images...");
		$venues = Venue::with('images', 'halls', 'halls.images');

		if ($idMin && $idMax)
		{
			$venues->where('id', '>=', $idMin)
			       ->where('id', '<=', $idMax);
		}

		$venues = $venues->get();

		foreach ($venues as $venue)
		{
			$command->info("------------------------");
			$command->info("Optimizing for venue: " . $venue->name);

			// optimize venue images
			foreach ($venue->images as $venueImage)
			{
				$command->info("venue image: " . $venueImage->url);

				$base = '../gallery/venues/' . $venue->id . '/images/';
				$imgPath = $base . $venueImage->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';

				$img = Image::make($imgPath);

				// profile
				self::resizeImg($img, "Profile", $profileImgPath, $venueImage->url, $command, $profileWidth);

				// results
				self::resizeImg($img, "Results", $resultsImgPath, $venueImage->url, $command, $resultWidth);

				// thumbs
				self::resizeImg($img, "Thumbs", $thumbsImgPath, $venueImage->url, $command, $thumbsWidth);
			}

			foreach ($venue->halls as $venueHall)
			{
				$command->info("*****");
				$command->info("Optimizing for venue: " . $venue->name . ' and hall: ' . $venueHall->code);

				foreach ($venueHall->images as $venueHallImage)
				{
					$command->info("hall image: " . $venueHallImage->url);

					$hallBase = '../gallery/venues/' . $venue->id . '/halls/' . $venueHall->id . '/images/';
					$hallImgPath = $hallBase . $venueHallImage->url;
					$hallProfileImgPath = $hallBase . 'profile/';
					$hallResultsImgPath = $hallBase . 'results/';
					$hallThumbsImgPath = $hallBase . 'thumbs/';

					$img = Image::make($hallImgPath);

					// profile
					self::resizeImg($img, "Profile", $hallProfileImgPath, $venueHallImage->url, $command, $profileWidth);

					// results
					self::resizeImg($img, "Results", $hallResultsImgPath, $venueHallImage->url, $command, $resultWidth);

					// thumbs
					self::resizeImg($img, "Thumbs", $hallThumbsImgPath, $venueHallImage->url, $command, $thumbsWidth);
				}
			}
		}
	}

	private function resizeImg($img, $type, $newImgPath, $imgUrl, $command, $width, $height = null)
	{
		// check if directory is available, else create
		if (!file_exists($newImgPath))
		{
			mkdir($newImgPath, 0777, true);
		}

		$img->resize($width, $height, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		$img->save($newImgPath . $imgUrl);

		$command->info("$type resolution of $width for $imgUrl done");
	}
}