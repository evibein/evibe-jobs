<?php

namespace Evibe\Handlers;

use App\Models\Cake\Cake;
use Exception;
use Intervention\Image\Facades\Image;

class OptCakeImgsCommandHandler extends BaseOptiImgCommandHandler
{
	public function optimize($command, $options)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '512M');

		$ids = $options['id'];
		$sid = $options['sid'];
		$eid = $options['eid'];
		$tw = $options['thumbsWidth'];
		$pw = $options['profileWidth'];
		$rw = $options['resultWidth'];

		$command->info("starting optimizing cake images...");
		$cakes = Cake::with('gallery');

		if ($sid)
		{
			$cakes->where('id', '>=', $sid);
		}
		if ($eid)
		{
			$cakes->where('id', '<=', $eid);
		}
		if (count($ids))
		{
			$cakes->whereIn('id', $ids);
		}

		$cakes = $cakes->get();
		foreach ($cakes as $cake)
		{
			$command->info("------------------------");
			$command->info("Optimizing for cake: " . $cake->code);

			// optimize package images
			foreach ($cake->gallery as $cakeGallery)
			{
				if ($cakeGallery->type != 0)
				{
					continue; // not image
				}

				$command->info(" - cake image: " . $cakeGallery->url);

				$base = $cakeGallery->getPath();
				$imgPath = $base . $cakeGallery->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';
				$image = null;

				try
				{
					$image = Image::make($imgPath);
				} catch (Exception $e)
				{
					$command->error(" ** Image not found ***");
					continue;
				}

				$resizeOptions = [
					'command'  => $command,
					'image'    => $image,
					'imageUrl' => $cakeGallery->url,
					'height'   => null
				];

				// profile
				$resizeOptions['width'] = $pw;
				$resizeOptions['newImgPath'] = $profileImgPath;
				$this->resizeImg($resizeOptions);

				// results
				$resizeOptions['width'] = $rw;
				$resizeOptions['newImgPath'] = $resultsImgPath;
				$this->resizeImg($resizeOptions);

				// thumbs
				$resizeOptions['width'] = $tw;
				$resizeOptions['newImgPath'] = $thumbsImgPath;
				$this->resizeImg($resizeOptions);
			}
		}
	}
}