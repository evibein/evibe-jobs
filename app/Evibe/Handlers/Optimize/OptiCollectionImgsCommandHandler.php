<?php

namespace Evibe\Handlers;

use App\Models\Collection\Collection;
use Exception;
use Intervention\Image\Facades\Image;

class OptiCollectionImgsCommandHandler extends BaseOptiImgCommandHandler
{

	public function optimize($command, $options)
	{
		ini_set('memory_limit', '128M');

		$phw = $options['profileHomeWidth'];
		$phh = $options['profileHomeHeight'];
		$plw = $options['profileListWidth'];
		$plh = $options['profileListHeight'];
		$clw = $options['coverListWidth'];
		$clh = $options['coverListHeight'];
		$ppw = $options['profileProfileWidth'];
		$pph = $options['profileProfileHeight'];
		$cpw = $options['coverProfileWidth'];
		$cph = $options['coverProfileHeight'];

		$command->info("starting optimizing collection images...");
		$collections = Collection::all();

		foreach ($collections as $collection)
		{
			$command->info("----------------------------------");
			$command->info("Optimizing for Collection: " . $collection->code);

			$base = config('evibe.gallery.root') . '/collection/' . $collection->id . '/images/';
			//$base ='../gallery/collection/' . $collection->id . '/images/';
			//Log::info($base);
			$profileImgPath = $base . $collection->profile_image;
			$coverImgPath = $base . $collection->cover_image;
			$homePath = $base . 'home/';
			$listPath = $base . 'list/';
			$profilePath = $base . 'profile/';
			$profileImage = null;
			$coverImage = null;

			try
			{
				$profileImage = Image::make($profileImgPath);
			} catch (Exception $e)
			{
				$command->error(" ** Profile image not found ** ");
			}
			if ($profileImage)
			{
				$profileResizeOptions = [
					'command'  => $command,
					'image'    => $profileImage,
					'imageUrl' => $collection->profile_image
				];

				// home
				$profileResizeOptions['width'] = $phw;
				$profileResizeOptions['height'] = $phh;
				$profileResizeOptions['newImgPath'] = $homePath;
				$this->resizeImg($profileResizeOptions);

				// profile
				$profileResizeOptions['width'] = $ppw;
				$profileResizeOptions['height'] = $pph;
				$profileResizeOptions['newImgPath'] = $profilePath;
				$this->resizeImg($profileResizeOptions);

				// list
				$profileResizeOptions['width'] = $plw;
				$profileResizeOptions['height'] = $plh;
				$profileResizeOptions['newImgPath'] = $listPath;
				$this->resizeImg($profileResizeOptions);
			}

			try
			{
				$coverImage = Image::make($coverImgPath);
			} catch (Exception $e)
			{
				$command->error(" ** Cover image not found ** ");
			}

			if ($coverImage)
			{
				$coverResizeOptions = [
					'command'  => $command,
					'image'    => $coverImage,
					'imageUrl' => $collection->cover_image
				];

				// profile
				$coverResizeOptions['width'] = $cpw;
				$coverResizeOptions['height'] = $cph;
				$coverResizeOptions['newImgPath'] = $profilePath;
				$this->resizeImg($coverResizeOptions);

				// list
				$coverResizeOptions['width'] = $clw;
				$coverResizeOptions['height'] = $clh;
				$coverResizeOptions['newImgPath'] = $listPath;
				$this->resizeImg($coverResizeOptions);
			}
		}
	}
}