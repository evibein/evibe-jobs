<?php

namespace Evibe\Handlers;

use Exception;
use Intervention\Image\Facades\Image;

class BaseOptiImgCommandHandler
{
	public function iterateAndOptimize($options)
	{
		$command = $options['command'];
		$list = $options['list'];
		$type = $options['type'];
		$tw = $options['thumbsWidth'];
		$pw = $options['profileWidth'];
		$rw = $options['resultWidth'];

		foreach ($list as $item)
		{
			$command->info("------------------------");
			$command->info("Optimizing for $type: " . $item->code);

			// optimize $type images
			foreach ($item->gallery as $itemImage)
			{
				if ($itemImage->type != 0)
				{
					continue;
				}

				$command->info(" - $type image: " . $itemImage->url);
				$providerId = $item->provider ? $item->provider->id : $item->id;
				$base = $itemImage->getBasePath($providerId);
				$imgPath = $base . $itemImage->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';
				$image = null;

				try
				{
					$image = Image::make($imgPath);
				} catch (Exception $e)
				{
					$command->error(" ** Image not found ***");
					continue;
				}

				$resizeOptions = [
					'command'  => $command,
					'image'    => $image,
					'imageUrl' => $itemImage->url,
					'height'   => null
				];

				// profile
				$resizeOptions['width'] = $pw;
				$resizeOptions['newImgPath'] = $profileImgPath;
				$this->resizeImg($resizeOptions);

				// results
				$resizeOptions['width'] = $rw;
				$resizeOptions['newImgPath'] = $resultsImgPath;
				$this->resizeImg($resizeOptions);

				// thumbs
				$resizeOptions['width'] = $tw;
				$resizeOptions['newImgPath'] = $thumbsImgPath;
				$this->resizeImg($resizeOptions);
			}
		}
	}

	public function resizeImg($options)
	{
		$command = $options['command'];
		$image = $options['image'];
		$width = $options['width'];
		$height = $options['height'];
		$imageUrl = $options['imageUrl'];
		$newImagePath = $options['newImgPath'];

		// check if directory is available, else create
		if (!file_exists($newImagePath))
		{
			mkdir($newImagePath, 0777, true);
		}

		$image->resize($width, $height, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$image->save($newImagePath . $imageUrl);

		$command->info("    Resolution of $width for $imageUrl done");
	}
}