<?php

namespace Evibe\Handlers;

use Exception;
use Intervention\Image\Facades\Image;
use App\Models\Package;

class OptPkgImgCommandHandler
{
	public function optimize($command, $options)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '512M');

		$ids = $options['id'];
		$pIds = $options['pid'];
		$sid = $options['sid'];
		$eid = $options['eid'];
		$tw = $options['thumbsWidth'];
		$pw = $options['profileWidth'];
		$rw = $options['resultWidth'];

		$command->info("starting optimizing package images...");
		$packages = Package::with('gallery');

		if ($sid)
		{
			$packages->where('id', '>=', $sid);
		}
		if ($eid)
		{
			$packages->where('id', '<=', $eid);
		}
		if (count($ids))
		{
			$packages->whereIn('id', $ids);
		}
		if (count($pIds))
		{
			$packages->whereIn('map_id', $pIds);
		}

		$packages = $packages->get();
		foreach ($packages as $package)
		{
			$command->info("------------------------");
			$command->info("Optimizing for package: " . $package->code);

			// optimize package images
			foreach ($package->gallery as $packageImage)
			{
				if ($packageImage->type != 0)
				{
					continue;
				}

				$command->info(" - package image: " . $packageImage->url);

				$base = $packageImage->getBasePath($package->map_id);
				$imgPath = $base . $packageImage->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';
				$image = null;

				try
				{
					$image = Image::make($imgPath);
				} catch (Exception $e)
				{
					$command->error(" ** Image not found ***");
					continue;
				}

				$resizeOptions = [
					'command'  => $command,
					'image'    => $image,
					'imageUrl' => $packageImage->url,
					'height'   => null
				];

				// profile
				$resizeOptions['width'] = $pw;
				$resizeOptions['newImgPath'] = $profileImgPath;
				self::resizeImg($resizeOptions);

				// results
				$resizeOptions['width'] = $rw;
				$resizeOptions['newImgPath'] = $resultsImgPath;
				self::resizeImg($resizeOptions);

				// thumbs
				$resizeOptions['width'] = $tw;
				$resizeOptions['newImgPath'] = $thumbsImgPath;
				self::resizeImg($resizeOptions);
			}
		}

	}

	private function resizeImg($options)
	{
		$command = $options['command'];
		$image = $options['image'];
		$width = $options['width'];
		$height = $options['height'];
		$imageUrl = $options['imageUrl'];
		$newImagePath = $options['newImgPath'];

		// check if directory is available, else create
		if (!file_exists($newImagePath))
		{
			mkdir($newImagePath, 0777, true);
		}

		$image->resize($width, $height, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$image->save($newImagePath . $imageUrl);

		$command->info("    Resolution of $width for $imageUrl done");
	}
}