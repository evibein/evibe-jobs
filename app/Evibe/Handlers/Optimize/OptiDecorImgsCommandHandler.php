<?php

namespace Evibe\Handlers;

use App\Models\Decor;
use Intervention\Image\Facades\Image;

/**
 *
 * @author Anji <anji@evibe.in>
 * @since  13 Oct 2015
 */
class OptiDecorImgsCommandHandler
{

	public function optiImgs($command, $profileWidth, $resultWidth, $thumbsWidth, $idMin = null, $idMax = null)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '128M');

		$command->info("starting optimizing decor images...");
		$decors = Decor::with('images');

		if ($idMin && $idMax)
		{
			$decors->where('id', '>=', $idMin)
			       ->where('id', '<=', $idMax);
		}

		$decors = $decors->get();

		foreach ($decors as $decor)
		{
			$command->info("------------------------");
			$command->info("Optimizing for decor: " . $decor->name);

			// optimize decor images
			foreach ($decor->images as $decorImage)
			{
				$command->info("decor image: " . $decorImage->url);

				$base = '../gallery/decors/' . $decor->id . '/images/';
				$imgPath = $base . $decorImage->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';

				$img = Image::make($imgPath);

				// profile
				self::resizeImg($img, "Profile", $profileImgPath, $decorImage->url, $command, $profileWidth);

				// results
				self::resizeImg($img, "Results", $resultsImgPath, $decorImage->url, $command, $resultWidth);

				// thumbs
				self::resizeImg($img, "Thumbs", $thumbsImgPath, $decorImage->url, $command, $thumbsWidth);
			}
		}
	}

	private function resizeImg($img, $type, $newImgPath, $imgUrl, $command, $width, $height = null)
	{
		// check if directory is available, else create
		if (!file_exists($newImgPath))
		{
			mkdir($newImgPath, 0777, true);
		}

		$img->resize($width, $height, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		$img->save($newImgPath . $imgUrl);

		$command->info("$type resolution of $width for $imgUrl done");
	}
}