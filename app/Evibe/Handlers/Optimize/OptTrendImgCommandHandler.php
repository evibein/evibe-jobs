<?php

namespace Evibe\Handlers;

use App\Models\Trend;
use Exception;
use Intervention\Image\Facades\Image;

class OptTrendImgCommandHandler extends BaseOptiImgCommandHandler
{
	public function optimize($command, $options)
	{
		/**
		 * @todo bad practise, need to update this.
		 */
		ini_set('memory_limit', '512M');

		$ids = $options['id'];
		$sid = $options['sid'];
		$eid = $options['eid'];
		$tw = $options['thumbsWidth'];
		$pw = $options['profileWidth'];
		$rw = $options['resultWidth'];

		$command->info("starting optimizing trend images...");
		$trends = Trend::with('gallery');

		if ($sid)
		{
			$trends->where('id', '>=', $sid);
		}
		if ($eid)
		{
			$trends->where('id', '<=', $eid);
		}
		if (count($ids))
		{
			$trends->whereIn('id', $ids);
		}

		$trends = $trends->get();
		foreach ($trends as $trend)
		{
			$command->info("------------------------");
			$command->info("Optimizing for trend: " . $trend->code);

			// optimize package images
			foreach ($trend->gallery as $trendGallery)
			{
				if ($trendGallery->type != 0)
				{
					continue;
				}

				$command->info(" - trend image: " . $trendGallery->url);

				$base = $trendGallery->getBasePath($trend->id);
				$imgPath = $base . $trendGallery->url;
				$profileImgPath = $base . 'profile/';
				$resultsImgPath = $base . 'results/';
				$thumbsImgPath = $base . 'thumbs/';
				$image = null;

				try
				{
					$image = Image::make($imgPath);
				} catch (Exception $e)
				{
					$command->error(" ** Image not found ***");
					continue;
				}

				$resizeOptions = [
					'command'  => $command,
					'image'    => $image,
					'imageUrl' => $trendGallery->url,
					'height'   => null
				];

				// profile
				$resizeOptions['width'] = $pw;
				$resizeOptions['newImgPath'] = $profileImgPath;
				$this->resizeImg($resizeOptions);

				// results
				$resizeOptions['width'] = $rw;
				$resizeOptions['newImgPath'] = $resultsImgPath;
				$this->resizeImg($resizeOptions);

				// thumbs
				$resizeOptions['width'] = $tw;
				$resizeOptions['newImgPath'] = $thumbsImgPath;
				$this->resizeImg($resizeOptions);
			}
		}
	}
}