<?php

namespace App\Evibe\Handlers\Payment;

use App\Jobs\Email\Payment\MailPaymentTransactionReportToTeam;
use App\Models\Payment\PaymentInitiations;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\SiteErrorLog;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class PaymentReportToTeamCommandHandler extends BaseCommandHandler
{
	public function sendReport()
	{
		$startDay = Carbon::today()->startOfDay()->subHours(4);
		$endDay = Carbon::today()->endOfDay()->subHours(4);

		$paymentTransactions = PaymentTransactionAttempts::orderBy("created_at", "desc")
		                                                 ->forTickets()
		                                                 ->whereBetween("created_at", [$startDay, $endDay])
		                                                 ->pluck("id")
		                                                 ->toArray();

		$uniquePaymentClicks = count(array_unique(PaymentTransactionAttempts::whereIn("id", $paymentTransactions)->pluck("ticket_id")->toArray()));

		$paymentInitiations = PaymentInitiations::whereBetween("created_at", [$startDay, $endDay])
		                                        ->forTickets()
		                                        ->pluck("ticket_id")
		                                        ->toArray();

		$totalCount = count($paymentTransactions);
		if ($totalCount > 0)
		{
			$successfulCount = PaymentTransactionAttempts::whereIn("id", $paymentTransactions)
			                                             ->forTickets()
			                                             ->where("payment_bank_success", "2")
			                                             ->where("payment_evibe_success", "2")
			                                             ->pluck("ticket_id")
			                                             ->toArray();

			$manualAcceptedCount = PaymentTransactionAttempts::whereIn("id", $paymentTransactions)
			                                                 ->forTickets()
			                                                 ->whereNull("payment_bank_success")
			                                                 ->where("payment_evibe_success", "2")
			                                                 ->pluck("ticket_id")
			                                                 ->toArray();

			$failedComments = PaymentTransactionAttempts::select("id", "ticket_id", "comments", "payment_transaction_id")
			                                            ->forTickets()
			                                            ->whereIn("id", $paymentTransactions)
			                                            ->whereNotIn("ticket_id", array_merge($successfulCount, $manualAcceptedCount))
			                                            ->get();

			$failedCount = $uniquePaymentClicks - count($successfulCount) - count($manualAcceptedCount);

			$failedCommentsData = [];
			if ($failedComments->count() > 0)
			{
				foreach ($failedComments as $failedComment)
				{
					$comments = explode(",", $failedComment->comments);
					if (count($comments) > 0)
					{
						foreach ($comments as $comment)
						{
							if (($comment != "") && strpos(str_replace("\"", "", $comment), "nmappedstatus"))
							{
								$reason = explode("-", $comment);
								$failedCommentsData[$failedComment->ticket_id][] = isset($reason[1]) ? $reason[1] : $comment;

								break;
							}
						}
					}
				}
			}

			$failedComments = $failedComments->filter(function ($query) {
				return is_null($query->comments);
			});

			$payumoneyString = implode('|', $failedComments->pluck("payment_transaction_id")->toArray());
			$payumoneyData = [];

			try
			{
				$client = new Client();
				$client = $client->request('post', 'https://www.payumoney.com/payment/payment/chkMerchantTxnStatus?merchantKey=2LEHxq&merchantTransactionIds=' . $payumoneyString, [
					'headers' => [
						'Authorization' => 'pLJzPBY2Ye3M2Nhhr3FaXUOyA1Lr+SOOGyBPRaD0nQ8='
					],
					'json'    => []
				]);

				$client = $client->getBody();
				$client = \GuzzleHttp\json_decode($client, true);

				if (isset($client["result"]) && count($client["result"]) > 0)
				{
					foreach ($client["result"] as $item)
					{
						$payumoneyData[$item["merchantTransactionId"]] = $item["status"];
					}
				}
			} catch (ClientException $e)
			{
				$errorData = [
					'url'        => "https://www.payumoney.com/payment/payment/chkMerchantTxnStatus",
					'exception'  => "JOB Notification Api",
					'code'       => $e->getCode(),
					'details'    => $e->getTraceAsString(),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				];

				SiteErrorLog::create($errorData);
			}

			$invalidFailedData = [];
			if ($failedComments->count() > 0)
			{
				foreach ($failedComments as $failedComment)
				{
					if (is_null($failedComment->comments))
					{
						$invalidFailedData[$failedComment->ticket_id][] = isset($payumoneyData[$failedComment->payment_transaction_id]) ? $payumoneyData[$failedComment->payment_transaction_id] : "No status found, txn Id: " . $failedComment->payment_transaction_id;
					}
				}
			}

			$validFailedCount = count($failedCommentsData);
			$invalidFailedCount = count($invalidFailedData);

			dispatch(new MailPaymentTransactionReportToTeam([
				                                                "paymentInitiations"       => count($paymentInitiations),
				                                                "uniquePaymentInitiations" => count(array_unique($paymentInitiations)),
				                                                "totalCount"               => $totalCount,
				                                                "uniquePaymentClicks"      => $uniquePaymentClicks,
				                                                "successCount"             => count($successfulCount),
				                                                "manualAcceptCount"        => count($manualAcceptedCount),
				                                                "failedCount"              => $failedCount,
				                                                "validFailedCount"         => $validFailedCount,
				                                                "invalidFailedCount"       => $invalidFailedCount,
				                                                "failedCommentsData"       => $failedCommentsData,
				                                                "invalidFailedData"        => $invalidFailedData,
				                                                "is_piab"                  => "0"
			                                                ]));
		}
	}

	public function sendPIABReport()
	{
		$startDay = Carbon::today()->startOfDay()->subHours(4);
		$endDay = Carbon::today()->endOfDay()->subHours(4);

		$paymentTransactions = PaymentTransactionAttempts::orderBy("created_at", "desc")
		                                                 ->forPIAB()
		                                                 ->whereBetween("created_at", [$startDay, $endDay])
		                                                 ->pluck("id")
		                                                 ->toArray();

		$uniquePaymentClicks = count(array_unique(PaymentTransactionAttempts::whereIn("id", $paymentTransactions)->pluck("piab_id")->toArray()));

		$paymentInitiations = PaymentInitiations::whereBetween("created_at", [$startDay, $endDay])
		                                        ->forPIAB()
		                                        ->pluck("piab_id")
		                                        ->toArray();

		$totalCount = count($paymentTransactions);
		if ($totalCount > 0)
		{
			$successfulCount = PaymentTransactionAttempts::whereIn("id", $paymentTransactions)
			                                             ->forPIAB()
			                                             ->where("payment_bank_success", "2")
			                                             ->where("payment_evibe_success", "2")
			                                             ->pluck("piab_id")
			                                             ->toArray();

			$manualAcceptedCount = PaymentTransactionAttempts::whereIn("id", $paymentTransactions)
			                                                 ->forPIAB()
			                                                 ->whereNull("payment_bank_success")
			                                                 ->where("payment_evibe_success", "2")
			                                                 ->pluck("piab_id")
			                                                 ->toArray();

			$failedComments = PaymentTransactionAttempts::select("id", "piab_id", "comments", "payment_transaction_id")
			                                            ->forPIAB()
			                                            ->whereIn("id", $paymentTransactions)
			                                            ->whereNotIn("piab_id", array_merge($successfulCount, $manualAcceptedCount))
			                                            ->get();

			$failedCount = $uniquePaymentClicks - count($successfulCount) - count($manualAcceptedCount);

			$failedCommentsData = [];
			if ($failedComments->count() > 0)
			{
				foreach ($failedComments as $failedComment)
				{
					$comments = explode(",", $failedComment->comments);
					if (count($comments) > 0)
					{
						foreach ($comments as $comment)
						{
							if (($comment != "") && strpos(str_replace("\"", "", $comment), "nmappedstatus"))
							{
								$reason = explode("-", $comment);
								$failedCommentsData[$failedComment->piab_id][] = isset($reason[1]) ? $reason[1] : $comment;

								break;
							}
						}
					}
				}
			}

			$failedComments = $failedComments->filter(function ($query) {
				return is_null($query->comments);
			});

			$payumoneyString = implode('|', $failedComments->pluck("payment_transaction_id")->toArray());
			$payumoneyData = [];

			try
			{
				$client = new Client();
				$client = $client->request('post', 'https://www.payumoney.com/payment/payment/chkMerchantTxnStatus?merchantKey=2LEHxq&merchantTransactionIds=' . $payumoneyString, [
					'headers' => [
						'Authorization' => 'pLJzPBY2Ye3M2Nhhr3FaXUOyA1Lr+SOOGyBPRaD0nQ8='
					],
					'json'    => []
				]);

				$client = $client->getBody();
				$client = \GuzzleHttp\json_decode($client, true);

				if (isset($client["result"]) && count($client["result"]) > 0)
				{
					foreach ($client["result"] as $item)
					{
						$payumoneyData[$item["merchantTransactionId"]] = $item["status"];
					}
				}
			} catch (ClientException $e)
			{
				$errorData = [
					'url'        => "https://www.payumoney.com/payment/payment/chkMerchantTxnStatus",
					'exception'  => "JOB Notification Api",
					'code'       => $e->getCode(),
					'details'    => $e->getTraceAsString(),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				];

				SiteErrorLog::create($errorData);
			}

			$invalidFailedData = [];
			if ($failedComments->count() > 0)
			{
				foreach ($failedComments as $failedComment)
				{
					if (is_null($failedComment->comments))
					{
						$invalidFailedData[$failedComment->piab_id][] = isset($payumoneyData[$failedComment->payment_transaction_id]) ? $payumoneyData[$failedComment->payment_transaction_id] : "No status found, txn Id: " . $failedComment->payment_transaction_id;
					}
				}
			}

			$validFailedCount = count($failedCommentsData);
			$invalidFailedCount = count($invalidFailedData);

			dispatch(new MailPaymentTransactionReportToTeam([
				                                                "paymentInitiations"       => count($paymentInitiations),
				                                                "uniquePaymentInitiations" => count(array_unique($paymentInitiations)),
				                                                "totalCount"               => $totalCount,
				                                                "uniquePaymentClicks"      => $uniquePaymentClicks,
				                                                "successCount"             => count($successfulCount),
				                                                "manualAcceptCount"        => count($manualAcceptedCount),
				                                                "failedCount"              => $failedCount,
				                                                "validFailedCount"         => $validFailedCount,
				                                                "invalidFailedCount"       => $invalidFailedCount,
				                                                "failedCommentsData"       => $failedCommentsData,
				                                                "invalidFailedData"        => $invalidFailedData,
				                                                "is_piab"                  => "1"
			                                                ]));
		}
	}
}