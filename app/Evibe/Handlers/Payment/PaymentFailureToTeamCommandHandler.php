<?php

namespace App\Evibe\Handlers\Payment;

use App\Jobs\Email\Payment\MailPaymentTransactionToTeam;
use App\Jobs\SMS\Payment\SMSPaymentRetryToCustomer;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\Ticket;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\Hash;

class PaymentFailureToTeamCommandHandler extends BaseCommandHandler
{
	public function sendReminders()
	{
		$oneMinute = 60;
		$startTime = Carbon::now()->timestamp - (15 * 60);
		$endTime = $startTime + $oneMinute;

		$paymentTransactionTickets = PaymentTransactionAttempts::where("payment_start_time", ">", $startTime)
		                                                       ->forTickets()
		                                                       ->orderBy("created_at", "DESC")
		                                                       ->get();

		if ($paymentTransactionTickets->count() > 0)
		{
			$validTickets = Ticket::select("id", "phone", "email", "name", "is_auto_booked")
			                      ->whereIn("id", $paymentTransactionTickets->pluck("ticket_id")->toArray())
			                      ->whereNotIn("status_id", [config("evibe.ticket.status.booked"), config("evibe.ticket.status.auto_pay"), config("evibe.ticket.status.service_auto_pay")])
			                      ->get();

			foreach ($validTickets as $ticket)
			{
				$isSendEmail = false;
				$isSendSMS = false;
				$retryCount = PaymentTransactionAttempts::where("ticket_id", $ticket->id)->count();
				$deviceInfo = $errorMessage = "";

				if ($retryCount > 1)
				{
					$latestTransaction = $paymentTransactionTickets->where("ticket_id", $ticket->id)->first();

					if ($latestTransaction->payment_start_time > (time() - 60))
					{
						if ($retryCount > 2)
						{
							$isSendEmail = true;
						}
						$isSendSMS = true;
						$errorMessage = "Customer tried the payment for " . $retryCount . " times & haven't completed the payment yet";
						$deviceInfo = $latestTransaction->browser_info;
					}
				}
				else
				{
					$initialTransaction = $paymentTransactionTickets->where("ticket_id", $ticket->id)->first();

					if (($initialTransaction->payment_start_time >= $startTime) && ($initialTransaction->payment_start_time < $endTime))
					{
						$isSendEmail = false;
						$isSendSMS = true;
						$errorMessage = "Customer initiated the payment 15min ago & haven't completed the payment yet";
						$deviceInfo = $initialTransaction->browser_info;
					}
				}

				if ($isSendEmail)
				{
					dispatch(new MailPaymentTransactionToTeam([
						                                          "ticket"       => $ticket,
						                                          "errorMessage" => $errorMessage,
						                                          "deviceInfo"   => $deviceInfo
					                                          ]));
				}

				if ($isSendSMS)
				{
					if ($ticket->is_auto_booked == 1)
					{
						$paymentLink = config('evibe.live.ab-checkout');
						$paymentLink .= "?id=" . $ticket->id;
						$paymentLink .= "&token=" . Hash::make($ticket->id);
						$paymentLink .= "&uKm8=" . Hash::make($ticket->phone);
					}
					else
					{
						$paymentLink = config('evibe.live.checkout');
						$paymentLink .= "?id=" . $ticket->id;
						$paymentLink .= "&token=" . Hash::make($ticket->id);
						$paymentLink .= "&uKm8=" . Hash::make($ticket->email);
					}
					$paymentLink .= "&txnCnt=" . $retryCount;

					dispatch(new SMSPaymentRetryToCustomer([
						                                       'shortUrl'      => $this->getShortUrl($paymentLink),
						                                       "customerName"  => $ticket->name,
						                                       "customerPhone" => $ticket->phone
					                                       ]));
				}
			}
		}
	}
}