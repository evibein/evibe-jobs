<?php

namespace App\Evibe\Handlers\Payment;

use App\Jobs\Email\Payment\TriggerPaymentFailureNotifications;
use App\Jobs\Email\Payment\TriggerPaymentSuccessNotifications;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\SiteErrorLog;
use App\Models\Ticket;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class PaymentSuccessComForNonRedirectSuccessPageCommandHandler extends BaseCommandHandler
{
	public function sendReminders()
	{
		$endTime = Carbon::now()->timestamp;
		$startTime = $endTime - (10 * 60);

		$paymentTransactionTickets = PaymentTransactionAttempts::whereBetween("payment_start_time", [$startTime, $endTime])
		                                                       ->forTickets()
		                                                       ->pluck("ticket_id")
		                                                       ->unique()
		                                                       ->toArray();

		if (count($paymentTransactionTickets) > 0)
		{
			$bookedTickets = Ticket::whereIn("id", array_unique($paymentTransactionTickets))
			                       ->whereNotIn("status_id", [config("evibe.ticket.status.booked"), config("evibe.ticket.status.auto_pay"), config("evibe.ticket.status.service_auto_pay")])
			                       ->pluck("id")
			                       ->toArray();

			$failedTransactionIds = PaymentTransactionAttempts::wherein("ticket_id", $bookedTickets)
			                                                  ->forTickets()
			                                                  ->whereNotNull("payment_transaction_id")
			                                                  ->pluck("payment_transaction_id")
			                                                  ->toArray();

			if (count($failedTransactionIds) > 0)
			{
				$payumoneyString = implode('|', $failedTransactionIds);
				Log::info("Txn Ids: " . $payumoneyString);
				$payumoneyData = [];

				// Getting all the successful transaction Ids from the API
				try
				{
					$client = new Client();
					$client = $client->request('post', 'https://www.payumoney.com/payment/payment/chkMerchantTxnStatus?merchantKey=2LEHxq&merchantTransactionIds=' . $payumoneyString, [
						'headers' => [
							'Authorization' => 'pLJzPBY2Ye3M2Nhhr3FaXUOyA1Lr+SOOGyBPRaD0nQ8='
						],
						'json'    => []
					]);

					$client = $client->getBody();
					$client = \GuzzleHttp\json_decode($client, true);

					if (isset($client["result"]) && count($client["result"]) > 0)
					{
						foreach ($client["result"] as $item)
						{
							if (in_array($item["status"], ["Settlement in process", "Money with Payumoney", "Completed", "Failed"]))
							{
								$payumoneyData[] = $item["merchantTransactionId"];
							}
						}
					}
				} catch (ClientException $e)
				{
					$errorData = [
						'url'        => "https://www.payumoney.com/payment/payment/chkMerchantTxnStatus",
						'exception'  => "JOB Notification Api",
						'code'       => $e->getCode(),
						'details'    => $e->getTraceAsString(),
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					];

					SiteErrorLog::create($errorData);
				}

				if (count($payumoneyData) > 0)
				{
					// For those successful transactions getting the payumoney reference Id for storing it in DB
					$successTransactions = [];
					$failedTransactionIds = [];

					try
					{
						$client = new Client();
						$client = $client->request('post', 'https://www.payumoney.com/payment/op/getPaymentResponse?merchantKey=2LEHxq&merchantTransactionIds=' . implode('|', $payumoneyData), [
							'headers' => [
								'Authorization' => 'pLJzPBY2Ye3M2Nhhr3FaXUOyA1Lr+SOOGyBPRaD0nQ8='
							],
							'json'    => []
						]);

						$client = $client->getBody();
						$client = \GuzzleHttp\json_decode($client, true);

						if (isset($client["result"]) && count($client["result"]) > 0)
						{
							foreach ($client["result"] as $item)
							{
								if (isset($item["postBackParam"]) && in_array($item["postBackParam"]["status"], ["success"]))
								{
									$successTransactions[$item["merchantTransactionId"]] = $item["postBackParam"]["payuMoneyId"];
								}
								else
								{
									if (isset($item["postBackParam"]) && in_array($item["postBackParam"]["status"], ["failed", "failure"]))
									{
										$failedTransactionIds[$item["merchantTransactionId"]] = $item["postBackParam"]["payuMoneyId"];
									}
								}
							}
						}
					} catch (ClientException $e)
					{
						$errorData = [
							'url'        => "https://www.payumoney.com/payment/op/getPaymentResponse",
							'exception'  => "JOB Notification Api",
							'code'       => $e->getCode(),
							'details'    => $e->getTraceAsString(),
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						];

						SiteErrorLog::create($errorData);
					}

					if (count($successTransactions) > 0)
					{
						dispatch(new TriggerPaymentSuccessNotifications(
							         $successTransactions
						         ));
					}

					if (count($failedTransactionIds) > 0)
					{
						dispatch(new TriggerPaymentFailureNotifications(
							         $failedTransactionIds
						         ));
					}
				}
			}
		}
	}
}