<?php

namespace Evibe\Handlers;

/**
 * @author Anji <anji@evibe.in>
 * @since  3 Jan 2015
 */
use App\Models\TicketUpdate;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Queue;
use App\Models\Ticket;

class AskFeedbackCommandHandler extends BaseCommandHandler
{
	public function askFeedback()
	{
		/**
		 * Calculate start and end time based on current day
		 *
		 * Feedback email will be sent the following day irrespective of Sat or Sun
		 * If party date P, then first mail will be sent on P + 1, at 3:00 PM
		 * and remaining followup at 12:00 PM for next 4 days, until P + 5
		 * Start and end times will be calculated based on the current time
		 *
		 * --- This changed to ---
		 *
		 * 3 notifications as follows:
		 * P + 1 at 3:00 PM
		 * P + 2 at 12:00 PM
		 * P + 5 at 12:00 PM
		 *
		 */

		$currentTimestamp = time();

		// @see: do not change Carbon::createFromTimestamp($currentTimestamp) to a single variable
		if (Carbon::createFromTimestamp($currentTimestamp)->toDateTimeString() < Carbon::createFromTimestamp($currentTimestamp)->startOfDay()->addHours(15)->toDateTimeString())
		{
			// 12:00 PM slot
			$prevStart = Carbon::createFromTimestamp($currentTimestamp)->subDays(6)->startOfDay()->toDateTimeString();
			$prevEnd = Carbon::createFromTimestamp($currentTimestamp)->subDays(2)->endOfDay()->toDateTimeString();
		}
		else
		{
			// 3:00 PM slot
			$prevStart = Carbon::createFromTimestamp($currentTimestamp)->subDays(1)->startOfDay()->toDateTimeString();
			$prevEnd = Carbon::createFromTimestamp($currentTimestamp)->subDays(1)->endOfDay()->toDateTimeString();
		}

		$prevStartTime = strtotime($prevStart);
		$prevEndTime = strtotime($prevEnd);

		// for each ticket, there can be multiple bookings with different `party_date_time`
		// where 'feedback_received_at' is null
		$finishedTickets = Ticket::join('ticket_bookings', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                         ->select('ticket.id', 'ticket.name', 'ticket.phone', 'ticket.email', 'ticket.alt_email', 'ticket.status_id', 'ticket.feedback_url',
		                                  DB::raw("MIN(ticket_bookings.party_date_time) AS first_order_party_time,
			                                        MAX(ticket_bookings.party_date_time) AS last_order_party_time,
			                                        SUM(ticket_bookings.booking_amount) AS total_booking_amount")
		                         )
		                         ->where("ticket.status_id", 4)
		                         ->where("ticket_bookings.is_advance_paid", 1)
		                         ->whereNULL('ticket.feedback_received_at')
		                         ->whereNull("ticket_bookings.deleted_at")
		                         ->groupBy("ticket_bookings.ticket_id")
		                         ->havingRaw("MAX(ticket_bookings.party_date_time) >= $prevStartTime AND MAX(ticket_bookings.party_date_time) < $prevEndTime")
		                         ->get();

		foreach ($finishedTickets as $ticket)
		{
			$ticketId = $ticket->id;
			$todayStart = Carbon::createFromTimestamp($currentTimestamp)->startOfDay()->timestamp;
			$partyDateStart = Carbon::createFromTimestamp($ticket->last_order_party_time)->startOfDay()->timestamp;
			$reminderCount = round(($todayStart - $partyDateStart) / (24 * 60 * 60)); // although we'll be getting round values

			$feedbackUrl = $ticket->feedback_url ?: $this->generateFeedbackUrl($ticketId);
			$feedbackEmailSentAt = null;
			$feedbackSMSSentAt = null;

			$ccAddresses = [];
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			}

			$partyDate = date("d/m/y", $ticket->first_order_party_time);
			if ($ticket->first_order_party_time == $ticket->last_order_party_time)
			{
				$partyDate = date("d M Y h:i A", $ticket->first_order_party_time);
			}
			elseif (date("d m y", $ticket->first_order_party_time) != date("d m y", $ticket->last_order_party_time))
			{
				$partyDate = date("d/m/y", $ticket->first_order_party_time) . " - " . date("d/m/y", $ticket->last_order_party_time);
			}

			$validity = false;
			$emailSubject = "How did your party go?";
			switch ($reminderCount)
			{
				case 1:
					$emailSubject = "How did your party go?";
					$validity = true;
					break;
				case 2:
					$emailSubject = "Your feedback is very important to us, please share!";
					//$emailSubject = "We are waiting to hear your party experience";
					break;
				case 3:
					$emailSubject = "Your feedback is very important to us, please share!";
					//$emailSubject = "How can we improve our services? We need your help";
					$validity = true;
					break;
				case 4:
					$emailSubject = "Your feedback is very important to us, please share!";
					break;
				case 5:
					$emailSubject = "We are still waiting to hear your experience";
					break;
				case 6:
					$emailSubject = "Please share your party experience with us";
					$validity = true;
					break;
			}

			if(!$validity)
			{
				// 2nd, 4th and 5th day notifications are not required
				continue;
			}

			$queueData = [
				'name'               => $ticket->name,
				'phone'              => $ticket->phone,
				'email'              => $ticket->email,
				'ccAddresses'        => $ccAddresses,
				'partyDate'          => $partyDate,
				'feedbackUrl'        => $feedbackUrl,
				'emailSubject'       => $emailSubject,
				'totalBookingAmount' => $ticket->total_booking_amount
			];

			// few tickets may not contain both email and phone number
			$isAskSent = false;
			if ($queueData['email'])
			{
				Queue::push('Evibe\Utilities\SendEmail@mailAskFeedback', $queueData);
				$isAskSent = true;
				$feedbackEmailSentAt = date('Y-m-d H:i:s');
			}

			if ($queueData['phone'])
			{
				Queue::push('Evibe\Utilities\SendSMS@smsAskFeedback', $queueData);
				$isAskSent = true;
				$feedbackSMSSentAt = date('Y-m-d H:i:s');
			}

			if ($isAskSent)
			{
				$ticket->feedback_url = $feedbackUrl;
				$ticket->feedback_email_sent_at = $ticket->feedback_email_sent_at ?: $feedbackEmailSentAt;
				$ticket->feedback_sms_sent_at = $ticket->feedback_sms_sent_at ?: $feedbackSMSSentAt;

				$updateData = [
					'ticket_id'   => $ticket->id,
					'status_id'   => $ticket->status_id,
					'comments'    => "Ask feedback email sent, iteration: $reminderCount",
					'status_at'   => time(),
					'type_update' => "Auto"
				];
				TicketUpdate::create($updateData);

				$ticket->updated_at = date('Y-m-d H:i:s');
				$ticket->save();
			}
		}
	}

	private function generateFeedbackUrl($ticketId)
	{
		$hashedTicketId = Hash::make($ticketId);
		$longUrl = config('evibe.main_url') . "give-feedback?id=" . $ticketId . "&token=" . $hashedTicketId;

		return $this->getShortUrl($longUrl);
	}
}