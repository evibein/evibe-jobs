<?php

namespace Evibe\Handlers;

use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class DropOffFeedbackCommandHandler extends BaseCommandHandler
{
	public function askFeedback()
	{
		/**
		 *
		 * Drop Off Feedback sms will be sent on Wednesday for the last week customers
		 *
		 * Target Customers: Customers who are not in booked status and their party date is completed
		 * Notification will be sent to those customers on Wednesday, whose supposed party date is in between last week Monday to Sunday
		 *
		 * Notification Time: Wednesday, 4 PM
		 *
		 */

		$currentTimestamp = time();

		$startTime = Carbon::createFromTimestamp($currentTimestamp)->startOfWeek()->subWeeks(1)->toDateTimeString();
		$endTime = Carbon::createFromTimestamp($currentTimestamp)->endOfWeek()->subWeeks(1)->toDateTimeString();

		$startTimestamp = strtotime($startTime);
		$endTimestamp = strtotime($endTime);

		// get all the applicable drop off tickets from start time to end time
		$nonDropOffStatus = [
			config('evibe.ticket.status.confirmed'),
			config('evibe.ticket.status.booked'),
			config('evibe.ticket.status.auto_pay'),
			config('evibe.ticket.status.service_auto_pay')
		];

		$dropOffTickets = Ticket::whereNotIn('status_id', $nonDropOffStatus)
		                        ->whereNotNull('phone')
		                        ->whereNotNull('event_date')
		                        ->whereNull('deleted_at')
		                        ->where('event_date', '>=', $startTimestamp)
		                        ->where('event_date', '<=', $endTimestamp)
		                        ->get();

		if (!count($dropOffTickets))
		{
			return false;
		}

		$dropOffMobileNumbers = $dropOffTickets->pluck('phone')->toArray();

		$dropOffTicketsArray = [];
		foreach ($dropOffTickets as $ticket)
		{
			array_push($dropOffTicketsArray, [
				'ticketId'      => $ticket->id,
				'name'          => $ticket->name,
				'phone'         => $ticket->phone,
				'partyDateTime' => $ticket->event_date,
				'createdAt'     => Carbon::createFromTimestamp(strtotime($ticket->created_at))->toDateTimeString(),
			]);
		}

		// remove tickets for which a non drop-off ticket exists in the scheduled week
		// remove tickets for which any ticket exists after schedule week
		// get all the tickets with the drop off mobile numbers and party date from schedule week start
		// @see: logic is based on event_date completely (not on created_at)
		$allDropOffMobileNumberTickets = Ticket::whereIn('phone', $dropOffMobileNumbers)
		                                       ->where('event_date', '>=', $startTimestamp)
		                                       ->whereNull('deleted_at')
		                                       ->get();

		if (count($allDropOffMobileNumberTickets) && count($dropOffTicketsArray))
		{
			foreach ($dropOffTicketsArray as $key => $datum)
			{
				$inWeekPartyNonDropOffTickets = $allDropOffMobileNumberTickets->where('phone', $datum['phone'])
				                                                              ->whereIn('status_id', $nonDropOffStatus)
				                                                              ->where('event_date', '>=', $startTimestamp)
				                                                              ->where('event_date', '<=', $endTimestamp);

				$postWeekPartyTickets = $allDropOffMobileNumberTickets->where('phone', $datum['phone'])
				                                                      ->where('event_date', '>', $endTimestamp);

				if (count($inWeekPartyNonDropOffTickets) || count($postWeekPartyTickets))
				{
					unset($dropOffTicketsArray[$key]);
				}
			}
		}

		// get unique mobile numbers tickets
		// sort and remove duplicate mobile number array data [also consider data with customer name]
		$notificationNumbersData = [];

		// @see: random order

		if (count($dropOffTicketsArray))
		{
			foreach ($dropOffTicketsArray as $datum)
			{
				if (isset($notificationNumbersData[$datum['phone']]) && $notificationNumbersData[$datum['phone']])
				{
					// if data exists, compare and update data
					// compare and update name
					if (isset($datum['name']) && $datum['name'])
					{
						$notificationNumbersData[$datum['phone']]['name'] = $datum['name'];
					}
				}
				else
				{
					// if data doesn't exist - for the first time
					$notificationNumbersData[$datum['phone']] = [
						'phone' => $datum['phone'], // mobile number exists for sure
						'name'  => (isset($datum['name']) && $datum['name']) ? $datum['name'] : "Customer", // update by default
					];
				}
			}
		}

		if (!count($notificationNumbersData))
		{
			// no numbers data to send survey
			return false;
		}

		// proceed with sending notifications
		foreach ($notificationNumbersData as $datum)
		{
			$notificationData = [
				'name'       => (isset($datum['name']) && $datum['name']) ? $datum['name'] : "Customer",  // although it never happens, just in case
				'phone'      => (isset($datum['phone']) && $datum['phone']) ? $datum['phone'] : null,
				'amazonCash' => '25',
				'formLink'   => 'bit.ly/2QtSJth'
			];

			if ($notificationData['phone'])
			{
				Queue::push('Evibe\Utilities\SendSMS@smsDropOffFeedback', $notificationData);
			}
		}
	}
}