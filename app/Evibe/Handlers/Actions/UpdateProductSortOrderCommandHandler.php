<?php

namespace App\Evibe\Handlers\Actions;

use App\Jobs\Util\ComputeAndSaveCategorySortOrder;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;

class UpdateProductSortOrderCommandHandler extends BaseCommandHandler
{
	public function updateSortScores($command, $options)
	{
		// @see: if not declared, run foreach loop on unknown param from validArray
		$productTypeId = $options['productTypeId'];
		$eventId = $options['eventId'];
		$cityId = $options['cityId'];

		// @todo: get all values from database
		$validArray = [
			'city'        => [
				config('evibe.city.bangalore'),
				config('evibe.city.hyderabad'),
				config('evibe.city.delhi'),
				config('evibe.city.mumbai'),
				config('evibe.city.pune')
			],
			'event'       => [
				config('evibe.event.kids_birthday'),
				config('evibe.event.bachelor_party'),
				config('evibe.event.pre_post'),
				config('evibe.event.house_warming'),
				config('evibe.event.special_experience')
			],
			'productType' => [
				config('evibe.ticket.type.packages'),
				//config('evibe.ticket.type.venues'),
				//config('evibe.ticket.type.services'),
				config('evibe.ticket.type.trends'),
				config('evibe.ticket.type.cakes'),
				config('evibe.ticket.type.decors'),
				config('evibe.ticket.type.entertainments'),
				config('evibe.ticket.type.venue_halls'),
				config('evibe.ticket.type.resort'),
				config('evibe.ticket.type.villa'),
				config('evibe.ticket.type.lounge'),
				config('evibe.ticket.type.food'),
				config('evibe.ticket.type.couple-experiences'),
				config('evibe.ticket.type.venue-deals'),
				config('evibe.ticket.type.priests'),
				config('evibe.ticket.type.tents'),
				//config('evibe.ticket.type.generic-package')
			]
		];

		// todo: expect from command
		// @see: time gap between start time and end time should be at least 60 days
		$startTimeString = $options['startTimeString'] ?: 1498847400; // default: 1 July 2017 00:00:00
		$endTimeString = time();
		$startTime = Carbon::createFromTimestamp($startTimeString)->toDateTimeString();
		$endTime = Carbon::createFromTimestamp($endTimeString)->toDateTimeString();
		$startDay = Carbon::createFromTimestamp($startTimeString)->startOfDay()->toDateTimeString();
		$startDayString = strtotime($startDay);
		$endDay = Carbon::createFromTimestamp($endTimeString)->startOfDay()->toDateTimeString();
		$endDayString = strtotime($endDay);

		$command->info("time --------");
		$command->info("startTime: " . $startTime . " - " . $startTimeString);
		$command->info("endTime: " . $endTime . " - " . $endTimeString);
		$command->info("startDay: " . $startDay . " - " . $startDayString);
		$command->info("endDay: " . $endDay . " - " . $endDayString);
		$command->info("time --------");
		$command->info(" ");

		// time intervals
		$timeIntervalStarts = config('product-sort-order.time-interval');
		$timeInterval = [];
		if (count($timeIntervalStarts))
		{
			foreach ($timeIntervalStarts as $key => $timeIntervalStart)
			{
				$timeInterval[$key]['startTime'] = Carbon::createFromTimestamp($startDayString)->addDays($timeIntervalStarts[$key])->toDateTimeString();
				if (array_key_exists($key + 1, $timeIntervalStarts))
				{
					$timeInterval[$key]['endTime'] = Carbon::createFromTimestamp($startDayString)->addDays($timeIntervalStarts[$key + 1])->toDateTimeString();
				}
				else
				{
					$timeInterval[$key]['endTime'] = $endDay;
				}
			}

			$command->info(" ");
			$command->info("----- Computed intervals -----");
			$command->info("intervals: " . print_r($timeInterval, true));
			$command->info("-------------------------------");
			$command->info(" ");

			$cityIdArray = $cityId ? [$cityId] : $validArray['city'];
			$eventIdArray = $eventId ? [$eventId] : $validArray['event'];
			$productTypeIdArray = $productTypeId ? [$productTypeId] : $validArray['productType'];

			foreach ($cityIdArray as $cityId)
			{
				foreach ($eventIdArray as $eventId)
				{
					foreach ($productTypeIdArray as $productTypeId)
					{
						$command->info(" ");
						$command->info("---------- Category Specific Computation starts ----------");
						$command->info("--- cityId: $cityId --- eventId: $eventId --- productTypeId: $productTypeId ---");
						$command->info("----------------------------------------------------------");

						// @see: if city level weights needed, uncomment this code
						/* If city level weights are required
						$priceRangeWeights = config("product-sort-order.weights.overall.defaults");
						if (config("product-sort-order.weights.overall.$cityId"))
						{
							$priceRangeWeights = config("product-sort-order.weights.overall.$cityId.defaults");
							if (config("product-sort-order.weights.overall.$cityId.$eventId"))
							{
								$priceRangeWeights = config("product-sort-order.weights.overall.$cityId.$eventId.defaults");
								if (config("product-sort-order.weights.overall.$cityId.$eventId.$productTypeId"))
								{
									$priceRangeWeights = config("product-sort-order.weights.overall.$cityId.$eventId.$productTypeId");
								}
							}
						}
						*/

						$priceRangeWeights = config("product-sort-order.weights.overall.defaults");
						if (config("product-sort-order.weights.overall.$eventId"))
						{
							$priceRangeWeights = config("product-sort-order.weights.overall.$eventId.defaults");
							if (config("product-sort-order.weights.overall.$eventId.$productTypeId"))
							{
								$priceRangeWeights = config("product-sort-order.weights.overall.$eventId.$productTypeId");
							}
						}

						$weights = [
							'compute' => config('product-sort-order.weights.compute'),
							'boost'   => config('product-sort-order.weights.boost'),
							'overall' => $priceRangeWeights,
						];

						dispatch(new ComputeAndSaveCategorySortOrder([
							                                             'weights'       => $weights,
							                                             'timeInterval'  => $timeInterval,
							                                             'productTypeId' => $productTypeId,
							                                             'eventId'       => $eventId,
							                                             'cityId'        => $cityId
						                                             ]));
						$command->info("----------------------------------------------------------");
						$command->info("----------------------------------------------------------");
						$command->info(" ");
					}
				}
			}

		}
		else
		{
			$command->info("ERROR:: Unable to perform as time intervals haven't been declared properly");
			// todo: inform team
		}
	}
}