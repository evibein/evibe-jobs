<?php

namespace App\Evibe\Handlers\Actions;

use App\Models\Cake\Cake;
use App\Models\Decor;
use App\Models\Package;
use App\Models\Types\TypeCategoryTags;
use App\Models\Util\Tags;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\DB;

class UpdateCategoryTagsCommandHandler extends BaseCommandHandler
{
	public function updateCategoryTags($command, $options)
	{
		$now = time();

		// for 8 hours interval
		$lastCheckedAt = $now - 12 * 60 * 60;

		// for 4 PM and 10 PM
		if (((strtotime('today midnight') + 16 * 60 * 60) >= $now) && ((strtotime('today midnight') + 16 * 60 * 60) <= $now + 1 * 60))
		{
			$lastCheckedAt = $now - 18 * 60 * 60;
		}
		elseif (((strtotime('today midnight') + 22 * 60 * 60) >= $now) && ((strtotime('today midnight') + 22 * 60 * 60) <= $now + 1 * 60))
		{
			$lastCheckedAt = $now - 6 * 60 * 60;
		}

		if ($options['firstCatTags'])
		{
			$command->info("Running the command to update all the applicable category tags for the first time.");
			$lastCheckedAt = 0;
			TypeCategoryTags::query()->truncate(); // truncate table before logging table with fresh data
		}

		$command->info("Fetching tags from 'type_tags' table ...");
		$parentTagIds = Tags::whereNull('deleted_at')
		                    ->whereNotNull('parent_id')
		                    ->distinct('parent_id')
		                    ->pluck('parent_id')
		                    ->toArray();

		$filteredTags = Tags::withTrashed()
		                    ->whereNotIn('id', $parentTagIds)
		                    ->where('updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
		                    ->get();

		if (count($filteredTags))
		{
			$command->info("Updating/creating category tags in 'type_category_tags' table...");
			foreach ($filteredTags as $tag)
			{
				$catTag = TypeCategoryTags::where('type_tag_id', $tag->id)->whereNull('deleted_at')->first();

				if ($catTag)
				{
					if ($tag->deleted_at)
					{
						$command->info("Deleting type category tag with id " . $tag->id);
						$catTag->deleted_at = Carbon::now();
						$catTag->updated_at = Carbon::now();

						$catTag->save();
					}
					else
					{
						$command->info("Updating type category tag with id " . $tag->id);
						$catTag->info = $tag->info;
						$catTag->updated_at = Carbon::now();

						$catTag->save();
					}
				}
				else
				{
					$command->info("Creating type category tag with id " . $tag->id);
					TypeCategoryTags::create([
						                         'type_tag_id' => $tag->id,
						                         'info'        => $tag->info,
						                         'created_at'  => Carbon::now(),
						                         'updated_at'  => Carbon::now()
					                         ]);
				}

			}
		}
		else
		{
			$command->info("No new tags to update/create.");
		}

		// function to create/update type_category_tags related details
		$command->info("Fetching 'type_category_tags' for which, changes needs to be done ...");

		// for 8 hours interval
		$lastCheckedAt = $now - 12 * 60 * 60;

		// for 4 PM and 10 PM
		if (((strtotime('today midnight') + 16 * 60 * 60) >= $now) && ((strtotime('today midnight') + 16 * 60 * 60) <= $now + 1 * 60))
		{
			$lastCheckedAt = $now - 18 * 60 * 60;
		}
		elseif (((strtotime('today midnight') + 22 * 60 * 60) >= $now) && ((strtotime('today midnight') + 22 * 60 * 60) <= $now + 1 * 60))
		{
			$lastCheckedAt = $now - 6 * 60 * 60;
		}

		if ($options['allTagsInfo'])
		{
			$command->info("Running the command to update the details of all category tags for the first time.");
			$lastCheckedAt = 0;
		}

		$typeCategoryTagsArray = [];

		// fetch tag_id, map_type_id
		if ($lastCheckedAt == 0)
		{
			$typeCategoryTags = TypeCategoryTags::join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                                    ->select('type_category_tags.type_tag_id', DB::raw('type_tags.map_type_id AS map_type_id ,type_tags.parent_id AS parent_id'))
			                                    ->whereNull('type_category_tags.deleted_at')
			                                    ->whereNull('type_tags.deleted_at')
			                                    ->get();

		}
		else
		{
			// @see: do not change the order because while getting unique array, tags with 'map_type_id' = null may override
			// @see: tag should be updated, whether the option changes or the tag-option relation changes
			$packages = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
			                   ->select(DB::raw('planner_package_tags.tile_tag_id AS tag_id'))
			                   ->where('planner_package.is_live', 1)
			                   ->whereNull('planner_package.deleted_at')
			                   ->whereNull('planner_package_tags.deleted_at')
			                   ->where(function ($query) use ($lastCheckedAt)
			                   {
				                   $query->where('planner_package_tags.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
				                         ->orWhere('planner_package.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt));
			                   })
			                   ->distinct('planner_package_tags.tile_tag_id')
			                   ->get();

			if ($packages)
			{
				foreach ($packages as $package)
				{
					array_push($typeCategoryTagsArray, [
						'id'        => $package->tag_id,
						'mapTypeId' => config('evibe.ticket.type.packages') // generic - in place of type ticket id
					]);
				}
			}

			$cakes = Cake::Join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
			             ->select(DB::raw('cake_tags.tag_id AS tag_id'))
			             ->where('cake.is_live', 1)
			             ->whereNull('cake.deleted_at')
			             ->whereNull('cake_tags.deleted_at')
			             ->where(function ($query) use ($lastCheckedAt)
			             {
				             $query->where('cake_tags.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
				                   ->orWhere('cake.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt));
			             })
			             ->distinct('cake_tags.tag_id')
			             ->get();

			if ($cakes)
			{
				foreach ($cakes as $cake)
				{
					array_push($typeCategoryTagsArray, [
						'id'        => $cake->tag_id,
						'mapTypeId' => config('evibe.ticket.type.cakes')
					]);
				}
			}

			$decors = Decor::join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
			               ->select(DB::raw('decor_tags.tag_id AS tag_id'))
			               ->where('decor.is_live', 1)
			               ->whereNull('decor.deleted_at')
			               ->whereNull('decor_tags.deleted_at')
			               ->where(function ($query) use ($lastCheckedAt)
			               {
				               $query->where('decor_tags.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
				                     ->orWhere('decor.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt));
			               })
			               ->distinct('decor_tags.tag_id')
			               ->get();

			if ($decors)
			{
				foreach ($decors as $decor)
				{
					array_push($typeCategoryTagsArray, [
						'id'        => $decor->tag_id,
						'mapTypeId' => config('evibe.ticket.type.decors')
					]);
				}
			}

			// @see: tag should be in the update stack, if it has 'map_type_id' and has been recently updated
			$typeCategoryTags = TypeCategoryTags::join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                                    ->select('type_category_tags.type_tag_id', DB::raw('type_tags.map_type_id AS map_type_id ,type_tags.parent_id AS parent_id'))
			                                    ->whereNull('type_category_tags.deleted_at')
			                                    ->whereNull('type_tags.deleted_at')
			                                    ->where('type_category_tags.updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
			                                    ->get();
		}

		if ($typeCategoryTags)
		{
			foreach ($typeCategoryTags as $typeCat)
			{
				$mapTypeId = $typeCat->map_type_id;
				// if 'map_type_id' is not present, particularly for child tag
				if ((!$mapTypeId) && $typeCat->parent_id)
				{
					$command->info("Fetching 'map_type_id' from parent for tag " . $typeCat->type_tag_id);
					$parentTag = Tags::find($typeCat->parent_id);
					$mapTypeId = $parentTag->map_type_id;
					if(!$mapTypeId)
					{
						$command->info("'map_type_id' of parent tag is also 'null'");
					}
				}
				array_push($typeCategoryTagsArray, [
					'id'        => $typeCat->type_tag_id,
					'mapTypeId' => $mapTypeId
				]);
			}
		}

		// fetch unique tags
		$typeCategoryTagsArray = $this->uniqueCategoryTags($typeCategoryTagsArray, 'id');

		if (count($typeCategoryTagsArray))
		{
			foreach ($typeCategoryTagsArray as $tag)
			{
				$minPrice = 0;
				$maxPrice = 0;
				$count = 0;

				$command->info("****** typeTagId: " . $tag['id']);
				$command->info("****** mapTypeId: " . $tag['mapTypeId']);
				switch ($tag['mapTypeId'])
				{
					case config('evibe.ticket.type.packages'):
					case config('evibe.ticket.type.resort'):
					case config('evibe.ticket.type.villa'):
					case config('evibe.ticket.type.lounge'):
					case config('evibe.ticket.type.food'):
					case config('evibe.ticket.type.couple-experiences'):
					case config('evibe.ticket.type.venue-deals'):
					case config('evibe.ticket.type.priests'):
					case config('evibe.ticket.type.tents'):
					case config('evibe.ticket.type.generic-package'):
						$packages = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
						                   ->select('planner_package.*')
						                   ->where('planner_package_tags.tile_tag_id', $tag['id'])
						                   ->where('planner_package.is_live', 1)
						                   ->whereNull('planner_package.deleted_at')
						                   ->whereNull('planner_package_tags.deleted_at')
						                   ->get();

						$command->info("    found " . $packages->count() . " packages");
						$minPrice = $packages->min('price');
						$maxPrice = $packages->max('price');
						$count = $packages->count();
						break;

					case config('evibe.ticket.type.cakes'):
						$cakes = Cake::join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
						             ->select('cake.*')
						             ->where('cake_tags.tag_id', $tag['id'])
						             ->where('cake.is_live', 1)
						             ->whereNull('cake.deleted_at')
						             ->whereNull('cake_tags.deleted_at')
						             ->get();

						$command->info("    found " . $cakes->count() . " cakes");
						$minPrice = $cakes->min('price_per_kg');
						$maxPrice = $cakes->max('price_per_kg');
						$count = $cakes->count();
						break;

					case config('evibe.ticket.type.decors'):
						$decors = Decor::join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
						               ->select('decor.*')
						               ->where('decor_tags.tag_id', $tag['id'])
						               ->where('decor.is_live', 1)
						               ->whereNull('decor.deleted_at')
						               ->whereNull('decor_tags.deleted_at')
						               ->get();

						$command->info("    found " . $decors->count() . " decors");
						$minPrice = $decors->min('min_price');
						$maxPrice = $decors->max('min_price');
						$count = $decors->count();
						break;
				}

				$typeCatTag = TypeCategoryTags::where('type_tag_id', $tag['id'])->first();

				if ($typeCatTag)
				{
					$command->info('Updating details for type tag with id ' . $tag['id']);

					$typeCatTag->count = $count;
					$typeCatTag->min_price = $minPrice;
					$typeCatTag->max_price = $maxPrice;
					$typeCatTag->updated_at = Carbon::now();

					$typeCatTag->save();

				}
			}
		}
	}

	public function uniqueCategoryTags($array, $key)
	{
		$temp_array = array();
		$i = 0;
		$key_array = array();

		foreach($array as $val) {
			if (!in_array($val[$key], $key_array)) {
				$key_array[$i] = $val[$key];
				$temp_array[$i] = $val;
			}
			$i++;
		}
		return $temp_array;
	}
}