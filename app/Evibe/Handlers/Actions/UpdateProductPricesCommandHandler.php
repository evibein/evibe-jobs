<?php

namespace App\Evibe\Handlers\Actions;

use App\Jobs\Email\SendPriceRevisionUpdatesToPartnersJob;
use App\Models\Cake\Cake;
use App\Models\Decor;
use App\Models\Package;
use App\Models\Partner\PartnerAverageRating;
use App\Models\Service\ServiceEvent;
use App\Models\Trend;
use App\Models\TypeService;
use App\Models\Vendor;
use App\Models\Vendor\PlannerReview;
use App\Models\Venue;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class UpdateProductPricesCommandHandler extends BaseCommandHandler
{
	public function updatePrices($command, $options)
	{
		$startId = $options['startId'] ? $options['startId'] : 1;
		$category = $options['category'];

		if (in_array("decors", $category))
		{
			$this->updateDecorPrices($command, $startId);
		}
		elseif (in_array("services", $category))
		{
			$this->updateServicePrices($command, $startId);
		}
		elseif (in_array("packages", $category))
		{
			$this->updatePackagePrices($command, $startId);
		}
		elseif (in_array("venue_deals", $category))
		{
			$this->updateVenueDealPrices($command, $startId);
		}
		elseif (in_array("cakes", $category))
		{
			$this->updateCakePrices($command, $startId);
		}
		elseif (in_array("trends", $category))
		{
			$this->updateTrendPrices($command, $startId);
		}
		elseif (in_array("venues", $category))
		{
			$this->updateVenuePrices($command, $startId);
		}
		elseif (in_array("all", $category))
		{
			$this->updateDecorPrices($command, $startId);
			$this->updateServicePrices($command, $startId);
			$this->updatePackagePrices($command, $startId);
			$this->updateVenueDealPrices($command, $startId);
			$this->updateCakePrices($command, $startId);
			$this->updateTrendPrices($command, $startId);
			$this->updateVenuePrices($command, $startId);
		}
		else
		{
			$command->error("No matching category found");
		}
	}

	public function notifyPartners($command)
	{
		$planners = Vendor::where('is_live', 1)->get();
		if ($planners)
		{
			$command->info("..sending emails to planners");
			foreach ($planners as $planner)
			{
				$cc = $planner->alt_email;
				$cc = $cc ? [$cc] : [];
				$data = [
					'partnerName' => ucwords($planner->person),
					'to'          => $planner->email,
					'cc'          => $cc
				];

				dispatch(new SendPriceRevisionUpdatesToPartnersJob($data));
				$command->comment("...email dispatched to " . $planner->name);
			}
		}
		$command->info("..alerts sent to all planners");

		$venues = Venue::where('is_live', 1)->get();
		if ($venues)
		{
			$command->info("..sending emails to venues");
			foreach ($venues as $venue)
			{
				$cc = $venue->alt_email;
				$cc = $cc ? [$cc] : [];
				$data = [
					'partnerName' => $venue->person,
					'to'          => $venue->email,
					'cc'          => $cc
				];

				dispatch(new SendPriceRevisionUpdatesToPartnersJob($data));
				$command->comment("...email dispatched to " . $venue->name);
			}
		}
		$command->info("..alerts sent to all venues");
	}

	private function updateDecorPrices($command, $startId)
	{
		$decors = Decor::where('is_live', 1)
		               ->where("id", ">=", $startId)
		               ->get();
		if ($decors)
		{
			$command->info("..updating prices for decors, found " . $decors->count());
			foreach ($decors->chunk(100) as $chunk)
			{
				foreach ($chunk as $decor)
				{
					$decor->update([
						               'min_price' => $this->getUpdatedValue($decor->min_price, true),
						               'max_price' => $this->getUpdatedValue($decor->max_price, true),
						               'worth'     => $this->getUpdatedValue($decor->worth),
					               ]);

					$command->info("....decor prices updated for " . $decor->id);
				}
			}

			$command->info("all decor prices updated..");
		}
	}

	private function updateServicePrices($command, $startId)
	{
		$services = TypeService::where('is_live', 1)
		                       ->where("id", ">=", $startId)
		                       ->get();
		if ($services)
		{
			$command->info("..updating prices for services, found " . $services->count());
			foreach ($services->chunk(100) as $chunk)
			{
				foreach ($chunk as $service)
				{
					$service->update([
						                 'min_price'   => $this->getUpdatedValue($service->min_price, false, 5),
						                 'max_price'   => $this->getUpdatedValue($service->max_price, false, 5),
						                 'worth_price' => $this->getUpdatedValue($service->worth_ptice),
					                 ]);

					$command->info("....service prices updated for " . $service->id);
				}
			}

			$command->info("all service prices updated..");
		}
	}

	private function updatePackagePrices($command, $startId)
	{
		$packages = Package::where('is_live', 1)
		                   ->where("id", ">=", $startId)
		                   ->whereNotIn('type_ticket_id', [config('evibe.ticket.type.venue-deals')])
		                   ->get();

		if ($packages)
		{
			$command->info("..updating prices for packages, found " . $packages->count());
			foreach ($packages->chunk(100) as $chunk)
			{
				foreach ($chunk as $package)
				{
					// todo: revert back to normal
					if($package->type_ticket_id == config('evibe.ticket.type.food'))
					{
						$package->update([
							                 'price'                 => $this->getUpdatedValue($package->price, true, 50, 10),
							                 'price_max'             => $this->getUpdatedValue($package->price_max, true, 50, 10),
							                 'price_worth'           => $this->getUpdatedValue($package->price_worth, false, 50, 10),
							                 'price_per_extra_guest' => $this->getUpdatedValue($package->price_per_extra_guest, false, 5, 10),
						                 ]);

						$command->info("....[food] package prices updated for " . $package->id);
					}
					else
					{
						$package->update([
							                 'price'                 => $this->getUpdatedValue($package->price, true),
							                 'price_max'             => $this->getUpdatedValue($package->price_max, true),
							                 'price_worth'           => $this->getUpdatedValue($package->price_worth),
							                 'price_per_extra_guest' => $this->getUpdatedValue($package->price_per_extra_guest, false, 5),
						                 ]);

						$command->info("....package prices updated for " . $package->id);
					}
				}
			}

			$command->info("all package prices updated..");
		}
	}

	private function updateVenueDealPrices($command, $startId)
	{
		$venueDeals = Package::where('is_live', 1)
		                     ->where("id", ">=", $startId)
		                     ->where('type_ticket_id', config('evibe.ticket.type.venue-deals'))
		                     ->get();
		if ($venueDeals)
		{
			$command->info("..updating prices for venue deals, found " . $venueDeals->count());
			foreach ($venueDeals->chunk(100) as $chunk)
			{
				foreach ($chunk as $venueDeal)
				{
					$venueDeal->update([
						                   'price'                 => $this->getUpdatedValue($venueDeal->price, false, 5),
						                   'price_max'             => $this->getUpdatedValue($venueDeal->price_max, false, 5),
						                   'price_worth'           => $this->getUpdatedValue($venueDeal->price_worth),
						                   'price_per_extra_guest' => $this->getUpdatedValue($venueDeal->price_per_extra_guest, false, 5),
					                   ]);

					$command->info("....venue deal prices updated for " . $venueDeal->id);
				}
			}

			$command->info("all venue deal prices updated..");
		}
	}

	private function updateTrendPrices($command, $startId)
	{
		$trends = Trend::where('is_live', 1)
		               ->where("id", ">=", $startId)
		               ->get();
		if ($trends)
		{
			$command->info("..updating prices for trends, found " . $trends->count());
			foreach ($trends->chunk(100) as $chunk)
			{
				foreach ($chunk as $trend)
				{
					$trend->update([
						               'price'       => $this->getUpdatedValue($trend->price, true),
						               'price_max'   => $this->getUpdatedValue($trend->price_max, true),
						               'price_worth' => $this->getUpdatedValue($trend->price_worth),
					               ]);

					$command->info("....trend prices updated for " . $trend->id);
				}
			}

			$command->info("all trend prices updated..");
		}
	}

	private function updateCakePrices($command, $startId)
	{
		$cakes = Cake::where('is_live', 1)
		             ->where("id", ">=", $startId)
		             ->get();
		if ($cakes)
		{
			$command->info("..updating prices for cakes, found " . $cakes->count());
			foreach ($cakes->chunk(100) as $chunk)
			{
				foreach ($chunk as $cake)
				{
					$cake->update([
						              'price'        => $this->getUpdatedValue($cake->price, false, 5),
						              'price_per_kg' => $this->getUpdatedValue($cake->price_per_kg, false, 5),
						              'price_worth'  => $this->getUpdatedValue($cake->price_worth),
					              ]);

					$command->info("....cake prices updated for " . $cake->id);
				}
			}

			$command->info("all cake prices updated..");
		}
	}

	private function updateVenuePrices($command, $startId)
	{
		$venues = Venue::where('is_live', 1)
		               ->where("id", ">=", $startId)
		               ->get();
		if ($venues)
		{
			$command->info("..updating prices for venues, found " . $venues->count());
			foreach ($venues->chunk(100) as $chunk)
			{
				foreach ($chunk as $venue)
				{
					// @todo: need to verify the pricing and apply appropriate round off
					$venue->update([
						               'worth_rent'       => $this->getUpdatedValue($venue->worth_rent, true),
						               'price_min_rent'   => $this->getUpdatedValue($venue->price_min_rent, true),
						               'price_max_rent'   => $this->getUpdatedValue($venue->price_max_rent, true),
						               'price_kid'        => $this->getUpdatedValue($venue->price_kid, false, 5),
						               'price_adult'      => $this->getUpdatedValue($venue->price_adult, false, 5),
						               'worth_kid'        => $this->getUpdatedValue($venue->worth_kid),
						               'worth_adult'      => $this->getUpdatedValue($venue->worth_adult),
						               'price_min_veg'    => $this->getUpdatedValue($venue->price_min_veg, false, 5),
						               'min_veg_worth'    => $this->getUpdatedValue($venue->min_veg_worth),
						               'price_min_nonveg' => $this->getUpdatedValue($venue->price_min_nonveg, false, 5),
						               'min_nonveg_worth' => $this->getUpdatedValue($venue->min_nonveg_worth),
					               ]);

					$command->info("....venue prices updated for " . $venue->id);
				}
			}

			$command->info("all venue prices updated..");
		}
	}

	public function getUpdatedValue($value, $negate = false, $round = 50, $percentage = 5)
	{
		if ($value && $value > 0)
		{
			$value = (int)round($value + (int)$value * $percentage / 100, 0);
			$y = (int)round($value % $round, 0);
			if ($y != 0)
			{
				$value = $value + ($round - $y);
			}

			if ($negate)
			{
				$value--;
			}
		}

		return $value;
	}
}