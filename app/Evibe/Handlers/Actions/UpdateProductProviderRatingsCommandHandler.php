<?php

namespace App\Evibe\Handlers\Actions;

use App\Models\Partner\PartnerAverageRating;
use App\Models\Vendor\PlannerReview;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\DB;

class UpdateProductProviderRatingsCommandHandler extends BaseCommandHandler
{
	public function updateRatings($command, $options)
	{
		$now = time();
		$lastCheckedAt = $now - 12 * 60 * 60;
		if ($options['first'])
		{
			$command->info("Running the command to fetch all the review ratings so far.");
			$lastCheckedAt = 0;
			PartnerAverageRating::query()->truncate(); // truncate table before logging table with fresh data
		}

		$command->info("Fetching updated reviews from 'planner_review' table...");
		$updatedReviews = PlannerReview::select('map_id', 'map_type_id', DB::raw('CONCAT(map_id, "-", map_type_id) AS unique_partner'), DB::raw('AVG(rating) as avg_rating'), DB::raw('COUNT(*) as review_count'))
		                               ->groupBy('unique_partner')
		                               ->whereNotNull('map_id')
		                               ->whereNotNull('map_type_id')
		                               ->where('is_accepted', 1)
		                               ->where('updated_at', '>=', Carbon::createFromTimestamp($lastCheckedAt))
		                               ->get();

		if (count($updatedReviews))
		{
			$command->info("Updating/creating ratings in 'partner_average_rating' table...");
			foreach ($updatedReviews as $updatedReview)
			{
				$partnerAvgRating = PartnerAverageRating::where([
					                                                'partner_id'      => $updatedReview->map_id,
					                                                'partner_type_id' => $updatedReview->map_type_id
				                                                ])
				                                        ->first();
				if ($partnerAvgRating)
				{
					$newCount = (int)$partnerAvgRating->review_count + (int)$updatedReview->review_count;
					$newRating = (($partnerAvgRating->avg_rating * $partnerAvgRating->review_count) + ($updatedReview->avg_rating * $updatedReview->review_count)) / ($newCount);

					$partnerAvgRating->avg_rating = $newRating;
					$partnerAvgRating->review_count = $newCount;
					$partnerAvgRating->save();
				}
				else
				{
					PartnerAverageRating::create([
						                             'partner_id'      => $updatedReview->map_id,
						                             'partner_type_id' => $updatedReview->map_type_id,
						                             'avg_rating'      => $updatedReview->avg_rating,
						                             'review_count'    => $updatedReview->review_count
					                             ]);
				}
			}
		}
		else
		{
			$command->info("No new reviews to update/create.");
		}
	}
}