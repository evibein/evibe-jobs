<?php

namespace App\Evibe\Handlers\Actions;

use App\Models\Area;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class AutoFillPincodesHandler extends BaseCommandHandler
{
	public function AutoFillNullPincodes($command)
	{
		$nullPinCodes = Area::with("city")
		                    ->select('id', 'name', 'city_id')
		                    ->whereNull('zip_code')
		                    ->get();

		foreach ($nullPinCodes as $nullPinCode)
		{
			$res = $this->makeGoogleApiCall($command, $nullPinCode->name, $nullPinCode->city->name);

			if ($res == null)
			{
				$command->error("Unable to fetch pincode for " . $nullPinCode->name);
				continue;
			}

			$command->info("found pincode for " . $nullPinCode->name . ": " . $res);
			$nullPinCode->update(['zip_code' => $res]);
		}

		return true;
	}

	private function makeGoogleApiCall($command, $areaName, $cityName)
	{
		$command->comment("Initiating geocode API for area: " . $areaName);
		$formattedName = str_replace(' ', '', $areaName . ", " . $cityName);

		try
		{
			$url = "https://maps.googleapis.com/maps/api/geocode/json";
			$queryString = "address=$formattedName&region=.in&key=" . config('evibe.google.geocode_key');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url . '?' . $queryString);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$cResponse = (curl_exec($ch));
			curl_close($ch);
			$geoCodeApiResults1 = json_decode($cResponse, true);

			$nearAreaPinCode = null;

			if (($geoCodeApiResults1["status"] == "OK") && (isset($geoCodeApiResults1['results'])))
			{
				$areaLatitude = $geoCodeApiResults1['results'][0]['geometry']['location']['lat'];
				$areaLangitude = $geoCodeApiResults1['results'][0]['geometry']['location']['lng'];

				$url2 = "https://maps.googleapis.com/maps/api/geocode/json";
				$queryString2 = "address=$areaLatitude,$areaLangitude&region=.in&key=" . config('evibe.google.geocode_key');

				$ch2 = curl_init();
				curl_setopt($ch2, CURLOPT_URL, $url2 . '?' . $queryString2);
				curl_setopt($ch2, CURLOPT_HEADER, false);
				curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				$cResponse2 = (curl_exec($ch2));
				curl_close($ch2);
				$geoCodeApiResults2 = json_decode($cResponse2, true);

				if ($geoCodeApiResults2["status"] == "OK" && (isset($geoCodeApiResults2['results'])))
				{
					$addressOfArea = $geoCodeApiResults2['results'][0]['address_components'];

					foreach ($addressOfArea as $area)
					{
						if ($area["types"][0] == "postal_code")
						{
							$nearAreaPinCode = $area["long_name"];
							break;
						}
					}
				}

				return $nearAreaPinCode;

			}
		} catch (ClientException $e)
		{
			Log::error($e->getMessage());

			return false;
		}

	}
}