<?php

/**
 * @author Harish <harish.annavajjala@evibe.in>
 * @since  4 Mar 2015
 */

namespace Evibe\Handlers;

use App\Models\Ticket;
use App\Models\TicketUpdate;
use App\Models\Util\TypeCancellationReason;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class SendCancellationMailCommandHandler extends BaseCommandHandler
{
	public function sendCancellationMail()
	{
		$oneMinute = 60;
		$startTime = Carbon::now()->timestamp;
		$endTime = $startTime + $oneMinute - 1;

		$ticketsList = Ticket::where('status_id', 3)
		                     ->whereBetween('payment_deadline', [$startTime, $endTime])
		                     ->where('is_auto_cancellation_mail', 1)
		                     ->where('event_date', '>', Carbon::now()->timestamp)
		                     ->get();

		$reason = TypeCancellationReason::where('id', config('evibe.cancellation_reason_default_id'))->first();
		$reasonId = $reason->id;
		$cancellationReason = ucwords($reason->message);

		foreach ($ticketsList as $ticket)
		{
			$ccAddresses = [config('evibe.contact.customer.group')];
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			} // alt email in CC

			$paymentLink = config('evibe.live.checkout');
			$paymentLink .= "?id=" . $ticket->id;
			$paymentLink .= "&token=" . Hash::make($ticket->id);
			$paymentLink .= "&uKm8=" . Hash::make($ticket->email);

			$bookings = $ticket->bookings;
			$minBookTime = $bookings->min('party_date_time');
			$maxBookTime = $bookings->max('party_date_time');

			$partyDateTime = date("d M Y", $minBookTime);
			$shortLink = $this->getShortUrl($paymentLink);

			if ($minBookTime == $maxBookTime)
			{
				$partyDateTime = date("d M Y h:i A", $minBookTime);
			}
			elseif (date("d m y", $minBookTime) != date("d m y", $maxBookTime))
			{
				$partyDateTime = date("d/m/y", $minBookTime) . ' - ' . date("d/m/y", $maxBookTime);
			}

			$data = [
				'name'             => $ticket->name,
				'phone'            => $ticket->phone,
				'email'            => $ticket->email,
				'partyDate'        => $partyDateTime,
				'ccAddresses'      => $ccAddresses,
				'pgLink'           => $shortLink,
				'replyToAddresses' => config('evibe.contact.customer.group'),
				'reason'           => $cancellationReason
			];

			// send email & SMS to customer
			Queue::push('Evibe\Utilities\SendEmail@sendCancellationMailToCustomer', $data);
			Queue::push('Evibe\Utilities\SendSMS@smsCancellationToCustomer', $data);

			// send SMS to all partners
			foreach ($bookings as $booking)
			{
				$vendor = $booking->vendor;

				if ($vendor)
				{
					$vendorSmsData = [
						'phone'     => $vendor->phone,
						'name'      => $vendor->person,
						'customer'  => $ticket->name,
						'partyDate' => date("d-M-Y", $booking->party_date_time),
						'reason'    => $cancellationReason
					];

					$subject = "[Evibe.in] " . $vendor->person . ", order cancelled for " . $data['partyDate'];

					$vendorEmailData = [
						'to'               => [$vendor->email],
						'ccAddresses'      => $this->getPartnerCC($vendor),
						'replyToAddresses' => [config('evibe.contact.customer.group')],
						'subject'          => $subject,
						'customerName'     => ucfirst($ticket->name),
						'vendorName'       => $vendor->person,
						'partyDate'        => date("d-M-Y", $booking->party_date_time),
						'location'         => $ticket->area ? $ticket->area->name : '<i>--N/A--</i>',
						'reason'           => $cancellationReason
					];

					Queue::push('Evibe\Utilities\SendEmail@sendCancellationMailToVendor', $vendorEmailData);
					Queue::push('Evibe\Utilities\SendSMS@smsCancellationToVendor', $vendorSmsData);
				}
			}

			// update ticket status
			$ticket->update([
				                'type_cancellation_id' => $reasonId,
				                'updated_at'           => date('Y-m-d H:i:s'),
				                'status_id'            => config('evibe.ticket.status.cancelled')
			                ]);

			// insert data in ticket update table
			$updateData = [
				'ticket_id'   => $ticket->id,
				'status_id'   => $ticket->status_id,
				'comments'    => "Cancellation email sent to the customer & partners from jobs",
				'status_at'   => time(),
				'type_update' => "Auto"
			];

			TicketUpdate::create($updateData);

			try
			{
				// send push notification to CRM team
				$nTitle = "[" . Carbon::now()->format('h:i, d M') . "] Booking CANCELLED For $ticket->name's Party (Non Payment)";
				$nMsg = "Party Date: " . $partyDateTime .
					"; Phone: $ticket->phone" .
					"; Place: " . ($ticket->area ? $ticket->area->name : "--");
				$nUrl = config("evibe.dash_url") . "tickets/$ticket->id";
				$nButtons = json_encode([["id" => "ticket_details", "text" => "Show Ticket Details", "url" => $nUrl]]);
				$nData = [
					'title'   => $nTitle,
					'message' => $nMsg,
					'url'     => $nUrl,
					'buttons' => $nButtons
				];

				$this->sendWebPushNotification($nData);
			} catch (\Exception $e)
			{
				Log::error("Failed to send web push notification. Trace: " . $e->getTraceAsString());
			}

		}
	}

	private function getPartnerCC($vendor)
	{
		$vendorCc = [
			$vendor->email,
			config('evibe.contact.customer.group')
		];
		$checkFields = [
			'alt_email',
			'alt_email_1',
			'alt_email_2',
			'alt_email_3'
		];

		foreach ($checkFields as $checkField)
		{
			if ($vendor->$checkField)
			{
				array_push($vendorCc, $vendor->$checkField);
			}
		}

		return $vendorCc;
	}
}