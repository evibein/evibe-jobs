<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  2 Jan 2015
 */

namespace Evibe\Handlers;

use App\Models\LogScrapeError;
use App\Models\Venue;
use App\Models\VenueExtRatings;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\DomCrawler\Crawler;

class UpdateReviewsCommandHandler
{
	public $options = [];

	public function __construct($options = [])
	{
		$this->options = $options;
	}

	public function fetchLatestReviews()
	{
		$ids = $this->options['ids'];
		$typeIds = $this->options['type'];
		$logRatings = [];
		$venueExtRatings = VenueExtRatings::whereNull('deleted_at');

		// check if has options
		if ($ids)
		{
			$venueExtRatings->whereIn('venue_id', explode(",", $ids));
		}
		if ($typeIds)
		{
			$venueExtRatings->whereIn('type_ratings_website_id', explode(",", $typeIds));
		}
		$venueExtRatings = $venueExtRatings->get();

		foreach ($venueExtRatings as $rating)
		{
			$vendor = Venue::find($rating->venue_id);
			if (!$vendor)
			{
				Log::info("Error with finding venue with id: " . $rating->venue_id);
				continue;
			}

			$type = $rating->type_ratings_website_id;
			$url = $rating->url;

			// initialize with existing values
			$newRatings = [
				'rating_value' => $rating->rating_value,
				'rating_count' => $rating->rating_count
			];

			// check for invalid pages (404, 500 etc)
			try
			{
				$headers = get_headers($url);
				$statusCode = count($headers) ? substr($headers[0], 9, 3) : "404";
			} catch (\Exception $e)
			{
				Log::info("Could now fetch headers for $url");
				continue;
			}

			if ($statusCode != "200")
			{
				Log::info("Invalid URL for Venue: " . $rating->venue_id . ", Website type: " . $rating->type_ratings_website_id);
				continue;
			}

			$html = file_get_contents($url);
			$crawler = new Crawler($html);

			switch ($type)
			{
				case 1:
					// ZOMATO RATING
					$newRatings = $this->fetchZomatoRatings($rating, $crawler, $newRatings, $html);
					break;
				case 2:
					// BURRP RATING
					$newRatings = $this->fetchBurrpRatings($rating, $crawler, $newRatings, $html);
					break;
				case 3:
					$newRatings = $this->fetchTripAdvisorRatings($rating, $crawler, $newRatings, $html);
					break;
				case 4:
					// TIMESCITY
					$newRatings = $this->fetchTimesCityRatings($rating, $crawler, $newRatings, $html);
					break;
			}

			$logRatings[$vendor->name][] = [
				'sname' => array_key_exists('name', $newRatings) ? $newRatings['name'] : ' Name not found ',
				'type'  => $rating->type->name,
				'old'   => $rating->rating_value . ' (' . $rating->rating_count . ')',
				'new'   => $newRatings['rating_value'] . ' (' . $newRatings['rating_count'] . ')'
			];

			// push values
			$rating->rating_value = $newRatings['rating_value'];
			$rating->rating_count = $newRatings['rating_count'];
			$rating->updated_at = date('Y-m-d H:i:s');

			if (!$rating->save())
			{
				$this->logError($rating, 'save error');
			}

		}

		// send success email notification
		$data = ['logRatings' => $logRatings];
		Queue::push('Evibe\Utilities\SendEmail@mailScrapeSuccess', $data);

		return;
	}

	private function fetchZomatoRatings($rating, $crawler, $newRatings, $html)
	{
		$newRatings['name'] = '';
		$nameBox = $crawler->filter('span[itemprop="name"]');
		$ratingBox = $crawler->filter('div[itemprop="aggregateRating"]');

		if ($nameBox->count())
		{
			$newRatings['name'] = $nameBox->text();
		}

		if ($ratingBox->count())
		{
			$ratingValue = $ratingBox->first()->filter('div[itemprop="ratingValue"]');
			$ratingCount = $ratingBox->first()->filter('span[itemprop="ratingCount"]');

			if ($ratingValue->count() && $ratingCount->count())
			{
				$newRatings['rating_value'] = (float)trim($ratingValue->text());
				$newRatings['rating_count'] = $ratingCount->text();
			}
			else
			{
				$this->logError($rating, $html);
			}
		}

		return $newRatings;
	}

	private function fetchBurrpRatings($rating, $crawler, $newRatings, $html)
	{
		$newRatings['name'] = '';
		$nameBox = $crawler->filter('h1[itemprop="name"]');
		$ratingBox = $crawler->filter('li[itemtype="http://data-vocabulary.org/Rating"]');

		if ($nameBox->count())
		{
			$newRatings['name'] = $nameBox->text();
		}

		if ($ratingBox->count())
		{
			$ratingBox = $ratingBox->first();
			$ratingCount = $ratingBox->filter('[itemprop="ratingCount"]')->first();
			$ratingValue = $ratingBox->filter('span')->first();

			if ($ratingValue->count() && $ratingCount->count())
			{
				$newRatings['rating_value'] = (float)trim($ratingValue->text());
				$newRatings['rating_count'] = (int)trim($ratingCount->text());
			}
			else
			{
				$this->logError($rating, $html);
			}
		}

		return $newRatings;
	}

	private function fetchTripAdvisorRatings($rating, $crawler, $newRatings, $html)
	{
		$newRatings['name'] = '';
		$nameBox = $crawler->filter('h1[rel="v:name"]');
		$ratingValue = $crawler->filter('img[property="ratingValue"]');
		$ratingCount = $crawler->filter('a[property="reviewCount"]');

		if ($nameBox->count())
		{
			$newRatings['name'] = trim($nameBox->text());
		}

		if ($ratingValue->count() && $ratingCount->count())
		{
			$newRatings['rating_value'] = (float)trim($ratingValue->attr('content'));
			$newRatings['rating_count'] = $ratingCount->text();
		}
		else
		{
			$this->logError($rating, $html);
		}

		return $newRatings;
	}

	private function fetchTimesCityRatings($rating, $crawler, $newRatings, $html)
	{
		$ratingBox = $crawler->filter('div.bnnr_box');

		if ($ratingBox->count())
		{
			$newRatings['name'] = '';
			$nameBox = $ratingBox->filter('h1')->first();
			$ratingValue = $ratingBox->filter('div#rating_circle_div');
			$ratingCount = $ratingBox->filter('span.dflt_color');

			if ($nameBox->count())
			{
				$newRatings['name'] = $nameBox->attr('content');
			}

			if ($ratingValue->count() && $ratingCount->count())
			{
				$newRatings['rating_value'] = (float)trim($ratingValue->text());
				$newRatings['rating_count'] = (int)explode(" ", $ratingCount->text())[0];
			}
			else
			{
				$this->logError($rating, $html);
			}
		}

		return $newRatings;
	}

	private function logError($rating, $html)
	{
		// save to database
		LogScrapeError::create(['url' => $rating->url, 'venue_id' => $rating->venue_id, 'type_ratings_website_id' => $rating->type_ratings_website_id, 'html' => $html, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
	}

}