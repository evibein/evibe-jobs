<?php

namespace App\Evibe\Handlers\Actions;

use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class TestingJobHandler extends BaseCommandHandler
{
	public function requestToMainUsingGuzzleHTTP($command)
	{
		try
		{
			$url = "https://evibe.in";
			$client = new Client();
			$response = $client->request('GET', $url);
			$response = $response->getBody();
			//$response = \GuzzleHttp\json_decode($response, true);

			echo $response;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			echo $res["error"];
			echo "Error in http request \n";
		}

		echo "Jobs Working \n";
	}
}