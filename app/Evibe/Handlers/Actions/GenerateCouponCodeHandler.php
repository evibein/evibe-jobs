<?php

namespace App\Evibe\Handlers\Actions;

use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponMapping;
use Evibe\Handlers\BaseCommandHandler;

class GenerateCouponCodeHandler extends BaseCommandHandler
{
	public function GenerateCouponCodes($command, $options)
	{
		$couponType = $options["cid"];
		$count = isset($options["count"]) ? $options["count"] : 0;
		$couponDetails = [];

		if ($couponType == "FCP")
		{
			$command->info("generating coupons for freecharge paid users.. ");
			$couponDetails = [
				"couponPrefix"    => "FCPEVB",
				"discountPercent" => 10,
				"maxDiscount"     => 300,
				"description"     => "Promo code for paid users on Freecharge"
			];
		}
		elseif ($couponType == "FCF")
		{
			$command->info("generating coupons for freecharge free users.. ");
			$couponDetails = [
				"couponPrefix"    => "FCFEVB",
				"discountPercent" => 5,
				"maxDiscount"     => 200,
				"description"     => "Promo code for free users on Freecharge"
			];
		}
		elseif ($couponType == "VC")
		{
			$command->info("generating coupons for vantage circle users.. ");
			$couponDetails = [
				"couponPrefix"    => "VCEVB",
				"discountPercent" => 10,
				"maxDiscount"     => 250,
				"offerStartTime"  => "2018-05-10 00:00:00",
				"offerEndTime"    => "2018-06-30 23:59:59",
				"description"     => "Promo code for VantageCircle customers",
				"hasMapping"      => true,
				"cityIds"         => [config("evibe.city.bangalore"), config("evibe.city.hyderabad")]
			];
		}
		else
		{
			$command->error("invalid option, please try again");

			return false;
		}

		for ($i = 1; $i <= $count; $i++)
		{
			$couponId = $this->createCoupon($couponDetails, $command);
			$command->comment("    ...coupon #$i created with id $couponId");
		}

		return $command->info("All coupons created created successfully");
	}

	private function createCoupon($couponDetails, $command)
	{
		$couponCode = $this->generateCouponCode($couponDetails["couponPrefix"], 6);
		if (!$couponCode)
		{
			$command->error("ERROR! while creating coupon code");
		}

		$isMapping = isset($couponDetails["hasMapping"]) && $couponDetails["hasMapping"] ? true : false;
		$cityIds = isset($couponDetails["cityIds"]) && $couponDetails["cityIds"] ? $couponDetails["cityIds"] : [];

		$coupon = Coupon::create([
			                         "coupon_code"         => $couponCode,
			                         "max_usage_count"     => 1,
			                         "discount_percent"    => isset($couponDetails["discountPercent"]) ? $couponDetails["discountPercent"] : null,
			                         "max_discount_amount" => isset($couponDetails["maxDiscount"]) ? $couponDetails["maxDiscount"] : null,
			                         "offer_start_time"    => isset($couponDetails["offerStartTime"]) ? $couponDetails["offerStartTime"] : null,
			                         "offer_end_time"      => isset($couponDetails["offerEndTime"]) ? $couponDetails["offerEndTime"] : null,
			                         "description"         => isset($couponDetails["description"]) ? $couponDetails["description"] : null
		                         ]);

		if ($isMapping)
		{
			if (count($cityIds) > 0)
			{
				foreach ($cityIds as $cityId)
				{
					CouponMapping::create([
						                      "coupon_id"   => $coupon->id,
						                      "city_id"     => $cityId,
						                      "occasion_id" => config("evibe.event.special_experience")
					                      ]);

					$command->comment("        ...coupon mapping created for coupon " . $coupon->id);
				}
			}
		}

		return $coupon->id;
	}

	private function generateCouponCode($prefix, $stringLength)
	{
		$couponCode = false;

		while (true)
		{
			$randomString = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($stringLength / strlen($x)))), 1, $stringLength);
			$couponCode = $prefix . $randomString;

			$existingCoupons = Coupon::where('coupon_code', $couponCode)->get();

			if ($existingCoupons->count() == 0)
			{
				break;
			}
		}

		return $couponCode;
	}
}