<?php

namespace Evibe\Handlers;

use App\Jobs\Email\MailGenericErrorToTechTeamJob;
use App\Models\Package\PackageTag;
use App\Models\SiteErrorLog;
use App\Models\Util\Tags;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class BaseCommandHandler
{
	/**
	 * @author Anji <anji@evibe.in>
	 * @since  29 Oct 2016
	 *
	 * Using OneSignal (onesignal.com) to send web push notifications
	 */
	protected function sendWebPushNotification($data)
	{
		$notificationTitle = isset($data['title']) ? $data['title'] : "New Notification";
		$notificationMessage = isset($data['message']) ? $data['message'] : "New message from Evibe.in APIs.";
		$includedSegments = isset($data['segments']) ? $data['segments'] : array_merge(config("onesignal.segments.crm"), config("onesignal.segments.admins")); // default CRM
		$nUrl = isset($data['url']) ? $data['url'] : false;
		$nWebButtons = isset($data['buttons']) ? $data['buttons'] : false;

		// post request
		$headers = [
			"Content-Type"  => "application/json",
			"Authorization" => "Basic " . config("onesignal.ApiKey")
		];

		$payload = [
			"app_id"            => config("onesignal.appId"),
			"included_segments" => $includedSegments,
			"headings"          => [
				"en" => $notificationTitle
			],
			"contents"          => [
				"en" => $notificationMessage
			],
		];

		if ($nUrl)
		{
			$payload['url'] = $nUrl;
		}

		if ($nWebButtons)
		{
			$payload["web_buttons"] = $nWebButtons;
		}

		$oneSignalApi = config("onesignal.path") . "/notifications";
		$client = new Client();

		$response = $client->post($oneSignalApi, [
			"headers" => $headers,
			"json"    => $payload
		]);
	}

	public function getWebsiteLiveLink($eventId, $mapper, $mapTypeId, $cityUrl)
	{
		$fullUrl = '';

		if ($mapTypeId == config('evibe.ticket_type.packages'))
		{
			$fullUrl = $this->getPackageLiveLink($eventId, $mapper, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.venues'))
		{
			$fullUrl = $this->getVenueLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.services'))
		{
			$fullUrl = $this->getEntertainmentLiveLInk($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.decors'))
		{
			$fullUrl = $this->getDecorLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.cakes'))
		{
			$fullUrl = $this->getCakeLiveLink($eventId, $mapper->url, $cityUrl);
		}
		elseif ($mapTypeId == config('evibe.ticket_type.trends'))
		{
			$fullUrl = $this->getTrendLiveLink($eventId, $mapper->url, $cityUrl);
		}

		return $fullUrl;
	}

	private function getEntertainmentLiveLInk($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.main_url') . $cityUrl . '/';

		if ($eventId == config('evibe.event.bachelor_party'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.bachelor.entertainment');
		}
		elseif ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.entertainment');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.entertainment');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getDecorLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.main_url') . $cityUrl . '/';

		if ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.decors');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.decors');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getVenueLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.main_url') . $cityUrl . '/';

		if ($eventId == config('evibe.event.pre_post'))
		{
			$liveLink = $liveHost . config('evibe.live.profile_url.pre-post.venues');
		}
		else
		{
			$liveLink = $liveHost . config('evibe.live.profile_url.venues');
		}

		return $liveLink . '/' . $profileUrl;
	}

	private function getCakeLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.main_url') . $cityUrl . '/';

		if ($eventId == config('evibe.event.bachelor_party'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.bachelor.cakes');
		}
		elseif ($eventId == config('evibe.event.pre_post'))
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.pre-post.cakes');
		}
		else
		{
			$liveUrl = $liveHost . config('evibe.live.profile_url.cakes');
		}

		return $liveUrl . '/' . $profileUrl;
	}

	private function getTrendLiveLink($eventId, $profileUrl, $cityUrl)
	{
		$liveHost = config('evibe.main_url') . $cityUrl . '/';
		$liveUrl = $liveHost . config('evibe.live.profile_url.trends');

		return $liveUrl . '/' . $profileUrl;
	}

	private function getPackageLiveLink($eventId, $package, $cityUrl)
	{
		$packageUrl = $package->url;
		$liveHost = config('evibe.main_url') . $cityUrl . '/';
		$url = $liveHost . config('evibe.live.profile_url.packages');

		if ($eventId == config('evibe.event.kids_birthday'))
		{
			if ($this->checkPackageType($package, config('evibe.ticket_type.venue-deals')))
			{
				$url = $liveHost . config('evibe.live.profile_url.venue-deals');
			}
		}
		elseif ($eventId == config('evibe.event.bachelor_party'))
		{
			//resort
			if ($this->checkPackageType($package, config('evibe.ticket_type.resort')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.resorts');
			}
			//villa
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.villa')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.villas');
			}
			//lounge
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.lounge')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.lounges');
			}
			//food
			elseif ($this->checkPackageType($package, config('evibe.ticket_type.food')))
			{
				$url = $liveHost . config('evibe.live.profile_url.bachelor.food');
			}
		}
		elseif ($eventId == config('evibe.event.special_experience'))
		{
			$url = $liveHost . config('evibe.live.profile_url.couple_experience.packages');
		}

		return $url . '/' . $packageUrl;
	}

	public function checkPackageType($package, $checkForType)
	{
		$isType = false;
		$eventId = $package->event_id;

		$tagIds = Tags::where('type_event', $eventId)
		              ->where('map_type_id', $checkForType)
		              ->pluck('id');

		$packageTagObj = PackageTag::whereIn('tile_tag_id', $tagIds)
		                           ->where('planner_package_id', $package->id)
		                           ->get();

		if ($packageTagObj->count() > 0)
		{
			$isType = true;
		}

		return $isType;
	}

	public function getShortUrl($longUrl)
	{
		$shortUrl = $longUrl;

		if ($longUrl && strpos($longUrl, "goo.gl") === false && strpos($longUrl, "bit.ly") === false)
		{
			/* @see: disabling evib.es temporarily and implementing bit.ly
			$url = "http://evib.es/api/v2/action/shorten";
			$method = "POST";
			$accessToken = "";
			$jsonData = [
				'url'           => $longUrl,
				'key'           => config("evibe.evibes.access_token"),
				'custom_ending' => '',
				'is_secret'     => false,
				'response_type' => 'json'
			];

			try
			{
				$client = new Client();
				$res = $client->request($method, $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $jsonData,
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);
				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;

				$this->saveError([
					                 'fullUrl' => request()->fullUrl(),
					                 'message' => 'Error occurred in Base controller while doing guzzle request',
					                 'code'    => 0,
					                 'details' => $res['error']
				                 ]);

				return false;
			}
			 */

			try
			{
				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if(time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);
				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => 'Bitly Url Shortener',
						                                     'method'    => 'GET',
						                                     'message'   => '[BaseCommandHandler.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'exception' => '[BaseCommandHandler.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'trace'     => print_r($response, true),
						                                     'details'   => print_r($response, true)
					                                     ]);
				}

			} catch (\Exception $exception)
			{
				$this->sendAndSaveReport($exception);
			}
		}

		return $shortUrl;
	}

	protected function sendAndSaveReport($e, $sendEmail = true)
	{
		$code = $e->getCode() ? $e->getCode() : 'Error';

		$data = [
			'fullUrl' => 'Exception error',
			'code'    => $code,
			'message' => $e->getMessage(),
			'details' => $e->getTraceAsString()
		];

		if ($sendEmail)
		{
			$emailData = [
				'code'    => $code,
				'url'     => $data['fullUrl'],
				'method'  => '--',
				'message' => $data['message'],
				'trace'   => $data['details']
			];
			dispatch(new MailGenericErrorToTechTeamJob($emailData));
		}

		$this->saveError($data);
	}

	// @see: careful with keys 'message' and 'messages'
	protected function sendAndSaveNonExceptionReport($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : 'Non-exception error',
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : '--',
			'message'    => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];

		if ($sendEmail)
		{
			$emailData = [
				'code'    => $errorData['code'],
				'url'     => $errorData['url'],
				'method'  => $errorData['method'],
				'message' => $errorData['message'],
				'trace'   => $errorData['details']
			];

			dispatch(new MailGenericErrorToTechTeamJob($emailData));
		}

		$this->saveError($errorData);
	}

	public function saveError($data)
	{
		SiteErrorLog::create([
			                     'project_id' => config('evibe.project_id'),
			                     'url'        => isset($data['fullUrl']) && $data['fullUrl'] ? $data['fullUrl'] : (isset($data['url']) && $data['url'] ? $data['url'] : null),
			                     'exception'  => $data['message'],
			                     'code'       => $data['code'],
			                     'details'    => $data['details']
		                     ]);
	}
}