<?php

namespace Evibe\Handlers\Reminders;

use App\Models\LogTicketReminders;
use App\Models\Ticket;
use App\Models\TicketRemindersStack;
use App\Models\Types\TypeEvent;
use App\Models\User;
use App\Models\Util\CheckoutFieldValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class ThankYouCardReminderCommandHandler extends TicketRemindersCommandHandler
{
	public function sendReminder($command)
	{
		$now = time();
		$minTime = 0;
		$maxTime = $now;
		$timeId = null;

		// @see: make sure that the job is being triggered in the below timings
		/*
		 * Time-1: 9AM
		 * Time-2: 12PM
		 * Time-3: 3PM
		 * Time-4: 6PM
		 */

		// time slots
		if ((Carbon::createFromTimestamp($now)->toDateTimeString() > Carbon::createFromTimestamp($now)->startOfDay()->addHours(8)->toDateTimeString()) &&
			(Carbon::createFromTimestamp($now)->toDateTimeString() < Carbon::createFromTimestamp($now)->startOfDay()->addHours(10)->toDateTimeString())
		)
		{
			$minTime = Carbon::createFromTimestamp($now)->startOfDay()->subDays(1)->toDateTimeString();
			$maxTime = Carbon::createFromTimestamp($now)->startOfDay()->subDays(1)->endOfDay()->toDateTimeString();
			$timeId = 1;
		}
		elseif ((Carbon::createFromTimestamp($now)->toDateTimeString() > Carbon::createFromTimestamp($now)->startOfDay()->addHours(11)->toDateTimeString()) &&
			(Carbon::createFromTimestamp($now)->toDateTimeString() < Carbon::createFromTimestamp($now)->startOfDay()->addHours(13)->toDateTimeString())
		)
		{
			$minTime = Carbon::createFromTimestamp($now)->startOfDay()->subDays(2)->addHours(12)->toDateTimeString();
			$maxTime = Carbon::createFromTimestamp($now)->startOfDay()->subDays(2)->endOfDay()->toDateTimeString();
			$timeId = 2;
		}
		elseif ((Carbon::createFromTimestamp($now)->toDateTimeString() > Carbon::createFromTimestamp($now)->startOfDay()->addHours(14)->addMinutes(30)->toDateTimeString()) &&
			(Carbon::createFromTimestamp($now)->toDateTimeString() < Carbon::createFromTimestamp($now)->startOfDay()->addHours(16)->toDateTimeString())
		)
		{
			$minTime = Carbon::createFromTimestamp($now)->startOfDay()->addDays(1)->toDateTimeString();
			$maxTime = Carbon::createFromTimestamp($now)->startOfDay()->addDays(1)->endOfDay()->toDateTimeString();
			$timeId = 3;
		}
		elseif ((Carbon::createFromTimestamp($now)->toDateTimeString() > Carbon::createFromTimestamp($now)->startOfDay()->addHours(17)->toDateTimeString()) &&
			(Carbon::createFromTimestamp($now)->toDateTimeString() < Carbon::createFromTimestamp($now)->startOfDay()->addHours(19)->toDateTimeString())
		)
		{
			$minTime = Carbon::createFromTimestamp($now)->startOfDay()->toDateTimeString();
			$maxTime = Carbon::createFromTimestamp($now)->startOfDay()->addHours(12)->subSeconds(1)->toDateTimeString();
			$timeId = 4;
		}

		// valid event ids
		$validEvents = [
			config('evibe.event.kids_birthday'),
			config('evibe.event.first_birthday'),
			config('evibe.event.birthday_2-5'),
			config('evibe.event.birthday_6-12'),
			config('evibe.event.naming_ceremony'),
			config('evibe.event.baby_shower')
		];

		// find all tickets
		$tickets = Ticket::where('event_date', '>=', strtotime($minTime))
		                 ->where('event_date', '<=', strtotime($maxTime))
		                 ->where('status_id', config('evibe.ticket.status.booked'))
		                 ->where('e_thank_you_card', 1)
		                 ->whereNotNull('paid_at')
		                 ->whereNull('deleted_at')
		                 ->whereIn('event_id', $validEvents)
		                 ->get();

		if (count($tickets))
		{
			foreach ($tickets as $ticket)
			{
				/*
				 * Thank you card reminders logic:
				 * P = party date
				 * if(P < 12)
				 * {
				 *      reminder-1: P-1, 3 PM;
				 *      reminder-2: P, 6 PM;
				 *      reminder-3: P+1, 9 AM;
				 * }
				 * elseif(P >= 12)
				 * {
				 *      reminder-1: P-1, 3 PM;
				 *      reminder-2: P+1, 9 AM;
				 *      reminder-3: P+2, 12 PM;
				 * }
				 */

				$nextReminderAt = null;
				$remindersCount = 3; // total 3 reminders will be sent (includes current reminder also)
				$partyDateTime = Carbon::createFromTimestamp($ticket->event_date)->toDateTimeString();
				if ($timeId == 1)
				{
					$yesterdayNoon = Carbon::createFromTimestamp(time())->startOfDay()->subDays(1)->addHours(12)->toDateTimeString();
					if ($partyDateTime < $yesterdayNoon)
					{
						$nextReminderAt = null;
						$remindersCount = 1;
					}
					else
					{
						// P >= 12
						// Next reminder: P+2 - 12 PM
						$nextReminderAt = strtotime(Carbon::createFromTimestamp(time())->startOfDay()->addDays(1)->addHours(12)->toDateTimeString());
						$remindersCount = 2;
					}
				}
				elseif ($timeId == 2)
				{
					$nextReminderAt = null;
					$remindersCount = 1;
				}
				elseif ($timeId == 3)
				{
					$tomorrowNoon = Carbon::createFromTimestamp(time())->startOfDay()->addDays(1)->addHours(12)->toDateTimeString();
					if ($partyDateTime < $tomorrowNoon)
					{
						// P < 12
						// Next reminder: P - 6 PM
						$nextReminderAt = strtotime(Carbon::createFromTimestamp(time())->startOfDay()->addDays(1)->addHours(18)->toDateTimeString());
						$remindersCount = 3;
					}
					else
					{
						// P >= 12
						// Next reminder: P+1 - 9 AM
						$nextReminderAt = strtotime(Carbon::createFromTimestamp(time())->startOfDay()->addDays(2)->addHours(9)->toDateTimeString());
						$remindersCount = 3;
					}
				}
				elseif ($timeId == 4)
				{
					// p < 12
					// Next reminder: P+1 - 9 AM
					$nextReminderAt = strtotime(Carbon::createFromTimestamp(time())->startOfDay()->addDays(1)->addHours(9)->toDateTimeString());
					$remindersCount = 2;

				}

				$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc1'); // default
				// todo: after SMS are approved
				switch ($remindersCount)
				{
					case 3:
						$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc1');
						break;
					case 2:
						$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc2');
						break;
					case 1:
						$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc3');
						break;
					default:
						// let typeReminderGroupId be the same
						break;
				}

				// check if ticket reminder exists
				$ticketReminder = TicketRemindersStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
				                                      ->join('type_reminder', 'type_reminder.id', '=', 'type_reminder_group.type_reminder_id')
				                                      ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
				                                      ->where('type_reminder.id', config('reminder.thank-you-card.id'))
				                                      ->where('ticket_reminders_stack.ticket_id', $ticket->id)
				                                      ->whereNull('ticket_reminders_stack.deleted_at')
				                                      ->first();

				if ($ticketReminder)
				{
					$typeReminderId = $ticketReminder->type_reminder_id;
					if ($ticketReminder->terminated_at || $ticketReminder->invalid_at)
					{
						continue;
					}
				}
				else
				{
					// defaults todo: check
					//$remindersCount = 3;
					//$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc1');

					$typeReminderId = config('reminder.thank-you-card.id');
					$ticketReminder = TicketRemindersStack::create([
						                                               'type_reminder_group_id' => $typeReminderGroupId,
						                                               'ticket_id'              => $ticket->id,
						                                               'next_reminder_at'       => $nextReminderAt,
						                                               'count'                  => $remindersCount, // only 3 reminders per ticket
						                                               'start_time'             => $now,
						                                               'end_time'               => $now + ($remindersCount * 24 * 60 * 60) + (60 * 60), // add 1 hr buffer
						                                               'created_at'             => Carbon::now(),
						                                               'updated_at'             => Carbon::now()
					                                               ]);
				}

				$thankYouCards = Ticket\TicketThankYouCards::where('ticket_id', $ticket->id)
				                                           ->whereNull('deleted_at')
				                                           ->orderBy('updated_at', 'DESC')
				                                           ->get();

				if (count($thankYouCards))
				{
					$thankYouCard = $thankYouCards->first();
				}
				else
				{
					// @see: image should be of type [.png] only
					if ($ticket->event_id == config('evibe.event.naming_ceremony'))
					{
						$img = Image::make(config('evibe.gallery.root') . 'img/app/tyc_nc.png');
					}
					elseif($ticket->event_id == config('evibe.event.baby_shower'))
					{
						$img = Image::make(config('evibe.gallery.root') . 'img/app/tyc_bs.png');
					}
					else
					{
						$img = Image::make(config('evibe.gallery.root') . 'img/app/tyc_1.png');
					}

					$imgWidth = $img->width();
					$imgHeight = $img->height();

					$babyCheckoutField = CheckoutFieldValue::join('checkout_field', 'checkout_field.id', '=', 'ticket_checkout_field_value.checkout_field_id')
					                                       ->select('ticket_checkout_field_value.*')
					                                       ->where('ticket_checkout_field_value.ticket_id', $ticket->id)
					                                       ->where('checkout_field.name', 'like', '%baby_name%')
					                                       ->whereNull('ticket_checkout_field_value.deleted_at')
					                                       ->first();
					$babyName = $babyCheckoutField ? $babyCheckoutField->value : null;

					// @see: Baby Shower doesn't have baby name
					// either get baby name (or) get first word(>4) of customer name and family
					$tyText = null;
					if ($babyName && $babyName != 'n/a' && $babyName != 'N/A')
					{
						$tyText = ucwords(strtolower($babyName));
					}
					else
					{
						$customerName = $ticket->name;
						$customerNameArray = explode('.', $customerName);
						foreach ($customerNameArray as $key => $item)
						{
							$subItems = explode(' ', $item);
							foreach ($subItems as $string)
							{
								if (strlen($string) >= 4)
								{
									$tyText = trim(ucfirst($string)) . ' & Family';
									break;
								}
							}

							if ($tyText)
							{
								break;
							}
						}
					}

					// @todo: based on text, adjust font size and position
					// @todo: positions and font options from config
					$img->text($tyText, ($imgWidth / 2), ($imgHeight * 0.68) + 20, function ($font) {
						$font->file(public_path('fonts/LittleBird.ttf'));
						$font->size(60);
						$font->color('#97F3FF');
						$font->align('center');
						$font->valign('top');
					});

					$img->text('With Love', ($imgWidth / 2), ($imgHeight * 0.68) - 18, function ($font) {
						$font->file(public_path('fonts/Birds of Paradise.ttf'));
						$font->size(24);
						$font->color('#ED3E72');
						$font->align('center');
						$font->valign('top');
					});

					// @see: need to change title appropriately after confirmation
					$imgTitle = 'thankyou-' . time();
					$imgUrl = $imgTitle . '.png';

					// save the image
					$newImgPath = config('evibe.gallery.root') . 'ticket/' . $ticket->id . '/thank-you-cards/';
					if (!file_exists($newImgPath))
					{
						$oldumask = umask(0);
						mkdir($newImgPath, 0777, true);
						umask($oldumask);
					}

					$img->save($newImgPath . $imgUrl);

					// create and save thumbnail
					$thumbPath = $newImgPath . 'thumbs/';
					if (!file_exists($thumbPath))
					{
						$oldumask = umask(0);
						mkdir($thumbPath, 0777, true);
						umask($oldumask);
					}

					$img->resize(200, 200, function ($constraint) {
						$constraint->aspectRatio();
						$constraint->upsize();
					});
					$img->save($thumbPath . $imgUrl);

					$thankYouCard = Ticket\TicketThankYouCards::create([
						                                                   'ticket_id'  => $ticket->id,
						                                                   'title'      => $imgTitle,
						                                                   'url'        => $imgUrl,
						                                                   'created_at' => Carbon::now(),
						                                                   'updated_at' => Carbon::now()
					                                                   ]);
				}

				$TYCUrl = config('evibe.gallery.host') . '/ticket/' . $ticket->id . '/thank-you-cards/' . $thankYouCard->url;

				// send email
				// send sms
				$handler = $ticket->handler_id ? User::find($ticket->handler_id) : null;
				$handlerName = $handler ? $handler->name : 'Team Evibe.in';
				$event = TypeEvent::find($ticket->event_id);
				$thankYouCardLink = config('evibe.main_url') . 'my/thank-you-card?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id);

				$emailTYCLink = $this->campaignUrlForLink($thankYouCardLink . "&ref=email", $ticketReminder, config('reminder.communication.email'), $typeReminderId);
				$smsTYCLink = $this->campaignUrlForLink($thankYouCardLink . "&ref=sms", $ticketReminder, config('reminder.communication.sms'), $typeReminderId);
				$smsTYCLink = $this->getShortUrl($smsTYCLink);

				$data = [
					'customerName'    => $ticket->name ? ucwords($ticket->name) : 'Customer',
					'phone'           => $ticket->phone,
					'email'           => $ticket->email,
					'altPhone'        => $ticket->alt_phone,
					'altEmail'        => $ticket->alt_email,
					'eventName'       => $event ? $event->name : 'occasion',
					'partyDate'       => date('d/m/y', $ticket->event_date),
					'partyDateString' => $ticket->event_date,
					'evibePhone'      => config('evibe.phone'),
					'evibePhonePlain' => config('evibe.phone_plain'),
					'emailTYCLink'    => $emailTYCLink,
					'smsTYCLink'      => $smsTYCLink,
					'TYCUrl'          => $TYCUrl,
					'handlerName'     => $handlerName,
					'handlerPhone'    => $handler ? $handler->phone : config('evibe.phone_plain'),
					'from'            => config('evibe.contact.invitations.group'),
					'fromText'        => 'Evibe Invites',
					'replyTo'         => config('evibe.contact.invitations.group')
				];

				$comments = 'Thank you card reminder';
				$reminderGroupData = config('reminder.thank-you-card.group.' . $typeReminderGroupId);
				if (!$reminderGroupData)
				{
					// defaults
					$typeReminderGroupId = config('evibe.type_reminder_group.thank-you-card.tc1');
					$reminderGroupData = config('reminder.thank-you-card.group.' . $typeReminderGroupId);
				}

				$data = array_merge($data, $reminderGroupData);

				$mailSubReplaces = [
					'{eventName}' => $data['eventName']
				];

				$smsTextReplaces = [
					'#customerName#' => $data['customerName'],
					'#partyDate#'    => $data['partyDate'],
					'#eventName#'    => $data['eventName'],
					'#smsTYCLink#'   => $data['smsTYCLink'],
					'#evibePhone#'   => $data['evibePhonePlain']
				];

				$data['emailSub'] = str_replace(array_keys($mailSubReplaces), array_values($mailSubReplaces), $data['emailSub']);
				$data['smsText'] = str_replace(array_keys($smsTextReplaces), array_values($smsTextReplaces), $data['smsTpl']);

				$smsData = [
					'to'   => $data['phone'],
					'text' => $data['smsText']
				];

				$reminderEmail = false;
				$reminderSMS = false;
				$reminderNotification = false;

				// dispatch reminders
				$remResponse = $this->dispatchReminders($data, $smsData, $notificationData = null);
				$reminderEmail = isset($remResponse['reminderEmail']) ? $remResponse['reminderEmail'] : $reminderEmail;
				$reminderSMS = isset($remResponse['reminderSMS']) ? $remResponse['reminderSMS'] : $reminderSMS;
				$reminderNotification = isset($remResponse['reminderNotification']) ? $remResponse['reminderNotification'] : $reminderNotification;

				// log reminder
				LogTicketReminders::create([
					                           'ticket_id'              => $ticket->id,
					                           'type_reminder_group_id' => $ticketReminder->type_reminder_group_id,
					                           'comments'               => $comments ?: null,
					                           'email_sent_at'          => $reminderEmail ? time() : null,
					                           'sms_sent_at'            => $reminderSMS ? time() : null,
					                           'notification_sent_at'   => $reminderNotification ? time() : null,
					                           'created_at'             => Carbon::now(),
					                           'updated_at'             => Carbon::now()
				                           ]);

				// reduce next reminders count (@see: important)
				$remindersCount--;
				$ticketReminder->count = $remindersCount;

				// update next reminder
				if ($nextReminderAt)
				{
					$ticketReminder->next_reminder_at = $nextReminderAt;
				}
				else
				{
					$ticketReminder->invalid_at = time();
					$ticketReminder->stop_message = 'Ticket stack row has completed sending all the thank you card reminders';
				}

				$ticketReminder->updated_at = Carbon::now();
				$ticketReminder->save();
			}
		}
	}
}