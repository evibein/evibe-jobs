<?php

namespace App\Evibe\Handlers\Reminders;

use App\Jobs\Email\MailRSVPResponseRemindersToCustomer;
use App\Jobs\SMS\SMSTicketReminderToCustomer;
use App\Models\Ticket;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;

class RSVPAlertToCustomerCommandHandler extends BaseCommandHandler
{
	public function sendReminders()
	{
		// daily reminder at 8 PM
		$now = time();
		$startOfTomorrow = Carbon::createFromTimestamp($now)->startOfDay()->addDays(1)->toDateTimeString();
		$maxLimit = strtotime($startOfTomorrow);

		// fetch tickets
		// all other validations are taken care when asking for invitation card
		$tickets = Ticket::where('e_invite', 1)
		                 ->whereNull('deleted_at')
		                 ->where('event_date', '>=', $maxLimit)
		                 ->get();

		$responseStartTime = Carbon::createFromTimestamp($now)->startOfDay()->subDays(1)->addHours('20')->toDateTimeString();
		$responseEndTime = Carbon::createFromTimestamp($now)->startOfDay()->addHours(20)->subSeconds(1)->toDateTimeString();
		if (count($tickets))
		{
			foreach ($tickets as $ticket)
			{
				// validate if any invite has been sent or not
				$ticketInvite = Ticket\TicketInvite::where('ticket_id', $ticket->id)
				                                   ->whereNotNull('rsvp_link')
				                                   ->whereNotNull('invite_sent_at')
				                                   ->orderBy('invite_sent_at', 'DESC')
				                                   ->orderBy('updated_at', 'DESC')
				                                   ->first();

				if ($ticketInvite)
				{
					$ticketRSVPs = Ticket\TicketRSVP::where('ticket_id', $ticket->id)
					                                ->whereNull('deleted_at')
					                                ->get();
					$totalRSVPsCount = count($ticketRSVPs);

					$createdTicketRSVPsCount = $ticketRSVPs->filter(function ($item) use ($responseStartTime, $responseEndTime) {
						return $item->created_at >= $responseStartTime &&
							$item->created_at <= $responseEndTime;
					})->count();

					$updatedTicketRSVPsCount = $ticketRSVPs->filter(function ($item) use ($responseStartTime, $responseEndTime) {
						return $item->created_at < $responseStartTime &&
							$item->updated_at >= $responseStartTime &&
							$item->updated_at <= $responseEndTime;
					})->count();

					$customerName = $ticket->name ? ucwords($ticket->name) : 'Customer';
					if ($createdTicketRSVPsCount)
					{
						$mailSubject = $customerName . ', you have ' . $createdTicketRSVPsCount . ' new RSVP response(s)';
					}
					else
					{
						$mailSubject = $customerName . ', checkout RSVP responses for your party';
					}
					// todo: change subject and matter based on time line

					// alert customer
					$emailData = [
						'email'            => $ticket->email,
						'customerName'     => $customerName,
						'mailView'         => 'emails.reminder.rsvp-response',
						'from'             => config('evibe.email'),
						'fromText'         => 'Team Evibe.in',
						'emailSub'         => $mailSubject,
						'newRSVPCount'     => $createdTicketRSVPsCount,
						'updatedRSVPCount' => $updatedTicketRSVPsCount,
						'totalRSVPCount'   => $totalRSVPsCount,
						'RSVPLink'         => $ticketInvite->rsvp_link,
						'RSVPImage'        => config('evibe.gallery.host') . '/ticket/' . $ticket->id . '/invites/' . $ticketInvite->url
					];

					$smsTpl = config('evibe.sms_tpl.rsvp-responses');
					$smsTextReplaces = [
						'#customerName#' => $customerName,
						'#RSVPLink#'     => $ticketInvite->rsvp_link,
					];
					$smsText =
					$data['smsText'] = str_replace(array_keys($smsTextReplaces), array_values($smsTextReplaces), $smsTpl);

					$smsData = [
						'to'   => $ticket->phone,
						'text' => $smsText
					];

					if($createdTicketRSVPsCount)
					{
						dispatch(new MailRSVPResponseRemindersToCustomer($emailData));
						dispatch(new SMSTicketReminderToCustomer($smsData));
					}
				}
			}
		}
		// if new responses are available, dynamic reminder
		// else, stock reminder
	}
}