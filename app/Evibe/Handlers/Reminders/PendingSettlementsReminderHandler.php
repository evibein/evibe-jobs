<?php

namespace Evibe\Handlers\Reminders;

use App\Models\Settlements;
use Evibe\Utilities\EvibeUtil;
use Illuminate\Support\Facades\Mail;

class PendingSettlementsReminderHandler
{
	public function sendReminder()
	{
		$settlementsList = [];
		$settlements = Settlements::whereNull('settlement_done_at')
		                          ->whereNull('deleted_at')
		                          ->get();

		if (count($settlements))
		{
			$evibeUtil = new EvibeUtil();
			foreach ($settlements as $settlement)
			{
				$partner = $settlement->partner;

				array_push($settlementsList, [
					'settlementId'       => $settlement->id,
					'partnerId'          => $settlement->partner_type_id,
					'partnerTypeId'      => $settlement->partner_id,
					'partnerName'        => $partner ? $partner->person : null,
					'partnerCompanyName' => $partner ? $partner->name : null,
					'settlementAmount'   => $evibeUtil->formatPrice($settlement->settlement_amount)
				]);
			}
		}

		$data = [
			'settlementsList'     => $settlementsList,
			'sub'                 => 'Pending partner settlements that needs to be settled',
			'settlementsDashLink' => config('evibe.dash_url') . 'finance/settlements'
		];

		if (count($settlementsList))
		{
			Mail::send("emails.reminder.finance.pending-settlements-team", ['data' => $data], function ($mail) use ($data)
			{
				$mail->from(config('evibe.email'), 'Team Evibe.in');
				$mail->to(config('evibe.accounts_email'));
				$mail->subject($data['sub']);
			});
		}
	}
}