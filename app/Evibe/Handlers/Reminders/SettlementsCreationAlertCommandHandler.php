<?php

namespace Evibe\Handlers\Reminders;

use Illuminate\Support\Facades\Mail;

class SettlementsCreationAlertCommandHandler
{
	public function sendReminder()
	{
		$data = [
			'sub' => 'Settlement Creation Alert'
		];

		Mail::send("emails.reminder.finance.settlement-creation-alert-team", ['data' => $data], function ($mail) use ($data)
		{
			$mail->from(config('evibe.email'), 'Team Evibe.in');
			$mail->to(config('evibe.accounts_email'));
			$mail->subject($data['sub']);
		});

	}
}