<?php

namespace Evibe\Handlers;

use App\Models\TicketBooking;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class EventRemindersTeamHandler
{
	// sending reminder for tomorrow pre 12 events
	public function sendRemindersPre12($name)
	{
		$halfDay = 12 * 60 * 60;
		$startTime = Carbon::tomorrow()->startOfDay()->timestamp;
		$endTime = $startTime + $halfDay - 1;
		$timeRange = [$startTime, $endTime];
		$greetings = 'Here are the list of all events happening tomorrow before 12 PM. Followup with all the vendors to ensure 100% guarantee.';

		$data = [
			'timeRange' => $timeRange,
			'greetings' => $greetings,
			'subject'   => 'Events happening tomorrow before 12 PM - ' . date("D, jS M", Carbon::tomorrow()->timestamp)
		];

		$this->sendReminders($data);
	}

	// sending reminders for today post 12 events
	public function sendRemindersPost12($name)
	{
		$halfDay = 12 * 60 * 60;
		$startTime = Carbon::today()->startOfDay()->timestamp + $halfDay;
		$endTime = $startTime + $halfDay - 1;
		$timeRange = [$startTime, $endTime];
		$greetings = 'Here are the list of all events happening today after 12 PM . Followup with all the vendors to ensure 100 % guarantee . ';

		$data = [
			'timeRange' => $timeRange,
			'greetings' => $greetings,
			'subject'   => 'Events happening today after 12 PM - ' . date("D, jS M")
		];

		$this->sendReminders($data);
	}

	// sending reminders for Friday, Saturday, Sunday on Friday
	public function sendRemindersAtWeekends($name)
	{
		if (Carbon::today()->isFriday())
		{
			$fullDay = 24 * 60 * 60;
			$startTime = Carbon::today()->startOfDay()->timestamp;
			$endTime = $startTime + 3 * ($fullDay) - 1;
			$greetings = 'Here are the list of all events happening this weekends(including today). Followup with all the vendors to ensure 100 % guarantee . ';
			$timeRange = [$startTime, $endTime];

			$data = [
				'timeRange' => $timeRange,
				'greetings' => $greetings,
				'subject'   => 'Events happening this weekends (including today)'
			];

			$this->sendReminders($data, true);
		}
	}

	// send Event reminder to team for after office hours party
	public function sendAfterOfficeHoursPartyReminders()
	{
		$startTime = Carbon::today()->startOfDay()->addHour('21')->timestamp;
		$endTime = Carbon::tomorrow()->startOfDay()->addHour('11')->timestamp;
		$timeRange = [$startTime, $endTime];
		$greetings = 'Here are the list of all events happening between today 9:00 PM to tomorrow 11:00 AM. Followup with all the vendors to ensure 100% guarantee.';

		$data = [
			'timeRange' => $timeRange,
			'greetings' => $greetings,
			'subject'   => '[Evibe.in] Events happening between ' . date('d/m/y, h:i A', $startTime) . ' to ' . date('d/m/y, h:i A', $endTime)
		];

		$this->sendReminders($data);
	}

	private function sendReminders($data, $isWeekend = false)
	{
		$emailData = [
			'to'   => [],
			'list' => []
		];

		$bookings = TicketBooking::with('ticket', 'vendor', 'area', 'bookingConcept')
		                         ->join('ticket', 'ticket_bookings.ticket_id', ' = ', 'ticket.id')
		                         ->select('ticket_bookings.*')
		                         ->where('ticket_bookings.is_advance_paid', 1)
		                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                         ->whereBetween('ticket_bookings.party_date_time', $data['timeRange'])
		                         ->orderBy('ticket_bookings.party_date_time')
		                         ->get();
		foreach ($bookings as $booking)
		{
			$ticket = $booking->ticket;
			$vendor = $booking->vendor;

			if (!$ticket || !$vendor)
			{
				if (!$ticket)
				{
					Log::info("--- Reminder email skipped because ticket is not available for booking id " . $booking->id . "---");
				}
				if (!$vendor && $ticket)
				{
					Log::info("--- Reminder email skipped because vendor is not available for booking id " . $booking->id . "---");
					Log::info("Ticket name --" . $ticket->name . " Ticket Id -" . $ticket->id);
				}

				continue;
			}

			// collect details
			$customerName = $ticket->name;
			$customerPhone = $ticket->phone;
			$customerAltPhone = $ticket->alt_phone;

			$vendorName = $vendor->person;
			$vendorPhone = $vendor->phone;
			$vendorAltPhone = $vendor->alt_phone;

			$formattedTime = date('j F, g:i a', $booking->party_date_time);

			if ($isWeekend)
			{
				$formattedTime = date('l, g:i A', $booking->party_date_time);
			}
			$areaName = $ticket->area ? $ticket->area->name : '<i>--N/A--</i>';

			// send SMS to operational head
			$operationHeadSMSTpl = config('evibe.sms_tpl.reminder.team');
			$replaces = [
				'#field1#' => $vendorName,
				'#field2#' => $vendorPhone,
				'#field3#' => $customerName,
				'#field4#' => $formattedTime,
				'#field5#' => $areaName,
				'#field6#' => $customerPhone,
			];

			$operationHeadSMSText = str_replace(array_keys($replaces), array_values($replaces), $operationHeadSMSTpl);
			$operationHeadSMSData = [
				'to'   => config('evibe.contact.operations.phone'),
				'text' => $operationHeadSMSText
			];

			// send sms to user
			// @since 7 April 2016: removed sending SMS to operations team
			// Queue::push('Evibe\Utilities\SendSMS@smsEventReminder', $operationHeadSMSData);

			// collect data for email
			$bookingAmount = $booking->booking_amount;
			$advanceAmount = $booking->advance_amount;
			$balanceAmount = $booking->is_advance_paid ? ($bookingAmount - $advanceAmount) : $bookingAmount;

			array_push($emailData['list'], [
				'conceptType'      => $booking->bookingConcept ? $booking->bookingConcept->name : "--",
				'areaName'         => $areaName,
				'customerName'     => $customerName,
				'customerPhone'    => $customerPhone,
				'customerAltPhone' => $customerAltPhone,
				'vendorName'       => $vendorName,
				'vendorPhone'      => $vendorPhone,
				'vendorAltPhone'   => $vendorAltPhone,
				'eventDateTime'    => $formattedTime,
				'bookingInfo'      => $booking->booking_info,
				'bookingAmount'    => $bookingAmount,
				'balanceAmount'    => $balanceAmount,
				'venueAddress'     => $ticket->venue_address,
				'venueLandmark'    => $ticket->venue_landmark
			]);
		}

		if (count($emailData['list']) > 0)
		{
			// send email to operations team - single email with all today's booking details
			$emailData['to'] = [
				'sub'              => $data['subject'],
				'name'             => config('evibe.contact.business.name'),
				'email'            => [config('evibe.contact.customer.group')],
				'ccAddress'        => [config('evibe.contact.business.group')],
				'replyToAddresses' => [config('evibe.email')],
				'greetings'        => $data['greetings']];

			Queue::push('Evibe\Utilities\SendEmail@sendReminderEmailToOperationHead', $emailData);
		}
	}
}