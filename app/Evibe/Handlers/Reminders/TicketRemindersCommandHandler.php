<?php

namespace Evibe\Handlers\Reminders;

use App\Jobs\Email\MailTicketReminderToCustomer;
use App\Jobs\SMS\SMSTicketReminderToCustomer;
use App\Models\Area;
use App\Models\Coupon\Coupon;
use App\Models\LogTicketReminders;
use App\Models\SiteErrorLog;
use App\Models\Ticket;
use App\Models\TicketRemindersStack;
use App\Models\Types\TypeEvent;
use App\Models\User;
use App\Models\Util\City;
use App\Models\Util\SuppressionEmailList;
use App\Models\Util\UnSubscribedUsers;
use Carbon\Carbon;
use Evibe\Handlers\BaseCommandHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class TicketRemindersCommandHandler extends BaseCommandHandler
{
	public function sendTicketReminders($options)
	{
		// error notification system
		try {
			$startTime = time();
			$endTime = $startTime + 59;
			//$endTime = $startTime + (1 * 60 * 60) - 1; // @see: for testing purpose

			/*
			 * @see: ----- Use this option only for retention reminders -----
			 * If reminder ids are provided, then do follow-up for those reminders only
			 * Else, follow-up for the reminders scheduled in this time frame
			 */

			$reminders = TicketRemindersStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
				->whereNull('type_reminder_group.deleted_at')
				->whereNull('ticket_reminders_stack.deleted_at')
				->whereNull('ticket_reminders_stack.invalid_at')
				->whereNull('ticket_reminders_stack.terminated_at');

			if (isset($options['reminderIds']) && count($options['reminderIds'])) {
				$reminders->whereIn('ticket_reminders_stack.id', $options['reminderIds']);
			} else {
				$reminders->where('ticket_reminders_stack.next_reminder_at', '>=', $startTime)
					->where('ticket_reminders_stack.next_reminder_at', '<=', $endTime);
			}

			$reminders = $reminders->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
				->get();

			$this->validateAndSendTicketReminders($reminders);

			// fetch reminders
			// qualify and validate reminders
			// send reminders
			// set next reminders

		} catch (Exception $e) {
			$errorData = [
				'project_id' => config('evibe.project_id'),
				'url'        => request()->fullUrl(),
				'exception'  => "Some error while sending reminder notification",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);
		}
	}

	private function getShortenedUrl($longUrl)
	{
		return $this->getShortUrl($longUrl);
	}

	private function stopTicketAutoFollowup($ticket, $reminder, $data)
	{
		$errorData = [
			'project_id' => config('evibe.project_id'),
			'url'        => request()->fullUrl(),
			'exception'  => "Some error while making API call to update reminder[Stack Id: " . $reminder->id . "] for ticket[id: " . $ticket->id . "]",
			'code'       => 'Custom Log Error',
			'details'    => "Some error while making API call to update reminder[Stack Id: " . $reminder->id . "] for ticket[id: " . $ticket->id . "]",
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		// call evibe.in api
		if ($ticket->handler_id) {
			$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken($ticket->handler_id);
		} else {
			$errorData['exception'] = 'Handler Id does not exist for ticket[Id: ' . $ticket->id . ']';
			SiteErrorLog::create($errorData);

			// @todo: update admin that handler is not there
			return false;
		}

		$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . $ticket->id . '/stop/' . $reminder->type_reminder_id;

		try {
			$client = new Client();
			$res = $client->request('PUT', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $data
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res['success']) && $res['success'] == false) {
				$errorData = [
					'url'     => request()->fullUrl(),
					'details' => isset($res['error']) ? $res['error'] : "Some error occurred while updating ticket reminder for ticket: " . $ticket->id
				];

				SiteErrorLog::create($errorData);
			}
		} catch (ClientException $e) {
			$errorData['exception'] = "Ticket auto followup stop Api";
			$errorData['code'] = $e->getCode();
			$errorData['details'] = $e->getTraceAsString();

			SiteErrorLog::create($errorData);
		}
	}

	private function updateTicketAutoFollowup($ticket, $reminder, $typeReminderId = null)
	{
		$errorData = [
			'project_id' => config('evibe.project_id'),
			'url'        => request()->fullUrl(),
			'exception'  => "Some error while making API call to update reminder[Stack Id: " . $reminder->id . "] for ticket[id: " . $ticket->id . "]",
			'code'       => 'Custom Log Error',
			'details'    => "Some error while making API call to update reminder[Stack Id: " . $reminder->id . "] for ticket[id: " . $ticket->id . "]",
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		// call evibe.in api
		if ($ticket->handler_id) {
			$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken($ticket->handler_id);
		} else {
			if ($typeReminderId == config('reminder.retention.id')) {
				$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken(config('evibe.default_handler'));
			} else {
				$errorData['exception'] = 'Handler Id does not exist for ticket[Id: ' . $ticket->id . ']';
				SiteErrorLog::create($errorData);

				// @todo: update admin that handler is not there
				return false;
			}
		}

		$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . $ticket->id . '/recalculate/' . $reminder->type_reminder_id;

		try {
			$client = new Client();
			$res = $client->request('PUT', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => ''
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res['success']) && $res['success'] == false) {
				$errorData = [
					'url'     => request()->fullUrl(),
					'details' => isset($res['error']) ? $res['error'] : "Some error occurred while updating ticket reminder for ticket: " . $ticket->id
				];

				SiteErrorLog::create($errorData);
			}
		} catch (ClientException $e) {
			$errorData['exception'] = "Ticket auto followup update Api";
			$errorData['code'] = $e->getCode();
			$errorData['details'] = $e->getTraceAsString();

			SiteErrorLog::create($errorData);
		}
	}

	protected function campaignUrlForLink($url, $reminder, $medium, $reminderTypeId = null)
	{
		$diff = ((strtotime(date('Y-m-d', $reminder->end_time)) - strtotime(date('Y-m-d', $reminder->start_time))) / (24 * 60 * 60)) - 1;
		$utmMedium = "auto-reminders";
		$utmSource = "auto-reminders";
		$utmCampaign = "auto-reminders";

		switch ($medium) {
			case config('reminder.communication.email'):
				$utmMedium = 'email';
				break;

			case config('reminder.communication.sms'):
				$utmMedium = 'sms';
				break;

			case config('reminder.communication.notification'):
				$utmMedium = 'notification';
				break;
		}

		$reminderTypeId = $reminderTypeId ?: $reminder->type_reminder_id;
		switch ($reminderTypeId) {
			case config('reminder.followup.id'):
				$utmSource = 't' . ($diff - $reminder->count + 1) . '_p' . $reminder->count . '_' . strtolower(config('reminder.followup.campaign-code')) . '_' . $reminder->type_reminder_group_id;
				$utmCampaign = config('reminder.followup.campaign-code');
				break;

			case config('reminder.thank-you-card.id'):
				$utmSource = strtolower(config('reminder.thank-you-card.campaign-code')) . '_' . $reminder->type_reminder_group_id;
				$utmCampaign = config('reminder.thank-you-card.campaign-code');
				break;

			case config('reminder.retention.id'):
				$utmSource = strtolower(config('reminder.retention.campaign-code')) . '_' . $reminder->type_reminder_group_id;
				$utmCampaign = config('reminder.retention.campaign-code');
				break;
		}

		$url .= '&utm_source=' . $utmSource;
		$url .= '&utm_medium=' . $utmMedium;
		$url .= '&utm_campaign=' . $utmCampaign;

		return $url;
	}

	protected function dispatchReminders($data, $smsData, $notificationData)
	{
		$reminderEmail = false;
		$reminderSMS = false;
		$reminderNotification = false;

		if ($data && isset($data['email']) && $data['email']) {
			$validEmail = $data['email'];
			$suppressionEmails = SuppressionEmailList::whereRaw('LOWER(email) = ?', [strtolower($data['email'])])
				->whereNull('deleted_at')
				->orderBy('updated_at', 'DESC')
				->get();

			if (count($suppressionEmails)) {
				$validEmail = null;

				foreach ($suppressionEmails as $suppressionEmail) {
					if ($suppressionEmail->is_corrected && $suppressionEmail->corrected_email) {
						$validEmail = $suppressionEmail->corrected_email;
						break;
					}
				}
			}

			if (
				$validEmail &&
				isset($data['from']) && $data['from'] &&
				isset($data['emailSub']) && $data['emailSub'] &&
				isset($data['mailView']) && $data['mailView']
			) {
				$data['email'] = $validEmail;
				dispatch(new MailTicketReminderToCustomer($data));
				$reminderEmail = true;
			}
		}

		if ($smsData) {
			if (isset($smsData['phoneArray']) && count($smsData['phoneArray'])) {
				foreach ($smsData['phoneArray'] as $phone) {
					$smsData['to'] = $phone;

					dispatch(new SMSTicketReminderToCustomer($smsData));
					$reminderSMS = true;
				}
			} else {
				dispatch(new SMSTicketReminderToCustomer($smsData));
				$reminderSMS = true;
			}
		}

		return [
			'reminderEmail'        => $reminderEmail,
			'reminderSMS'          => $reminderSMS,
			'reminderNotification' => $reminderNotification,
		];
	}

	private function validateAndSendTicketReminders($reminders)
	{
		try {
			$errorData = [
				'project_id' => config('evibe.project_id'),
				'url'        => request()->fullUrl(),
				'exception'  => "Some error while sending reminder notification",
				'code'       => 'Custom Log Error',
				'details'    => 'Custom text explaining the log position',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			if (!count($reminders)) {
				return false;
			}

			foreach ($reminders as $reminder) {
				$stopData = [
					'terminatedAt' => null,
					'invalidAt'    => null,
					'stopMessage'  => '[JOBs] - Validation before sending reminders - '
				];

				$reminderEmail = false;
				$reminderSMS = false;
				$reminderNotification = false;

				$ticket = Ticket::with('mappings')->find($reminder->ticket_id);

				switch ($reminder->type_reminder_id) {
					case config('reminder.followup.id'):

						$mappings = $ticket->mappings;
						if (!$mappings) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Could not find mappings for the ticket.';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						if (($reminder->next_reminder_at < $reminder->start_time) || ($reminder->next_reminder_at > $reminder->end_time)) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Reminder date time should be in-between start time and end time';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						// not in followup
						// shortlisted
						// rated low
						// did not like
						$lastShortlistedAt = $mappings->max('selected_at') ? $mappings->max('selected_at') : null;

						if ($ticket->status_id != config('evibe.ticket.status.followup')) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Ticket in not in followup state.';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						if (!is_null($ticket->last_reco_dislike_at) && $ticket->last_reco_dislike_at >= $reminder->start_time) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Customer responded [Did not like recommendations]';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						if (!is_null($lastShortlistedAt) && $lastShortlistedAt >= $reminder->start_time) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Customer responded [Shortlisted some recommendations]';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						if ($ticket->last_reco_rated_at >= $reminder->start_time && $ticket->last_reco_rating < 3) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Customer rated < 3 for our recommendations';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						$handler = $ticket->handler_id ? User::find($ticket->handler_id) : null;
						$handlerName = $handler ? $handler->name : 'Team Evibe.in';
						$event = TypeEvent::find($ticket->event_id);
						$daysCount = (strtotime(date("Y-m-d", $ticket->event_date)) - strtotime('today midnight')) / (24 * 60 * 60);
						$recommendationUrl = config('evibe.main_url') . 'recommendation' . '?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id);

						// UTM tagging
						$emailRecoLink = $this->campaignUrlForLink($recommendationUrl . "&ref=email", $reminder, config('reminder.communication.email'));
						$smsRecoLink = $this->campaignUrlForLink($recommendationUrl . "&ref=sms", $reminder, config('reminder.communication.sms'));
						$smsRecoLink = $this->getShortenedUrl($smsRecoLink);
						$notLikeLink = $recommendationUrl . '#didNotLike';
						$cancelLink = config('evibe.main_url') . 'fallout-feedback' .
							'?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id) . '&reminderId=' . $reminder->id;
						$smsCancelLink = $cancelLink . '&utm_campaign=not_interested&utm_source=auto_followups&utm_medium=sms';
						$emailCancelLink = $cancelLink . '&utm_campaign=not_interested&utm_source=auto_followups&utm_medium=email';

						$data = [
							'customerName'       => $ticket->name ? $ticket->name : 'Customer',
							'phone'              => $ticket->phone,
							'email'              => $ticket->email,
							'altPhone'           => $ticket->alt_phone,
							'altEmail'           => $ticket->alt_email,
							'eventName'          => $event->name,
							'partyDate'          => date('d/m/y', $ticket->event_date),
							'partyDateFormatted' => date('d M Y', $ticket->event_date),
							'enquiryId'          => $ticket->enquiry_id,
							'partyDateString'    => $ticket->event_date,
							'evibePhone'         => config('evibe.phone'),
							'evibePhonePlain'    => config('evibe.phone_plain'),
							'recommendationUrl'  => $emailRecoLink,
							'recoLink'           => $smsRecoLink,
							'recosCount'         => count($mappings),
							'daysCount'          => $daysCount,
							'handlerName'        => $handlerName,
							'handlerPhone'       => $handler ? $handler->phone : config('evibe.phone_plain'),
							'notLikeLink'        => $this->getShortenedUrl($notLikeLink),
							'notLikeShortLink'   => $this->getShortenedUrl($notLikeLink),
							'cancelLink'         => $emailCancelLink,
							'cancelShortLink'    => $this->getShortenedUrl($smsCancelLink),
							'from'               => $handler ? $handler->username : config('evibe.contact.customer.group'),
							'fromText'           => $handler ? explode(' ', $handler->name)[0] . ' from Evibe' : 'Team Evibe'
						];

						// defaults
						$comments = 'Auto follow up reminders';
						$data['emailSub'] = 'Top recommendations for your ' . $data['eventName'];
						$data['mailView'] = 'emails.reminder.followup.rr1';
						$smsTpl = config('evibe.sms_tpl.followup.reco');

						// validate and get data from config
						$reminderConfigData = config('reminder.followup.group.' . $reminder->type_reminder_group_id);
						if (!$reminderConfigData) {
							Log::error("The reminder type does not exist");
							$errorData['details'] = 'Either the ticket does not have valid type_reminder_group_id or it is not included in config files';
							$errorData['exception'] = 'Ticket[Id: ' . $ticket->id . '] - reminder type does not exist';
							SiteErrorLog::create($errorData);
							continue;
						} else {
							if ($reminder->type_reminder_group_id == config('evibe.type_reminder_group.followup.rr7')) {
								// @todo: may be provide a default and store for every ticket
								$ticketCoupon = Ticket\TicketCoupon::where('ticket_id', $ticket->id)->first();
								$data['couponCode'] = $ticketCoupon ? $ticketCoupon->code : null;
								$data['discountPrice'] = $ticketCoupon ? $ticketCoupon->amount : null;
								$data['couponValidString'] = $ticketCoupon ? ((($ticketCoupon->validity_end_time >= strtotime('today midnight')) && ($ticketCoupon->validity_end_time < (strtotime('today midnight') + (24 * 60 * 60)))) ? 'for today' : 'till ' . date('d M', $ticketCoupon->validity_end_time)) : null;
							}

							$emailSub = isset($reminderConfigData['emailSub']) ? $reminderConfigData['emailSub'] : $data['emailSub'];
							$mailView = isset($reminderConfigData['mailView']) ? $reminderConfigData['mailView'] : $data['mailView'];
							$smsTpl = isset($reminderConfigData['smsTpl']) ? $reminderConfigData['smsTpl'] : $smsTpl;

							$mailSubReplaces = [
								'{eventName}'          => $data['eventName'],
								'{partyDateFormatted}' => $data['partyDateFormatted'],
								'{enquiryId}'          => $data['enquiryId'],
							];

							$smsTextReplaces = [
								'#customerName#'      => $data['customerName'],
								'#partyDate#'         => $data['partyDate'],
								'#eventName#'         => $data['eventName'],
								'#recoLink#'          => $data['recoLink'],
								'#evibePhone#'        => $data['evibePhonePlain'],
								'#partyDaysCount#'    => $data['daysCount'],
								'#notLikeLink#'       => $data['notLikeShortLink'], // may be changed
								'#cancelLink#'        => $data['cancelShortLink'],
								'#partyDateString#'   => $data['daysCount'] == 1 ? 'tomorrow' : 'on ' . date('d M', $data['partyDateString']),
								'#couponCode#'        => isset($data['couponCode']) ? $data['couponCode'] : null,
								'#discountPrice#'     => isset($data['discountPrice']) ? 'Rs. ' . $data['discountPrice'] : null,
								'#couponValidString#' => isset($data['couponValidString']) ? $data['couponValidString'] : null,
							];

							$data['emailSub'] = str_replace(array_keys($mailSubReplaces), array_values($mailSubReplaces), $emailSub);
							$data['smsText'] = str_replace(array_keys($smsTextReplaces), array_values($smsTextReplaces), $smsTpl);
							$data['mailView'] = $mailView;

							$smsData = [
								'to'   => $data['phone'],
								'text' => $data['smsText']
							];

							// dispatch reminders
							$remResponse = $this->dispatchReminders($data, $smsData, $notificationData = null);
							$reminderEmail = isset($remResponse['reminderEmail']) ? $remResponse['reminderEmail'] : $reminderEmail;
							$reminderSMS = isset($remResponse['reminderSMS']) ? $remResponse['reminderSMS'] : $reminderSMS;
							$reminderNotification = isset($remResponse['reminderNotification']) ? $remResponse['reminderNotification'] : $reminderNotification;

							$reminder->past_reminded_at = time();
							$reminder->save();

							// update log_ticket_reminders
							$ticketReminderLogData = [
								'ticket_id'              => $ticket->id,
								'type_reminder_group_id' => $reminder->type_reminder_group_id,
								'comments'               => $comments ? $comments : null,
								'email_sent_at'          => $reminderEmail ? time() : null,
								'sms_sent_at'            => $reminderSMS ? time() : null,
								'notification_sent_at'   => $reminderNotification ? time() : null,
							];

							$logTicketReminder = LogTicketReminders::create($ticketReminderLogData);

							if (!$logTicketReminder) {
								$errorData['exception'] = 'Ticket[Id: ' . $ticket->id . '] - Unable to save auto followup reminder[Id: ' . $reminder->id . '] to log table';
								SiteErrorLog::create($errorData);
							}

							$this->updateTicketAutoFollowup($ticket, $reminder);
						}

						break;
					case config('reminder.thank-you-card.id'):
						// @see: thank you card reminders are being processed in another job utilising the same reminders system
						// @see: do not remove them from this system as termination and invalidation are required
						continue;
						break;
					case config('reminder.retention.id'):
						// validate
						$user = User::find($reminder->user_id);

						if (!$user) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Could not find user for the reminder.';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						// check if any ticket has been raised in last 30 days
						// @see: some initiated tickets might not have email ids yet
						$userEmail = $user->username;
						$userPhone = $user->phone;
						$raisedTickets = Ticket::whereNull('deleted_at')
							->where('created_at', '>=', Carbon::createFromTimestamp(time())->startOfDay()->subDays(30)->toDateTimeString())
							->where(function ($query) use ($userEmail, $userPhone) {
								$query->where('email', $userEmail)
									->orWhere('phone', $userPhone);
							})
							->get();

						if (count($raisedTickets)) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'An enquiry has already been raised for the user.';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						$unSubscribedUser = UnSubscribedUsers::where('user_id', $user->id)->first();
						if ($unSubscribedUser) {
							$stopData['terminatedAt'] = time();
							$stopData['stopMessage'] .= 'User has already un-subscribed for retention notifications.';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						// form urls
						// utm tagging

						// fetch city to form Plan Now url
						$city = City::find($ticket->city_id);
						if (!$city) {
							// try to get city from location
							$area = Area::find($ticket->area_id);
							if ($area) {
								$city = City::find($area->city_id);
							}

							// even now if city doesn't exist, default it to bangalore
							if (!$city) {
								$city = City::find(config('evibe.city.bangalore'));
							}
						}

						// quick enquiry url
						$quickEnquiryLink = config('evibe.main_url') . $city->url;
						$emailQELink = $this->campaignUrlForLink($quickEnquiryLink . '?type=RT', $reminder, config('reminder.communication.email'));
						$emailQELink = $emailQELink . '#quickEnquiry';
						$smsQELink = $this->campaignUrlForLink($quickEnquiryLink . '?type=RT', $reminder, config('reminder.communication.sms'));
						$smsQELink = $smsQELink . '#quickEnquiry';
						$smsQELink = $this->getShortenedUrl($smsQELink);

						// browse options url
						$browseOptionsLink = config('evibe.main_url') . $city->url;
						$emailBOLink = $this->campaignUrlForLink($browseOptionsLink . '?type=RT', $reminder, config('reminder.communication.email'));
						$smsBOLink = $this->campaignUrlForLink($browseOptionsLink . '?type=RT', $reminder, config('reminder.communication.sms'));
						$smsBOLink = $this->getShortenedUrl($smsBOLink);

						// un-subscribe link
						$unSubscribeLink = config('evibe.main_url') . 'fallout-feedback?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id) . '&reminderId=' . $reminder->id;
						$emailUnSubLink = $unSubscribeLink . '&utm_campaign=not_interested&utm_source=retention&utm_medium=email';
						$emailUnSubLink = $this->getShortenedUrl($emailUnSubLink);
						$smsUnSubLink = $unSubscribeLink . '&utm_campaign=not_interested&utm_source=retention&utm_medium=sms';
						$smsUnSubLink = $this->getShortenedUrl($smsUnSubLink);

						// form data
						$data = [
							'customerName'    => $ticket->name ? $ticket->name : 'Customer',
							'phone'           => $ticket->phone,
							'email'           => $ticket->email,
							'altPhone'        => $ticket->alt_phone,
							'altEmail'        => $ticket->alt_email,
							'partyDate'       => date('d/m/y', $ticket->event_date),
							'partyDateString' => $ticket->event_date,
							'evibePhone'      => config('evibe.phone'),
							'evibePhonePlain' => config('evibe.phone_plain'),
							'emailQELink'     => $emailQELink,
							'smsQELink'       => $smsQELink,
							'emailBOLink'     => $emailBOLink,
							'smsBOLink'       => $smsBOLink,
							'emailUnSubLink'  => $emailUnSubLink,
							'smsUnSubLink'    => $smsUnSubLink,
							'from'            => config('evibe.email'),
							'fromText'        => 'Team Evibe.in'
						];

						$coupon = Coupon::select('coupon.*')
							->join('coupon_users', 'coupon_users.coupon_id', '=', 'coupon.id')
							->whereNull('coupon.deleted_at')
							->whereNull('coupon_users.deleted_at')
							->where('coupon_users.user_id', $user->id)
							->first();

						if (!$coupon) {
							$stopData['invalidAt'] = time();
							$stopData['stopMessage'] .= 'Coupon code is mandatory for retention reminder and there is no coupon code for this user';
							$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);
							continue;
						}

						$defaultDiscountAmt = 50;

						$data['couponCode'] = $coupon ? $coupon->coupon_code : null;
						$data['discountAmount'] = $coupon ? $coupon->discount_amount : null;
						$data['maxDiscountAmount'] = $coupon ? $coupon->max_discount_amount : null;
						$data['maxDiscountAmtString'] = $coupon->max_discount_amount ? 'Rs. ' . $coupon->max_discount_amount : 'Rs. ' . $defaultDiscountAmt;
						$data['discountPercent'] = $coupon ? $coupon->discount_percent : null;
						$data['discountPercentString'] = $coupon->discount_percent ? $coupon->discount_percent . '%' : ($coupon->discount_amount ? 'Rs. ' . $coupon->discount_amount : 'Rs. ' . $defaultDiscountAmt);
						$data['validDate'] = date('d M Y', strtotime($coupon->offer_end_time));
						$data['offerStart'] = $coupon ? $coupon->offer_start_time : null;
						$data['offerEnd'] = $coupon ? $coupon->offer_end_time : null;
						$data['countCE'] = $coupon ? ceil(((strtotime($coupon->offer_end_time) - time()) / (24 * 60 * 60))) : null;

						// defaults
						$comments = 'Retention reminders';
						$data['emailSub'] = '10%* OFF exclusively for you';
						$data['mailView'] = 'emails.reminder.retention.rt1'; // default email
						$smsTplKey = "reminder.retention.group" . config('evibe.type_reminder_group.retention.rt1') . "smsTpl";
						$smsTpl = config($smsTplKey); // default sms

						// validate and get data from config
						$reminderConfigData = config('reminder.retention.group.' . $reminder->type_reminder_group_id);
						if (!$reminderConfigData) {
							Log::error("The reminder type does not exist");
							$errorData['details'] = 'Either the ticket does not have valid type_reminder_group_id or it is not included in config files';
							$errorData['exception'] = 'Ticket[Id: ' . $ticket->id . '] - reminder type does not exist';
							SiteErrorLog::create($errorData);
							continue;
						} else {
							$emailSub = isset($reminderConfigData['emailSub']) ? $reminderConfigData['emailSub'] : $data['emailSub'];
							$mailView = isset($reminderConfigData['mailView']) ? $reminderConfigData['mailView'] : $data['mailView'];
							$smsTpl = isset($reminderConfigData['smsTpl']) ? $reminderConfigData['smsTpl'] : $smsTpl;

							$mailSubReplaces = [
								'{customerName}'    => $data['customerName'],
								'{discountPercent}' => $data['discountPercent'],
								'{countCE}'         => $data['countCE'],
							];

							$smsTextReplaces = [
								'#customerName#'    => $data['customerName'],
								'#partyDate#'       => $data['partyDate'],
								'#smsQELink#'       => $data['smsQELink'],
								'#smsBOLink#'       => $data['smsBOLink'],
								'#evibePhone#'      => $data['evibePhonePlain'],
								'#unSubLink#'       => $data['smsUnSubLink'], // may be changed
								'#couponCode#'      => isset($data['couponCode']) ? $data['couponCode'] : null,
								'#discountAmount#'  => isset($data['discountAmount']) ? $data['discountAmount'] : null,
								'#maxDiscountAmt#'  => isset($data['maxDiscountAmount']) ? $data['maxDiscountAmount'] : null,
								'#discountPercent#' => isset($data['discountPercent']) ? $data['discountPercent'] : null,
								'#validDate#'       => isset($data['validDate']) ? $data['validDate'] : null,
								'#countCE#'         => isset($data['countCE']) ? $data['countCE'] : null,
							];

							$data['emailSub'] = str_replace(array_keys($mailSubReplaces), array_values($mailSubReplaces), $emailSub);
							$data['smsText'] = str_replace(array_keys($smsTextReplaces), array_values($smsTextReplaces), $smsTpl);
							$data['mailView'] = $mailView;

							$userTickets = Ticket::where('email', $ticket->email)->get();
							$phoneArr = $userTickets->pluck('phone')->toArray();
							$altPhoneArr = $userTickets->pluck('alt_phone')->toArray();

							$phoneArr = array_filter($phoneArr);
							$altPhoneArr = array_filter($altPhoneArr);

							$phoneArr = array_merge($phoneArr, $altPhoneArr);
							$phoneArr = array_unique($phoneArr);

							$smsData = [
								'phoneArray' => $phoneArr,
								'to'         => $data['phone'],
								'text'       => $data['smsText']
							];

							// @see: RT1 is also transactional now
							// if ($reminder && $reminder->type_reminder_group_id && $reminder->type_reminder_group_id == config('evibe.type_reminder_group.retention.rt1'))
							// {
							// 	$smsData['smsType'] = 'PROMOTIONAL';
							// }

							// dispatch reminders
							$remResponse = $this->dispatchReminders($data, $smsData, $notificationData = null);
							$reminderEmail = isset($remResponse['reminderEmail']) ? $remResponse['reminderEmail'] : $reminderEmail;
							$reminderSMS = isset($remResponse['reminderSMS']) ? $remResponse['reminderSMS'] : $reminderSMS;
							$reminderNotification = isset($remResponse['reminderNotification']) ? $remResponse['reminderNotification'] : $reminderNotification;

							if (!($reminderEmail || $reminderSMS || $reminderNotification)) {
								$stopData['invalidAt'] = time();
								$stopData['stopMessage'] .= 'Unable to send any kind of retention reminders';
								$this->stopTicketAutoFollowup($ticket, $reminder, $stopData);

								$this->sendAndSaveNonExceptionReport([
									'code'      => config('evibe.error_code.update_function'),
									'url'       => 'Retention Reminders',
									'method'    => 'GET',
									'message'   => '[TicketRemindersCommandHandler.php - ' . __LINE__ . '] ' . ' Unable to send any type of retention reminder.',
									'exception' => '[TicketRemindersCommandHandler.php - ' . __LINE__ . '] ' . ' Unable to send any type of retention reminder.',
									'trace'     => '[Ticket Id: ' . $ticket->id . '][Reminder Id: ' . $reminder->id . ']',
									'details'   => '[Ticket Id: ' . $ticket->id . '][Reminder Id: ' . $reminder->id . ']'

								]);

								continue;
							}

							$reminder->past_reminded_at = time();
							$reminder->save();

							// update log_ticket_reminders
							$ticketReminderLogData = [
								'ticket_id'              => $ticket->id,
								'type_reminder_group_id' => $reminder->type_reminder_group_id,
								'comments'               => $comments ? $comments : null,
								'email_sent_at'          => $reminderEmail ? time() : null,
								'sms_sent_at'            => $reminderSMS ? time() : null,
								'notification_sent_at'   => $reminderNotification ? time() : null,
							];

							$logTicketReminder = LogTicketReminders::create($ticketReminderLogData);

							if (!$logTicketReminder) {
								$errorData['exception'] = 'Ticket[Id: ' . $ticket->id . '] - Unable to save retention reminder[Id: ' . $reminder->id . '] to log table';
								SiteErrorLog::create($errorData);
							}

							$this->updateTicketAutoFollowup($ticket, $reminder, config('reminder.retention.id'));
						}

						break;
				}
			}

			return true;
		} catch (Exception $e) {
			$errorData = [
				'project_id' => config('evibe.project_id'),
				'url'        => request()->fullUrl(),
				'exception'  => "Some error while sending reminder notification in the private method of validate and send reminders",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);

			return false;
		}
	}
}
