<?php

/**
 * @author   Anji <anji@evibe.in>
 * @since    2 Jan 2015
 * @modified By Vikash <vikash@evibe.in> since 20th Sep 2016
 */

namespace Evibe\Handlers;

use App\Models\SiteErrorLog;
use App\Models\TicketUpdate;
use Carbon\Carbon;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;
use App\Models\TicketBooking;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class SendEventRemindersCommandHandler
{
	public function sendReminders()
	{
		$ticketData = [];
		$bookingData = [];

		$tomorrowStart = Carbon::now()->tomorrow()->startOfDay()->timestamp;
		$tomorrowEnd = Carbon::now()->tomorrow()->endOfDay()->timestamp;

		// fetch all bookings with party date tomorrow
		$bookings = TicketBooking::with('ticket', 'area')
		                         ->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                         ->select('ticket_bookings.*')
		                         ->where('ticket_bookings.is_advance_paid', 1)
		                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                         ->whereBetween('ticket_bookings.party_date_time', [$tomorrowStart, $tomorrowEnd])
		                         ->whereNotNull('ticket.paid_at')
		                         ->orderBy('ticket_bookings.party_date_time', 'ASC')
		                         ->get();

		foreach ($bookings as $booking)
		{
			$ticket = $booking->ticket;
			$partner = $booking->vendor;

			// for defunct tickets
			if (!$ticket)
			{
				Log::info("---Event Reminder email booking skipped, REASON :: ticket was not available, #BoookingId :: " . $booking->booking_id . "---");
				continue;
			}
			elseif (!$partner)
			{
				Log::info("---Event Reminder email booking skipped, REASON :: vendor was not available, #BoookingId :: " . $booking->booking_id . "---");
				continue;
			}
			elseif ((!$ticket->area))
			{
				Log::info("---Event reminder email booking skipped for non-venue booking , REASON :: Ticket location not available. #BookingId" . $booking->booking_id . "---");
				continue;
			}

			// compute
			$customerPhone = $ticket->phone;
			$customerName = $ticket->name;
			$partnerName = $partner->person;
			$partnerPhone = $partner->phone;
			$partnerEmail = $partner->email;
			$ticketId = $ticket->id;

			$partyDate = $booking->party_date_time ? date('j M Y', $booking->party_date_time) : date('j M Y', $ticket->event_date);
			$partyDateTime = $booking->party_date_time ? date('j M Y, h:i A', $booking->party_date_time) : date('j M Y, h:i A', $ticket->event_date);
			$partyStartTime = $booking->party_date_time ? date('g:i A', $booking->party_date_time) : date('g:i A', $ticket->event_date);
			$partyLocation = $ticket->area->name;
			$bookingAmount = $booking->booking_amount;
			$advanceAmount = $booking->advance_amount;
			$balanceAmount = $bookingAmount - $advanceAmount;

			// collect ticket booking data
			$mapTypeId = $booking->map_type_id;
			$uniqueKey = "$mapTypeId-$partner->id";

			if (!array_key_exists($uniqueKey, $bookingData))
			{
				// fill the needed data here
				$bookingData[$uniqueKey] = [
					'id'            => $booking->id, // only used for single order
					'count'         => 1,
					'partnerName'   => $partnerName,
					'partnerPhone'  => $partnerPhone,
					'partnerEmail'  => $partnerEmail,
					'customerName'  => $customerName,
					'partyDate'     => $partyDate,
					'partyTime'     => $partyStartTime,
					'partyLocation' => $partyLocation,
					'isVenue'       => $booking->is_venue_booking,
					'link'          => $this->generatePartnerODLink($booking),
					'partnerCC'     => EvibeUtil::getPartnerCCAddresses($partner),
					'userId'        => $partner->user_id,
					'eventDate'     => $ticket->event_date,
				];
			}
			else
			{
				// updated the link first time only, in case of multiple party.
				if ($bookingData[$uniqueKey]['count'] == 1)
				{
					$bookingData[$uniqueKey]['link'] = $this->generatePartnerODLink($booking, true);
				}

				$bookingData[$uniqueKey]['count'] += 1;
				$bookingData[$uniqueKey]['partyDateWy'] = date("j M", $ticket->event_date);
			}

			// collect the data for ticket email and sms
			if (!array_key_exists($ticketId, $ticketData))
			{
				//generating the url for customer page
				$hashedTicketId = Hash::make($ticketId);
				$hashedTicketToken = $this->generateSecretToken($ticket);
				$customerOrderDetailsLink = config('evibe.main_url') . 'c/order/' . $ticket->id . '?t1=' . $hashedTicketId . '&t2=' . $hashedTicketToken;
				$customerOrderDetailsLink = EvibeUtil::getShortLink($customerOrderDetailsLink);

				$ticketData[$ticketId] = [
					'email'              => $ticket->email,
					'name'               => $customerName,
					'phone'              => $customerPhone,
					'statusId'           => $ticket->status_id,
					'partyDate'          => $partyDate,
					'partyDateTime'      => $partyDateTime,
					'partyLocation'      => $partyLocation,
					'od_link'            => $customerOrderDetailsLink,
					'totalAdvanceAmount' => $advanceAmount,
					'totalBalanceAmount' => $balanceAmount
				];
			}
			else
			{
				$ticketData[$ticketId]['totalBalanceAmount'] += $balanceAmount;
				$ticketData[$ticketId]['totalAdvanceAmount'] += $advanceAmount;
			}
		}

		// send notification to customer
		if (count($ticketData) > 0)
		{
			$this->sendNotificationToCustomer($ticketData);
		}

		// send notification to partner
		if (count($bookingData) > 0)
		{
			$this->sendNotificationToPartner($bookingData);
		}
	}

	private function sendNotificationToCustomer($ticketData)
	{
		foreach ($ticketData as $ticketId => $ticket)
		{
			$subject = "[Evibe.in] " . $ticket['name'] . ", provider(s) details for your party on " . $ticket['partyDate'];
			$customerEmailData = [
				'to'                 => [$ticket['email']],
				'cc'                 => [],
				'replyTo'            => [config('evibe.contact.customer.group')],
				'name'               => $ticket['name'],
				'sub'                => $subject,
				'od_link'            => $ticket['od_link'],
				'partyDateTime'      => $ticket['partyDateTime'],
				'partyLocation'      => $ticket['partyLocation'],
				'totalBalanceAmount' => EvibeUtil::formatPrice($ticket['totalBalanceAmount']),
				'totalAdvanceAmount' => EvibeUtil::formatPrice($ticket['totalAdvanceAmount'])
			];

			$token = Hash::make($ticketId . "EVBTMO");

			if ($ticket['totalBalanceAmount'] > 0)
			{
				$customerSMSTpl = config('evibe.sms_tpl.reminder.customer');
			}
			else
			{
				$customerSMSTpl = config('evibe.sms_tpl.reminder.customer_paid');
			}
			$replaces = [
				'#field1#' => $ticket['name'],
				'#field2#' => EvibeUtil::formatPrice($ticket['totalBalanceAmount']),
				'#field3#' => EvibeUtil::getShortLink(config("evibe.main_url") . "track?ref=SMS&id=" . $ticketId . "&token=" . $token)
			];

			$customerSMSText = str_replace(array_keys($replaces), array_values($replaces), $customerSMSTpl);
			$customerSMSData = [
				'to'   => $ticket['phone'],
				'text' => $customerSMSText
			];

			// send email and sms to the customer
			Queue::push('Evibe\Utilities\SendEmail@mailEventReminderToCustomer', $customerEmailData);
			Queue::push('Evibe\Utilities\SendSMS@smsEventReminder', $customerSMSData);

			// update the ticket
			$updateData = [
				'ticket_id'   => $ticketId,
				'status_id'   => $ticket['statusId'],
				'comments'    => "Auto Reminder have been sent",
				'status_at'   => time(),
				'type_update' => "Auto"
			];

			TicketUpdate::create($updateData);
		}
	}

	private function sendNotificationToPartner($bookingData)
	{
		foreach ($bookingData as $bookingInfo)
		{
			if ($bookingInfo['count'] > 1)
			{
				$this->sendNotificationToPartnerWithMultipleOrder($bookingInfo);
			}
			elseif ($bookingInfo['count'] == 1)
			{
				$this->sendNotificationToPartnerWithSingleOrder($bookingInfo);
			}
			else
			{
				// should never reach to this case
				Log::info("Some error occurred");
			}
		}
	}

	private function sendNotificationToPartnerWithSingleOrder($bookingInfo)
	{
		// send sms
		$smsTpl = config('evibe.sms_tpl.reminder.partner_single_order');

		$textReplace = [
			'#field1#' => $bookingInfo['partnerName'],
			'#field2#' => $bookingInfo['customerName'] . '\'s',
			'#field3#' => $bookingInfo['partyTime'],
			'#field4#' => $bookingInfo['partyLocation'],
			'#field5#' => $bookingInfo['link'],
			'#field6#' => config('evibe.phone_plain') . ' (ext:4)'
		];

		$smsText = str_replace(array_keys($textReplace), array_values($textReplace), $smsTpl);

		$smsData = [
			'to'   => $bookingInfo['partnerPhone'],
			'text' => $smsText
		];

		// send email
		$sub = '[Evibe.in] ' . $bookingInfo['partnerName'] . ', gentle reminder for ' . $bookingInfo['customerName'] . '\'s party at ' . $bookingInfo['partyLocation'] . ', tomorrow ' . $bookingInfo['partyTime'];

		$emailData = [
			'to'            => [$bookingInfo['partnerEmail']],
			'cc'            => $bookingInfo['partnerCC'],
			'replyTo'       => [config('evibe.contact.customer.group')],
			'sub'           => $sub,
			'isVenue'       => $bookingInfo['isVenue'],
			'partnerName'   => $bookingInfo['partnerName'],
			'customerName'  => $bookingInfo['customerName'],
			'partyTime'     => $bookingInfo['partyTime'],
			'partyLocation' => $bookingInfo['partyLocation'],
			'orderLink'     => $bookingInfo['link'],
			'partyDate'     => $bookingInfo['partyDate']
		];

		Queue::push('Evibe\Utilities\SendSMS@smsEventReminder', $smsData);
		Queue::push('Evibe\Utilities\SendEmail@mailEventReminderToPartnerWithSingleOrder', $emailData);

		// send app notification to partner
		$this->sendNotificationToPartnerSingleOrder($bookingInfo);
	}

	private function sendNotificationToPartnerWithMultipleOrder($bookingInfo)
	{
		// send sms
		$smsTpl = config('evibe.sms_tpl.reminder.partner_multiple_order');

		$textReplace = [
			"#field1#" => $bookingInfo['partnerName'],
			'#field2#' => $bookingInfo['count'],
			'#field3#' => $bookingInfo['partyDateWy'],
			'#field4#' => $bookingInfo['link'],
			'#field5#' => config('evibe.phone_plain') . ' (ext:4)',
		];

		$smsText = str_replace(array_keys($textReplace), array_values($textReplace), $smsTpl);

		$smsData = [
			'to'   => $bookingInfo['partnerPhone'],
			'text' => $smsText
		];

		//send email
		$sub = "[Evibe.in] " . $bookingInfo['partnerName'] . ", gentle reminder for " . $bookingInfo['count'] . " party orders scheduled for tomorrow";

		$emailData = [
			'to'          => [$bookingInfo['partnerEmail']],
			'cc'          => $bookingInfo['partnerCC'],
			'replyTo'     => [config('evibe.contact.customer.group')],
			'sub'         => $sub,
			'count'       => $bookingInfo['count'],
			'partyDate'   => $bookingInfo['partyDateWy'],
			'partnerName' => $bookingInfo['partnerName'],
			'orderLink'   => $bookingInfo['link']
		];

		Queue::push('Evibe\Utilities\SendSMS@smsEventReminder', $smsData);
		Queue::push('Evibe\Utilities\SendEmail@mailEventReminderToPartnerWithMultipleOrder', $emailData);

		// send app notification to partner
		$this->sendNotificationToPartnerMultipleOrder($bookingInfo);

	}

	/**
	 * @param $obj : this should be instance of modal, not array
	 */
	private function generateSecretToken($obj)
	{
		if ($obj->secret_token)
		{
			$hashedToken = Hash::make($obj->secret_token);
		}
		else
		{
			$token = str_random(7);
			$obj->update(['secret_token' => $token]);
			$hashedToken = Hash::make($token);
		}

		return $hashedToken;
	}

	protected function generatePartnerODLink($booking, $multiple = false)
	{
		$mainHost = config('evibe.main_url');
		if ($multiple)
		{
			$providerId = $booking->vendor->id;
			$providerToken = Hash::make($providerId);
			$mapTypeId = $booking->map_type_id;
			$orderLink = $mainHost . "p/order/m/$providerId/$mapTypeId?token=$providerToken";
		}
		else
		{
			$hashedBookingId = Hash::make($booking->id);
			$hashedBookingToken = $this->generateSecretToken($booking);
			$orderLink = $mainHost . 'p/order/';
			$orderLink .= $booking->id . '?t1=' . $hashedBookingId . '&t2=' . $hashedBookingToken;
		}

		return $this->getShortenLink($orderLink);
	}

	protected function getShortenLink($link)
	{
		return EvibeUtil::getShortLink($link);
	}

	private function sendNotificationToPartnerSingleOrder($order)
	{
		$ticketName = $order['customerName'];
		$eventDate = $order['eventDate'];
		$userId = $order['userId'];

		$data = [
			'screen'   => config('evibe.partner_app_key.order.details'),
			'screenId' => $order['id'],
			'title'    => "$ticketName's party tomorrow " . date("h:i A", $eventDate),
			'message'  => "Review order details. For issues, call " . config('evibe.contact.company') . " immediately",
		];

		$this->callCreateNotificationApi($userId, $data);
	}

	private function sendNotificationToPartnerMultipleOrder($orderDetails)
	{
		$eventDate = $orderDetails['eventDate'];
		$orderCount = $orderDetails['count'];
		$userId = $orderDetails['userId'];

		$data = [
			'screen'  => config('evibe.partner_app_key.order.list'),
			'hasData' => true,
			'title'   => "$orderCount party orders tomorrow (" . date("d/m/y", $eventDate) . ")",
			'message' => "Review all order details. For issues, call " . config('evibe.contact.company') . " immediately",
		];

		$this->callCreateNotificationApi($userId, $data);
	}

	private function callCreateNotificationApi($userId, $data)
	{
		// call evibe.in api
		$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken($userId);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'notification/create?isFromJobs=true';

		try
		{
			$client = new \GuzzleHttp\Client();
			$client->request('POST', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $data
			]);

			$res['success'] = true;
		} catch (ClientException $e)
		{
			$errorData = [
				'url'        => request()->fullUrl(),
				'exception'  => "JOB Notification Api Reminder 1 day before",
				'code'       => $e->getCode(),
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);
		}
	}

}