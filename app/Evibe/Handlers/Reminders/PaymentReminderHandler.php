<?php
/**
 * Payment reminder handler:send payment reminder before 2 hours.
 *
 * @Author: vikash <vikash@evibe.in>
 * @Since 11th June 2016.
 */

namespace Evibe\Handlers;

use Evibe\Facades\EvibeUtilFacade as EvibeUtil;
use App\Models\Ticket;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Queue;

class PaymentReminderHandler extends BaseCommandHandler
{
	public function sendReminders()
	{
		$oneMinute = 60;
		$firstDeadlineStartTime = Carbon::now()->timestamp + (60 * 60);
		$firstDeadlineEndTime = $firstDeadlineStartTime + $oneMinute - 1;
		$secondDeadlineStartTime = Carbon::now()->timestamp + (30 * 60);
		$secondDeadlineEndTime = $secondDeadlineStartTime + $oneMinute - 1;

		$tickets = Ticket::where('status_id', config('evibe.ticket.status.confirmed'))
		                 ->where('payment_reminders_sent_count', '<=', '1')
		                 ->where(function ($query) use ($firstDeadlineEndTime, $firstDeadlineStartTime, $secondDeadlineEndTime, $secondDeadlineStartTime) {
			                 $query->whereBetween('payment_deadline', [$firstDeadlineStartTime, $firstDeadlineEndTime])
			                       ->orWhereBetween('payment_deadline', [$secondDeadlineStartTime, $secondDeadlineEndTime]);
		                 })
		                 ->get();

		foreach ($tickets as $ticket)
		{
			$bookings = $ticket->bookings;
			$totalAdvanceAmount = 0;
			$deadlineTime = date("d/m/y g:i A", $ticket->payment_deadline);
			$partyTime = date("d F", $ticket->event_date);

			if ($bookings->count() > 0)
			{
				foreach ($bookings as $booking)
				{
					if (!$booking->is_advance_paid)
					{
						$totalAdvanceAmount += $booking->advance_amount;
					}
				}

				$minBookTime = $bookings->min('party_date_time');
				$maxBookTime = $bookings->max('party_date_time');

				$partyTime = date("d M Y", $minBookTime);
				if ($minBookTime == $maxBookTime)
				{
					$partyTime = date("d M Y h:i A", $minBookTime);
				}
				elseif (date("d m y", $minBookTime) != date("d m y", $maxBookTime))
				{
					$partyTime = date("d/m/y", $minBookTime) . ' - ' . date("d/m/y", $maxBookTime);
				}
			}

			if ($totalAdvanceAmount > 0)
			{
				$totalAdvanceAmount = EvibeUtil::formatPrice($totalAdvanceAmount);
				$paymentReminderTpl = config('evibe.sms_tpl.payment.reminder');

				$paymentLink = config('evibe.live.checkout');
				$paymentLink .= "?id=" . $ticket->id;
				$paymentLink .= "&token=" . Hash::make($ticket->id);
				$paymentLink .= "&uKm8=" . Hash::make($ticket->email);
				$paymentLink = $this->getShortUrl($paymentLink);

				$replaces = [
					'#field1#' => $ticket->name,
					'#field2#' => $totalAdvanceAmount,
					'#field3#' => $deadlineTime,
					'#field4#' => $partyTime,
					'#field5#' => $paymentLink
				];

				$reminderSms = str_replace(array_keys($replaces), array_values($replaces), $paymentReminderTpl);

				$smsData = [
					'to'   => $ticket->phone,
					'text' => $reminderSms
				];

				// send sms
				Queue::push('Evibe\Utilities\SendSMS@smsAdvPayReminder', $smsData);

				$updatedReminderCount = is_null($ticket->payment_reminder_sent_at) || $ticket->payment_reminder_sent_at == 0 ? 1 : $ticket->payment_reminders_sent_count + 1;
				$ticket->update([
					                'payment_reminder_sent_at'     => Carbon::now(),
					                'payment_reminders_sent_count' => $updatedReminderCount
				                ]);
			}

		}
	}
}