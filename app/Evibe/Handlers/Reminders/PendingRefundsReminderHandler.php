<?php

namespace Evibe\Handlers\Reminders;

use App\Models\CustomerRefunds;
use App\Models\TicketBooking;
use Evibe\Handlers\BaseCommandHandler;
use Illuminate\Support\Facades\Mail;

class PendingRefundsReminderHandler extends BaseCommandHandler
{
	public function sendReminder()
	{
		$refundsList = [];
		$customerRefunds = CustomerRefunds::whereNull('deleted_at')
		                                  ->whereNull('refunded_at')
		                                  ->whereNull('rejected_at')
		                                  ->get();

		if (count($customerRefunds))
		{
			foreach ($customerRefunds as $customerRefund)
			{
				// @see: can join table with refunds, but to debug error, left like this
				$ticketBooking = TicketBooking::find($customerRefund->ticket_booking_id);
				if (!$ticketBooking)
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.fetch_function'),
						                                     'url'       => 'Pending Refunds',
						                                     'method'    => 'GET',
						                                     'message'   => '[PendingRefundsReminderHandler.php - ' . __LINE__ . '] ' . ' Unable to fetch ticket booking from id.',
						                                     'exception' => '[PendingRefundsReminderHandler.php - ' . __LINE__ . '] ' . ' Unable to fetch ticket booking from id.',
						                                     'trace'     => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . ']',
						                                     'details'   => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . ']'
					                                     ]);
					continue;
				}

				$ticket = $ticketBooking->ticket;

				if (!$ticket)
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.fetch_function'),
						                                     'url'       => 'Pending Refunds',
						                                     'method'    => 'GET',
						                                     'message'   => '[PendingRefundsCommandHandler.php - ' . __LINE__ . '] ' . ' Unable to fetch ticket details from ticket booking.',
						                                     'exception' => '[PendingRefundsCommandHandler.php - ' . __LINE__ . '] ' . ' Unable to fetch ticket details from ticket booking.',
						                                     'trace'     => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . '] [Ticket Id: ' . $ticketBooking->ticket_id . ']',
						                                     'details'   => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . '] [Ticket Id: ' . $ticketBooking->ticket_id . ']'
					                                     ]);
				}

				array_push($refundsList, [
					'refundId'        => $customerRefund->id,
					'ticketBookingId' => $customerRefund->ticket_booking,
					'customerName'    => $ticket ? $ticket->name : null,
					'customerPhone'   => $ticket ? $ticket->phone : null,
					'partyDateTime'   => $ticketBooking->party_date_time ? date("d M Y h:i A", $ticketBooking->party_date_time) : null,
					'refundAmount'    => $customerRefund->refund_amount,
					'partnerBear'     => $customerRefund->partner_bear,
					'evibeBear'       => $customerRefund->evibe_bear
				]);
			}
		}

		$data = [
			'refundsList'     => $refundsList,
			'sub'             => 'Pending customer refunds',
			'refundsDashLink' => config('evibe.dash_url') . 'finance/customer-refunds'
		];

		if (count($refundsList))
		{
			Mail::send("emails.reminder.finance.pending-refunds-team", ['data' => $data], function ($mail) use ($data)
			{
				$mail->from(config('evibe.email'), 'Team Evibe.in');
				$mail->to(config('evibe.accounts_email'));
				$mail->subject($data['sub']);
			});
		}
	}
}