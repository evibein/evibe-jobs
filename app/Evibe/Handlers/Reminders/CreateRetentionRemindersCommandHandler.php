<?php

namespace App\Evibe\Handlers\Reminders;

use App\Models\SiteErrorLog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use Evibe\Handlers\BaseCommandHandler;

class CreateRetentionRemindersCommandHandler extends BaseCommandHandler
{
	public function createReminders($command)
	{
		try
		{
			$errorData = [
				'project_id' => config('evibe.project_id'),
				'url'        => request()->fullUrl(),
				'exception'  => "Some error while making API call to create retention reminders",
				'code'       => 'Custom Log Error',
				'details'    => "Some error while making API call to create retention reminders",
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			$accessToken = \Evibe\Utilities\EvibeUtil::getAccessToken(config('evibe.default_handler'));
			$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . 'create/retention-reminders';

			$client = new Client();
			$res = $client->request('POST', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => ''
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res['success']) && $res['success'] == false)
			{
				$errorData = [
					'url'     => request()->fullUrl(),
					'details' => isset($res['error']) ? $res['error'] : "Some error occurred while creating retention reminders"
				];

				SiteErrorLog::create($errorData);
			}
		} catch (ClientException $e)
		{
			$errorData['exception'] = "Retention reminder creation Api trigger";
			$errorData['code'] = $e->getCode();
			$errorData['details'] = $e->getTraceAsString();

			SiteErrorLog::create($errorData);
		}
	}
}