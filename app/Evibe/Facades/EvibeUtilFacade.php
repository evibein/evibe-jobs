<?php namespace Evibe\Facades;

/**
 * EvibeUtil Facade Resolver
 *
 * @author Anji <anji@evibe.in>
 * @since  2 Jan 2015
 */

use Evibe\Utilities\EvibeUtil;
use Illuminate\Support\Facades\Facade;

class EvibeUtilFacade extends Facade
{

	protected static function getFacadeAccessor()
	{
		return new EvibeUtil;
	}

}