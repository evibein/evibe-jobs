<?php

namespace App\Models\Search;

use App\Models\BaseModel;
use Elasticquent\ElasticquentTrait;

class SearchableProduct extends BaseModel
{
	use ElasticquentTrait;
	
	protected $table = "searchable_product";
}
