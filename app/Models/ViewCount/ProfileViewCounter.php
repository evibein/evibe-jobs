<?php

namespace App\Models\ViewCount;

use App\Models\BaseModel;

class ProfileViewCounter extends BaseModel
{
	protected $table = 'view_counter_profile';
}