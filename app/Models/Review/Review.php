<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Review\PartnerReviewAnswer;
use \Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	use SoftDeletes;

	protected $table = 'planner_review';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function booking()
	{
		return $this->belongsTo(\App\Models\TicketBooking::class, 'ticket_booking_id');
	}

	public function individualAnswers()
	{
		return $this->hasMany(PartnerReviewAnswer::class, 'review_id');
	}

	public function bookingConcept()
	{
		return $this->belongsTo(\App\Models\TicketBooking::class, 'ticket_booking_id');
	}
}