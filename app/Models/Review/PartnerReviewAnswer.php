<?php

namespace App\Models\Review;

use App\Models\Util\TypePartnerReviewQuestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerReviewAnswer extends Model
{
	use SoftDeletes;
	protected $table = 'partner_review_answer';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function individualAnswers()
	{
		return $this->belongsTo(\Review::class, 'review_id');
	}

	public function question()
	{
		return $this->belongsTo(TypePartnerReviewQuestion::class, 'question_id');
	}
}