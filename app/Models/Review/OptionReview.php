<?php

namespace App\Models\Review;

use App\Models\BaseModel;

class OptionReview extends BaseModel
{
	protected $table = 'option_review';
}