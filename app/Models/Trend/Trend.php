<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trend extends Model
{
	use SoftDeletes;

	protected $table = 'trending';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function gallery()
	{
		return $this->hasMany(TrendGallery::class, 'trending_id');
	}

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'planner_id');
	}

	public function scopeIsLive($query)
	{
		return $query->where('trending.is_live', 1);
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('trend_event', 'trend_event.trend_id', '=', 'trending.id')
		             ->whereNull('trend_event.deleted_at')
		             ->where('trend_event.type_event_id', $eventId);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->join('planner', 'planner.id', '=', 'trending.planner_id')
		             ->where('planner.city_id', $cityId);

	}
}