<?php

namespace App\Models;

class TrendGallery extends BaseModel
{
	protected $table = 'trending_gallery';

	public function getBasePath($trendId)
	{
		$basePath = config('evibe.gallery.root');
		$basePath .= '/trends/' . $trendId . '/images/';

		return $basePath;
	}
}