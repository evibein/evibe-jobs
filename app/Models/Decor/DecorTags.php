<?php

namespace App\Models\Decor;

use App\Models\BaseModel;

class DecorTags extends BaseModel
{
	protected $table = 'decor_tags';
}