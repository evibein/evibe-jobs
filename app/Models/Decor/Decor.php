<?php namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  13 Oct 2015
 */

use App\Models\Decor\DecorEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Decor extends Model
{

	use SoftDeletes;

	protected $table = 'decor';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function images()
	{
		return $this->hasMany('App\Models\DecorGallery', 'decor_id');
	}

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'provider_id');
	}

	public function getCity()
	{
		return $this->provider && $this->provider->city ? $this->provider->city->id : null;
	}

	public function getLink()
	{
		return config('evibe.dash_url') . 'decors/' . $this->id . '/info';
	}

	public function firstEvent()
	{
		$decorEvent = DecorEvent::where('decor_id', $this->id)->value('event_id');
		$event = $decorEvent ? $decorEvent : config('event.kids_birthday');

		return $event;
	}

	public function scopeIsLive($query)
	{
		return $query->where('decor.is_live', 1);
	}
}