<?php namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  13 Sep 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class DecorGallery extends \Eloquent
{

	use SoftDeletes;

	protected $table = 'decor_gallery';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function decor()
	{
		return $this->belongsTo('App\Models\Decor', 'decor_id');
	}

	public function getPath()
	{
		$decor = $this->decor;
		$path = config('evibe.gallery.host');

		$path .= '/decors/' . $decor->id;
		$path .= '/images/' . $this->url;

		return $path;
	}
}