<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeEvent extends Model
{
	use SoftDeletes;

	protected $table = 'type_event';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}