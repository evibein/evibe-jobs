<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeReminder extends Model
{
	use SoftDeletes;

	protected $table = 'type_reminder';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}