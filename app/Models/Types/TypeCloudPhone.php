<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCloudPhone extends Model
{
	use SoftDeletes;

	protected $table = 'type_cloud_phone';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];
}
