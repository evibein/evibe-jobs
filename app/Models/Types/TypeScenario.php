<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeScenario extends Model
{
	use SoftDeletes;

	protected $table = 'type_scenario';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];
}
