<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeCategoryTags extends Model
{
	use SoftDeletes;

	protected $table = 'type_category_tags';
	protected $guarded = [];
}