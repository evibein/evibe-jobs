<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeReminderGroup extends Model
{
	use SoftDeletes;

	protected $table = 'type_reminder_group';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}