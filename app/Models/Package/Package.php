<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
	use SoftDeletes;

	protected $table = 'planner_package';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
	public $timestamps = true;

	public function gallery()
	{
		return $this->hasMany('App\Models\PackageGallery', 'planner_package_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function getLink()
	{
		$route = "";
		if ($this->map_type_id == config('evibe.ticket.type.venues'))
		{
			$route = config('evibe.dash_url') . 'packages/venue/view/' . $this->id;
		}
		elseif ($this->map_type_id == config('evibe.ticket.type.planners'))
		{
			$route = config('evibe.dash_url') . 'packages/vendor/view/' . $this->id;
		}
		elseif ($this->map_type_id == config('evibe.ticket.type.artists'))
		{
			$route = config('evibe.dash_url') . 'packages/artist/view/' . $this->id;
		}

		return $route;
	}

	public function scopeIsLive($query)
	{
		return $query->where('planner_package.is_live', 1);
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->where('planner_package.event_id', $eventId);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->where('planner_package.city_id', '=', $cityId);
	}
}
