<?php namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since 31 Dec 2014
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VendorPackage extends \Eloquent
{

    use SoftDeletes;

    protected $table = 'planner_package';
    protected $guarded = array();
    public static $rules = array();
    protected $dates = ['deleted_at'];

}
