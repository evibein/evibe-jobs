<?php

namespace App\Models;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class PackageGallery extends Model
{
	protected $table = 'planner_package_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	public $timestamps = true;

	public function package()
	{
		return $this->belongsTo('App\Models\Package', 'planner_package_id');
	}

	public function getBasePath($providerId)
	{
		$basePath = config('evibe.gallery.root');
		$basePath .= '/planner/' . $providerId . '/images/packages/' . $this->planner_package_id . '/';

		return $basePath;
	}
}
