<?php

namespace App\Models\Cake;

use App\Models\BaseModel;
use App\Models\Vendor;

class Cake extends BaseModel
{
	protected $table = 'cake';

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'provider_id');
	}

	public function gallery()
	{
		return $this->hasMany(CakeGallery::class, 'cake_id');
	}

	public function getCity()
	{
		return $this->provider && $this->provider->city ? $this->provider->city->id : null;
	}

	public function getLink()
	{
		$url = "cakes/$this->id/info";

		$currentUrl = config('evibe.dash_url') . $url;

		return $currentUrl;
	}

	public function firstEvent()
	{
		$cakeEvent = CakeEvent::where('cake_id', $this->id)->value('event_id');
		$event = $cakeEvent ? $cakeEvent : config('event.kids_birthday');

		return $event;
	}

	public function scopeIsLive($query)
	{
		return $query->where('cake.is_live', 1);
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
		             ->whereNull('cake_event.deleted_at')
		             ->where('cake_event.event_id', $eventId);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->join('planner', 'planner.id', '=', 'cake.provider_id')
		             ->where('planner.city_id', $cityId);
	}
}