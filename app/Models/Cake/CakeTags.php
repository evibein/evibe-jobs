<?php

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakeTags extends BaseModel
{
	protected $table = 'cake_tags';
}