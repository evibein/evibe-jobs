<?php

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakeGallery extends BaseModel
{
	protected $table = 'cake_gallery';

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}

	public function getPath()
	{
		$basePath = config('evibe.gallery.root');
		$cake = $this->cake;
		$provider = $cake->provider;

		if ($cake && $provider)
		{
			$cakeId = $cake->id;
			$providerId = $provider->id;
			$basePath .= "planner/$providerId/images/cakes/$cakeId/";
		}

		return $basePath;
	}
}