<?php

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakeEvent extends BaseModel
{
	protected $table = 'cake_event';
}
