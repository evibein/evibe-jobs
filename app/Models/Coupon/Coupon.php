<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{

	use SoftDeletes;

	protected $table = 'coupon';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}