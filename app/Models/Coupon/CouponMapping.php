<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponMapping extends Model
{

	use SoftDeletes;

	protected $table = 'coupon_mappings';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}