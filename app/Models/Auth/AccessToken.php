<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
	protected $table = 'access_tokens';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];
}
