<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class AuthClient extends Model
{
	protected $table = 'auth_clients';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];
}
