<?php

namespace App\Models\Ticket;

/**
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketCoupon extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_coupons';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;

}