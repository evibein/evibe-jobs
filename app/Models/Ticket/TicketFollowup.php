<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFollowup extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_followups';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public $rules = [];

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function handler()
	{
		return $this->belongsTo('App\Models\User', 'handler_id');
	}
}
