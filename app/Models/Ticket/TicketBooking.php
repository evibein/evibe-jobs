<?php

namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketMapping;
use App\Models\Types\TypeBookingConcept;
use App\Models\Vendor\DeliveryGallery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketBooking extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_bookings';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;

	public function vendor()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function bookingConcept()
	{
		return $this->belongsTo(TypeBookingConcept::class, 'type_booking_concept_id');
	}

	public function vendorReview()
	{
		return $this->hasOne(\Review::class, 'ticket_booking_id');
	}

	public function mapping()
	{
		return $this->belongsTo(TicketMapping::class, 'ticket_mapping_id');
	}

	public function deliveryImages()
	{
		return $this->hasMany(DeliveryGallery::class, 'ticket_booking_id');
	}

	public function bookingImages()
	{
		return $this->hasMany(TicketBookingGallery::class, 'ticket_booking_id');
	}

	public function scopeIsBooked($query)
	{
		$query->where('is_advance_paid', 1);
	}
}