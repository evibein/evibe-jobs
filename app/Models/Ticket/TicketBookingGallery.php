<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\TicketBooking;

class TicketBookingGallery extends BaseModel
{
	protected $table = 'ticket_booking_gallery';

	public function ticketBooking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}
}