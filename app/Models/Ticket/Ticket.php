<?php

namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

use App\Models\Ticket\TicketMapping;
use App\Models\Util\City;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'ticket';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function bookings()
	{
		return $this->hasMany('App\Models\TicketBooking', 'ticket_id');
	}

	public function area()
	{
		return $this->belongsTo('App\Models\Area', 'area_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function mappings()
	{
		return $this->hasMany(TicketMapping::class, 'ticket_id');
	}

	public function handler()
	{
		return $this->belongsTo(\App\Models\User::class, 'handler_id');
	}

	public function getNameAttribute($value)
	{
		return ucwords(strtolower($value));
	}
}
