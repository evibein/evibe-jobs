<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketRSVP extends BaseModel
{
	protected $table = 'ticket_rsvp';
}