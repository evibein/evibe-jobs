<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketMapping extends Model
{
	protected $table = 'ticket_mapping';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

}
