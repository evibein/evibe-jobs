<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketUpdate extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_update';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
