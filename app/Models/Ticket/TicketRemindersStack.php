<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketRemindersStack extends Model
{
	use SoftDeletes;

	protected $table = 'ticket_reminders_stack';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
