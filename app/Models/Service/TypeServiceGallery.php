<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeServiceGallery extends Model
{
	use SoftDeletes;

	protected $table = 'service_gallery';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function getBasePath($serviceId)
	{
		$basePath = config('evibe.gallery.root');
		$basePath .= '/services/' . $serviceId . '/images/';

		return $basePath;
	}
}
