<?php

namespace App\Models\Service;

use App\Models\BaseModel;

class ServiceTags extends BaseModel
{
	protected $table = 'service_tags';
}