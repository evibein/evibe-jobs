<?php

namespace App\Models;

use App\Models\Service\ServiceEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeService extends Model
{
	use SoftDeletes;

	protected $table = 'type_service';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function gallery()
	{
		return $this->hasMany(TypeServiceGallery::class, 'type_service_id');
	}

	public function firstEvent()
	{
		$serviceEvent = ServiceEvent::where('type_service_id', $this->id)->value('type_event_id');
		$event = $serviceEvent ? $serviceEvent : config('event.kids_birthday');

		return $event;
	}

	public function getLink()
	{
		return config('evibe.dash_url') . 'services/details/' . $this->id;
	}

	public function scopeIsLive($query)
	{
		return $query->where('type_service.is_live', 1);
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
		             ->whereNull('service_event.deleted_at')
		             ->where('service_event.type_event_id', $eventId);
	}

	public function scopeForCity($query, $cityId)
	{
		return $query->where('type_service.city_id', '=', $cityId);
	}
}