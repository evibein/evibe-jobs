<?php namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueHallGallery extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'venue_hall_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function hall()
	{
		return $this->belongsTo('App\Models\VenueHall', 'venue_hall_id');
	}
}