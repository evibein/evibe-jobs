<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venue extends Model
{
	use SoftDeletes;

	protected $table = 'venue';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function halls()
	{
		return $this->hasMany(VenueHall::class, 'venue_id');
	}

	public function images()
	{
		return $this->hasMany(VenueGallery::class, 'venue_id');
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function packages()
	{
		return $this->morphMany(VendorPackage::class, null, 'map_type_id', 'map_id');
	}

	public function bookingSettlements()
	{
		return $this->morphMany(ScheduleSettlementBookings::class, null, 'partner_type_id', 'partner_id');
	}

	public function settlements()
	{
		return $this->morphMany(Settlements::class, null, 'partner_type_id', 'partner_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function getPersonAttribute($value)
	{
		return ucwords(strtolower($value));
	}
}