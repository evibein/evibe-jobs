<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueExtRatings extends \Eloquent
{

    use SoftDeletes;

    protected $table = 'venue_ext_ratings';
    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    public static $rules = array();

    public function venue()
    {
        return $this->belongsTo('App\Models\Vendor', 'venue_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\TypeExtRatings', 'type_ratings_website_id');
    }

}
