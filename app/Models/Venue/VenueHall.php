<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueHall extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'venue_hall';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function provider()
	{
		return $this->belongsTo('App\Models\Venue', 'venue_id');
	}

	public function venue()
	{
		return $this->belongsTo('App\Models\Venue', 'venue_id');
	}

	public function images()
	{
		return $this->hasMany('App\Models\VenueHallGallery', 'venue_hall_id');
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('venue_hall_event', 'venue_hall_event.venue_hall_id', '=', 'venue_hall.id')
		             ->whereNull('venue_hall_event.deleted_at')
		             ->where('venue_hall_event.event_id', $eventId);
	}

}