<?php namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since 10 June 2015
 */

use Illuminate\Database\Eloquent\SoftDeletes;

class VenueGallery extends \Eloquent
{

    use SoftDeletes;

    protected $table = 'venue_gallery';
    protected $guarded = array('id');
    public static $rules = array();
    protected $dates = ['deleted_at'];

    public function venue()
    {
        return $this->belongsTo('App\Models\Venue', 'venue_id');
    }

}