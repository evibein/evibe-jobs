<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorSignup extends Model
{
	use SoftDeletes;

	protected $table = 'vendor_signup';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

}
