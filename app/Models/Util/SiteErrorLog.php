<?php namespace App\Models;

class SiteErrorLog extends BaseModel
{

	protected $table = 'log_site_errors';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;

}