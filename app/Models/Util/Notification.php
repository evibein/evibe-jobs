<?php

namespace App\Models\Util;

use App\Models\BaseModel;
use App\Models\Ticket;

class Notification extends BaseModel
{
	protected $table = 'notifications';

	public function scopeIsActive($query)
	{
		$invalidIds = [
			config('evibe.ticket.status.confirmed'),
			config('evibe.ticket.status.booked'),
			config('evibe.ticket.status.cancelled'),
			config('evibe.ticket.status.enquiry'),
			config('evibe.ticket.status.auto_cancel'),
		];

		return $query->join('ticket', 'ticket.id', '=', 'notifications.ticket_id')
		             ->join('ticket_mapping', 'notifications.ticket_mapping_id', '=', 'ticket_mapping.id')
		             ->whereNotIn('ticket.status_id', $invalidIds)
		             ->whereNull('notifications.is_replied')
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('ticket_mapping.deleted_at');
	}

	public function scopeSelectCols($query)
	{
		$query->select('notifications.*');
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

}
