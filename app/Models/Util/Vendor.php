<?php

namespace App\Models;

use App\Models\Util\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
	use SoftDeletes;

	protected $table = 'planner';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function packages()
	{
		return $this->morphMany(VendorPackage::class, null, 'map_type_id', 'map_id');
	}

	public function bookingSettlements()
	{
		return $this->morphMany(ScheduleSettlementBookings::class, null, 'partner_type_id', 'partner_id');
	}

	public function settlements()
	{
		return $this->morphMany(Settlements::class, null, 'partner_type_id', 'partner_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function getPersonAttribute($value)
	{
		return ucwords(strtolower($value));
	}
}