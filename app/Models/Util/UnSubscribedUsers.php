<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class UnSubscribedUsers extends BaseModel
{
	protected $table = 'unsubscribed_users';
}