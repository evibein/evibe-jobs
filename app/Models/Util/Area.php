<?php

namespace App\Models;

/**
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

use App\Models\Util\City;
use Guzzle\Service\Resource\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends \Eloquent
{
	use SoftDeletes;

	protected $table = 'area';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}
}
