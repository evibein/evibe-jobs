<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class ProductSortOrder extends BaseModel
{
	protected $table = 'product_sort_order';
}