<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class TypeCancellationReason extends BaseModel
{
	protected $table = 'type_cancellation_reason';
}
