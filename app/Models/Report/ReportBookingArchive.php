<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportBookingArchive extends Model
{
	use SoftDeletes;

	protected $table = 'report_booking_archive';
	protected $primaryKey = 'id';

	public function booking()
	{
		return $this->belongsTo('App\Models\TicketBooking', 'ticket_booking_id');
	}
}