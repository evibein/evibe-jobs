<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackBookingLive extends Model
{
	use SoftDeletes;

	protected $table = 'track_booking_live';
	protected $guarded = [];
	protected $dates = ['deleted_at'];
	public static $rules = [];

	public function booking()
	{
		return $this->belongsTo('App\Models\TicketBooking', 'ticket_booking_id');
	}

	public function cloudPhone()
	{
		return $this->belongsTo('App\Models\TypeCloudPhone', 'type_cloud_phone_id');
	}
}
