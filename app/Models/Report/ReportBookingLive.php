<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportBookingLive extends Model
{
	use SoftDeletes;
	protected $table = 'report_booking_live';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function booking()
	{
		return $this->belongsTo('App\Models\TicketBooking', 'ticket_booking_id');
	}

	public function cloudPhone()
	{
		return $this->belongsTo('App\Models\TypeCloudPhone', 'type_cloud_phone_id');
	}
}