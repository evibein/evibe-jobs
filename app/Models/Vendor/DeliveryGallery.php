<?php

namespace App\Models\Vendor;

use App\Models\TicketBooking;
use Illuminate\Database\Eloquent\Model;

class DeliveryGallery extends Model
{
	protected $table = 'delivery_gallery';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function delivery()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}
}