<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;

class PlannerReview extends BaseModel
{
	protected $table = 'planner_review';
}