<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerAverageRating extends Model
{
	protected $table = 'partner_average_rating';
	protected $guarded = ['id'];
	public static $rules = [];
	public $timestamps = true;
}