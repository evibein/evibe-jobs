<?php

namespace App\Models\Landing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandingSignUpUser extends Model
{
	use SoftDeletes;

	protected $table = 'landing_signup_users';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];
}