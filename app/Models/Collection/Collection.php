<?php

namespace App\Models\Collection;

use App\Models\BaseModel;

class Collection extends BaseModel
{
	protected $table = 'collection';
}
