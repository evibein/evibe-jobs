<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogTicketReminders extends Model
{
	use SoftDeletes;

	protected $table = 'log_ticket_reminders';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}
