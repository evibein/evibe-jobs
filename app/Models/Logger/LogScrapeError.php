<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogScrapeError extends \Eloquent
{

    use SoftDeletes;

    protected $table = 'log_scrape_error';
    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    public static $rules = array();

}