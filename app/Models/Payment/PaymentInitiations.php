<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentInitiations extends Model
{
	use SoftDeletes;

	protected $table = 'payment_initiations';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
	public static $rules = [];

	public function scopeForTickets($query)
	{
		$query->whereNull("piab_id")
		      ->whereNotNull("ticket_id");
	}

	public function scopeForPIAB($query)
	{
		$query->whereNull("ticket_id")
		      ->whereNotNull("piab_id");
	}
}