<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settlements extends Model
{
	use SoftDeletes;

	protected $table = 'settlements';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;

	public function partner()
	{
		return $this->morphTo(null, 'partner_type_id', 'partner_id');
	}
}