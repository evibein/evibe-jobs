<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleSettlementBookings extends Model
{
	use SoftDeletes;

	protected $table = 'schedule_settlement_bookings';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;

	public function partner()
	{
		return $this->morphTo(null, 'partner_type_id', 'partner_id');
	}
}