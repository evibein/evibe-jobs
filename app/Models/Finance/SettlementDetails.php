<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettlementDetails extends Model
{
	use SoftDeletes;

	protected $table = 'settlement_details';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;
}