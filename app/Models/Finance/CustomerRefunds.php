<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRefunds extends Model
{
	use SoftDeletes;

	protected $table = 'customer_refunds';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $softDelete = true;
}