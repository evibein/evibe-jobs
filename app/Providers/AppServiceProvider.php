<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Queue::after(function (JobProcessed $event) {
			Log::info("Queue Log");
			Log::info(print_r($event->job->payload(), true));
		});

		// DB::listen(function ($query) {
		// 	File::append(
		// 		storage_path('/logs/query.log'),
		// 		microtime() . " - " . $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
		// 	);
		// });
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
