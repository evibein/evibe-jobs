<?php

namespace App\Providers;

/**
 * @author Anji <anji@evibe.in>
 * @since  2 Jan 2015
 */

use Evibe\Utilities\EvibeUtil;
use Illuminate\Support\ServiceProvider;

class EvibeUtilServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		//$this->package('evibe/util');
	}

	public function register()
	{
		// No code
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['evibe.util'];
	}
}
