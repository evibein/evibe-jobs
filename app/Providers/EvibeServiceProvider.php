<?php

namespace App\Providers;

use App\Models\Vendor;
use App\Models\Venue;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class EvibeServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Model morphs
		$morphs = [
			config('evibe.ticket_type.planners') => Vendor::class,
			config('evibe.ticket_type.artists')  => Vendor::class,
			config('evibe.ticket_type.venues')   => Venue::class,
			config('evibe.ticket_type.trends')   => Vendor::class,
			config('evibe.ticket_type.decors')   => Vendor::class,
			config('evibe.ticket_type.cakes')    => Vendor::class,
			config('evibe.ticket_type.packages') => Vendor::class,
		];

		Relation::morphMap($morphs, false);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
