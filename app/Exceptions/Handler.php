<?php

namespace App\Exceptions;

use App\Models\SiteErrorLog;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		AuthorizationException::class,
		HttpException::class,
		ModelNotFoundException::class,
		ValidationException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 *
	 * @return void
	 */
	public function report(Exception $e)
	{
		parent::report($e);

		$code = ' ERROR';

		Log::info('---------------- ERROR LOG BEGINS ------------------');
		Log::info(' Request URL: ' . request()->fullUrl());
		Log::info(' Method: ' . request()->method());
		Log::info('     *** error trace being ***');
		Log::error($e->getTraceAsString());
		Log::info('     *** error trace end ***');
		Log::info('---------------- ERROR LOG ENDS ------------------');

		if ($e instanceof ModelNotFoundException)
		{
			$exceptionType = 'ModelNotFoundException';
		}

		elseif ($e instanceof HttpException)
		{
			$exceptionType = 'HttpException';
			$code = $e->getStatusCode();
		}
		elseif ($e instanceof ModelNotFoundException)
		{
			$exceptionType = 'ModelNotFoundException';
		}
		elseif ($e instanceof AuthorizationException)
		{
			$exceptionType = 'AuthorizationException';
		}
		else
		{
			$exceptionType = 'Error Exception';
		}

		$errorData = [
			'url'        => request()->fullUrl(),
			'exception'  => $exceptionType,
			'code'       => $code,
			'project_id' => config('evibe.project_id'),
			'details'    => $e->getTraceAsString(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		SiteErrorLog::create($errorData);

		if (config('evibe.mail_errors') &&
			strpos($e->getMessage(), "web_buttons") == false)
		{
			$emailData = [
				'code'    => $code,
				'method'  => request()->getMethod(),
				'error'   => $code,
				'message' => $e->getMessage(),
				'trace'   => $e->getTraceAsString()
			];

			Queue::push('Evibe\Utilities\SendEmail@sendErrorMessageToTeam', $emailData);
		}
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception               $e
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		return parent::render($request, $e);
	}
}
