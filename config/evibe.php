<?php

/**
 * Evibe admin config file
 *
 * @author Harish <harish.annavajjala@evibe.in>
 */
return [

	/*
	|--------------------------------------------------------------------------
	| Contacts
	|--------------------------------------------------------------------------
	|
	| Contacts (name, email, phone) of all relevant teams
	|
	*/
	'contact' => [
		'operations'  => [
			'name'                  => env('OPERATION_HEAD_NAME', 'Ashok Mettu'),
			'phone'                 => env('OPERATION_HEAD_PHONE', '9640807000'),
			'email'                 => env('OPERATION_HEAD_EMAIL', 'ashok.mettu@evibe.in'),
			'group'                 => env('OPERATION_GROUP_EMAIL', 'ops@evibe.in'),
			'alert_no_action_email' => env('OPERATIONS_ALERT_NO_ACTION_EMAIL', 'ops.alert.noaction@evibe.in')
		],
		'admin'       => [
			'group' => env('ADMIN_GROUP', 'admins@evibe.in'),
			'name'  => env('ADMIN_NAME', 'Anji'),
			'phone' => env('ADMIN_PHONE', '7259509827'),
			'email' => env('ADMIN_EMAIL', 'anji@evibe.in')
		],
		'customer'    => [
			'email' => env('CUSTOMER_HEAD_EMAIL', 'swathi@evibe.in'),
			'group' => env('CUSTOMER_GROUP_EMAIL', 'enquiry@evibe.in')
		],
		'business'    => [
			'name'  => env('BIZ_HEAD_NAME', 'Business Team'),
			'email' => env('BIZ_HEAD_EMAIL', 'business@evibe.in'),
			'group' => env('BIZ_GROUP_EMAIL', 'business@evibe.in'),
			'sr_bd' => env('BIZ_SR_EMAIL', 'business@evibe.in')
		],
		'tech'        => [
			'group' => env('TECH_GROUP_EMAIL', 'tech@evibe.in'),
			'email' => env('TECH_ADMIN_EMAIL', 'sysad@evibe.in'),
		],
		'marketing'   => [
			'group' => env('MARKETING_GROUP_EMAIL', 'marketing@evibe.in')
		],
		'company'     => '9640204000',
		'invitations' => [
			'group' => env('INVITE_GROUP_EMAIL', 'invites@evibe.in')
		],
		'system_alert_email' => env('SYSTEM_ALERT_EMAIL', 'system.alert@evibe.in'),
	],

	'main_url'             => env('MAIN_URL', 'http://evibe.in/'),
	'dash_url'             => env('DASH_URL', 'http://dash.evibe.in/'),
	'phone'                => '+91 9640204000',
	'phone_plain'          => '9640204000',
	'google_provider_code' => env('GOOGLE_PROVIDER_CODE', 'AIzaSyC3wD8SEsHtISBtiUjxOm3e9BQnT6vP0js'),
	'google_shorten_key'   => env('GOOGLE_SHORTEN_KEY', 'AIzaSyC6FR0Ajmt-28vUUwSfLMluBIkCezeilRU'),
	'email'                => env('PING_EMAIL', 'ping@evibe.in'),
	'email_hello'          => env('PING_EMAIL_HELLO', 'hello@evibe.in'),
	'accounts_email'       => env("ACCOUNTS_EMAIL", "accounts@evibe.in"),
	'mail_errors'          => env('MAIL_ERROR', true),

	/*
	|--------------------------------------------------------------------------
	| Cancellation email types
	|--------------------------------------------------------------------------
	|
	| Values used to check if order cancellation email is to be sent / not
	|
	*/

	'cancellation_mail' => [
		'auto_action'    => 'autoCancellationMail',
		'is_auto_on'     => '1',
		'is_auto_off'    => '0',
		'is_success_on'  => '1',
		'is_success_off' => '0'
	],

	'cancellation_reason_default_id' => env('TYPE_CANCELLATION_DEFAULT_ID', 3),

	/*
	|--------------------------------------------------------------------------
	| SMS Templates
	|--------------------------------------------------------------------------
	*/

	'sms_tpl' => [
		'cust_feedback'     => 'Hi #field1#, we hope you had a great party. Please share your feedback here: #field2# and help us build the best platform for celebrations! Thanks in advance, Team Evibe',
		'cancel'            => [
			'customer' => 'Hi #field1#, we regret to inform that your party order for #field2# is canceled as #field3#. If you want to book, click here #field5# to request a new payment link. Call #field4# for any queries. Team Evibe.in',
			'vendor'   => 'Hi #field1#, we regret to inform that booking order for #field2# by #field3# got cancelled since #field4# Evibe.in'
		],
		'reminder'          => [
			'customer'                      => 'Hi #field1#, we are all set for your event. Kindly carry Rs. #field2# for balance settlement. Wish you a memorable event from all of us at Evibe.in. Track your order with coordinator details here: #field3#',
			'customer_paid'                 => 'Hi #field1#, we are all set for your event. Wish you a memorable event from all of us at Evibe.in. Track your order with coordinator details here: #field3#',
			'team'                          => 'Hi, followup #field1# (#field2#) for #field3# event. Date: #field4#, Area: #field5#, Phone: #field6#. Evibe.in',
			'partner_single_order'          => 'Hi #field1#, you have #field2# party tomorrow at #field3#, #field4#. We hope everything is on track. Click #field5# to review order details. If you have any issue, please call #field6# immediately. Team Evibe.in',
			'partner_multiple_order'        => 'Hi #field1#, you have #field2# party orders for tomorrow (#field3#). We hope everything is on track. Click #field4# for all order details. If you have any issue, please call #field5# immediately. Team Evibe.in',
			'partner_non_venue_3hrs_before' => 'Hi #field1#, you have #field2# party in #field3# hours at #field4#. We hope you are on your way. Click #field5# to cross check order details. If you have any issue,  please call #field6# immediately. Team Evibe.in',
			'partner_venue_3hrs_before'     => 'Hi #field1#, you have #field2# party in #field3# hours. We hope all the arrangements are ready. Click #field4# to cross check order details. If you have any issue,  please call #field5# immediately. Team Evibe.in',
			'followup'                      => [
				'rr1'  => 'Hi #customerName#, here are the best recommendations for your party: #recoLink#. Please check and submit your shortlist. Team Evibe.in',
				'rr2'  => 'Hi #customerName#, have you checked handpicked recommendations for your party on #partyDate#? Click #recoLink# to check and submit your shortlist. Need help? Pls. call #evibePhone#. Evibe.in',
				'rr3'  => 'Hi #customerName#, we did not get your shortlist yet. Pls. check top recommendations for your party here: #recoLink# and submit your shortlist for best deals. Team Evibe.in',
				'rr4'  => 'Hi #customerName#, your party is just in #partyDaysCount# days. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#. Evibe.in',
				'rr5'  => 'Hi #customerName#, we recommended top options for your party. Pls. select options you liked and submit your shortlist here: #recoLink#. Need help? Call #evibePhone#. Did not like anything? Click #notLikeLink#. Evibe.in',
				'rr6'  => 'Hi #customerName#, your party is just in #partyDaysCount# days, let Evibe.in: No. 1 party planners, help you host a memorable party. Submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr7'  => 'Hi #customerName#, last minute offer for your #eventName# party #partyDateString#. Get #discountPrice#* OFF valid only #couponValidString#. Use coupon code #couponCode#. Need help? Call #evibePhone#. Evibe.in',
				'rr8'  => 'Hi #customerName#, at Evibe.in, you get best price and service for your party. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr9'  => 'Hi #customerName#, at Evibe.in, you get 100% service delivery guarantee for your party. Pls check top recommendations and submit your shortlist here: #recoLink#  to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr10' => 'Hi #customerName#, at Evibe.in, you get best prices and 100% service delivery guarantee for your party. Pls check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.',
				'rr11' => 'Hi #customerName#, book your party hasslefree at Evibe.in: No. 1 party planners. Pls check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			]
		],
		'track'             => [
			'vendor' => 'Hi #field1#, hope everything is set for #field2# party (#field3#, #field4#) at #field5# #daytype# and you / team will reach venue by #field6#. Give missed call to #field7# before #field8# to inform #field9#. Evibe.in',
			'team'   => [
				'fail' => 'Hi #field1#, there was no response from #field2# for #field3# party tracking (#field4#, #dayType#, #field5#). Pls call #field6# & act immediately. Evibe.in'
			]
		],
		'report'            => [
			'vendor'   => [
				'pin'  => 'Hi #field1#, PIN number for #field2# party (#field3#) is #field4#. Pls collect OTP from #field5# (#field6#) & call #field7# to record your reporting time. Need help? call #field8#. Evibe.in',
				'fail' => 'Hi #field1#, we have not received your reporting time for #field2# party (#field3#). Pls. call #field4# to report. Evibe.in'
			],
			'customer' => [
				'otp' => 'Hi #field1#, pls pass following PIN & OTP to #field2# / team to record reporting time. PIN: #field3#, OTP: #field4#. Number to dial: #field5#. Team Evibe',
			],
			'team'     => [
				'fail' => 'We did not get reporting time for #field1# party (#field2#, #field3#). Party start time: #field4#. Pls call #field5# (#field6#). Evibe.in'
			]
		],
		'payment'           => [
			'reminder'        => 'Hi #field1#, we really do not want you to miss a memorable party. Gentle reminder to pay advance of Rs. #field2# by #field3# for your party on #field4# here: #field5#. As there are other enquiries lined up, we will not be able to block your slot beyond that. Evibe.in',
			'retry_customer'  => 'Hi #field1#, facing problem making payment? Use GooglePay/PhonePe/PayTM/BHIM and send to #field2#. We will confirm your order within #field3# business hours. Need help? Pls. write to #field4# or call #field5# Evibe.in',
			'retry_customer1' => 'Hi #field1#, facing problem making payment? Use link: #field2# to complete your payment. Need help? Pls. write to #field3# or call #field4#. Evibe.in',
			'failed'          => 'Your payment for order #field1# has failed. We apologize for the inconvenience caused. Any amount debited will be refunded within #field2# business days. Evibe.in'
		],
		'settlement'        => [
			'customer' => [
				'single_vendor' => 'Hi #field1#, to make it easy for you, here is summary of balance payment. #field2#: Rs. #field3#. Team Evibe',
				'double_vendor' => 'Hi #field1#, to make it easy for you, here is summary of balance payment. #field2#: Rs. #field3#; #field4#: Rs. #field5#. Team Evibe',
				'triple_vendor' => 'Hi #field1#, to make it easy for you, here is summary of balance payment. #field2#: Rs. #field3#; #field4#: Rs. #field5#; #field6#: Rs. #field7#. Team Evibe',
				'four_vendor'   => 'Hi #field1#, to make it easy for you, here is summary of balance payment. #field2#: Rs. #field3#; #field4#: Rs. #field5#; #field6#: Rs. #field7#; #field8#: Rs. #field9#. Team Evibe'
			]
		],
		'followup'          => [
			'reco' => 'Hi #field1#, have you checked handpicked recommendations for your party on #field2#? Click here: #field3# to select your choices. Have questions? pls. call #field4#. Team Evibe.in'
		],
		'refer_earn'        => [
			"referral" => [
				"signup_success" => "Hi #field1#, thanks for booking with Evibe.in. Now refer your friends and get unlimited rewards. Please share your unique link #field2# with your friends on WhatsApp/Facebook etc. Your friend gets #field3#* OFF on sign up and you will get #field4#* OFF when they book."
			]
		],
		'rsvp-responses'    => "Hi #customerName#, we wish you to have a memorable party. Click here: #RSVPLink# to find the list of all responses for your invitation RSVP. Team Evibe.in",
		'landing'           => [
			'surprises' => "Hi, I am #field1# from Evibe.in - Trusted Surprise Planner. Are you still looking for best surprises? Use your code #field2# to get Rs. #field3#* on top candlelight dinners, flash mobs, balloons, rose decors and more surprises at best prices only at #field4#. Need help? pls. call me on #field5#."
		],
		'drop-off-feedback' => 'Hi #field1#,
We need your help in building Evibe.in.
Take short survey & get Rs. #field2# Amazon cash
Link - #field3#',
	],

	/*
	|--------------------------------------------------------------------------
	| Tickets
	|--------------------------------------------------------------------------
	*/
	'ticket'  => [
		'status' => [
			'initiated'        => 1,
			'progress'         => 2,
			'confirmed'        => 3,
			'booked'           => 4,
			'cancelled'        => 5,
			'enquiry'          => 6,
			'followup'         => 7,
			'auto_pay'         => 8,
			'auto_cancel'      => 9,
			'service_auto_pay' => env('TYPE_STATUS_SERVICE_AP', 12),
		],

		'type' => [
			'packages'           => 1,
			'planners'           => 2,
			'venues'             => 3,
			'services'           => 4,
			'trends'             => 5,
			'cakes'              => 6,
			'home_page'          => 7,
			'no_results'         => 8,
			'dash'               => 9,
			'header'             => 10,
			'error'              => 11,
			'experience'         => 12,
			'decors'             => 13,
			'entertainments'     => 14,
			'venue_halls'        => 15,
			'artists'            => 16,
			'resort'             => 17,
			'villa'              => 18,
			'lounge'             => 19,
			'food'               => 20,
			'couple-experiences' => 21,
			'venue-deals'        => 22,
			'priests'            => 23,
			'tents'              => 24,
			'party_bag'          => 25,
			'campaign'           => 26,
			'generic-package'    => 27,
			'add-ons'            => 28
		],
	],

	'ticket_type_booking' => [
		'photo' => env('TYPE_BOOKING_PHOTO_ID', 12),
		'video' => env('TYPE_BOOKING_VIDEO_ID', 13)
	],

	'gallery'  => [
		'root' => env('GALLERY_ROOT', '../gallery/'),
		'host' => env('GALLERY_HOST', 'http://gallery.evibe.in'),
		'type' => [
			'image' => 0,
			'video' => 1
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Track & Reporting
	|--------------------------------------------------------------------------
	|
	| These values should be in sync with database table type_scenario
	*/
	'scenario' => [
		'track'   => 1,
		'report'  => 2,
		'timings' => [
			'otp_send'  => env('REPORTING_OTP_TIMING', 5),
			'otp_check' => env('REPORTING_CHECK_TIMING', 10)
		]
	],

	'ticket_type' => [
		'packages'           => 1,
		'planners'           => 2,
		'venues'             => 3,
		'services'           => 4,
		'trends'             => 5,
		'cakes'              => 6,
		'home_page'          => 7,
		'no_results'         => 8,
		'dash'               => 9,
		'header'             => 10,
		'error'              => 11,
		'experience'         => 12,
		'decors'             => 13,
		'entertainments'     => 14,
		'venue_halls'        => 15,
		'artists'            => 16,
		'resort'             => 17,
		'villa'              => 18,
		'lounge'             => 19,
		'food'               => 20,
		'couple-experiences' => 21,
		'venue-deals'        => 22,
		'priests'            => 23,
		'tents'              => 24,
		'party_bag'          => 25,
		'campaign'           => 26
	],

	'api' => [
		'base_url'       => env('API_BASE_URL', 'http://apis.evibe.in/'),
		'client_id'      => env('JOB_CLIENT_ID'),
		'partner'        => [
			'prefix' => 'partner/v1/',
		],
		'auto-followups' => [
			'prefix' => 'common/auto-followups/v1/'
		],
		'finance'        => [
			'settlement'         => 'common/finance/v1/settlements/',
			'booking-settlement' => 'common/finance/v1/bookings/'
		],
		'watermark'      => [
			'prefix' => 'images/water-mark-images-a9640204000',
			'remove' => [
				'prefix' => 'images/remove-watermark-a9640204000'
			]
		],
	],

	'google' => [
		'geocode_key' => env('GEOCODE_API_KEY', 'AIzaSyD_Qo7W34ogn1GuZZcVCFQhmDj1K9Yx3Rs'),
	],

	'bitly' => [
		'access_token'     => env('BITLY_ACCESS_TOKEN'),
		'alt_access_token' => env('BITLY_ALT_ACCESS_TOKEN'),
	],

	'evibes' => [
		'access_token' => env('EVIBES_SHORTEN_ACCESS_TOKEN', "11feae8ebff7ca417beab3586e0f8a")
	],

	/*
	|----------------------------------------------------------------------------
	| Partner app key, being used for app notification for partner application
	|----------------------------------------------------------------------------
	*/

	'partner_app_key' => [
		'enquiry'  => [
			'list'    => 'enquiry_list',
			'details' => 'enquiry_details'
		],
		'order'    => [
			'list'    => 'order_list',
			'details' => 'order_details'
		],
		'delivery' => [
			'list'    => 'delivery_list',
			'details' => 'delivery_details'
		]
	],

	'enquiry'         => [
		'avl_check'    => 2,
		'custom_quote' => 4,

		'deadline_hrs' => [
			'avl_check'    => 2,
			'custom_quote' => 4
		]
	],
	'project_id'      => env('PROJECT_ID_JOBS', 5),
	'default_handler' => env('DEFAULT_HANDLER', 1),

	'advance'             => [
		'percentage' => env('PAYMENT_ADVANCE', 50),
	],
	/*
	|--------------------------------------------------------------------------
	| Live Website Profile Urls
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `evibe` config file in `EvibeMain` project
	|
	*/
	'live'                => [
		'checkout'    => env('LIVE_CHECKOUT', 'https://evibe.in/pay/checkout'),
		'ab-checkout' => env('LIVE_AB_CHECKOUT', 'https://evibe.in/auto-book/now/checkout'),
		'profile_url' => [
			'planner'           => 'birthday-party-planners',
			'venues'            => 'birthday-party-planners/venues',
			'packages'          => 'birthday-party-planners/packages',
			'experiences'       => 'special-couple-experiences',
			'trends'            => 'birthday-party-planners/trends',
			'cakes'             => 'birthday-party-planners/cakes',
			'decors'            => 'birthday-party-planners/birthday-party-decorations',
			'entertainment'     => 'birthday-party-planners/entertainment-activities-stalls',
			'bachelor'          => [
				'resorts'       => 'bachelor-party/resorts',
				'villas'        => 'bachelor-party/villas',
				'lounges'       => 'bachelor-party/lounges',
				'entertainment' => 'bachelor-party/entertainment',
				'food'          => 'bachelor-party/food',
				'cakes'         => 'bachelor-party/cakes',
			],
			'venue-deals'       => 'birthday-party-planners/venue-deals',
			'pre-post'          => [
				'venues'        => 'engagement-wedding-reception/venues',
				'decors'        => 'engagement-wedding-reception/decorations/all',
				'entertainment' => 'engagement-wedding-reception/entertainment',
				'cakes'         => 'engagement-wedding-reception/cakes'
			],
			'couple_experience' => [
				'packages' => 'special-couple-experiences/packages'
			],
		],
	],
	/*
	|--------------------------------------------------------------------------
	| Event type
	|--------------------------------------------------------------------------
	*/
	'event'               => [
		'kids_birthday'      => env('EVENT_KIDS_BIRTHDAYS', 1),
		'bachelor_party'     => env('EVENT_BACHELOR_PARTY', 11),
		'pre_post'           => env('EVENT_PRE_POST', 13),
		'house_warming'      => env('EVENT_HOUSE_WARMING', 14),
		'special_experience' => env('EVENT_COUPLE_EXPERIENCES', 16),
		'first_birthday'     => env('EVENT_FIRST_BIRTHDAY', 36),
		'birthday_2-5'       => env('EVENT_BIRTHDAY_2-5', 37),
		'birthday_6-12'      => env('EVENT_BIRTHDAY_6-12', 38),
		'naming_ceremony'    => env('EVENT_NAMING_CEREMONY', 8),
		'baby_shower'        => env('EVENT_BABY_SHOWER', 19)
	],

	/*
	 * Type Reminder Group
	 */
	'type_reminder_group' => [
		'followup'       => [
			'rr1'  => 1,
			'rr2'  => 2,
			'rr3'  => 3,
			'rr4'  => 4,
			'rr5'  => 5,
			'rr6'  => 6,
			'rr7'  => 7,
			'rr8'  => 8,
			'rr9'  => 9,
			'rr10' => 10,
			'rr11' => 11,
		],
		'thank-you-card' => [
			'tc1' => env('TYPE_REMINDER_GROUP_TC1', 12),
			'tc2' => env('TYPE_REMINDER_GROUP_TC2', 18),
			'tc3' => env('TYPE_REMINDER_GROUP_TC3', 19)
		],
		'retention'      => [
			'rt1' => env('TYPE_REMINDER_GROUP_RT1', 13)
		]
	],

	'email_footer' => 'Thanks in advance,',

	/* star icon image paths for rating in email */
	'star_icon'    => [
		'full'    => "/img/icons/full_star.png",
		'34th'    => "/img/icons/34th_star.png",
		'half'    => "/img/icons/half_star.png",
		'quarter' => "/img/icons/quarter_star.png"
	],

	/*
	|--------------------------------------------------------------------------
	| Cities (Must be in sync with 'city' table)
	|--------------------------------------------------------------------------
	|
	| Used for Google places API && valid cities
	*/
	'city'         => [
		'bangalore'     => env('CITY_BANGALORE_ID', 1),
		'hyderabad'     => env('CITY_HYDERABAD_ID', 2),
		'scecunderabad' => env('CITY_HYDERABAD_ID', 2),
		'new_delhi'     => env('CITY_DELHI_ID', 3),
		'delhi'         => env('CITY_DELHI_ID', 3),
		'mumbai'        => env('CITY_MUMBAI_ID', 4),
		'pune'          => env('CITY_PUNE_ID', 5)

	],

	'default'    => [
		'evibe_service_fee' => 0.15,
		'gst'               => 0.18,
		'partial_gst'       => 0.09,
	],

	// default error codes for four methods
	'error_code' => [
		'create_function'  => 21,
		'fetch_function'   => 22,
		'update_function'  => 23,
		'deleted_function' => 24,
	],

	/*
	| Should be in sync with database `role` table
	*/
	'role'       => [
		'customer'         => env('CUSTOMER_ROLE_ID', 13),
		'super_admin'      => env('ROLE_SUPER_ADMIN', 1),
		'admin'            => env('ROLE_ADMIN', 2),
		'planner'          => env('ROLE_PLANNER', 3),
		'venue'            => env('ROLE_VENUE', 4),
		'bd'               => env('ROLE_BD', 5),
		'customer_delight' => env('ROLE_CUSTOMER_DELIGHT', 6),
		'artist'           => env('ROLE_ARTIST', 7),
		'operations'       => env('ROLE_OPERATIONS', 8),
		'sr_crm'           => env('ROLE_SENIOR_CRM', 9),
		'invite_usr'       => env('ROLE_INVITE_USER', 10),
		'marketing'        => env('ROLE_MARKETING', 11),
		'tech'             => env('ROLE_TECH', 12),
	]
];
