<?php

return [

	// start of time interval
	'time-interval' => [
		'1' => 0,
		'2' => 15,
		'3' => 60
	],

	// start of weights
	'weights'       => [
		'compute' => [
			'visits' => [
				'product-views' => 0.25,
				'shortlists'    => 0.40,
				'party-bag'     => 0.35
			],
			'sales'  => [
				'orders'              => 0.60,
				'enquiries'           => 0.25,
				'availability-checks' => 0.15
			]
		],
		'boost'   => [
			'visits' => [
				'i1' => 1.2,
				'i2' => 0.70,
				'i3' => 0.30
			],
			'sales'  => [
				'i1' => 1.50,
				'i2' => 1.20,
				'i3' => 0.80
			]
		],
		'overall' => [
			// @see: similar for all cities
			// occasion level
			'defaults' => [
				[
					'minPrice' => 0,
					'maxPrice' => 5000,
					'visits'   => 0.5,
					'sales'    => 0.6,
					'recency'  => 1.5
				],
				[
					'minPrice' => 5000,
					'maxPrice' => 10000,
					'visits'   => 0.8,
					'sales'    => 1.2,
					'recency'  => 1.75
				],
				[
					'minPrice' => 10000,
					'maxPrice' => 20000,
					'visits'   => 1,
					'sales'    => 1.5,
					'recency'  => 2
				],
				[
					'minPrice' => 20000,
					'maxPrice' => 40000,
					'visits'   => 1,
					'sales'    => 1.3,
					'recency'  => 1.85
				],
				[
					'minPrice' => 40000,
					'visits'   => 0.8,
					'sales'    => 1.2,
					'recency'  => 1.75
				]
			],
			'1'        => [
				// category level
				'defaults' => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'1'        => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'5'        => [
					[
						'minPrice' => 0,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					]
				],
				'6'        => [
					[
						'minPrice' => 0,
						'maxPrice' => 800,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 800,
						'maxPrice' => 1000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 1000,
						'maxPrice' => 1200,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1200,
						'maxPrice' => 1500,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1500,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'13'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'14'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 2000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 2000,
						'maxPrice' => 2500,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 2500,
						'maxPrice' => 3500,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 3500,
						'maxPrice' => 5000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 5000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'15'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					]
				],
				'20'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 4000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 4000,
						'maxPrice' => 8000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 8000,
						'maxPrice' => 16000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 16000,
						'maxPrice' => 24000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 24000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					]
				],
				'22'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 300,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 300,
						'maxPrice' => 500,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 800,
						'maxPrice' => 1000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					]
				],
				'24'       => [
					[
						'minPrice' => 0,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					]
				]
			],
			'14'       => [
				'defaults' => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'13'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 4000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 4000,
						'maxPrice' => 8000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 8000,
						'maxPrice' => 15000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 15000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'14'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 3000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 3000,
						'maxPrice' => 8000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 8000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					]
				],
				'6'        => [
					[
						'minPrice' => 0,
						'maxPrice' => 800,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 800,
						'maxPrice' => 1000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 1000,
						'maxPrice' => 1200,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1200,
						'maxPrice' => 1500,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1500,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'20'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 4000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 4000,
						'maxPrice' => 8000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 8000,
						'maxPrice' => 16000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 16000,
						'maxPrice' => 24000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 24000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'24'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 5000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					]
				],
				'23'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 20000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					]
				]
			],
			'16'       => [
				'defaults' => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'21'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 8000,
						'visits'   => 1,
						'sales'    => 1.2,
						'recency'  => 1.85
					],
					[
						'minPrice' => 8000,
						'maxPrice' => 12000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 12000,
						'maxPrice' => 15000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 15000,
						'maxPrice' => 18000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 18000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				]
			],
			'11'       => [
				'defaults' => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'18'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 8000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 8000,
						'maxPrice' => 12000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 12000,
						'maxPrice' => 16000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 16000,
						'maxPrice' => 24000,
						'visits'   => 0.5,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 24000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					]
				]
			],
			'13'       => [
				'defaults' => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				],
				'13'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 40000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					]
				],
				'14'       => [
					[
						'minPrice' => 0,
						'maxPrice' => 5000,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 5000,
						'maxPrice' => 10000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 10000,
						'maxPrice' => 20000,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 20000,
						'maxPrice' => 40000,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					],
					[
						'minPrice' => 40000,
						'visits'   => 0.5,
						'sales'    => 0.6,
						'recency'  => 1.5
					]
				],
				'6'        => [
					[
						'minPrice' => 0,
						'maxPrice' => 800,
						'visits'   => 0.75,
						'sales'    => 1.15,
						'recency'  => 1.7
					],
					[
						'minPrice' => 800,
						'maxPrice' => 1000,
						'visits'   => 1,
						'sales'    => 1.3,
						'recency'  => 1.85
					],
					[
						'minPrice' => 1000,
						'maxPrice' => 1200,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1200,
						'maxPrice' => 1500,
						'visits'   => 1,
						'sales'    => 1.5,
						'recency'  => 2
					],
					[
						'minPrice' => 1500,
						'visits'   => 0.8,
						'sales'    => 1.2,
						'recency'  => 1.75
					]
				]
			]
		]
	]

];