<?php

return [

	"path"      => env("ONESIGNAL_API_PATH", "https://onesignal.com/api/v1"),
	"appId"     => env("ONESIGNAL_APP_ID", "2fddf801-5723-4d68-bec4-8db284bd7c1c"),
	"ApiKey"    => env("ONESIGNAL_API_KEY", "MzAyYTc2NWUtNGY4My00YWQ3LTkzOWYtMjk1Y2FlNWI5YzJi"),
	"subdomain" => env("ONESIGNAL_SUBDOMAIN", "evibe-dash"),
	"segments"  => [
		"crm"    => [env("ONESIGNAL_SEGMENT_CRM", "CRM Team"), env("ONESIGNAL_SEGMENT_SR_CRM", "Sr CRM")],
		"admins" => [env("ONESIGNAL_SEGMENT_ADMIN", "Admins")],
		"bd"     => [env("ONESIGNAL_SEGMENT_BD", "BD Team")]
	]

];