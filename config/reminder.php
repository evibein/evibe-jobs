<?php

return [

	'communication' => [
		'email'        => 1,
		'sms'          => 2,
		'notification' => 3
	],

	'followup' => [
		'id'            => env('TYPE_REMINDER_FOLLOWUP', 1),
		'campaign-code' => 'RR',
		'group'         => [
			1  => [
				'name'     => 'rr1',
				'mailView' => 'emails.reminder.followup.rr1',
				//'emailSub' => 'Top recommendations for your {eventName}',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, here are the best recommendations for your party: #recoLink#. Please check & submit your shortlist. Team Evibe.in'
			],
			2  => [
				'name'     => 'rr2',
				'mailView' => 'emails.reminder.followup.rr2',
				//'emailSub' => 'Have you checked best recommendations for your {eventName}',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, have you checked handpicked recommendations for your party on #partyDate#? Click #recoLink# to check and submit your shortlist. Need help? Pls. call #evibePhone#. Evibe.in'
			],
			3  => [
				'name'     => 'rr3',
				'mailView' => 'emails.reminder.followup.rr3',
				//'emailSub' => 'Regarding recommendations for your {eventName}',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, we did not get your shortlist yet. Pls. check top recommendations for your party here: #recoLink# and submit your shortlist for best deals. Team Evibe.in'
			],
			4  => [
				'name'     => 'rr4',
				'mailView' => 'emails.reminder.followup.rr4',
				//'emailSub' => 'Your {eventName} is very close, we didn’t get your shortlist yet',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, your party is just in #partyDaysCount# days. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#. Evibe.in'
			],
			5  => [
				'name'     => 'rr5',
				'mailView' => 'emails.reminder.followup.rr5',
				//'emailSub' => 'Make your party memorable and blissful',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, we recommended top options for your party. Pls. select options you liked and submit your shortlist here: #recoLink#. Need help? Call #evibePhone#. Did not like anything? Click #notLikeLink#. Evibe.in'
			],
			6  => [
				'name'     => 'rr6',
				'mailView' => 'emails.reminder.followup.rr6',
				//'emailSub' => 'Your party is right around the corner',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, your party is just in #partyDaysCount# days, let Evibe.in: No. 1 party planners, help you host a memorable party. Submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			],
			7  => [
				'name'     => 'rr7',
				'mailView' => 'emails.reminder.followup.rr7',
				//'emailSub' => 'Last minute offer for your {eventName}',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, last minute offer for your #eventName# party #partyDateString#. Get #discountPrice#* OFF valid only #couponValidString#. Use coupon code #couponCode#. Need help? Call #evibePhone#. Evibe.in'
			],
			8  => [
				'name'     => 'rr8',
				'mailView' => 'emails.reminder.followup.rr8',
				//'emailSub' => 'Get your {eventName} party done at best services',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, at Evibe.in, you get best price and service for your party. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			],
			9  => [
				'name'     => 'rr9',
				'mailView' => 'emails.reminder.followup.rr9',
				//'emailSub' => 'We provide 100% service delivery guarantee for your {eventName}',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, at Evibe.in, you get 100% service delivery guarantee for your party. Pls check top recommendations and submit your shortlist here: #recoLink#  to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			],
			10 => [
				'name'     => 'rr10',
				'mailView' => 'emails.reminder.followup.rr10',
				//'emailSub' => 'Host a memorable {eventName} with Evibe',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, at Evibe.in, you get best prices and 100% service delivery guarantee for your party. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			],
			11 => [
				'name'     => 'rr11',
				'mailView' => 'emails.reminder.followup.rr11',
				//'emailSub' => 'Plan your {eventName} hassle free',
				'emailSub' => 'Regarding your event on {partyDateFormatted} - #{enquiryId}',
				'smsTpl'   => 'Hi #customerName#, book your party hasslefree at Evibe.in: No. 1 party planners. Pls. check top recommendations and submit your shortlist here: #recoLink# to process your order. Need help? Call #evibePhone#. Not interested? Click #cancelLink#.'
			]
		]
	],

	'thank-you-card' => [
		'id'            => env('TYPE_REMINDER_THANK_YOU_CARD', 2),
		'campaign-code' => 'TC',
		'group'         => [
			env('TYPE_REMINDER_GROUP_TC1', 12) => [
				'name'     => 'tc1',
				'mailView' => 'emails.reminder.thank-you-card.tc1',
				'emailSub' => 'Share this “Thank You” card with your guests after your party',
				'smsTpl'   => 'Hi #customerName#, share this e-thank you card on whatsapp/facebook with your guests after your party. This is a cool way to thank them. Click #smsTYCLink# to download the card. Team Evibe.in'
			],
			env('TYPE_REMINDER_GROUP_TC2', 18) => [
				'name'     => 'tc2',
				'mailView' => 'emails.reminder.thank-you-card.tc2',
				'emailSub' => 'Time to thank your party guests. Share this cool e-thank you card',
				'smsTpl'   => 'Hi #customerName#, time to thank your guests for making your party a memorable one. Click #smsTYCLink# to download the cool e-thank you card and share it with your guests on whatsapp/facebook. Team Evibe.in'
			],
			env('TYPE_REMINDER_GROUP_TC3', 19) => [
				'name'     => 'tc3',
				'mailView' => 'emails.reminder.thank-you-card.tc3',
				'emailSub' => 'Did you thank you guests yet?',
				'smsTpl'   => 'Hi #customerName#, did you thank your guests yet? Share this cool e-thank you card with your guests on whatsapp/facebook and show your gratitude. Click #smsTYCLink# to download the card now. Team Evibe.in'
			]
		]
	],

	'retention' => [
		'id'            => env('TYPE_REMINDER_RETENTION', 3),
		'campaign-code' => 'RT',
		'group'         => [
			env('TYPE_REMINDER_GROUP_RT1', 13) => [
				'name'     => 'rt1',
				'mailView' => 'emails.reminder.retention.rt1',
				'emailSub' => '{customerName}, how are you planning this time?',
				'smsTpl'   => 'Hi #customerName#, we loved celebrating your special occasion last time. Here is an exclusive offer only for you. Use coupon #couponCode# to get #discountPercent#%* OFF (max. Rs. #maxDiscountAmt#, valid till #validDate#). Plan Now: #smsBOLink# or make Quick Enquiry: #smsQELink# Evibe.in'
			],
			env('TYPE_REMINDER_GROUP_RT2', 14) => [
				'name'     => 'rt2',
				'mailView' => 'emails.reminder.retention.rt2',
				'emailSub' => 'You can still get {discountPercent}%* off on your next party booking',
				'smsTpl'   => 'Hi #customerName#, busy planning for your special day? Here is a special offer for you. Use coupon #couponCode# and get #discountPercent#%* OFF (max. Rs. #maxDiscountAmt#, valid up to #validDate#) on your party booking. Plan Now: #smsBOLink# or make Quick Enquiry: #smsQELink# Evibe.in'
			],
			env('TYPE_REMINDER_GROUP_RT3', 15) => [
				'name'     => 'rt3',
				'mailView' => 'emails.reminder.retention.rt3',
				'emailSub' => 'Planning your event at last minute? We are here for you with a special offer',
				'smsTpl'   => 'Hi #customerName#, planning your special day at the last minute? Trust Evibe.in to make it a grand success. Use coupon #couponCode# to get #discountPercent#%* OFF (max. Rs. #maxDiscountAmt#, valid up to #validDate#). Plan Now: #smsBOLink# or make Quick Enquiry: #smsQELink#'
			],
			env('TYPE_REMINDER_GROUP_RT4', 16) => [
				'name'     => 'rt4',
				'mailView' => 'emails.reminder.retention.rt4',
				'emailSub' => 'Only few days left. Offer expires soon!',
				'smsTpl'   => 'Hi #customerName#, your special offer expires in #countCE# days. Use coupon #couponCode# for your party booking to get #discountPercent#%* OFF (max Rs. #maxDiscountAmt#, valid upto #validDate#). Plan Now: #smsBOLink# or make Quick Enquiry: #smsQELink# Evibe.in'
			],
			env('TYPE_REMINDER_GROUP_RT5', 17) => [
				'name'     => 'rt5',
				'mailView' => 'emails.reminder.retention.rt5',
				'emailSub' => 'Last {countCE} days to claim your offer!',
				'smsTpl'   => 'Hi #customerName#! Last #countCE# days to claim your special offer. Use coupon #couponCode# to book a party and get #discountPercent#%* OFF (max Rs. #maxDiscountAmt#, valid upto #validDate#). Plan Now: #smsBOLink# or make Quick Enquiry: #smsQELink# Evibe.in'
			]
		]
	]

];
