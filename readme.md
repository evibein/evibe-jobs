## Evibe JOBS

Evibe JOBS is designed to run automation like sending reminders, followups, collecting feedback, etc., in an organised and timely manner.

### Installation procedure
1. Clone/checkout the code from GIT.
2. Install composer[https://getcomposer.org/download/].
3. Go to your project folder in terminal and run `composer install`.
4. Copy .env.example to .env and change the DB credentials.
5. You are good to go!