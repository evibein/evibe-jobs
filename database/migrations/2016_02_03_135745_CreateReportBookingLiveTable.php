<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportBookingLiveTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_booking_live', function (Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('ticket_booking_id');
			$table->unsignedInteger('type_scenario_id');
			$table->unsignedInteger('type_cloud_phone_id');
			$table->string('otp');
			$table->string('pin');
			$table->timestamp('expires_at')->nullable();
			$table->string('vendor_phone');
			$table->text('vendor_sms_text')->nullable();
			$table->timestamp('send_vendor_sms_at');
			$table->timestamp('vendor_sms_sent_at')->nullable();
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('ticket_booking_id')
			      ->references('id')->on('ticket_bookings')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');

			$table->foreign('type_scenario_id')
			      ->references('id')->on('type_scenario')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');

			$table->foreign('type_cloud_phone_id')
			      ->references('id')->on('type_cloud_phone')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('report_booking_live', function (Blueprint $table)
		{
			$table->dropForeign('report_booking_live_ticket_booking_id_foreign');
			$table->dropForeign('report_booking_live_type_scenario_id_foreign');
			$table->dropForeign('report_booking_live_type_cloud_phone_id_foreign');
		});

		Schema::drop('report_booking_live');
	}
}
