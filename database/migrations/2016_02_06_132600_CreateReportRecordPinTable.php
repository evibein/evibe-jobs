<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportRecordPinTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_record_pin', function (Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('type_cloud_phone_id');
			$table->string('reporting_phone');
			$table->string('pin');
			$table->string('call_ref_id');
			$table->timestamp('pin_entered_at');
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('type_cloud_phone_id')
			      ->references('id')->on('type_cloud_phone')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('report_record_pin', function (Blueprint $table)
		{
			$table->dropForeign('report_record_pin_type_cloud_phone_id_foreign');
		});

		Schema::drop('report_record_pin');
	}
}
