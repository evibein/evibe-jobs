## Changelog

This change log will contain all the code changes for bug fixes, new features etc. Each version will contain sections: `Added`, `Changed`, `Removed`, `Fixed` (whichever is applicable).

## v2.2.10 (*06 Jan 2022*) `@NGK`
##### Changed
- Stopped feedback-reward SMS

## v2.2.9 (*03 Jan 2022*) `@NGK`
##### Changed
- Added isset check for message key of an array in ImportProductsToTableCommandHandler 

## v2.2.8 (*14 Mar 2020*) `@NGK`
##### Changed
- evib.es to bit.ly

## v2.2.7 (*13 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 2

## v2.2.6 (*08 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 2

## v2.2.5 (*07 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 1

## v2.2.4 (*20 Feb 2020*) `@NGK`
##### Added
- Drop off customers feedback

### v2.2.3 (*15 Jan 2020*) `@NGK`
##### Changed
- Feedback reminder timeline

### v2.2.2 (*28 Jan 2020*) `@JR`
##### Added
- DB Monitor

### v2.2.1 (*27 Dec 2019*) `@JR`
##### Modified
- Laravel upgrade to 5.4

### v2.2.0 (*27 Dec 2019*) `@JR`
##### Modified
- Laravel upgrade to 5.3

### v2.1.62 (*30 Nov 2019*) `@NGK`
##### Fixed
- Fetching feedback url from ticket which is being used later in the feedback controller

### v2.1.61 (*21 Sep 2019*) `@NGK`
##### Added
- Job to generate partner settlement invoices

### v2.1.60 (*08 Aug 2019*) `@NGK`
##### Changed
- 3 feedback reminder notification to customers (from 5)

### v2.1.59 (*11 Jul 2019*) `@JR`
##### Fixed
- Laravel backup timeout fix

### v2.1.58 (*03 Jul 2019*) `@NGK`
##### Added
- Job to automatically map partner reviews to options

### v2.1.57 (*21 May 2019*) `@GS`
##### Added
- Changed communication from business to ops for AB related emails.

### v2.1.56 (*20 May 2019*) `@JR`
##### Added
- Sending failed SMS/Email to customer for failed transactions

### v2.1.55 (*20 May 2019*) `@JR`
##### Added
- Running job to auto accept payments if now redirected to the success page

### v2.1.54 (*14 May 2019*) `@JR`
##### Modified
- Modified followup reminder subject lines for same email thread

### v2.1.53 (*17 Apr 2019*) `@JR`
##### Added
- Sending daily report for payments

### v2.1.52 (*16 Apr 2019*) `@JR`
##### Added
- Sending retry payment link if payment was not successful for 2 times

### v2.1.51 (*04 Apr 2019*) `@JR`
##### Added
- Added support for removing the watermark

### v2.1.50 (*22 Mar 2019*) `@NGK`
##### Changed
- SMS sender id to EVIBES from EEVIBE

### v2.1.49 (*22 Mar 2019*) `@JR`
##### Modified
- Modified shortening URL from goo.gl to evib.es

### v2.1.48 (*27 Feb 2019*) `@NGK`
##### Changed
- Payment deadline reminder SMS to customer

### v2.1.47 (*25 Feb 2019*) `@JR`
##### Modified
- Removing bachelor & prepost related results from global search table

### v2.1.46 (*15 Jan 2019*) `@JR`
##### Changed
- Event reminder sender to customer

### v2.1.45 (*12 Jan 2019*) `@JR`
##### Added
- Sending SMS to customers after payment failure

### v2.1.44 (*08 Jan 2019*) `@JR`
##### Modified
- Changed biy.ly to goo.gl until 31th mar 2019

### v2.1.43 (*08 Jan 2019*) `@JR`
##### Added
- Added alerts to team for payment failures

### v2.1.42 (*16 Dec 2018*) `@JR`
##### Changed
- Modified booking reminder SMS

### v2.1.41 (*15 Dec 2018*) `@NGK`
##### Added
- Promotional route to retention SMS

### v2.1.40 (*12 Dec 2018*) `@JR`
##### Added
- Reminder for google Ad tickets, one day before the party

### v2.1.39 (*8 Dec 2018*) `@AR`
##### Removed
- Ticket followup reminders to team at 9:30 AM
- Ticket followup reminders to team before 10 mins & missed followup

### v2.1.38 (*4 Dec 2018*) `@AR`
##### Changed
- Company phone number

### v2.1.38 (*10 Nov 2018*) `@NGK`
##### Fixed
- Temporary time switch to avoid bitly error

### v2.1.37 (*1 Oct 2018*) `@NGK`
##### Changed
- ReplyTo for TYC from enquiry to invites group

### v2.1.36 (*21 sEP 2018*) `@NGK`
##### Added
- SMS to notify host regarding RSVP

### v2.1.35 (*17 Sep 2018*) `@NGK`
##### Added
- Baby Shower occasion and card in TYC

### v2.1.34 (*15 Sep 2018*) `@JR`
##### Added
- Modified code for getting the users in refer & earn

### v2.1.33 (*11 Sep 2018*) `@NGK`
##### Added
- Reminder to send RSVP response alerts to hosts

### v2.1.32 (*10 Sep 2018*) `@JR`
##### Fixed
- Role ID's fix for creating user

### v2.1.31 (*07 Sep 2018*) `@JR`
##### Fixed
- Added fillable for user name

### v2.1.30 (*29 Aug 2018*) `@NGK`
##### Fixed
- Fixed sms error details format
- RE SMS issue fix

### v2.1.29 (*25 Aug 2018*) `@NGK`
##### Fixed
- Included sms error details and will be sent to sysad@evibe.in

### v2.1.28 (*25 Aug 2018*) `@NGK`
##### Added
- Email and SMS content for thank you card reminders

### v2.1.27 (*25 Aug 2018*) `@JR`
##### Fixed
- Undefined index in RE sms & undefined cc in admin error email

### v2.1.26 (*23 Aug 2018*) `@JR`
##### Added
- Added RE reminders after 30min of payment success

### v2.1.25 (*22 Aug 2018*) `@NGK`
##### Added
- Added additional reminder to send thank you cards 1 day before party

### v2.1.24 (*17 Aug 2018*) `@Manav`
##### Added
- Benefits in reminder mails being sent.

### v2.1.23 (*04 Aug 2018*) `@NGK`
##### Fixed
- issues with retention utm link and corrected fallout url.

### v2.1.22 (*01 Aug 2018*)
##### Removed
- Removed percentage of advance sent in mails `@Manav`

### v2.1.21 (*21 July 2018*)
##### Added
- Adding regeneration link to cancellation mail and sms `@Manav`

### v2.1.20 (*20 July 2018*) `@NGK`
##### Fixed
- Issue with city id unavailability.
##### Added
- Functionality to send follow-up for a particular reminder.

### v2.1.19 (*17 July 2018*) `@NGK`
##### Added
- Auto followup reminders will now work for retention.

### v2.1.18 (*06 July 2018*)
##### Added
- Sending payment deadline 2 times before 30min & 60min `@Manav`

### v2.1.17 (*06 July 2018*)
##### Added
- Added job to send thank you card reminders to customers `@NGK`

### v2.1.16 (*22 June 2018*)
##### Added
- Job to update product sort order table `@NGK`

### v2.1.15 (*21 June 2018*)
##### Changed
- Feedback email will be followed up for 5 times with varied timings `@NGK`

### v2.1.14 (*11 June 2018 ~ 14 June 2018*)
##### Added
- Watermark images`@Ramu`

### v2.1.13 (*13 June 2018*)
##### Fixed
- Global search URL not getting updated & deleted_at not getting updated in searchable table `@JR`

### v2.1.12 (*02 June 2018*)
##### Fixed
- Job error of trying get non-object fixed in ticker followup reminders `@Ramu`

### v2.1.11 (*30 May 2018*)
##### Added
- Updating profile Image & URL along with the table updation through main`@JR`

### v2.1.10 (*18 May 2018 ~ 19 May 2018*)
##### Added
- Added Extra fields to the searchable table `@JR`

### v2.1.9 (*18 May 2018*)
##### Fixed
- Issue with sending pending refunds email to team `@NGK`

### v2.1.8 (*14 May 2018*)
##### Modified
- Modified name in inclusions in elasticsearch index `@JR`

### v2.1.7 (*04 May 2018*)
##### Removed
- Moved partner pending delivery calculation to APIs. Only trigger functionality `@NGK`

### v2.1.6 (*25 Apr 2018*)
##### Fixed
- Coupon Codes generation `@JR`

### v2.1.5 (*24 Apr 2018*)
##### Fixed
- Not sending error messages if it's related to onesignal web push notifications `@JR`

### v2.1.4 (*20 Apr 2018*)
##### Removed
- Commented job to create partner settlements every Wednesday at 12:00 PM `@NGK`
##### Changed
- Implemented bitly url shortener `@NGK`

### v2.1.3 (*19 Apr 2018*)
##### Fixed
- Fixed issue with RR1, RR10 and RR11 sms templates `@NGK`

### v2.1.2 (*16 Apr 2018*)
##### Added
- Added ENS to debug refunds reminder handler `@NGK`

### v2.1.1 (*27 Mar 2018 ~ 4 Apr 2018*)
##### Added
- Auto Settlements v2 - Jobs to remind team regarding settlements `@NGK`

### v2.1.0 (*14 Mar 2018*)
##### Added
- Auto Settlements v1 - Job to create settlements `@NGK`

### v2.0.9 (*23 Jan 2018*)
##### Added
- Added a new job for filling pin codes which are NULL in the area table `@Ramu`

### v2.0.8 (*11 Jan 2018*)
##### Changed
- Modified 'TicketRemindersCommandHandler' to work with updated APIs `@NGK`

### v2.0.7 (*08 Jan 2018*)
##### Added
- Included site logs in 'TicketRemindersCommandHandler' to debug reminders automation `@NGK`

### v2.0.6 (*06 Jan 2018*)
##### Fixed
- Fall back case for handler mobile number in reminders handler `@NGK`

### v2.0.5 (*03 Jan 2018*)
##### Fixed
- App download logo div shifted outside the completed orders div to show even if there are no bookings `
- Pending deliveries count is going as 0, if there are no bookings `@JR`

### v2.0.4 (*26 Dec 2017*)
##### Added
- CS2A Auto Followups: send notifications (SMS/Email) to the customer based on the followup status `@NGK`

### v2.0.3 (*14 Dec 2017*)
##### Fixed
- 'type_category_tags' info will be updated, even if that row is updated `@NGK`

### v2.0.2 (*12 Dec 2017*)
##### Added
- QC reports for partners `@JR`

### v2.0.1 (*05 Dec 2017*)
##### Added
- Job to constantly keep updating 'type_category_tags' table whenever a tag is created or an option is edited `@NGK`