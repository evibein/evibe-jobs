<div style="border:20px solid #f5f5f5; padding:15px;">
	Hi Team,<br><br>
	Please check the current DB user connections.
	<table border="1" style="border-collapse: collapse; padding:10px;margin-top:15px" width="100%">
		<tr>
			<td width=10% style="padding: 8px"><b>Trace</b></td>
			<td>{!! $data['trace'] !!}</td>
		</tr>
	</table>
	<br><br>
	<div>Thanks in advance,</div>
	<div>Team Evibe</div>
</div>'