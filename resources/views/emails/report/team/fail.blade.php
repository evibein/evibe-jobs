<div>
	Namaste {{ $name }},
	<p>
		<b>{{ $vendorName }}</b> has not entered OTP & PIN for
		<b>{{ $customerName }}'s</b> party happening on {{ $partyDate }} at {{ $partyLocation }}.
		Please call {{ $vendorPhone }} immediately and resolve it. If all well, you complete reporting using following data:
	</p>
	<table style="border: 1px solid black;">
		<tr>
			<td style="padding: 4px;">Dial Number:</td>
			<td style="padding: 4px;">{{ $cloudPhone }}</td>
		</tr>
		<tr>
			<td style="padding: 4px;">PIN</td>
			<td style="padding: 4px;">{{ $pin }}</td>
		</tr>
		<tr>
			<td style="padding: 4px;">OTP</td>
			<td style="padding: 4px;">{{ $otp }}</td>
		</tr>
	</table>
	<p>
		Best,<br/>
		Admin
	</p>
</div>
