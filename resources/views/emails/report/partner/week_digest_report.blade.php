<div style="width: 100%;font-family:Roboto,'Open Sans',Helvetica,Arial,sans-serif"
		align="center">
	<div style="max-width: 700px;background-color:#F6F6F6;">
		<div style="padding: 15px 0;margin-bottom: 30px;">
			<a href="http://evibe.in/" class="pull-left">
				<img style="display:inline-block; float: left; margin-left: 30px"
						src="http://gallery.evibe.in/img/logo/logo_evibe.png" height="26">
			</a>
			<a href="#" class=""
					style="color:#212121;text-decoration:none;font-weight:600;font-size:14px; padding: 10px 0; text-align: right; float: right;margin-right: 30px"
					target="_blank">Week report
			</a>
		</div>
		<div style="border-radius:4px;background-color:#FFFFFF;border:#E9E9E9 1px solid; padding: 15px 15px; margin: 0 30px 10px">
			<div style="font-weight:400; padding-left: 10px; color:#212121; font-size:18px; line-height:22px; text-align: left;">
				Namaste {{ $data['provider']['person'] }},
			</div>
			<div style="color:#757575;font-size:14px;line-height:20px;font-weight:400; text-align: justify; margin-top: 10px; padding:0 30px 0 30px">
				<span style="text-align: left;">
					@php $pendingDeliveryCount = $data['response']['data']['completed_orders_count'] - $data['response']['data']['total_delivered_count']; @endphp
					@if (($data['response']['data']['completed_orders_count'] > 0) && ($data['response']['data']['delivery_percent'] != 100))
						Hope you had an eventful week. Thank you for being a trusted Evibe.in partner. Here is your week summary.
						<span style="background-color: yellow"><b>There @if($pendingDeliveryCount == 1){{ "is" }}@else{{ "are" }}@endif {{ $pendingDeliveryCount }} pending event deliveries</b></span>. Kindly do the needful at the earliest.
					@else
						Hope you had an eventful week. Thank you for being a trusted Evibe.in partner. Here is your week summary.
					@endif
				</span>
				<div style="text-align: center; margin-top: 15px"><I>For more detailed view:</I></div>
				<div style="padding: 10px 0; text-align: center; margin-bottom: 15px">
					<a style="border-radius:4px;padding:10px 16px 10px 16px;text-decoration:none;font-size:13px;font-weight:700;color:#2BB6CB;border:1px solid #2BB6CB;background:#FFFFFF"
							href="{{ $data["dashboard_url"] }}">
						SEE MY DASHBOARD
					</a>
				</div>
				<span style="text-align: left;">
					Should you have any queries, please feel free to reply to this email. We will respond to you within 3 Business hours.
				</span>
			</div>
		</div>
		@if($data['response']['data']['total_bookings_count'] && $data['response']['data']['total_bookings_count'] > 0)
			<div style="border-radius:4px;background-color:#FFFFFF;border:#E9E9E9 1px solid; padding: 10px 15px; margin: 0 30px 10px">
				<div style="padding-left: 5px">
					<span style="font-weight:600;font-size:15px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;line-height:20px;text-align: left">New Orders<br></span>
					<span style="font-size:12px;color:#9E9E9E;font-weight:400;display: block; text-align:left">
						Summary of all the new orders that happened in past week.
					</span>
				</div>
				<div style="margin: 35px 0;">
					<div style="border-right:2px dashed #E0E0E0;width: 48%;display: inline-block">
						<div style="display: block;font-weight:700;font-size:32px;color:#2BB6CB;line-height:40px;">
							{{ $data['response']['data']['total_bookings_count'] }}
						</div>
						<div style="display: block;font-size:12px;color:#9E9E9E;line-height:20px">
							Total New Orders
						</div>
					</div>
					<div style="width: 50%; display: inline-block">
						<div style="display: block;font-weight:700;font-size:32px;color:#2BB6CB;line-height:40px;">
							<?php setlocale(LC_MONETARY, 'en_IN'); ?>
							&#x20b9; {{ substr_replace(money_format('%!i',$data['response']['data']['total_revenue']) ,"",-3) }}
						</div>
						<div style="display: block;font-size:12px;color:#9E9E9E;line-height:20px">
							New Revenue
						</div>
					</div>
				</div>
				<div style="padding: 15px 0; margin-bottom: 20px">
					<a style="border-radius:4px;padding:10px 16px 10px 16px;text-decoration:none;font-size:13px;font-weight:700;color:#2BB6CB;border:1px solid #2BB6CB;background:#FFFFFF"
							href="{{ $data["orders_url"] }}">
						SEE ALL NEW ORDERS
					</a>
				</div>
				<div style="font-size:13px;color:#1B1A1A;font-weight:400; text-align: left; padding: 0 20px 0 10px">
					<span>Quick tips for smooth execution:</span>
					@if($data['provider_type'] == config("evibe.ticket.type.venues"))
						<ul>
							<li style="margin-bottom: 5px">
								Talk to the customer at least one day prior to the party date to assure them from your side and re-confirm complete order details.
							</li>
							<li style="margin-bottom: 5px">
								Help customers with locating your venue.
							</li>
							<li style="margin-bottom: 5px">
								Keep all the arrangements ready at least 30 minutes prior to check in time to avoid any last minute hassle.
							</li>
							<li style="margin-bottom: 5px">
								Please be very polite with the customers even if they provoke any kind of dispute. This will save you from unnecessary consequences and time.
							</li>
							<li style="margin-bottom: 5px">
								Kindly take pictures after execution and upload on Evibe.in Partner App.
							</li>
							<li>
								Do not forget to enjoy your event :)
							</li>
						</ul>
					@else
						<ul>
							<li style="margin-bottom: 5px">
								Talk to the customer at least one day prior to the party date to assure them from your side and re-confirm complete order details, venue address etc.
							</li>
							<li style="margin-bottom: 5px">
								Be at least 15 mins before the original reporting time. Customers rate high for on time service delivery.
							</li>
							<li style="margin-bottom: 5px">
								Ensure you carry all the required material as per the order details.
							</li>
							<li style="margin-bottom: 5px">
								Please be very polite with the customers even if they provoke any kind of dispute. This will save you from unnecessary consequences and time.
							</li>
							<li style="margin-bottom: 5px">
								Kindly take pictures after execution and upload on Evibe.in Partner App.
							</li>
							<li>
								Do not forget to enjoy your event :)
							</li>
						</ul>
					@endif
				</div>
			</div>
		@endif
		@if($data['response']['data']['completed_orders_count'] && $data['response']['data']['completed_orders_count'] > 0)
			<div style="border-radius:4px;background-color:#FFFFFF;border:#E9E9E9 1px solid; padding: 10px 15px; margin: 0 30px 10px">
				<div style="padding-left: 5px">
					<span style="font-weight:600;font-size:15px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;line-height:20px;text-align: left">Completed Orders<br></span>
					<span style="font-size:12px;color:#9E9E9E;font-weight:400;display: block; text-align:left">
						Summary of all the orders executed by you in the past week.
					</span>
				</div>
				<div style="margin-top: 35px">
					<div style="width: 45%;display: inline-block;">
						<div style="display: block;border:1px solid #E9E9E9;border-collapse:collapse;color:#666666;font-size:12px;font-weight:bold;line-height:16px;margin:0;padding:8px 10px;text-align:center; letter-spacing: 0.5px">
							Total Completed Orders
						</div>
						<div style="border:1px solid #E9E9E9;color:#2BB6CB;font-size:32px;font-weight:bold;text-decoration:none;white-space:nowrap;padding: 25px 0">
							{{ $data['response']['data']['completed_orders_count'] }}
						</div>
					</div>
					<div style="width: 5%;display: inline-block;"></div>
					<div style="width: 45%;display: inline-block;">
						<div style="display: block;border:1px solid #E9E9E9;border-collapse:collapse;color:#666666;font-size:12px;font-weight:bold;line-height:16px;margin:0;padding:8px 10px;text-align:center; letter-spacing: 0.5px">
							Photo Deliveries Done
						</div>
						<div style="border:1px solid #E9E9E9;@if($data['response']['data']['delivery_percent'] != 100) color:#ff2500; @else color:#2BB6CB; @endif font-size:32px;font-weight:bold;text-decoration:none;white-space:nowrap; padding: 25px 0">
							{{ $data['response']['data']['total_delivered_count'] }}
						</div>
					</div>
				</div>
				@if(count($data['response']['data']['reviews']) > 1)
					<div style="margin-top: 35px">
						<div style="width: 45%;display: inline-block;">
							<div style="display: block;border:1px solid #E9E9E9;border-collapse:collapse;color:#666666;font-size:12px;font-weight:bold;line-height:16px;margin:0;padding:8px 10px;text-align:center; letter-spacing: 0.5px">
								Top Rating
							</div>
							<div style="border:1px solid #E9E9E9;color:#2BB6CB;font-size:32px;font-weight:bold;text-decoration:none;white-space:nowrap;padding: 25px 0">
								<span>{{ $data['response']['data']['top_review'] }} / 5</span>
							</div>
						</div>
						<div style="width: 5%;display: inline-block;"></div>
						<div style="width: 45%;display: inline-block;">
							<div style="display: block;border:1px solid #E9E9E9;border-collapse:collapse;color:#666666;font-size:12px;font-weight:bold;line-height:16px;margin:0;padding:8px 10px;text-align:center; letter-spacing: 0.5px">
								Least Rating
							</div>
							<div style="border:1px solid #E9E9E9;color:#2BB6CB;font-size:32px;font-weight:bold;text-decoration:none;white-space:nowrap; padding: 25px 0">
								<span>{{ $data['response']['data']['least_review'] }} / 5</span>
							</div>
						</div>
					</div>
				@elseif(count($data['response']['data']['reviews']) == 1)
					<div style="margin-top: 35px">
						<div style="width: 3%;display: inline-block;"></div>
						<div style="width: 70%;display: inline-block;">
							<div style="display: block;border:1px solid #E9E9E9;border-collapse:collapse;color:#666666;font-size:12px;font-weight:bold;line-height:16px;margin:0;padding:8px 10px;text-align:center; letter-spacing: 0.5px">
								Average Rating
							</div>
							<div style="border:1px solid #E9E9E9;color:#2BB6CB;font-size:32px;font-weight:bold;text-decoration:none;white-space:nowrap;padding: 25px 0">
							<span style="color:#2BB6CB;font-size:30px;font-weight:bold;text-decoration:none;white-space:nowrap">
								{{ $data['response']['data']['top_review'] }} / 5
							</span>
								<div style="font-size: 12px; color: grey"><I>(from 1 customer)</I></div>
							</div>
						</div>
						<div style="width: 3%;display: inline-block;"></div>
					</div>
				@endif
				@if($data['response']['data']['delivery_percent'] != 100)
					<div style="padding: 10px 0; text-align: center; margin-bottom: 15px; margin-top: 20px">
						<a style="border-radius:4px;padding:10px 16px 10px 16px;text-decoration:none;font-size:13px;font-weight:700;color:#2BB6CB;border:1px solid #2BB6CB;background:#FFFFFF"
								href="{{ $data["deliveries_url"] }}">
							SEE PENDING DELIVERIES
						</a>
					</div>
				@endif
				<div style="font-size:13px;color:#1B1A1A;font-weight:400;text-align:left;padding:30px 20px 0 10px">
					<div><b>What is photo delivery?</b></div>
					<div>After executing your order, please capture the photos and upload to the appropriate order under "Deliveries" section in the Evibe.in Partner App (iOS / Android).</div>
				</div>
				<div style="font-size:13px;color:#1B1A1A;font-weight:400;text-align:left;padding:30px 20px 0 10px">
					<span><b>Benefits of photo delivery for all orders</b></span>
					<ul style="margin-bottom: 30px">
						<li style="margin-bottom: 5px">
							Evibe.in will safeguard you against any false claim raised by customer if sufficient delivery reporting is done through app.
						</li>
						<li style="margin-bottom: 5px">
							Better profile rating on website, which will increase profile and service visibility on website.
						</li>
						<li style="margin-bottom: 5px">
							Your profile will be up to date with latest images, inclusions & new offerings to increase your Business.
						</li>
						<li>
							Based on research, customer chose partners with high QC profile rating over others.
						</li>
					</ul>
				</div>
			</div>
		@endif
		<div style="margin-top: 20px; font-size:13px; color: #6e6e6e; text-align: center">
			<div style="text-align: center">Download our Partner App now:</div>
			<a href="https://play.google.com/store/apps/details?id=in.evibe.evibe"><img
						src='{{ config('evibe.gallery.host') }}/img/icons/google-play-badge.png'
						style="width: 28%;" class="pad-t-10"></a>
			<a href="https://itunes.apple.com/in/app/evibe-in-partner/id1295541774?mt=8"><img
						src='{{ config('evibe.gallery.host') }}/img/icons/app-store.png'
						style="width: 22%; vertical-align: top; margin-top: 10px;" class="pad-t-10"></a>
		</div>
		<div style="padding: 6px 0"></div>
	</div>
</div>