<div style="padding:10px">
	<p>
		Hi Team,
	</p>
	<p>{{ $data["errorMessage"] }}, Please check with the customer and fix it immediately. <i>(Customer details attached below)</i></p>
	<table border="1" style="margin-top:10px; border-collapse: collapse">
		<tr>
			<th style="padding:8px">Name</th>
			<td style="padding:8px">{{ $data['ticket']->name }}</td>
		</tr>
		<tr>
			<th style="padding:8px">Phone</th>
			<td style="padding:8px">{{ $data['ticket']->phone }}</td>
		</tr>
		<tr>
			<th style="padding:8px">Email</th>
			<td style="padding:8px">{{ $data['ticket']->email }}</td>
		</tr>
		<tr>
			<th style="padding:8px">Device Info</th>
			<td style="padding:8px">{{ $data['deviceInfo'] }}</td>
		</tr>
	</table>
	<p>Dash Link: <a href="{{ config("evibe.dash_url") . "tickets/" . $data["ticket"]->id . "/bookings" }}" target="_blank">Click Here</a></p>
</div>