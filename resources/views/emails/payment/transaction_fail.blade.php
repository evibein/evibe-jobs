<div style='background-color:#d9d9d9;padding:30px 20px;width:700px;font-size:14px;line-height:22px;'>
	<div style="background-color:#FFFFFF;padding:20px;">
		<div>
			<div style='text-align: center;'>
				<div>
					<a href='http://evibe.in'>
						<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
					</a>
				</div>
			</div>
		</div>
		<hr>
		<div style="font-size:14px;line-height:22px;">
			<div style="margin-top:30px;">
				<p>Namaste {{ $data["customerName"] }},</p>
				<p>
					<span>Your order was not completed as we've received a failed status from the payment gateway. In case any money has been deducted from your bank account, it should be refunded to the same within 3-5 business days. We regret the inconvenience caused.</span>
				</p>
				<p>
					<span>Please reach out to your bank if your money is deducted and doesn't reach your bank account in the aforementioned time. Meanwhile, you can retry your transaction with us by using your card/netbanking or any other form of payment.</span>
				</p>
				<p>
					<span>Payment Reference: #{{ $data["paymentReferenceId"] }}</span>
				</p>
				<div style="padding:30px 0 15px; text-align:center;">
					<a href="{{ $data["paymentLink"] }}" style="font-size:20px;text-decoration:none;background-color:#4584EE;color:#FFFFFF;border-color:#4CAE4C;padding:8px 20px;border-radius:4px;">
						Retry Payment
					</a>
					<div style="padding-top: 12px; font-weight: 500; color: #4841FF;">
						(Please use Google Chrome for payment.)
					</div>
				</div>
			</div>
			<div style="margin-top:15px;">
				Thanks for choosing evibe.in to book your party. We wish you a wonderful and memorable party.
			</div>
			<div style="margin:15px 0;">
				<div>Thanks in advance,</div>
				<div>Team Evibe</div>
			</div>
		</div>
	</div>
</div>
<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>