<p>
	Hi team,
</p>
<p>
	Please go through the payments report [{{ $data["timeSlot"] }}].
</p>
<table style="border: 1px solid #c9c9c9; border-radius: 15px; margin: 30px auto 0 auto; background-color: #f6f6f6; table-layout: fixed; ">
	<thead>
	<tr style="text-align: center; color: #636363;">
		<td colspan="2" style="padding: 15px;">Total Payment Initiations</td>
	</tr>
	</thead>
	<tr style="color: #636363;">
		<td style="text-align: center; padding: 10px 0;">All</td>
		<td style="text-align: center; color: #fff;">
			<div style="padding: 5%; margin: 6% 20%; background-color: #7184d8; border-radius: 5px; min-width: 60px;">{{ $data["paymentInitiations"] }}</div>
		</td>
	</tr>
	<tr style="color: #636363;">
		<td style="text-align: center; padding: 10px 0;">Unique</td>
		<td style="text-align: center; color: #fff;">
			<div style="padding: 5%; margin: 6% 20%; background-color: #7184d8; border-radius: 5px; min-width: 60px;">{{ $data["uniquePaymentInitiations"] }}</div>
		</td>
	</tr>
</table>
<table style="border: 1px solid #c9c9c9; border-radius: 15px; margin: 30px auto 0 auto; width: 100%; background-color: #f6f6f6; table-layout: fixed;">
	<tr style="text-align: center; font-size: 22px; color: #636363;">
		<td style="padding: 10px 0;">Total</td>
		<td style="padding: 10px 0;">Success</td>
		<td style="padding: 10px 0;">Failed</td>
		<td rowspan="2">
			<div style="background-color: #2ba69b; color: #ffffff; font-size: 16px; padding: 3% 0; margin: 0 20%; border-radius: 5px;">{{ round(($data["successCount"] + $data["manualAcceptCount"])/$data["uniquePaymentClicks"], 2) * 100 }} %</div>
		</td>
	</tr>
	<tr style="color: #ffffff; font-size: 16px; text-align: center;">
		<td>
			<div style="padding: 3% 0; margin: 0 20% 6% 20%; background-color: #7184d8; border-radius: 5px;">{{ $data["uniquePaymentClicks"] }}
				<span style="font-size: 12px">({{ $data["totalCount"] }})</span></div>
		</td>
		<td>
			<div style="padding: 3% 0; margin: 0 20% 6% 20%; background-color: #7184d8; border-radius: 5px;">{{ $data["successCount"] + $data["manualAcceptCount"] }}
				<span style="font-size: 12px">({{ $data["successCount"] }} + {{ $data["manualAcceptCount"] }})</span></div>
		</td>
		<td>
			<div style="padding: 3% 0; margin: 0 20% 6% 20%; background-color: #F44336; border-radius: 5px;">{{ $data["failedCount"] }}</div>
		</td>
	</tr>
</table>
<table style="border: 1px solid #c9c9c9; border-radius: 15px; margin: 30px auto 0 auto; background-color: #f6f6f6; table-layout: fixed; ">
	<thead>
	<tr style="text-align: center; color: #636363;">
		<td colspan="2" style="padding: 15px;">Failed Transactions Details</td>
	</tr>
	</thead>
	<tr style="color: #636363;">
		<td style="text-align: center; padding: 10px 0;">Valid</td>
		<td style="text-align: center; color: #fff;">
			<div style="padding: 5%; margin: 6% 20%; background-color: #7184d8; border-radius: 5px;">{{ $data["validFailedCount"] }}</div>
		</td>
	</tr>
	<tr style="color: #636363;">
		<td style="text-align: center; padding: 10px 0;">Invalid</td>
		<td style="text-align: center; color: #fff;">
			<div style="padding: 5%; margin: 6% 20%; background-color: #7184d8; border-radius: 5px;">{{ $data["invalidFailedCount"] }}</div>
		</td>
	</tr>
</table>
@if(count($data["failedCommentsData"]) > 0)
	<table style="border: 1px solid #c9c9c9; border-radius: 15px; margin: 30px auto 0 auto; background-color: #f6f6f6; table-layout: fixed; ">
		<thead>
		<tr style="text-align: center; color: #636363;">
			<td colspan="2" style="padding: 15px;">Valid Payment Failures</td>
		</tr>
		</thead>
		@foreach($data["failedCommentsData"] as $key => $item)
			<tr style="color: #636363;">
				<td style="text-align: center; padding: 10px;"><a href="{{ config("evibe.dash_url") }}tickets/{{ $key }}/information">{{ $key }}</a></td>
				<td style="text-align: left; padding: 10px;">
					@foreach($item as $status)
						{!! $status !!}<br>
					@endforeach
				</td>
			</tr>
		@endforeach
	</table>
@endif
@if(count($data["invalidFailedData"]) > 0)
	<table style="border: 1px solid #c9c9c9; border-radius: 15px; margin: 30px auto 0 auto; background-color: #f6f6f6; table-layout: fixed; ">
		<thead>
		<tr style="text-align: center; color: #636363;">
			<td colspan="2" style="padding: 15px;">Invalid Payment Failures</td>
		</tr>
		</thead>
		@foreach($data["invalidFailedData"] as $key => $item)
			<tr style="color: #636363;">
				<td style="text-align: center; padding: 10px;"><a href="{{ config("evibe.dash_url") }}tickets/{{ $key }}/information">{{ $key }}</a></td>
				<td style="text-align: left; padding: 10px;">
					@foreach($item as $status)
						{!! $status !!}<br>
					@endforeach
				</td>
			</tr>
		@endforeach
	</table>
@endif