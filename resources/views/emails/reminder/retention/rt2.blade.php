Hi {{ $data['customerName'] }},
<p>We hope you are busy planning for your special day. Your special offer is waiting for you.</p>
<p>Apply the following coupon code at the time of booking your party and get <b>{{ $data['discountPercentString'] }}* OFF</b>.</p>
<div style="margin-top: 30px;">
	<div style="text-align: center">
		<div style="border: 1px dotted #333333; display: inline-block; padding: 15px 25px;">
			<div style="font-size: 22px;"><b>{{ $data['couponCode'] }}</b></div>
			<div style="margin-top: 15px; font-size: 24px;"><b>{{ $data['discountPercent'] }}%* OFF</b></div>
			<div style="margin-top: 15px; font-size: 12px; color: #7D7D7D;">Valid upto: {{ $data['validDate'] }} | Max discount: Rs. {{ $data['maxDiscountAmount'] }}</div>
		</div>
		<div style="margin-top: 25px;">
			<a href="{{ $data['emailBOLink'] }}">
				<div style="color: #FFFFFF; background-color: #ED3E72; padding: 10px 40px; font-size: 18px; border-radius: 6px; display: inline-block">
					<b>PLAN NOW</b></div>
			</a>
		</div>
		<div style="margin-top: 15px;">
			No time to browse? <a href="{{ $data['emailQELink'] }}">Make a quick enquiry</a>
		</div>
	</div>
</div>
<div style="margin-top: 20px;">
	We can't wait to be a part of your celebrations again :).
</div>
<div style="margin-top: 30px;">
	<div>Happy partying,</div>
	<div>Team Evibe.in</div>
</div>
<div style="margin-top: 30px; color: #7D7D7D;">
	Not interested in us? No hard feelings, just click here to let us know. {{ $data['emailUnSubLink'] }}. Either way, we want you to have a great celebration.
</div>