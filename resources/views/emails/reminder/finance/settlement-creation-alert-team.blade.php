<div style="border:20px solid #f5f5f5;padding: 15px ">
	<div>
		Hi Team,
		<p>
			New set of settlements will be created in the coming Wednesday. Please resolve any issues regarding all the completed parties.
			Kindly refund all the applicable refunds to customers, update bookings and create necessary adjustments to tally accounts.
		</p>
	</div>
	<div style="margin-top:15px">
		<div>{{ config('evibe.email_footer') }}</div>
		<br>Admin
	</div>
</div>