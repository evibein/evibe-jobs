<div style="border:20px solid #f5f5f5;padding: 15px ">
	<div>
		Hi Team,
		<p>
			Kindly verify and either process or reject the following refunds
		</p>
	</div>
	<table border="1" style="margin-top:15px; border-collapse: collapse">
		<tr>
			<th style="padding: 8px"><b>Customer Name</b></th>
			<th style="padding: 8px"><b>Customer Phone</b></th>
			<th style="padding: 8px"><b>Party Date Time</b></th>
			<th style="padding: 8px"><b>Refund Amount</b></th>
		</tr>
		@if(isset($data['refundsList']) && $data['refundsList'])
			@foreach($data['refundsList'] as $refund)
				<tr>
					<td style="padding: 8px;">
						@if(isset($refund['customerName']) && $refund['customerName'])
							{{ $refund['customerName'] }}
						@else
							--
						@endif
					</td>
					<td style="padding: 8px;">
						@if(isset($refund['customerPhone']) && $refund['customerPhone'])
							{{ $refund['customerPhone'] }}
						@else
							--
						@endif
					</td>
					<td style="padding: 8px;">
						@if(isset($refund['partyDateTime']) && $refund['partyDateTime'])
							{{ $refund['partyDateTime'] }}
						@else
							--
						@endif
					</td>
					<td style="padding: 8px;">
						@if(isset($refund['refundAmount']) && $refund['refundAmount'])
							&#8377;{{ $refund['refundAmount'] }}
						@else
							--
						@endif
					</td>
				</tr>
			@endforeach
		@endif
	</table>
	@if(isset($data['refundsDashLink']) && $data['refundsDashLink'])
		<div style="margin-top: 10px;">
			<a href="{{ $data['refundsDashLink'] }}" target="_blank">DASH link to refunds</a>
		</div>
	@endif
	<div style="margin-top:15px">
		<div>{{ config('evibe.email_footer') }}</div>
		<br>Admin
	</div>
</div>