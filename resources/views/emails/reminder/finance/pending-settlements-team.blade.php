<div style="border:20px solid #f5f5f5;padding: 15px ">
	<div>
		Hi Team,
		<p>
			Kindly settle the following pending partner settlements
		</p>
	</div>
	<table border="1" style="margin-top:15px; border-collapse: collapse">
		<tr>
			<th style="padding: 8px"><b>Partner Name</b></th>
			<th style="padding: 8px"><b>Partner Compnay Name</b></th>
			<th style="padding: 8px"><b>Settlement Amount</b></th>
		</tr>
		@if(isset($data['settlementsList']) && $data['settlementsList'])
			@foreach($data['settlementsList'] as $settlement)
				<tr>
					<td style="padding: 8px;">
						@if(isset($settlement['partnerName']) && $settlement['partnerName'])
							{{ $settlement['partnerName'] }}
						@else
							--
						@endif
					</td>
					<td style="padding: 8px;">
						@if(isset($settlement['partnerCompanyName']) && $settlement['partnerCompanyName'])
							{{ $settlement['partnerCompanyName'] }}
						@else
							--
						@endif
					</td>
					<td style="padding: 8px;">
						@if(isset($settlement['settlementAmount']) && $settlement['settlementAmount'])
							&#8377;{{ $settlement['settlementAmount'] }}
						@else
							--
						@endif
					</td>
				</tr>
			@endforeach
		@endif
	</table>
	@if(isset($data['settlementsDashLink']) && $data['settlementsDashLink'])
		<div style="margin-top: 10px;">
			<a href="{{ $data['settlementsDashLink'] }}" target="_blank">DASH link to settlements</a>
		</div>
	@endif
	<div style="margin-top:15px">
		<div>{{ config('evibe.email_footer') }}</div>
		<br>Admin
	</div>
</div>