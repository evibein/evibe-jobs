<p>Namaste {{ $data['customerName'] }},</p>
<p>Greetings from Evibe.in!</p>
<p>Based on your preferences, we've sent you the best <a style="text-decoration: none"
			href="{{ $data['recommendationUrl'] }}">recommendations</a> for your party. Please click on the button below to check and shortlist the options as per your choice.
</p>
<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>
<p>Should you have any questions, feel free to reply to this email or contact us on the undersigned. We will respond to you within 4 Business hours or earlier.</p>
@include("emails.reminder.next-steps")
@include('base.why-us')
<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>
<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
<a href="https://www.youtube.com/embed/6lwEXs51qJw?html5=1">See why customers love Evibe.in</a>
</p>
