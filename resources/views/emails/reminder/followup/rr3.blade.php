<p>Namaste {{ $data['customerName'] }},</p>
<p>Greetings from Evibe.in!</p>

<p>We did not get your shortlist yet. Kindly check our top recommendations that I have sent you based on your requirements.
	Let me know your choice by <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">shortlisting the options</a> to proceed further.</p>

<p><b><a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>
<p>We assure you best price for quality in market and 100% service ser ice gyatantee. Should you have any questions, feel free to reply to this email or contact us on the undersigned. We will respond to you within 4 Business hours or earlier.</p>
@include("emails.reminder.next-steps")
@include('base.why-us')
<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>
<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
</p>