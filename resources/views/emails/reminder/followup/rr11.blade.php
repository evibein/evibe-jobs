<p>Namaste {{ $data['customerName'] }},</p>

<p>Greetings from Evibe.in!</p>

<p>
	Evibe.in: #1 party planners helps you host a memorable party, hassle free.
	We provide best quality for price in market and 100% service delivery guarantee.
</p>

<p>
	Based on your party requirements, I’ve already sent you top recommendations for your party.
	Please check and <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">submit your shortlist</a> from the link below.
	I would love to help you host a memorable party, completely hassle free.
</p>

<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>

<p>
	Should you have any questions, feel free to reply to this email or contact us on the undersigned.
	We will respond to you within 4 Business hours or earlier.
</p>

@include("emails.reminder.next-steps")
@include('base.why-us')

<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>

<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
</p>

<p>
	<span style="text-decoration: underline" >PS: </span>Not interested in us? No hard feelings, just click here to let me know. {{ $data['cancelShortLink'] }}. Either ways, we would love you to have a wonderful party :)
</p>