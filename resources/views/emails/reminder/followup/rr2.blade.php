<p>Namaste {{ $data['customerName'] }},</p>
<p>Greetings from Evibe.in!</p>
<p>We have sent you top recommendations for your party on {{ $data['partyDate'] }} based on the requirements you have given us.
	We are waiting for you to  <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">check the recommendations</a>.
	Let me know your choice by shortlisting the options to proceed further.</p>
<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>
<p>Should you have any questions, feel free to reply to this email or contact us on the undersigned. We will respond to you within 4 Business hours or earlier.</p>
@include("emails.reminder.next-steps")
@include('base.why-us')
<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>
<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
</p>