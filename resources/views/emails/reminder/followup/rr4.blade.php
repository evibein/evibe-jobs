<p>Namaste {{ $data['customerName'] }},</p>

<p>Greetings from Evibe.in!</p>

<p>Your party is just in {{ $data['daysCount'] }} days. Kindly <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">check top recommendations</a> for your party and submit your shortlist to process your order.
	We assure you best price for quality in market and 100% service delivery guarantee.</p>

<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>

<p> I hope to hear back from you soon as it would be difficult for us to arrange few things in the last minute.
	Should you have any questions, feel free to reply to this email or contact us on the undersigned.
	We will respond to you within 4 Business hours or earlier.</p>

@include("emails.reminder.next-steps")
@include('base.why-us')

<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>

<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
</p>

<p>
	<span style="text-decoration: underline" >PS: </span>Not interested in us? No hard feelings, just click here to let me know. {{ $data['cancelShortLink'] }}. Either ways, we would love you to have a wonderful party :)
</p>