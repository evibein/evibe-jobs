<p>Namaste {{ $data['customerName'] }},</p>

<p>Greetings from Evibe.in!</p>

<p>
	As a last minute offer, I am exclusively giving you a discount of Rs. {{ $data['discountPrice'] }} for your party on {{ $data['partyDate'] }}.
	Use coupon code {{ $data['couponCode'] }} at the time of processing your order to avail discount.
	Offer valid only {{ $data['couponValidString'] }}.
</p>

<p>
	In-order to proceed, kindly check and <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">shortlist the recommendations</a> from the link below and I will help in applying the coupon code at the time of processing your order.
	I really want to help you in hosting a wonderful party.
</p>

<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>

<p>
	We assure best price for quality in market and 100% service delivery guarantee. Should you have any questions, feel free to reply to this email or contact us on the undersigned.
	We will respond to you within 4 Business hours or earlier.
</p>

@include("emails.reminder.next-steps")
@include('base.why-us')

<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>

<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
<a href="https://www.youtube.com/embed/6lwEXs51qJw?html5=1">See why customers love Evibe.in</a>
</p>

<p>
	<span style="text-decoration: underline" >PS: </span>Not happy with my recommendations? Please click here: {{ $data['notLikeLink'] }}. I will re-work on your requirements.
</p>