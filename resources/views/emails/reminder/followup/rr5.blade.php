<p>Namaste {{ $data['customerName'] }},</p>

<p>Greetings from Evibe.in!</p>

<p>I have recommended some of our top options for your party based on the requirements that you have given us.
	Please select the options you liked and <a style="text-decoration: none" href="{{ $data['recommendationUrl'] }}">submit your shortlist</a> from the below link, so that we can plan your party as early as possible.
	We assure you best price for quality in market and 100% service delivery guarantee</p>

<p><b><a href="{{ $data['recommendationUrl'] }}">Click to Check & Shortlist Recommendations</a></b></p>

<p>
	Let me help you in hosting a great party. Should you have any questions, feel free to reply to this email or contact us on the undersigned.
	We will respond to you within 4 Business hours or earlier.
</p>

@include("emails.reminder.next-steps")
@include('base.why-us')

<p style="padding-top:10px;"><u>Note:</u> Everything is subjected to availability.</p>

<p style="padding-top:3px;">
<div>Thanks in advance,</div>
<div>{{ $data['handlerName'] }}</div>
<div>@if(isset($data['handlerPhone']) && $data['handlerPhone']) {{ $data['handlerPhone'] }} @endif, {{ $data['evibePhonePlain'] }}</div>
</p>

<p>
	<span style="text-decoration: underline" >PS: </span>Not happy with my recommendations? Please click here: {{ $data['notLikeLink'] }}. I will re-work on your requirements.
</p>