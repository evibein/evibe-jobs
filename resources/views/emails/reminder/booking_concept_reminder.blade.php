<div style="padding: 15px; border: 20px solid #f5f5f5;">
	<p>Hi Team,</p>
	<P>Please find the list below with all Concept bookings happened in last week.</P>
	@foreach($items as $data)
		<div style="padding: 10px; border: 20px solid #e7e7e7;">
			<table style="margin-top:10px; width:100%; border-collapse: collapse;" border="1">
				<tr>
					<th style="padding: 8px; text-align: center; background-color: #e7e7e7;"
							colspan="2">Booking Info - Concept Type ({{ $data['booking']->bookingConcept->name }})
					</th>
				</tr>
				<tr>
					<td style="padding: 8px">Booking Id</td>
					@if($data['booking'] && $data['booking']->ticket)
						<td style="padding: 8px">
							<a href="{{ config('evibe.dash_url') . '/tickets/' . $data['booking']->ticket->id . '/bookings#'.$data['booking']->id }}">@if($data['booking']->booking_id){{ $data['booking']->booking_id }}@else --- @endif</a>
						</td>
					@endif
				</tr>
				<tr>
					<td style="padding: 8px">Party Location</td>
					<td style="padding: 8px">
						@if($data['booking']->ticket && $data['booking']->ticket->area && $data['booking']->ticket->area->name)
							{{ $data['booking']->ticket->area->name }}
						@else ---
						@endif
					</td>
				</tr>
				<tr>
					<td style="padding: 8px">Customer Name</td>
					<td style="padding: 8px">
						@if($data['booking']->ticket && $data['booking']->ticket->name)
							{{ $data['booking']->ticket->name }}
						@else ---
						@endif
					</td>
				</tr>
				<tr>
					<td style="padding: 8px">Booking Amount</td>
					<td style="padding: 8px">@if($data['booking']->booking_amount){{ $data['booking']->booking_amount }}@else --- @endif</td>
				</tr>
				<tr>
					<td style="padding: 8px">Reference Images</td>
					@if(count($data['booking']->bookingImages) > 0)
						<td style="padding: 8px">
							@foreach ($data['booking']->bookingImages as $image)
								@if ($image->type && ($image->type == 1))
									<a href="{{ $image->url }}"><img style="height: 100px;width: 100px"
												src="{{ $image->url }}"></a>
								@elseif ($image->type && ($image->type == 2))
									<?php $link = config('evibe.gallery.host') . '/ticket/' . $data['booking']->ticket->id . '/' . $data['booking']['id'] . '/' . $image->url; ?>
									<a href="{{ $link }}"><img style="max-height: 100px;max-width: 100px"
												src="{{ $link }}"></a>
								@endif
							@endforeach
						</td>
					@else
						<td style="padding: 8px">N/A</td>
					@endif
				</tr>
				<tr style="padding: 8px;visibility: hidden;"></tr>
				<tr>
					<th style="padding: 8px; text-align: center; background-color: #e7e7e7" colspan="2">FeedBack</th>
				</tr>
				@if(count($data['review']) > 0)
					@foreach($data['review'] as $review)
						<tr>
							<td style="padding: 8px">Customer rating</td>
							<td style="padding: 8px">@if($review['rating']){{ $review['rating'] }}@else --- @endif</td>
						</tr>
						<tr>
							<td style="padding: 8px">Customer Review</td>
							<td style="padding: 8px">@if($review['review']){{ $review['review'] }}@else --- @endif</td>
						</tr>
					@endforeach
					@if(count($data['reviewQuestions']) > 0)
						@foreach($data['reviewQuestions'] as $reviewQuestion)
							<tr>
								<td style="padding: 8px">{{ $reviewQuestion['question'] }}</td>
								<td style="padding: 8px">@if($reviewQuestion['answer']){{ $reviewQuestion['answer'] }}@else --- @endif</td>
							</tr>
						@endforeach
					@endif
				@else
					<tr>
						<td style="text-align: center;" colspan="2">N/A</td>
					</tr>
				@endif
				<tr>
					<th style="padding: 8px; text-align: center; background-color: #e7e7e7"
							colspan="2">Delivery images
					</th>
				</tr>
				@if(count($data['booking']->deliveryImages) > 0)
					<tr>
						<td colspan="2">
							@foreach($data['booking']->deliveryImages as $delivery)
								<?php $link = config('evibe.gallery.host') . '/ticket/' . $data['booking']->ticket->id . '/' . $data['booking']['id'] . '/partner/' . $delivery->url; ?>
								<a href="{{ $link }}"><img style="max-height: 100px;max-width: 100px" src="{{ $link }}"></a>
							@endforeach
						</td>
					</tr>
				@else
					<tr>
						<td style="text-align: center;" colspan="2">N/A</td>
					</tr>
				@endif
			</table>
		</div>
	@endforeach
	<div style="margin-top: 15px">
		Best Regards,
		<div>Team Evibe.in</div>
	</div>
</div>