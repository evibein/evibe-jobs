<p>Hello {{ $data['customerName'] }},</p>

<p>Thank you for choosing Evibe.in to book your party.</p>

<p>
	As a token of gratitude, we have designed a
	<a href="{{ $data['emailTYCLink'] }}">personalised e-thank you card</a> especially for you.
	You can share it with your guests to thank them for making your party memorable.
</p>

<p>
	Download the card using the link below and share it on WhatsApp / Facebook / Messenger / Email or using any other medium.
</p>

<div style="margin-top: 20px; margin-bottom: 30px;">
	<div style="width: 200px; text-align: center;">
		<a href="{{ $data['emailTYCLink'] }}"><img style="width: 100%; height: 100%;" src="{{ $data['TYCUrl'] }}" alt="Thank You Card"></a>
		<div style="margin-top: 10px;">
			<a style="border: 1px solid #1E347B; color: #FFFFFF; background-color: #1E347B; padding: 5px; text-decoration: none" href="{{ $data['emailTYCLink'] }}">Download</a>
		</div>
	</div>
</div>

<p>
	Hope you like it.
</p>

<div style="margin-top: 20px; margin-bottom: 15px;">
	<div>Cheers,</div>
	<div>Team Evibe.in</div>
</div>