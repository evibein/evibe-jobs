<p>Hello {{ $data['customerName'] }},</p>

<p>Did you thank your guests yet?</p>

<p>
	We designed a cool e-thank you card for you. Download the card using the link
	below and share it with your guests on WhatsApp / Facebook / Messenger / Email or using any other medium.
</p>

<div style="margin-top: 20px; margin-bottom: 30px;">
	<div style="width: 200px; text-align: center;">
		<a href="{{ $data['emailTYCLink'] }}"><img style="width: 100%; height: 100%;" src="{{ $data['TYCUrl'] }}" alt="Thank You Card"></a>
		<div style="margin-top: 10px;">
			<a style="border: 1px solid #1E347B; color: #FFFFFF; background-color: #1E347B; padding: 5px; text-decoration: none" href="{{ $data['emailTYCLink'] }}">Download</a>
		</div>
	</div>
</div>

<p>
	Hope you like it.
</p>

<div style="margin-top: 20px; margin-bottom: 15px;">
	<div>Cheers,</div>
	<div>Team Evibe.in</div>
</div>