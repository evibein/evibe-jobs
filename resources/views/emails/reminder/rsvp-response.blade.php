<p>Dear {{ $data['customerName'] }},</p>

<p>
	{{ $data['newRSVPCount'] }} new people have RSVPed to your invite. Total {{ $data['totalRSVPCount'] }} people RSVPed so far.
</p>

<p>
	You can invite more people by sharing the e-invite on WhatsApp / Messenger / Facebook & others. Do not forget to share your unique invite link: #inviteLink# to receive RSVPs.
</p>

<div style="margin-top: 20px; margin-bottom: 30px;">
	<div style="width: 200px; text-align: center;">
		<a href="{{ $data['RSVPLink'] }}"><img style="width: 100%; height: 100%;" src="{{ $data['RSVPImage'] }}" alt="RSVP Invitation card"></a>
		<div style="margin-top: 10px;">
			<a style="border: 1px solid #1E347B; color: #FFFFFF; background-color: #1E347B; padding: 5px; text-decoration: none" href="{{ $data['RSVPLink'] }}">Download</a>
		</div>
	</div>
</div>

<p>You can view all the RSVP responses here: {{ $data['RSVPLink'] }}</p>

<p>
<div>Cheers,</div>
<div>Team Evibe.in</div>
</p>