<div style="margin-top: 20px;">
	<div style="text-transform: capitalize; font-weight: normal; margin-bottom: 5px; display: inline-block;"><i>Next Steps:</i></div>
	<ol>
		<li>
			<b><span>Shortlist</span></b>:
			Shortlist your choices. Get your clarifications from our expert planning team (if any).
		</li>
		<li>
			<b><span>Avail. Check</span></b>:
			We will check availability of option(s) you finalize.
		</li>
		<li>
			<b><span>Order Process</span></b>:
			If option(s) available, an email with order details will be sent.
		</li>
		<li>
			<b><span>Book</span></b>:
			Securely pay advance amount to block your slot.
		</li>
		<li>
			<b><span>Relax & Enjoy</span></b>:
			A coordinator will be assigned to take care of your party.
		</li>
	</ol>
</div>