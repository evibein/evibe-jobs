<div>
	Namaste {{ $name }},
	<p>
		<b>{{ $vendorName }}</b> has not responded to the tracking message for
		<b>{{ $customerName }}'s</b> party happening on {{ $partyDate }} at {{ $partyLocation }}.
		Please call {{ $vendorPhone }} immediately and resolve it. If all is well, use EvibeDash to notify customer.
	</p>
	<p>
		Best,<br/>
		Admin
	</p>
</div>
