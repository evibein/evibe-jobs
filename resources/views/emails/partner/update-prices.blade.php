Namaste {{ $partnerName }},
<p>Trust you are doing great!</p>
<p>As communicated earlier, we have revised the prices of all your packages on our website by
	<b>5%</b> to accommodate the additional GST that was levied on the service fee from the month of
	September 2017 as per the GST Act by the Govt. of India. This will come into effect from today, i.e, Monday, 6 November 2017.
</p>
<p>
	Here is a simple example for a sample package of Rs. 10,000 and with a service fee
	of 15% to show you how this measure from our side will help you save the GST burden.
</p>

<table style="margin-top:10px; border-collapse: collapse;" cellpadding="1" border="1">
	<tr>
		<th style="padding:8px"></th>
		<th style="padding:8px"></th>
		<th style="padding:8px">Pre-GST</th>
		<th style="padding:8px">Current Method</th>
		<th style="padding:8px">New Method</th>
	</tr>
	<tr>
		<td style="padding: 5px">A</td>
		<td style="padding: 5px">Your Package Price (example)</td>
		<td style="padding: 5px">Rs. 10,000</td>
		<td style="padding: 5px">Rs. 10,000</td>
		<td style="padding: 5px">Rs. 10,500 (5% increased)</td>
	</tr>
	<tr>
		<td style="padding: 5px">B</td>
		<td style="padding: 5px">Service Fee (15%)</td>
		<td style="padding: 5px">Rs. 1,500</td>
		<td style="padding: 5px">Rs. 1,500</td>
		<td style="padding: 5px">Rs. 1,575</td>
	</tr>
	<tr>
		<td style="padding: 5px">C</td>
		<td style="padding: 5px">GST (18% on service fee)</td>
		<td style="padding: 5px">-</td>
		<td style="padding: 5px">Rs. 270</td>
		<td style="padding: 5px">Rs. 284</td>
	</tr>
	<tr>
		<td style="padding: 5px">D</td>
		<td style="padding: 5px"><b>Your Net Earnings (A-B-C)</b></td>
		<td style="padding: 5px"><b>Rs. 8,500</b></td>
		<td style="padding: 5px"><b>Rs. 8,230</b></td>
		<td style="padding: 5px"><b>Rs. 8,641</b></td>
	</tr>
</table>
<p>
	As you can clearly see in this example, your
	<b>net earning is higher than pre-GST</b> time :). If you have any questions, please feel free to reply to this email. We will be more than happy to help you.
</p>
<p>
	PS: We are working on weekly completed party payment settlements and a web dashboard for you to add new packages, see performance etc. You will hear from us soon.
</p>
<p>
	Thanks in advance,<br>Team Evibe.in
</p>