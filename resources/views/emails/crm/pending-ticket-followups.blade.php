<div>Hello Team,</div>

<p>
	We have in total
	<b>{{ $totalFollowupsCount }} pending followups</b>. Please find below the list with all the details.
	Kindly followup the customers on time to improve our conversion rate.
</p>

<p></p>

@foreach($handler as $handlerId => $handlerData)
	<table style="border-collapse: collapse;text-align: left;margin-bottom: 30px;">
		<thead>
		<tr>
			<th colspan="6"
					style="background-color: #1E347B; color: #ffffff; border: 1px solid #dddddd; padding: 8px; text-align: center;">
				{{ $handlerData["name"] }} <b>[{{ $handlerData["count"] }} followup(s)]</b>
			</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Lead</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Followup</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Comments</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Action</b></td>
		</tr>
		@foreach($handlerData["followups"] as $key => $followup)
			@if(isset($followup['count']) && $followup['count'] != 0)
				<tr>
					<th colspan="6"
							style="background-color: #e2e1d4; color: #313131; border: 1px solid #dddddd; padding: 8px; text-align: center;">
						{{ $key }}
						<b>[{{ $followup["count"] }} followup(s)]</b>
					</th>
				</tr>
				@foreach($followup["followups"] as $followupDetails)
					<tr>
						<td style="border: 1px solid #dddddd; padding: 8px;">{{ $followupDetails["leadStatus"] }}</td>
						<td style="border: 1px solid #dddddd; padding: 8px;">
							<a href="{{ $followupDetails["ticketLink"] }}">{{ $followupDetails["name"] }}</a>
						</td>
						<td style="border: 1px solid #dddddd; padding: 8px;">{{ $followupDetails["partyDate"] }}</td>
						<td style="border: 1px solid #dddddd; padding: 8px;">{{ $followupDetails["followupDate"] }}</td>
						<td style="border: 1px solid #dddddd; padding: 8px;">{{ $followupDetails["comment"] }}</td>
						<td style="border: 1px solid #dddddd; padding: 8px;">
							<a href="{{ $followupDetails["actionLink"] }}">Mark Done</a>
						</td>
					</tr>
				@endforeach
			@endif
		@endforeach
		</tbody>
	</table>
@endforeach

<p>
	Good luck, <br/>
	Admin
</p>