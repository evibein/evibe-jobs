<div style="border:20px solid #f5f5f5;padding: 15px ">
	<div>Hi {{ $handlerName }} <br><br>
		@if($isMissed)
			You have missed a followup which should happen at {{ $followupTime }}.
			<br>Please go through the details below & take an action immediately.
		@else
			You have a followup in next 10 minutes.<br>Please go through below details.
		@endif
	</div>
	<table border="1" style="margin-top:15px; border-collapse: collapse">
		<tr>
			<td style="padding: 8px"><b>Customer Name</b></td>
			<td style="padding: 8px"><b>Followup Time</b></td>
			<td style="padding: 8px"><b>Followup Comments</b></td>
		</tr>
		<tr>
			<td style="padding: 8px">{{ $customerName }}</td>
			<td style="padding: 8px; font-size: 14px;">
				@if($isMissed)
					<span style="background-color: #e74040; color: white; padding: 5px;">{{ $followupTime }}</span>
				@else
					{{ $followupTime }}
				@endif
			</td>
			<td style="padding: 8px">{{ $comment ? $comment : "Not Available" }}</td>
		</tr>
	</table>

	<div style="margin-top:10px;">
		<a style="color:#ff1100" href="{{ $link }}">Click Here</a> to mark this followup as complete.
	</div>
	<div style="margin-top:15px">
		<div>{{ config('evibe.email_footer') }}</div>
		<br>Team Evibe
	</div>
</div>